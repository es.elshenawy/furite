<script type="text/javascript">
    var public_lang = "{{ trans('/public/backLang.calendarLanguage') }}"; // this is a public var used in app.html.js to define path to js files
    var public_folder_path = "{{ URL::to('') }}"; // this is a public var used in app.html.js to define path to js files
</script>
<script src="{{ URL::to('/public/backEnd/scripts/app.html.js') }}"></script>
{!! Helper::SaveVisitorInfo("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") !!}

<script type="text/javascript">
  
  $("#selectco").change(function(){
  // alert("The text has been changed.");
     $('#selectCities').html('');
        var country_id = $('#selectco').val();
        console.log(country_id);
        $.ajax({
            type: "get",
            url:'{{url("cities_co")}}'+'/'+ country_id,
            success: function( data ) {
            	console.log(data);
			 $.each(data, function(key, element) {
			 	for(var i = 0; i < element.length; i++) {
			 		<?php  
                     if( trans('backLang.boxCode') =='ar'){
                     	?>
                $('#selectCities').append("<option value='" + element[i].id +"'>" + element[i].title_ar + "</option>");
                <?php
                     	
                     }else{
                     	?>
                $('#selectCities').append("<option value='" + element[i].id +"'>" + element[i].title_en + "</option>");
                     <?php 	
                     }

			 		 ?>
				}

            });


            }
        });
 
});

</script>
