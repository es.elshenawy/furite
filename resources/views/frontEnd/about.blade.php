@extends('frontEnd.layout')

@section('content')


    @if(empty($about))

    @else

        <div class="heroImage" style="    width: 100%;">
            <img src="{{url('uploads')}}/topics/{{$about->photo_file}}" class="heroImage-img" alt="About Us">
            <img class="heroImage-curve" src="{{url('uploads')}}/topics/{{$about->photo_file}}" alt="">
        </div>
        <div class="content-wrapper">

        <div class="blocks ">
            <div class="block-item blocks_first">
                <div class="">

                    <div class="textBlock">

                        <h1 class="hdg hdg_1">About Us</h1>


                        @if( trans('backLang.boxCode') == 'ar')
                            <h1>{{$about->title_ar}}</h1>
                        @else
                            <h1>{{$about->title_en}}</h1>

                        @endif
                        @if( trans('backLang.boxCode') == 'ar')
                            <p style="line-height: 2;">{!! $about->details_ar !!}</p>
                        @else
                            <p>{!! $about->details_ar !!}</p>
                        @endif
                    </div>

                </div>
            </div>

        </div>


    </div>




    @endif


@endsection
