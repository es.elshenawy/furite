@extends('frontEnd.layout')

@section('content')



    <form method="post" action="/" id="form1">
        <div class="aspNetHidden">
            <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE"
                   value="/wEPDwUKLTMwODUyNTY1OQ9kFgJmD2QWBAIBD2QWBAIZDxYCHgRUZXh0BWI8bGluayBocmVmPScvYXNzZXRzL2Nzcy9yb2JpbnNvbmZyZXNob3ZlcnJpZGUuY3NzLz92PTYzMzUyNDg0MycgcmVsPSdzdHlsZXNoZWV0JyB0eXBlPSd0ZXh0L2NzcycvPmQCGw8WAh8ABV08c2NyaXB0IHNyYz0vYXNzZXRzL2pzL3JvYmluc29uZnJlc2hvdmVycmlkZS5qcz92PTYzMzUyNDg0MycgdHlwZT0ndGV4dC9qYXZhc2NyaXB0Jz48L3NjcmlwdD5kAgMPFgIeBWNsYXNzBQVlbi1VUxYGAgEPZBYCAgEPZBYCZg9kFgJmD2QWAmYPFgIeC3BsYWNlaG9sZGVyBQZTZWFyY2hkAgMQZGQWAgIBD2QWBAIBD2QWAmYPZBYCZg9kFgJmDxYCHgtfIUl0ZW1Db3VudAIFZAIDD2QWAmYPZBYCZg9kFgRmDxYCHwAFFlNpZ24gdXAgZm9yIEVtYWlsIE5ld3NkAgEPFgIfAAV4Qy5ILiBSb2JpbnNvbiBXb3JsZHdpZGUsIEluYy4gQWxsIFJpZ2h0cyBSZXNlcnZlZC4gRU9FIDE0NzAxIENoYXJsc29uIFJvYWQsIEVkZW4gUHJhaXJpZSwgTU4gNTUzNDctNTA3NiB8IDEtODU1LTM1MC0wMDE0ZAIFD2QWAgIBD2QWAmYPZBYEZg8WAh8ABYkIPHNjcmlwdD4NCiAgKGZ1bmN0aW9uKGkscyxvLGcscixhLG0pe2lbJ0dvb2dsZUFuYWx5dGljc09iamVjdCddPXI7aVtyXT1pW3JdfHxmdW5jdGlvbigpew0KICAoaVtyXS5xPWlbcl0ucXx8W10pLnB1c2goYXJndW1lbnRzKX0saVtyXS5sPTEqbmV3IERhdGUoKTthPXMuY3JlYXRlRWxlbWVudChvKSwNCiAgbT1zLmdldEVsZW1lbnRzQnlUYWdOYW1lKG8pWzBdO2EuYXN5bmM9MTthLnNyYz1nO20ucGFyZW50Tm9kZS5pbnNlcnRCZWZvcmUoYSxtKQ0KICB9KSh3aW5kb3csZG9jdW1lbnQsJ3NjcmlwdCcsJy8vd3d3Lmdvb2dsZS1hbmFseXRpY3MuY29tL2FuYWx5dGljcy5qcycsJ2dhJyk7DQoNCiAgZ2EoJ2NyZWF0ZScsICdVQS01MDc0MTgxNS0xJywgJ3JvYmluc29uZnJlc2guY29tJyk7DQogIGdhKCdzZW5kJywgJ3BhZ2V2aWV3Jyk7DQoNCjwvc2NyaXB0Pg0KDQo8IS0tRnJlc2hzcGVjdGl2ZSBpY29uLS0+DQo8c2NyaXB0Pg0KJCggZG9jdW1lbnQgKS5yZWFkeShmdW5jdGlvbigpIHsNCgkJJCggIi5za3lsaW5lQ29udGVudC1zb2NpYWwiICkuYXBwZW5kKCAiPGxpIGNsYXNzPVwic2t5bGluZUNvbnRlbnQtc29jaWFsLWl0ZW1cIj48YSBocmVmPVwiaHR0cDovL2Jsb2cucm9iaW5zb25mcmVzaC5jb21cIiB0YXJnZXQ9XCJfYmxhbmtcIj48aSBjbGFzcz1cImljbiBpY25fZnJlc2hzcGVjdGl2ZVwiPkxpbmtlZEluPC9pPjwvYT48L2xpPiIgKTsNCn0pOw0KPC9zY3JpcHQ+DQoNCjxzdHlsZT4NCi5pY25fZnJlc2hzcGVjdGl2ZSB7DQogICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAtMTY0cHg7DQogICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdodHRwOi8vd3d3LmNocm9iaW5zb24uY29tL2Fzc2V0cy9yb2JpbnNvbmZyZXNoL3NvY2lhbF9zcHJpdGUucG5nJyk7DQp9DQouaWNuX2ZyZXNoc3BlY3RpdmU6aG92ZXIgew0KICAgIGJhY2tncm91bmQtcG9zaXRpb246IDAgLTE2NHB4Ow0KICAgIHpvb206IDE7DQogICAgZmlsdGVyOiBhbHBoYShvcGFjaXR5PTgwKTsNCiAgICBvcGFjaXR5OiAwLjg7DQp9DQo8L3N0eWxlPmQCAg8WAh8ABYsOPHNjcmlwdCB0eXBlPSJ0ZXh0L2phdmFzY3JpcHQiPg0KJCggZG9jdW1lbnQgKS5yZWFkeShmdW5jdGlvbigpIHsNCiAgICAkKCcjbG9nby1sYXJnZScpLmF0dHIoJ3NyYycsICdodHRwczovL3d3dy5jaHJvYmluc29uLmNvbS9hc3NldHMvcm9iaW5zb25mcmVzaC9sb2dvLWludmVyc2UucG5nJyk7DQogICAgJCgnI2xvZ28tbGFyZ2UnKS5hdHRyKCdhbHQnLCAnUm9iaW5zb24gRnJlc2ggTG9nbycpOw0KICAgICQoJyNsb2dvLXNtYWxsJykuYXR0cignc3JjJywgJ2h0dHBzOi8vd3d3LmNocm9iaW5zb24uY29tL2Fzc2V0cy9yb2JpbnNvbmZyZXNoL2xvZ28taW52ZXJzZS1tb2JpbGUucG5nJyk7DQogICAgJCgnI2xvZ28tc21hbGwnKS5hdHRyKCdhbHQnLCAnUm9iaW5zb24gRnJlc2ggTG9nbycpOw0KfSk7DQo8L3NjcmlwdD4NCg0KPCEtLSBTaXRlQ2F0YWx5c3QgY29kZSB2ZXJzaW9uOiBILjI2LjIuDQpDb3B5cmlnaHQgMTk5Ni0yMDE0IEFkb2JlLCBJbmMuIEFsbCBSaWdodHMgUmVzZXJ2ZWQNCk1vcmUgaW5mbyBhdmFpbGFibGUgYXQgaHR0cDovL3d3dy5vbW5pdHVyZS5jb20gLS0+DQo8c2NyaXB0IGxhbmd1YWdlPSJKYXZhU2NyaXB0IiB0eXBlPSJ0ZXh0L2phdmFzY3JpcHQiIHNyYz0iL2Rlc2lnbi9TY3JpcHRzL1JvYmluc29uRnJlc2gvc19jb2RlLmpzIj48L3NjcmlwdD4NCjxzY3JpcHQgbGFuZ3VhZ2U9IkphdmFTY3JpcHQiIHR5cGU9InRleHQvamF2YXNjcmlwdCI+PCEtLQ0KLyogWW91IG1heSBnaXZlIGVhY2ggcGFnZSBhbiBpZGVudGlmeWluZyBuYW1lLCBzZXJ2ZXIsIGFuZCBjaGFubmVsIG9uDQp0aGUgbmV4dCBsaW5lcy4gKi8NCnMucGFnZU5hbWU9IlJvYmluc29uRnJlc2giOw0Kcy5jaGFubmVsPSIiOw0Kcy5wcm9wMT0iIjsNCg0Kcy5zZXJ2ZXI9IiINCnMucGFnZVR5cGU9IiINCnMucHJvcDI9IiINCnMucHJvcDM9IiINCnMucHJvcDQ9IiINCnMucHJvcDU9IiINCnMucHJvcDEyPSJVbml0ZWQgU3RhdGVzIjsNCnMucHJvcDEzPSJFbmdsaXNoIjsNCi8qIENvbnZlcnNpb24gVmFyaWFibGVzICovDQpzLmNhbXBhaWduPSIiDQpzLnN0YXRlPSIiDQpzLnppcD0iIg0Kcy5ldmVudHM9IiINCnMucHJvZHVjdHM9IiINCnMucHVyY2hhc2VJRD0iIg0Kcy5lVmFyMT0iIg0Kcy5lVmFyMj0iIg0Kcy5lVmFyMz0iIg0Kcy5lVmFyND0iIg0Kcy5lVmFyNT0iIg0KLyoqKioqKioqKioqKiogRE8gTk9UIEFMVEVSIEFOWVRISU5HIEJFTE9XIFRISVMgTElORSAhICoqKioqKioqKioqKioqLw0KdmFyIHNfY29kZT1zLnQoKTtpZihzX2NvZGUpZG9jdW1lbnQud3JpdGUoc19jb2RlKS8vLS0+PC9zY3JpcHQ+DQo8c2NyaXB0IGxhbmd1YWdlPSJKYXZhU2NyaXB0IiB0eXBlPSJ0ZXh0L2phdmFzY3JpcHQiPjwhLS0NCmlmKG5hdmlnYXRvci5hcHBWZXJzaW9uLmluZGV4T2YoJ01TSUUnKT49MClkb2N1bWVudC53cml0ZSh1bmVzY2FwZSgnJTNDJykrJ1whLScrJy0nKQ0KLy8tLT48L3NjcmlwdD48bm9zY3JpcHQ+PGltZyBzcmM9Imh0dHA6Ly9jaHJvYmluc29ud29ybGR3aWRlLjEyMi4ybzcubmV0L2Ivc3MvY2hyLXJvYmluc29uZnJlc2gvMS9ILjI2LjItLU5TLzAiDQpoZWlnaHQ9IjEiIHdpZHRoPSIxIiBib3JkZXI9IjAiIGFsdD0iIiAvPjwvbm9zY3JpcHQ+PCEtLS9ETyBOT1QgUkVNT1ZFLy0tPg0KPCEtLSBFbmQgU2l0ZUNhdGFseXN0IGNvZGUgdmVyc2lvbjogSC4yNi4yLiAtLT4NCg0KPHNjcmlwdCBsYW5ndWFnZT0iamF2YXNjcmlwdCIgdHlwZT0idGV4dC9qYXZhc2NyaXB0Ij4NCiAgICB2YXIgcGFnZU5hbWVGb3JPbmNsaWNrID0gcy5wYWdlTmFtZTsNCjwvc2NyaXB0Pg0KZGTiajAO4hadcQ8AKG9IRJkv5u9blw=="/>
        </div>

        <div class="aspNetHidden">

            <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="8535A9D0"/>
            <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION"
                   value="/wEdAAJEKiTcvKOrtrKJ+MfxtviQITRI6FT38MFNu1NZ0Hpc700HOfWV5B+9CcYqZRvuFo91Ybkz"/>
        </div>


        <div id="ContentPlaceHolder1_content_0_rptModules_ModulePanel_0">

            <!-- Main Slider -->
            <div class="mainSlider">
                <ul>
                    <li class="mainSlider-slide"
                        style="background-image: url('{{url('public')}}/frontEnd/img/Freshspective_Hero.png?w=1280&h=480&as=1');">

                        <div class="mainSlider-content split-right_mainSlider"
                             style="background-color: rgba(255, 255, 255, 0.8); background:#ffffff\9; border-left:4px solid #76bb1f; margin: 69px 0 0; color:transparent; !important">

                            <p class="mainSlider-content-text">
                                <center><img style="margin-bottom: 8px;" alt="Freshspective Hero Ad" height="119"
                                             width="498" _languageinserted="true"
                                             src="{{url('public')}}/frontEnd/img/Freshspective_Logo.png"/>
                                    <h2 style="margin: 0px 0px 8px; color: #016836 !important;" class="hdg hdg_1">
                                        Introducing the official blog of Robinson Fresh<sup>&reg;</sup></h2>
                            <p style="font-size: 16px;">Your source for fresh ideas in produce, all from industry
                                experts.</p>
                            </center>
{{--                            <a style="margin-top: 8px; color: #016836 !important;" href="http://blog.robinsonfresh.com/"--}}
{{--                               class="mainSlider-content-link">Read the Blog<em class="icon icon-arrow-right"></em></a>--}}
{{--                            </p>--}}

                        </div>
                    </li>

                    <li class="mainSlider-slide"
                        style="background-image: url('{{url('public')}}/frontEnd/img/Wechs_Hero.jpg?w=1280&h=480&as=1');">

                        <div class="mainSlider-content split-right_mainSlider"
                             style="background-color: rgba(118, 187, 31, 0.8); background:#79ba30\9; !important">

                            <h2 class="hdg hdg_1"><span style="color:#fff;">Merchandising matters in the grape and berry category.</span>
                            </h2>

{{--                            <a class="mainSlider-content-link"--}}
{{--                               href="https://www.robinsonfresh.com/en-us/resources/downloads/">Learn More<i--}}
{{--                                        class="icon icon-arrow-right"></i></a>--}}

                        </div>
                    </li>

                    <li class="mainSlider-slide"
                        style="background-image: url('{{url('public')}}/frontEnd/img/MelonUp_Hero.jpg?w=1280&h=480&as=1');">

                        <div class="mainSlider-content split-left_mainSlider"
                             style="background-color: rgba(118, 187, 31, 0.8); background:#79ba30\9; !important">

                            <h2 class="hdg hdg_1"><span style="color:#fff;">Join the million dollar reason to smile club! </span>
                            </h2>

{{--                            <a class="mainSlider-content-link" href="{{url('about')}}">Learn More<i--}}
{{--                                        class="icon icon-arrow-right"></i></a>--}}

                        </div>
                    </li>

                </ul>
                <div class="mainSlider-pagination">
                    <!-- Pagination items will be generated in here -->
                </div>
            </div>
            <!-- End Main Slider -->
        </div>


        <div id="ContentPlaceHolder1_content_0_rptModules_ModulePanel_1">


            <div class="productNav-container">
                <img class="mainSlider-curve" src="{{url('public')}}/frontEnd/img/hero-curve.png" alt=""/>
                <ul class="productNav">

                </ul>
            </div>

            <!-- Carousel -->
            <div class="productSlider-wrapper">
                <div class="productSlider">
                    <ul class="slider_boot">

                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/product_applepear.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Apples & Pears"><img
                                        src="{{url('public')}}/frontEnd/img/product_applepear.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Apples & Pears'/></a>
                        </li>

                        <li class="prodcutSlider-item">
                            <a href="{{url("/frontEnd/img/asparagus.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff")}}"
                               title="Asparagus"><img
                                        src="{{url('public')}}/frontEnd/img/asparagus.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Asparagus'/></a>
                        </li>

                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/berries_blueberries.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Blueberries"><img
                                        src="{{url('public')}}/frontEnd/img/berries_blueberries.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Blueberries'/></a>
                        </li>

                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/berries_strawberries.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Strawberries"><img
                                        src="{{url('public')}}/frontEnd/img/berries_strawberries.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Strawberries'/></a>
                        </li>

                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Broccoli"><img
                                        src="{{url('public')}}/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Broccoli'/></a>
                        </li>
                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Broccoli"><img
                                        src="{{url('public')}}/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Broccoli'/></a>
                        </li>
                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Broccoli"><img
                                        src="{{url('public')}}/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Broccoli'/></a>
                        </li>
                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Broccoli"><img
                                        src="{{url('public')}}/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Broccoli'/></a>
                        </li>
                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Broccoli"><img
                                        src="{{url('public')}}/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Broccoli'/></a>
                        </li>
                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Broccoli"><img
                                        src="{{url('public')}}/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Broccoli'/></a>
                        </li>
                        <li class="prodcutSlider-item">
                            <a href="{{url('public/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Broccoli"><img
                                        src="{{url('public')}}/frontEnd/img/broccoli.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Broccoli'/></a>
                        </li>

                        <li class="prodcutSlider-item">
                            <a href="{{url('public/fontEnd/img/product_applepear.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff')}}"
                               title="Broccoli"><img
                                        src="{{url('public')}}/frontEnd/img/product_applepear.jpg?h=110&amp;w=170&amp;la=en-US&amp;bc=ffffff"
                                        class='productSlider-item-img' allowstretch='True' alt='Broccoli'/></a>
                        </li>


                    </ul>
                </div>
                <div class="productSlider-controls">
                    <!-- Controls Will be generated in here-->
                </div>
            </div>

        </div>

    </form>
    </div>



    <div class="table-container" style="width: 70.83333331%;margin-left: 18%;"><br>
        <table class="mini-table">
            <tbody>

            <tr>
                <th></th>
                <th> {{ trans('frontLang.January') }}</th>
                <th>{{ trans('frontLang.February') }}</th>
                <th>{{ trans('frontLang.March') }}</th>
                <th>{{ trans('frontLang.April') }}</th>
                <th>{{ trans('frontLang.May') }}</th>
                <th>{{ trans('frontLang.June') }}</th>
                <th>{{ trans('frontLang.July') }}</th>
                <th>{{ trans('frontLang.August') }}</th>
                <th>{{ trans('frontLang.September') }}</th>
                <th>{{ trans('frontLang.October') }}</th>
                <th>{{ trans('frontLang.November') }}</th>
                <th>{{ trans('frontLang.December') }}</th>
            </tr>

            <tr class=" jan-hit feb-hit mar-hit apr-hit may-hit jun-hit dec-hit">
                <td><a > {{ trans('frontLang.orange') }}</a></td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="jan feb mar oct nov dec ">
                <td><a href="">{{ trans('frontLang.Lemon') }}</a></td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="jan feb mar apr ">
                <td><a href="#">{{ trans('frontLang.Mandarin') }}</a></td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class=" sep-hit oct-hit nov-hit dec-hit">
                <td>{{ trans('frontLang.Garnet') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class=" may-hit jun-hit jul-hit">
                <td>{{ trans('frontLang.Grapes') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="may jun jul aug ">
                <td> {{ trans('frontLang.Peach') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="may jun ">
                <td> {{ trans('frontLang.Nectarine') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class=" ">
                <td> {{ trans('frontLang.Apricot') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="jun jul aug sep ">
                <td>{{ trans('frontLang.Mango') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="apr may jun jul ">
                <td> {{ trans('frontLang.Melon') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class=" ">
                <td> {{ trans('frontLang.Watermelon') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class=" jan-hit feb-hit mar-hit nov-hit dec-hit">
                <td><a href="strawberry.html">{{ trans('frontLang.Strawberry') }}</a></td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class=" feb-hit mar-hit apr-hit may-hit jun-hit">
                <td><a href=""> {{ trans('frontLang.Onion') }}</a></td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="jan feb mar apr nov dec ">
                <td>{{ trans('frontLang.Cherry_tomato') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="jan feb mar apr nov dec ">
                <td>{{ trans('frontLang.Tomatoes') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="jan feb mar apr nov dec ">
                <td>{{ trans('frontLang.Bulgarian_pepper') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class=" jan-hit feb-hit mar-hit apr-hit may-hit jun-hit">
                <td><a href="#">{{ trans('frontLang.Potatos') }}</a></td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="jan feb nov dec ">
                <td> {{ trans('frontLang.Iceberg') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="feb mar apr may oct nov ">
                <td>{{ trans('frontLang.Artichoke') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="jan feb mar apr dec nov ">
                <td> {{ trans('frontLang.Cauliflower') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            <tr class="jan feb mar dec ">
                <td> {{ trans('frontLang.Broccoli') }}</td>

                <td class="jan-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="feb-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="mar-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="apr-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="may-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jun-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="jul-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="aug-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="sep-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="oct-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="nov-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>
                <td class="dec-td">
                    <span class="disk"></span>
                    <span class="hit">{{ trans('frontLang.hit') }}</span>
                </td>

            </tr>
            </tbody>
        </table>
    </div>



@endsection
