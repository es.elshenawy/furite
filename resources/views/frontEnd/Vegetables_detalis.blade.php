@extends('frontEnd.layout')

@section('content')

@if(empty($Vegetables_detalis))

@else

<div class="container">
                <div class="row">
                   <!--  <div class="col-md-6">                  
                        <a href="application.html" class="button">Send request</a>
                        <br><br>
                    </div> -->
                    <div class="col-md-17 col-md-offset-1">
                            <div class="main-column">
                                <div class="breadcrumbs">

                              </div>
                           @if( trans('backLang.boxCode') == 'ar')
                                        <h1>{{$Vegetables_detalis->title_ar}}</h1>
                                  @else
                                        <h1>{{$Vegetables_detalis->title_en}}</h1>

                               @endif

                                <div class="row">
                                    <div class="wrap-wrap">
                                        <div class="img-wrapper relative">
                                            <img src="{{url('uploads')}}/topics/{{$Vegetables_detalis->photo_file}}" alt="Peach"><a href="images/catalog/peach.jpg" class="absolute fancybox"></a>




                                                <span class="metka hit">HIT</span>



                                        </div>
                                    </div>

                                    <div class="content">

                                        <div class="cleaner"></div>
                                    </div>

                                </div>


                                    <div class="content">

                                        <h3 style="margin-bottom: 10px;">Varieties:</h3>

                           @if( trans('backLang.boxCode') == 'ar')
                                        <p>{{strip_tags($Vegetables_detalis->details_ar)}}</p>
                                  @else
                                        <p>{{strip_tags($Vegetables_detalis->details_en)}}</p>

                               @endif


                                    </div>
                                    <div class="cleaner"></div>







                                    <br>
                                    <div class="content">

                                        <h3>Country of Origin:</h3>


                                        <div class="row">                                    

        @foreach(\App\Topic::where('id',$Vegetables_detalis->country_id)->get() as $key=>$value_co)
    <div class="col-sm-21">
        
        @if( trans('backLang.boxCode') == 'ar')

                </strong>{{strip_tags($value_co->details_ar)}}</strong> 
          @else
                </strong> {{strip_tags($value_co->details_en)}}</strong> 

       @endif
       
       
    </div><div class="col-sm-3">
        <img src="{{url('uploads')}}/topics/{{$value_co->photo_file}}" alt="Grapefruit">
         @if( trans('backLang.boxCode') == 'ar')

                </strong> {{$value_co->title_ar}}</strong> 
          @else
                </strong> {{$value_co->title_en}}</strong> 

       @endif
            
    </div>
        @endforeach
    
                                        </div>
                                    </div>

                                <br>




                            </div>
                    </div>
                </div>
            </div>
@endif


@endsection