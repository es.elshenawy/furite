@extends('frontEnd.layout')

@section('content')

@if(empty($Berries))

@else
<div class="catalog-wrapper">
 <div class="row">
    @foreach($Berries as $Berrie)
<div class="col-sm-8 col-xs-12">

    <div class="catalog-list-element text-center relative">
        <div class="img-wrapper">
                <img src="{{url('uploads')}}/topics/{{$Berrie->photo_file}}" alt="Grapefruit">
        </div>
           @if( trans('backLang.boxCode') == 'ar')

                <h4>{{$Berrie->title_ar}}</h4>
          @else
                <h4>{{$Berrie->title_en}}</h4>

       @endif

        <a href="{{url('/')}}/Berries/{{$Berrie->id}}" class="absolute"></a>
            <span class="metka hit">HIT</span>
    </div>
</div>
    @endforeach

@endif


     </div>
   </div>
@endsection