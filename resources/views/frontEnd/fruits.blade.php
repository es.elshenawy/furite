@extends('frontEnd.layout')

@section('content')

@if(empty($fruits))

@else
<div class="catalog-wrapper">
 <div class="row">
    @foreach($fruits as $fruit)
<div class="col-sm-8 col-xs-12">

    <div class="catalog-list-element text-center relative">
        <div class="img-wrapper">
                <img src="{{url('uploads')}}/topics/{{$fruit->photo_file}}" alt="Grapefruit">
        </div>
           @if( trans('backLang.boxCode') == 'ar')

                <h4>{{$fruit->title_ar}}</h4>
          @else
                <h4>{{$fruit->title_en}}</h4>

       @endif

        <a href="{{url('/')}}/fruits/{{$fruit->id}}" class="absolute"></a>
            <span class="metka hit">HIT</span>
    </div>
</div>
    @endforeach

@endif


     </div>
   </div>
@endsection