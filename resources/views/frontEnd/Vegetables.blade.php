@extends('frontEnd.layout')

@section('content')

@if(empty($Vegetables))

@else
<div class="catalog-wrapper">
 <div class="row">
    @foreach($Vegetables as $Vegetable)
<div class="col-sm-8 col-xs-12">

    <div class="catalog-list-element text-center relative">
        <div class="img-wrapper">
              <a href="{{url('/')}}/{{$Vegetable->id}}">  <img src="{{url('uploads')}}/topics/{{$Vegetable->photo_file}}"  alt="Grapefruit"></a>
        </div>
           @if( trans('backLang.boxCode') == 'ar')

                <h4>{{$Vegetable->title_ar}}</h4>
          @else
                <h4>{{$Vegetable->title_en}}</h4>

       @endif

        <a href="{{url('/')}}/Vegetables/{{$Vegetable->id}}" class="absolute"></a>
            <span class="metka hit">HIT</span>
    </div>
</div>
    @endforeach

@endif


     </div>
   </div>
@endsection