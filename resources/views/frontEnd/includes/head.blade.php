<meta charset="utf-8">
@if(empty($PageTitle))

@else



    <title>{{$PageTitle}} {{($PageTitle !="")? "|":""}} {{ Helper::GeneralSiteSettings("site_title_" . trans('backLang.boxCode')) }}</title>
@endif
@if(empty($PageDescription))

@else

<meta name="description" content="{{$PageDescription}}"/>
@endif
@if(empty($PageKeywords))

@else

<meta name="keywords" content="{{$PageKeywords}}"/>
@endif

<meta name="author" content="{{ URL::to('') }}"/>


<head>
        <!-- META DATA -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="canonical" href="https://www.robinsonfresh.com/en-us/"/><title>
    RobinsonFresh
</title>
    <link rel="stylesheet" href="{{url('public')}}/frontEnd/css/bootstrap24.css">
<link rel="stylesheet" href="{{url('public')}}/frontEnd/css/style.css">
<link href="{{url('public')}}/frontEnd/css/font-awesome.css" rel="stylesheet">
    <!-- MODERNIZR -->
    <script type="text/javascript" src="{{url('public')}}/frontEnd/js/modernizr.js"></script>
    <!-- ICONS -->
    <link rel="shortcut icon" type="image/ico" href="{{url('public')}}/frontEnd/img/RobinsonFresh/favicon.ico" />
    <link rel="apple-touch-icon" href="{{url('public')}}/frontEnd/img/apple-touch-icon.png" />
    <!-- STYLESHEETS -->
    <link rel="stylesheet" media="screen, projection" href="{{url('public')}}/frontEnd/css/screen.css?v=2" />
    <link rel="stylesheet" media="print" href="{{url('public')}}/frontEnd/css/print.css" />

    <!-- Default.css - TODO: Remove this file once project is finalized. All styles should be in above css files. -->
    <link rel="stylesheet" media="screen, projection" href="{{url('public')}}/frontEnd/css/default.css" />
    <link href="{{url('public')}}/frontEnd/css/robinsonfreshoverride.css/?v=633524843" rel='stylesheet' type='text/css'/>

</head>

 @if( trans('backLang.direction')=="rtl")
<link href="{{ URL::to('/public/frontEnd/css/rtl.css') }}" rel="stylesheet"/>
@endif


    <!-- JQUERY -->
    <script type="text/javascript" src="{{url('public')}}/frontEnd/js/jquery.min.js"></script>
    <script type="text/javascript">window.jQuery || document.write('<script type="text/javascript" src="{{url("public")}}/frontEnd/js/RobinsonFresh/jquery.min.js"><\/script>')</script>
    <script type="text/javascript" src="{{url('public')}}/frontEnd/js/RobinsonFresh/jquery.jcarousel.js"></script>
    <!-- JAVASCRIPT -->
    <script type="text/javascript" src="{{url('public')}}/frontEnd/js/RobinsonFresh/global.js"></script>
    <script type="text/javascript" src="{{url('public')}}/frontEnd/js/RobinsonFresh/default.js?v=2"></script>
    <!-- JW Player -->
    <script src="{{url('public')}}/frontEnd/js/RobinsonFresh/jwplayer/Gal5vb1EeKOryIACusDuQ.js" type="text/javascript"></script>
    <script src="{{url('public')}}/frontEnd/js/robinsonfreshoverride.js?v=633524843" type='text/javascript'></script>



