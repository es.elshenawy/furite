    <div class="content-wrapper">




<div class="skyline">
    <div class="skylineContent">
        <ul class="skylineContent-menu">

            <li class="skylineContent-menu-item">
                <a href='{{url("/")}}' class='skylineContent-menu-item-link' >{{ trans('frontLang.home') }}</a>
            </li>
            <li class="skylineContent-menu-item">
                <a href='{{url("about")}}' class='skylineContent-menu-item-link' >{{ trans('frontLang.About_Us') }}</a>
            </li>

            <li class="skylineContent-menu-item">
                <a href="{{url('view/contact/')}}" class='skylineContent-menu-item-link contacts' >{{ trans('frontLang.Contact_Us') }}</a>
            </li>

{{--            <li class="skylineContent-menu-item">--}}
{{--                <a href='#' class='skylineContent-menu-item-link' >Resources</a>--}}
{{--            </li>--}}
{{--            --}}
        </ul>
            <div class="skylineSearch">
                <form action="" id="cse-search-box">
                    <div>
                        <input name="ctl00$header$header_0$search" type="text" id="header_header_0_search" class="subInt" size="31" placeholder="Search" />
                        <input class="subBtn" type="submit" value="Submit">
                    </div>
                </form>
            </div>

        <ul class="skylineContent-social social_i">

        </ul>
    </div>
</div>

<div class="region-selector">
     <div class="pull-right">
                        &nbsp; | &nbsp;
                        <strong>
                            @if(App::getLocale()=="ar")
                                <a href="{{ URL::to('lang/en') }}">
                                    <i class="fa fa-language ">
                                    </i> {{ str_replace("[ ","",str_replace(" ]","",strip_tags(trans('backLang.englishBox')))) }}
                                </a>
                            @else
                                <a href="{{ URL::to('lang/ar') }}">
                                    <i class="fa fa-language ">
                                    </i> {{ str_replace("[ ","",str_replace(" ]","",strip_tags(trans('backLang.arabicBox')))) }}
                                </a>
                            @endif

                        </strong>
                </div>
</div>


<div id="header" class="header">
    <div class="split-left split-left_logo">
        <a href="{{url("/")}}">
            <img class="logo_image" src="{{url('public')}}/frontEnd/img/robinson_fresh_logo.png" alt="Robinson Fresh" />
        </a>
    </div>
    <div class="nav-icons">
        <a class="menu-icon" href="#">
            <img src="{{url('public')}}/frontEnd/img/menu.png" alt="" />
        </a>
        <a class="menu-icon-lang" href="/en-us/language/">
            <img src="{{url('public')}}/frontEnd/img/RFLanguage.png" alt="" />
        </a>
    </div>
    <div class="split-right split-right_header">
        <ul class="mainMenu">

            <li class="mainMenu-item">
                <a href="{{url('fruits')}}"> {{ trans('frontLang.Fruit') }}</a>
            </li>
            <li class="mainMenu-item">
                <a href="{{url('Berries')}}"> {{ trans('frontLang.Berries') }}</a>
            </li>
             <li class="mainMenu-item">
                <a href="{{url('Vegetables')}}">{{ trans('frontLang.Vegetables') }}</a>
            </li>


        </ul>
    </div>
</div>
