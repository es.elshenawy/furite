<div class="footer">
    <div class="footerWrapper">
        <div class="footerMenus">
            <ul class="footerMenus-block">                
                <li>
                    <a href='/en-us/about-us/' class='footerMenus-block-item' >About Us</a>
                </li>                                                
            </ul>
            
            <ul class="footerMenus-block">
                
                <li>
                    <a href="{{url('view/contact/')}}" class='footerMenus-block-item' >Contact Us</a>
                </li>  
                
            </ul>    
        </div>       
    </div>
        
</div>