@extends('frontEnd.layout')

@section('content')


<div class="container">
<h1>Contact US Form</h1>

@if(Session::has('success'))
        <div class="alert alert-success" role="alert" style="     margin-bottom: 3%;
    margin-top: 2%;
    color: #155724;
    background-color: #d4edda;
    border-color: #c3e6cb;
    position: relative;
    padding: .75rem 1.25rem;
    margin-bottom: 1rem;
    border: 1px solid transparent;
    border-radius: .25rem;">
     {{ Session::get('success') }}
   </div>
@endif
 
{!! Form::open(['route'=>'contactus.store']) !!}
 
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
{!! Form::label('Name:') !!}
{!! Form::text('name', old('name'), ['class'=>'form-control', 'placeholder'=>'Enter Name']) !!}
<span class="text-danger">{{ $errors->first('name') }}</span>
</div>

    <div class="form-group">
        <label for="contact_phone:">Phone : </label>
        <input class="form-control" placeholder="Enter Phone" name="contact_phone" type="number">
        <span class="text-danger"></span>
    </div>


<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
{!! Form::label('Email:') !!}
{!! Form::text('email', old('email'), ['class'=>'form-control', 'placeholder'=>'Enter Email']) !!}
<span class="text-danger">{{ $errors->first('email') }}</span>
</div>
 
<div class="form-group {{ $errors->has('message') ? 'has-error' : '' }}">
{!! Form::label('Message:') !!}
{!! Form::textarea('message', old('message'), ['class'=>'form-control', 'placeholder'=>'Enter Message']) !!}
<span class="text-danger">{{ $errors->first('message') }}</span>
</div>
 
<div class="form-group">
<button class="btn btn-success">Contact US!</button>
</div>
 
{!! Form::close() !!}
 
</div>



@endsection
