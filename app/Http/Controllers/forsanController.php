<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Webmail;
use Illuminate\Http\Request;
use App\Topic;
use App\Message;
use App\Http\Requests;
use App\ContactUs;
use Mail;
use Redirect;

class forsanController extends Controller
{
    //
   /*
   show fruits
   */ 
  public function show_fruits()
  {
   
   $fruits = Topic::where('webmaster_id','19')->where('status','1')->get();
//     dd($fruits);
    return view('frontEnd.fruits',compact('fruits'));
  }

     //
   /*
   show details fruits
   */
  public function show_fruits_detalis($id)
  {
   
   $Berries_details = Topic::find($id);
    // dd($Berries_details);
    return view('frontEnd.Berries_details',compact('Berries_details'));
  }


 /*
   show details fruits
   */
  public function show_Berries_detalis($id)
  {
   
   $Berries_details = Topic::find($id);
    // dd($Berries_details);
    return view('frontEnd.Berries_details',compact('Berries_details'));
  }

	public function show_Vegetables_detalis($id){
		
		 $Vegetables_detalis = Topic::find($id);
	return view('frontEnd.Vegetables_detalis',compact('Vegetables_detalis'));
	}

   //
   /*
   show fruits
   */
  public function show_Berries()
  {
   
   $Berries = Topic::where('webmaster_id','21')->where('status','1')->get();
    // dd($fruits);
    return view('frontEnd.Berries',compact('Berries'));
  }


     //
   /*
   show fruits
   */
  public function show_Vegetables()
  {
   
   $Vegetables = Topic::where('webmaster_id','22')->where('status','1')->get();
    return view('frontEnd.Vegetables',compact('Vegetables'));
  }

     //
   /*
   show fruits
   */
  public function contact()
  {         
    return view('frontEnd.contact');
  }


  //
   /*
   show fruits
   */
  public function about()
  {
      $about = Topic::where('webmaster_id','23')->where('status','1')->first();
//      dd($about);
      return view('frontEnd.about',compact("about"));
  }

public function contactUsPost(Request $request)
  {
      $this->validate($request, [
          'name' => 'required',
          'email' => 'required|email',
//          'contact_subject' => 'required',
          'message' => 'required'
      ]);

      if (env('NOCAPTCHA_STATUS', false)) {
          $this->validate($request, [
              'g-recaptcha-response' => 'required|captcha'
          ]);
      }
      // SITE SETTINGS
      $WebsiteSettings = Setting::find(1);
      $site_title_var = "site_title_" . trans('backLang.boxCode');
      $site_email = $WebsiteSettings->site_webmails;
      $site_url = $WebsiteSettings->site_url;
      $site_title = $WebsiteSettings->$site_title_var;
//    dd($request->all());

      $Webmail = new Webmail;
      $Webmail->cat_id = 0;
      $Webmail->group_id = null;
      $Webmail->title = $request->contact_subject;
      $Webmail->details = $request->message;
      $Webmail->date = date("Y-m-d H:i:s");
      $Webmail->from_email = $request->email;
      $Webmail->from_name = $request->name;
      $Webmail->from_phone = $request->contact_phone;
      $Webmail->to_email = $WebsiteSettings->site_webmails;
      $Webmail->to_name = $site_title;
      $Webmail->status = 0;
      $Webmail->flag = 0;
      $Webmail->save();

      // SEND Notification Email
      if ($WebsiteSettings->notify_messages_status) {
          if (env('MAIL_USERNAME') != "") {
              Mail::send('backEnd.emails.webmail', [
                  'title' => "NEW MESSAGE:" . $request->contact_subject,
                  'details' => $request->contact_message,
                  'websiteURL' => $site_url,
                  'websiteName' => $site_title
              ], function ($message) use ($request, $site_email, $site_title) {
                  $message->from(env('NO_REPLAY_EMAIL', $request->contact_email), $request->contact_name);
                  $message->to($site_email);
                  $message->replyTo($request->contact_email, $site_title);
                  $message->subject($request->contact_subject);

              });
          }
      }
    return redirect()->back()->with('success', 'your message, Send Successfully ');

    return "OK";
  }



}
