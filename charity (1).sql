-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 13, 2019 at 05:01 PM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `charity`
--

-- --------------------------------------------------------

--
-- Table structure for table `analytics_pages`
--

CREATE TABLE `analytics_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `visitor_id` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `query` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `load_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `analytics_pages`
--

INSERT INTO `analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, 1, '156.211.3.79', 'http://bsegypt.net/ad/5/topics/50/edit', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', '0.15625906', '2018-12-11', '04:33:44', '2018-12-11 11:33:44', '2018-12-11 11:33:44'),
(2, 2, '156.211.3.79', 'http://bsegypt.net/ad/5/topics/50/edit', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', '0.14689183', '2018-12-11', '04:34:17', '2018-12-11 11:34:17', '2018-12-11 11:34:17'),
(3, 3, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.08320808', '2018-12-11', '04:34:18', '2018-12-11 11:34:18', '2018-12-11 11:34:18'),
(4, 4, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.09584618', '2018-12-11', '04:34:18', '2018-12-11 11:34:18', '2018-12-11 11:34:18'),
(5, 5, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07624602', '2018-12-11', '04:34:18', '2018-12-11 11:34:18', '2018-12-11 11:34:18'),
(6, 6, '156.211.3.79', 'http://bsegypt.net/ad/5/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/5/topics?_pjax=%23view', '0.12507486', '2018-12-11', '04:34:27', '2018-12-11 11:34:27', '2018-12-11 11:34:27'),
(7, 7, '156.211.3.79', 'http://bsegypt.net/ad/5/topics/50/edit', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', '0.150841', '2018-12-11', '04:34:32', '2018-12-11 11:34:32', '2018-12-11 11:34:32'),
(8, 8, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.16582203', '2018-12-11', '04:34:32', '2018-12-11 11:34:32', '2018-12-11 11:34:32'),
(9, 9, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.15765715', '2018-12-11', '04:34:33', '2018-12-11 11:34:33', '2018-12-11 11:34:33'),
(10, 10, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.0835619', '2018-12-11', '04:34:33', '2018-12-11 11:34:33', '2018-12-11 11:34:33'),
(11, 11, '156.211.3.79', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', '0.13675714', '2018-12-11', '04:35:46', '2018-12-11 11:35:46', '2018-12-11 11:35:46'),
(12, 12, '156.211.3.79', 'http://bsegypt.net/ad/4/topics/create', 'unknown', 'http://bsegypt.net/ad/4/topics/create', '0.12426901', '2018-12-11', '04:35:55', '2018-12-11 11:35:55', '2018-12-11 11:35:55'),
(13, 13, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.16477084', '2018-12-11', '04:35:56', '2018-12-11 11:35:56', '2018-12-11 11:35:56'),
(14, 14, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.15746117', '2018-12-11', '04:35:56', '2018-12-11 11:35:56', '2018-12-11 11:35:56'),
(15, 15, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08304691', '2018-12-11', '04:35:56', '2018-12-11 11:35:56', '2018-12-11 11:35:56'),
(16, 16, '156.211.3.79', 'http://bsegypt.net/ad/settings?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/settings?_pjax=%23view', '0.13015699', '2018-12-11', '04:36:03', '2018-12-11 11:36:03', '2018-12-11 11:36:03'),
(17, 17, '156.211.3.79', 'http://bsegypt.net/ad/settings', 'unknown', 'http://bsegypt.net/ad/settings', '0.13161802', '2018-12-11', '04:36:03', '2018-12-11 11:36:03', '2018-12-11 11:36:03'),
(18, 18, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08430195', '2018-12-11', '04:36:04', '2018-12-11 11:36:04', '2018-12-11 11:36:04'),
(19, 19, '156.211.3.79', 'http://bsegypt.net/ad/admin?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/admin?_pjax=%23view', '0.18885708', '2018-12-11', '04:36:04', '2018-12-11 11:36:04', '2018-12-11 11:36:04'),
(20, 20, '156.211.3.79', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.19605303', '2018-12-11', '04:36:04', '2018-12-11 11:36:04', '2018-12-11 11:36:04'),
(21, 21, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.0876081', '2018-12-11', '04:36:05', '2018-12-11 11:36:05', '2018-12-11 11:36:05'),
(22, 22, '156.211.3.79', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08343697', '2018-12-11', '04:36:05', '2018-12-11 11:36:05', '2018-12-11 11:36:05'),
(23, 23, '156.217.194.51', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.09088683', '2018-12-11', '08:18:10', '2018-12-11 15:18:10', '2018-12-11 15:18:10'),
(24, 24, '156.217.194.51', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14105392', '2018-12-11', '08:18:33', '2018-12-11 15:18:33', '2018-12-11 15:18:33'),
(25, 25, '156.217.194.51', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14317417', '2018-12-11', '08:18:49', '2018-12-11 15:18:49', '2018-12-11 15:18:49'),
(26, 26, '156.217.194.51', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.13027', '2018-12-11', '08:18:59', '2018-12-11 15:18:59', '2018-12-11 15:18:59'),
(27, 27, '156.217.194.51', 'http://bsegypt.net/ad/4/topics', 'unknown', 'http://bsegypt.net/ad/4/topics', '0.12531614', '2018-12-11', '08:19:06', '2018-12-11 15:19:06', '2018-12-11 15:19:06'),
(28, 28, '156.217.194.51', 'http://bsegypt.net/ad/5/topics', 'unknown', 'http://bsegypt.net/ad/5/topics', '0.10142589', '2018-12-11', '08:19:08', '2018-12-11 15:19:08', '2018-12-11 15:19:08'),
(29, 29, '156.217.194.51', 'http://bsegypt.net/ad/8/topics', 'unknown', 'http://bsegypt.net/ad/8/topics', '0.10905099', '2018-12-11', '08:19:10', '2018-12-11 15:19:10', '2018-12-11 15:19:10'),
(30, 30, '156.217.194.51', 'http://bsegypt.net/ad/settings', 'unknown', 'http://bsegypt.net/ad/settings', '0.116436', '2018-12-11', '08:19:12', '2018-12-11 15:19:12', '2018-12-11 15:19:12'),
(31, 31, '196.138.2.12', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.10542083', '2018-12-11', '08:39:19', '2018-12-11 15:39:19', '2018-12-11 15:39:19'),
(32, 32, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09741211', '2018-12-11', '08:41:22', '2018-12-11 15:41:22', '2018-12-11 15:41:22'),
(33, 33, '196.138.2.12', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.13055301', '2018-12-11', '08:43:03', '2018-12-11 15:43:03', '2018-12-11 15:43:03'),
(34, 34, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08399701', '2018-12-11', '08:43:04', '2018-12-11 15:43:04', '2018-12-11 15:43:04'),
(35, 35, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09842587', '2018-12-11', '08:43:05', '2018-12-11 15:43:05', '2018-12-11 15:43:05'),
(36, 36, '196.138.2.12', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.13612914', '2018-12-11', '08:44:17', '2018-12-11 15:44:17', '2018-12-11 15:44:17'),
(37, 37, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08583784', '2018-12-11', '08:44:17', '2018-12-11 15:44:17', '2018-12-11 15:44:17'),
(38, 38, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09717298', '2018-12-11', '08:44:18', '2018-12-11 15:44:18', '2018-12-11 15:44:18'),
(39, 39, '196.138.2.12', 'http://bsegypt.net/ad/admin?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/admin?_pjax=%23view', '0.19623899', '2018-12-11', '08:44:22', '2018-12-11 15:44:22', '2018-12-11 15:44:22'),
(40, 40, '196.138.2.12', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.20248318', '2018-12-11', '08:44:22', '2018-12-11 15:44:22', '2018-12-11 15:44:22'),
(41, 41, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.09272981', '2018-12-11', '08:44:23', '2018-12-11 15:44:23', '2018-12-11 15:44:23'),
(42, 42, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08685899', '2018-12-11', '08:44:23', '2018-12-11 15:44:23', '2018-12-11 15:44:23'),
(43, 43, '196.138.2.12', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', '0.11963201', '2018-12-11', '08:44:27', '2018-12-11 15:44:27', '2018-12-11 15:44:27'),
(44, 44, '196.138.2.12', 'http://bsegypt.net/ad/4/topics/26/edit', 'unknown', 'http://bsegypt.net/ad/4/topics/26/edit', '0.16043496', '2018-12-11', '08:44:59', '2018-12-11 15:44:59', '2018-12-11 15:44:59'),
(45, 45, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.08187914', '2018-12-11', '08:44:59', '2018-12-11 15:44:59', '2018-12-11 15:44:59'),
(46, 46, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.41130018', '2018-12-11', '08:45:00', '2018-12-11 15:45:00', '2018-12-11 15:45:00'),
(47, 47, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.25662303', '2018-12-11', '08:45:10', '2018-12-11 15:45:10', '2018-12-11 15:45:10'),
(48, 48, '196.138.2.12', 'http://bsegypt.net/ad/4/topics', 'unknown', 'http://bsegypt.net/ad/4/topics', '0.12340093', '2018-12-11', '08:45:32', '2018-12-11 15:45:32', '2018-12-11 15:45:32'),
(49, 49, '196.138.2.12', 'http://bsegypt.net/ad/admin?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/admin?_pjax=%23view', '0.12209606', '2018-12-11', '08:45:35', '2018-12-11 15:45:35', '2018-12-11 15:45:35'),
(50, 50, '196.138.2.12', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14170504', '2018-12-11', '08:45:53', '2018-12-11 15:45:53', '2018-12-11 15:45:53'),
(51, 51, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08801198', '2018-12-11', '08:45:54', '2018-12-11 15:45:54', '2018-12-11 15:45:54'),
(52, 52, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09398484', '2018-12-11', '08:45:54', '2018-12-11 15:45:54', '2018-12-11 15:45:54'),
(53, 53, '196.138.2.12', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', '0.12034202', '2018-12-11', '08:46:15', '2018-12-11 15:46:15', '2018-12-11 15:46:15'),
(54, 54, '196.138.2.12', 'http://bsegypt.net/ad/4/topics/create', 'unknown', 'http://bsegypt.net/ad/4/topics/create', '0.10433578', '2018-12-11', '08:46:18', '2018-12-11 15:46:18', '2018-12-11 15:46:18'),
(55, 55, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.09533787', '2018-12-11', '08:46:19', '2018-12-11 15:46:19', '2018-12-11 15:46:19'),
(56, 56, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.09376502', '2018-12-11', '08:46:20', '2018-12-11 15:46:20', '2018-12-11 15:46:20'),
(57, 57, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09481406', '2018-12-11', '08:46:25', '2018-12-11 15:46:25', '2018-12-11 15:46:25'),
(58, 58, '196.138.2.12', 'http://bsegypt.net/ad/5/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/5/topics?_pjax=%23view', '0.12147808', '2018-12-11', '08:47:04', '2018-12-11 15:47:04', '2018-12-11 15:47:04'),
(59, 59, '196.138.2.12', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', '0.15135193', '2018-12-11', '08:47:33', '2018-12-11 15:47:33', '2018-12-11 15:47:33'),
(60, 60, '196.138.2.12', 'http://bsegypt.net/ad/8/topics/create', 'unknown', 'http://bsegypt.net/ad/8/topics/create', '0.12350798', '2018-12-11', '08:47:53', '2018-12-11 15:47:53', '2018-12-11 15:47:53'),
(61, 61, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.1890471', '2018-12-11', '08:47:53', '2018-12-11 15:47:53', '2018-12-11 15:47:53'),
(62, 62, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.1718111', '2018-12-11', '08:47:53', '2018-12-11 15:47:53', '2018-12-11 15:47:53'),
(63, 63, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09520507', '2018-12-11', '08:47:54', '2018-12-11 15:47:54', '2018-12-11 15:47:54'),
(64, 64, '196.138.2.12', 'http://bsegypt.net/ad/8/topics/51/edit', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', '0.1852932', '2018-12-11', '08:49:54', '2018-12-11 15:49:54', '2018-12-11 15:49:54'),
(65, 65, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.42055702', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(66, 66, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.39681101', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(67, 67, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', '0.42803907', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(68, 68, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', '0.45532894', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(69, 69, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', '0.45598888', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(70, 70, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', '0.43766189', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(71, 71, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', '0.17301702', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(72, 72, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', '0.18548584', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(73, 73, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', '0.14770603', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(74, 74, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09014893', '2018-12-11', '08:49:56', '2018-12-11 15:49:56', '2018-12-11 15:49:56'),
(75, 75, '196.138.2.12', 'http://bsegypt.net/ad/8/topics/51/edit', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', '0.16605091', '2018-12-11', '08:50:38', '2018-12-11 15:50:38', '2018-12-11 15:50:38'),
(76, 76, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.08358097', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(77, 77, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.14615989', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(78, 78, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', '0.12082195', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(79, 79, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', '0.195678', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(80, 80, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', '0.22795916', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(81, 81, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', '0.25382185', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(82, 82, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', '0.25055981', '2018-12-11', '08:50:40', '2018-12-11 15:50:40', '2018-12-11 15:50:40'),
(83, 83, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', '0.29739618', '2018-12-11', '08:50:40', '2018-12-11 15:50:40', '2018-12-11 15:50:40'),
(84, 84, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', '0.18548393', '2018-12-11', '08:50:40', '2018-12-11 15:50:40', '2018-12-11 15:50:40'),
(85, 85, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10485411', '2018-12-11', '08:50:40', '2018-12-11 15:50:40', '2018-12-11 15:50:40'),
(86, 86, '196.138.2.12', 'http://bsegypt.net/ad/settings?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/settings?_pjax=%23view', '0.27253294', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(87, 87, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', '0.49259591', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(88, 88, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', '0.49916601', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(89, 89, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', '0.4949441', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(90, 90, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', '0.46992898', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(91, 91, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', '0.49675918', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(92, 92, '196.138.2.12', 'http://bsegypt.net/ad/settings', 'unknown', 'http://bsegypt.net/ad/settings', '0.15516996', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(93, 93, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', '0.08445597', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(94, 94, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', '0.08054113', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(95, 95, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12436891', '2018-12-11', '08:51:04', '2018-12-11 15:51:04', '2018-12-11 15:51:04'),
(96, 96, '196.138.2.12', 'http://bsegypt.net/ad/settings', 'unknown', 'http://bsegypt.net/ad/settings', '0.13485599', '2018-12-11', '08:59:36', '2018-12-11 15:59:36', '2018-12-11 15:59:36'),
(97, 97, '196.138.2.12', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.0839839', '2018-12-11', '08:59:37', '2018-12-11 15:59:37', '2018-12-11 15:59:37'),
(98, 98, '31.13.127.18', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.09222388', '2018-12-11', '14:56:49', '2018-12-11 21:56:49', '2018-12-11 21:56:49'),
(99, 99, '31.13.127.18', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.27475405', '2018-12-11', '14:56:52', '2018-12-11 21:56:52', '2018-12-11 21:56:52'),
(100, 100, '31.13.127.18', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.2427218', '2018-12-11', '14:56:52', '2018-12-11 21:56:52', '2018-12-11 21:56:52'),
(101, 101, '31.13.127.16', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.25563002', '2018-12-11', '14:56:52', '2018-12-11 21:56:52', '2018-12-11 21:56:52'),
(102, 102, '31.13.127.10', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.19843197', '2018-12-11', '14:56:52', '2018-12-11 21:56:52', '2018-12-11 21:56:52'),
(103, 103, '197.43.27.65', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.08536792', '2018-12-11', '22:18:31', '2018-12-12 05:18:31', '2018-12-12 05:18:31'),
(104, 104, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12614608', '2018-12-11', '22:18:32', '2018-12-12 05:18:32', '2018-12-12 05:18:32'),
(105, 105, '197.43.27.65', 'http://bsegypt.net/ad/admin?fbclid=IwAR2ug5kphSRVAZufh7htQdhJEyhhxI2MN5J-nzlNlxtQ5DzNLUBC8t4SDH0', 'unknown', 'http://bsegypt.net/ad/admin?fbclid=IwAR2ug5kphSRVAZufh7htQdhJEyhhxI2MN5J-nzlNlxtQ5DzNLUBC8t4SDH0', '0.25974488', '2018-12-11', '22:19:20', '2018-12-12 05:19:20', '2018-12-12 05:19:20'),
(106, 106, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08523488', '2018-12-11', '22:19:20', '2018-12-12 05:19:20', '2018-12-12 05:19:20'),
(107, 107, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09366488', '2018-12-11', '22:19:21', '2018-12-12 05:19:21', '2018-12-12 05:19:21'),
(108, 108, '197.43.27.65', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', '0.15999198', '2018-12-11', '22:19:35', '2018-12-12 05:19:35', '2018-12-12 05:19:35'),
(109, 109, '197.43.27.65', 'http://bsegypt.net/ad/4/topics/23/edit', 'unknown', 'http://bsegypt.net/ad/4/topics/23/edit', '0.36428785', '2018-12-11', '22:20:22', '2018-12-12 05:20:22', '2018-12-12 05:20:22'),
(110, 110, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.30651808', '2018-12-11', '22:20:22', '2018-12-12 05:20:22', '2018-12-12 05:20:22'),
(111, 111, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.55908203', '2018-12-11', '22:20:23', '2018-12-12 05:20:23', '2018-12-12 05:20:23'),
(112, 112, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.32785201', '2018-12-11', '22:20:24', '2018-12-12 05:20:24', '2018-12-12 05:20:24'),
(113, 113, '197.43.27.65', 'http://bsegypt.net/ad/5/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/5/topics?_pjax=%23view', '0.64955401', '2018-12-11', '22:20:53', '2018-12-12 05:20:53', '2018-12-12 05:20:53'),
(114, 114, '197.43.27.65', 'http://bsegypt.net/ad/5/topics/27/edit', 'unknown', 'http://bsegypt.net/ad/5/topics/27/edit', '0.1437149', '2018-12-11', '22:21:01', '2018-12-12 05:21:01', '2018-12-12 05:21:01'),
(115, 115, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.17032194', '2018-12-11', '22:21:01', '2018-12-12 05:21:01', '2018-12-12 05:21:01'),
(116, 116, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.16626', '2018-12-11', '22:21:01', '2018-12-12 05:21:01', '2018-12-12 05:21:01'),
(117, 117, '197.43.27.65', 'http://bsegypt.net/ad/uploads/topics/https://www.youtube.com/watch?v=PCwL3-hkKrg', 'unknown', 'http://bsegypt.net/ad/uploads/topics/https://www.youtube.com/watch?v=PCwL3-hkKrg', '0.08578801', '2018-12-11', '22:21:02', '2018-12-12 05:21:02', '2018-12-12 05:21:02'),
(118, 118, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.15428591', '2018-12-11', '22:21:02', '2018-12-12 05:21:02', '2018-12-12 05:21:02'),
(119, 119, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.16708708', '2018-12-11', '22:21:02', '2018-12-12 05:21:02', '2018-12-12 05:21:02'),
(120, 120, '197.43.27.65', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', '0.21282506', '2018-12-11', '22:21:38', '2018-12-12 05:21:38', '2018-12-12 05:21:38'),
(121, 121, '197.43.27.65', 'http://bsegypt.net/ad/uploads/topics/https://www.youtube.com/watch?v=PCwL3-hkKrg', 'unknown', 'http://bsegypt.net/ad/uploads/topics/https://www.youtube.com/watch?v=PCwL3-hkKrg', '0.18091822', '2018-12-11', '22:21:38', '2018-12-12 05:21:38', '2018-12-12 05:21:38'),
(122, 122, '197.43.27.65', 'http://bsegypt.net/ad/8/topics/32/edit', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', '0.19673896', '2018-12-11', '22:21:44', '2018-12-12 05:21:44', '2018-12-12 05:21:44'),
(123, 123, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.36015296', '2018-12-11', '22:21:44', '2018-12-12 05:21:44', '2018-12-12 05:21:44'),
(124, 124, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', '0.55326295', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(125, 125, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.53699994', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(126, 126, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', '0.51392198', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(127, 127, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', '0.56935501', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(128, 128, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', '0.21588421', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(129, 129, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', '0.27625203', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(130, 130, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', '0.28541207', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(131, 131, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', '0.25906205', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(132, 132, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.25535488', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(133, 133, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', '0.44985604', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(134, 134, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', '0.45849204', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(135, 135, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', '0.44775796', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(136, 136, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', '0.47926283', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(137, 137, '197.43.27.65', 'http://bsegypt.net/ad/settings?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/settings?_pjax=%23view', '0.56595397', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(138, 138, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', '0.28975892', '2018-12-11', '22:21:49', '2018-12-12 05:21:49', '2018-12-12 05:21:49'),
(139, 139, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', '0.30304408', '2018-12-11', '22:21:49', '2018-12-12 05:21:49', '2018-12-12 05:21:49'),
(140, 140, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', '0.30156994', '2018-12-11', '22:21:49', '2018-12-12 05:21:49', '2018-12-12 05:21:49'),
(141, 141, '197.43.27.65', 'http://bsegypt.net/ad/settings', 'unknown', 'http://bsegypt.net/ad/settings', '0.34521794', '2018-12-11', '22:21:49', '2018-12-12 05:21:49', '2018-12-12 05:21:49'),
(142, 142, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07844901', '2018-12-11', '22:21:50', '2018-12-12 05:21:50', '2018-12-12 05:21:50'),
(143, 143, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09327197', '2018-12-11', '22:22:37', '2018-12-12 05:22:37', '2018-12-12 05:22:37'),
(144, 144, '197.43.27.65', 'http://bsegypt.net/ad/admin?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/admin?_pjax=%23view', '0.20463014', '2018-12-11', '22:22:41', '2018-12-12 05:22:41', '2018-12-12 05:22:41'),
(145, 145, '197.43.27.65', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.21634007', '2018-12-11', '22:22:41', '2018-12-12 05:22:41', '2018-12-12 05:22:41'),
(146, 146, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.07725501', '2018-12-11', '22:22:42', '2018-12-12 05:22:42', '2018-12-12 05:22:42'),
(147, 147, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07988405', '2018-12-11', '22:22:42', '2018-12-12 05:22:42', '2018-12-12 05:22:42'),
(148, 148, '197.43.27.65', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', '0.11510611', '2018-12-11', '22:22:46', '2018-12-12 05:22:46', '2018-12-12 05:22:46'),
(149, 149, '197.43.27.65', 'http://bsegypt.net/ad/4/topics/create', 'unknown', 'http://bsegypt.net/ad/4/topics/create', '0.13186717', '2018-12-11', '22:22:58', '2018-12-12 05:22:58', '2018-12-12 05:22:58'),
(150, 150, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.1688602', '2018-12-11', '22:22:58', '2018-12-12 05:22:58', '2018-12-12 05:22:58'),
(151, 151, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.15352392', '2018-12-11', '22:22:58', '2018-12-12 05:22:58', '2018-12-12 05:22:58'),
(152, 152, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08408189', '2018-12-11', '22:22:59', '2018-12-12 05:22:59', '2018-12-12 05:22:59'),
(153, 153, '197.43.27.65', 'http://bsegypt.net/ad/4/topics', 'unknown', 'http://bsegypt.net/ad/4/topics', '0.11060405', '2018-12-11', '22:23:01', '2018-12-12 05:23:01', '2018-12-12 05:23:01'),
(154, 154, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07711101', '2018-12-11', '22:23:01', '2018-12-12 05:23:01', '2018-12-12 05:23:01'),
(155, 155, '197.43.27.65', 'http://bsegypt.net/ad/4/topics/create', 'unknown', 'http://bsegypt.net/ad/4/topics/create', '0.10040188', '2018-12-11', '22:23:02', '2018-12-12 05:23:02', '2018-12-12 05:23:02'),
(156, 156, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.14995503', '2018-12-11', '22:23:03', '2018-12-12 05:23:03', '2018-12-12 05:23:03'),
(157, 157, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.14657807', '2018-12-11', '22:23:03', '2018-12-12 05:23:03', '2018-12-12 05:23:03'),
(158, 158, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08530903', '2018-12-11', '22:23:03', '2018-12-12 05:23:03', '2018-12-12 05:23:03'),
(159, 159, '197.43.27.65', 'http://bsegypt.net/ad/4/topics/create', 'unknown', 'http://bsegypt.net/ad/4/topics/create', '0.10477495', '2018-12-11', '22:23:07', '2018-12-12 05:23:07', '2018-12-12 05:23:07'),
(160, 160, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.14152002', '2018-12-11', '22:23:08', '2018-12-12 05:23:08', '2018-12-12 05:23:08'),
(161, 161, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.1533041', '2018-12-11', '22:23:08', '2018-12-12 05:23:08', '2018-12-12 05:23:08'),
(162, 162, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.14734197', '2018-12-11', '22:23:08', '2018-12-12 05:23:08', '2018-12-12 05:23:08'),
(163, 163, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.13885689', '2018-12-11', '22:23:08', '2018-12-12 05:23:08', '2018-12-12 05:23:08'),
(164, 164, '197.43.27.65', 'http://bsegypt.net/ad/4/topics/create', 'unknown', 'http://bsegypt.net/ad/4/topics/create', '0.10514498', '2018-12-11', '22:23:10', '2018-12-12 05:23:10', '2018-12-12 05:23:10'),
(165, 165, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.14877796', '2018-12-11', '22:23:11', '2018-12-12 05:23:11', '2018-12-12 05:23:11'),
(166, 166, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.14915204', '2018-12-11', '22:23:11', '2018-12-12 05:23:11', '2018-12-12 05:23:11'),
(167, 167, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07571292', '2018-12-11', '22:23:11', '2018-12-12 05:23:11', '2018-12-12 05:23:11'),
(168, 168, '197.43.27.65', 'http://bsegypt.net/ad/4/topics', 'unknown', 'http://bsegypt.net/ad/4/topics', '0.12640405', '2018-12-11', '22:23:19', '2018-12-12 05:23:19', '2018-12-12 05:23:19'),
(169, 169, '197.43.27.65', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08334899', '2018-12-11', '22:23:19', '2018-12-12 05:23:19', '2018-12-12 05:23:19'),
(170, 170, '156.205.249.91', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.13275218', '2018-12-12', '17:11:55', '2018-12-13 00:11:55', '2018-12-13 00:11:55'),
(171, 171, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11085892', '2018-12-12', '17:11:58', '2018-12-13 00:11:58', '2018-12-13 00:11:58'),
(172, 172, '156.205.249.91', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.0862422', '2018-12-12', '17:12:24', '2018-12-13 00:12:24', '2018-12-13 00:12:24'),
(173, 173, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10061598', '2018-12-12', '17:12:40', '2018-12-13 00:12:40', '2018-12-13 00:12:40'),
(174, 174, '156.205.249.91', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.21595502', '2018-12-12', '17:14:10', '2018-12-13 00:14:10', '2018-12-13 00:14:10'),
(175, 175, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.09330797', '2018-12-12', '17:14:11', '2018-12-13 00:14:11', '2018-12-13 00:14:11'),
(176, 176, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09498501', '2018-12-12', '17:14:19', '2018-12-13 00:14:19', '2018-12-13 00:14:19'),
(177, 177, '156.205.249.91', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.15192699', '2018-12-12', '17:32:48', '2018-12-13 00:32:48', '2018-12-13 00:32:48'),
(178, 178, '156.205.249.91', 'http://bsegypt.net/ad/admin?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/admin?_pjax=%23view', '0.13648987', '2018-12-12', '17:32:48', '2018-12-13 00:32:48', '2018-12-13 00:32:48'),
(179, 179, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08679295', '2018-12-12', '17:32:48', '2018-12-13 00:32:48', '2018-12-13 00:32:48'),
(180, 180, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08570695', '2018-12-12', '17:32:49', '2018-12-13 00:32:49', '2018-12-13 00:32:49'),
(181, 181, '156.205.249.91', 'http://bsegypt.net/ad/admin?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/admin?_pjax=%23view', '0.20739794', '2018-12-12', '17:32:52', '2018-12-13 00:32:52', '2018-12-13 00:32:52'),
(182, 182, '156.205.249.91', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.24011302', '2018-12-12', '17:32:53', '2018-12-13 00:32:53', '2018-12-13 00:32:53'),
(183, 183, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.17509103', '2018-12-12', '17:32:53', '2018-12-13 00:32:53', '2018-12-13 00:32:53'),
(184, 184, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09977388', '2018-12-12', '17:32:59', '2018-12-13 00:32:59', '2018-12-13 00:32:59'),
(185, 185, '156.205.249.91', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', '0.16603398', '2018-12-12', '17:33:02', '2018-12-13 00:33:02', '2018-12-13 00:33:02'),
(186, 186, '156.205.249.91', 'http://bsegypt.net/ad/5/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/5/topics?_pjax=%23view', '0.13469911', '2018-12-12', '17:33:16', '2018-12-13 00:33:16', '2018-12-13 00:33:16'),
(187, 187, '156.205.249.91', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', '0.14585805', '2018-12-12', '17:33:23', '2018-12-13 00:33:23', '2018-12-13 00:33:23'),
(188, 188, '156.205.249.91', 'http://bsegypt.net/ad/settings', 'unknown', 'http://bsegypt.net/ad/settings', '0.35029793', '2018-12-12', '17:33:45', '2018-12-13 00:33:45', '2018-12-13 00:33:45'),
(189, 189, '156.205.249.91', 'http://bsegypt.net/ad/settings?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/settings?_pjax=%23view', '0.68123198', '2018-12-12', '17:33:46', '2018-12-13 00:33:46', '2018-12-13 00:33:46'),
(190, 190, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12091589', '2018-12-12', '17:33:46', '2018-12-13 00:33:46', '2018-12-13 00:33:46'),
(191, 191, '156.205.249.91', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', '0.13085103', '2018-12-12', '17:34:53', '2018-12-13 00:34:53', '2018-12-13 00:34:53'),
(192, 192, '156.205.249.91', 'http://bsegypt.net/ad/settings', 'unknown', 'http://bsegypt.net/ad/settings', '0.20327806', '2018-12-12', '17:34:56', '2018-12-13 00:34:56', '2018-12-13 00:34:56'),
(193, 193, '156.205.249.91', 'http://bsegypt.net/ad/settings?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/settings?_pjax=%23view', '0.79736185', '2018-12-12', '17:34:57', '2018-12-13 00:34:57', '2018-12-13 00:34:57'),
(194, 194, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.46411109', '2018-12-12', '17:34:57', '2018-12-13 00:34:57', '2018-12-13 00:34:57'),
(195, 195, '156.205.249.91', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', '0.12751007', '2018-12-12', '17:34:59', '2018-12-13 00:34:59', '2018-12-13 00:34:59'),
(196, 196, '156.205.249.91', 'http://bsegypt.net/ad/8/topics/32/edit', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', '0.37593198', '2018-12-12', '17:35:02', '2018-12-13 00:35:02', '2018-12-13 00:35:02'),
(197, 197, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.08213806', '2018-12-12', '17:35:03', '2018-12-13 00:35:03', '2018-12-13 00:35:03'),
(198, 198, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.08338594', '2018-12-12', '17:35:03', '2018-12-13 00:35:03', '2018-12-13 00:35:03'),
(199, 199, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', '0.44153214', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(200, 200, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.42830801', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(201, 201, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', '0.53765607', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(202, 202, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', '0.52049708', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(203, 203, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', '0.52831483', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(204, 204, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.24831295', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(205, 205, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', '0.31799102', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(206, 206, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', '0.29207897', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04');
INSERT INTO `analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(207, 207, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', '0.30068088', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(208, 208, '156.205.249.91', 'http://bsegypt.net/ad/settings', 'unknown', 'http://bsegypt.net/ad/settings', '0.51435399', '2018-12-12', '17:36:16', '2018-12-13 00:36:16', '2018-12-13 00:36:16'),
(209, 209, '156.205.249.91', 'http://bsegypt.net/ad/settings?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/settings?_pjax=%23view', '0.72250295', '2018-12-12', '17:36:16', '2018-12-13 00:36:16', '2018-12-13 00:36:16'),
(210, 210, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', '0.79819012', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(211, 211, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', '0.81077695', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(212, 212, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', '0.82612205', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(213, 213, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', '0.84560084', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(214, 214, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', '0.81665301', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(215, 215, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', '0.81513', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(216, 216, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', '0.34376884', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(217, 217, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07861209', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(218, 218, '156.205.249.91', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', '0.11761498', '2018-12-12', '17:36:19', '2018-12-13 00:36:19', '2018-12-13 00:36:19'),
(219, 219, '156.205.249.91', 'http://bsegypt.net/ad/8/topics', 'unknown', 'http://bsegypt.net/ad/8/topics', '0.11167598', '2018-12-12', '17:37:16', '2018-12-13 00:37:16', '2018-12-13 00:37:16'),
(220, 220, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.49047899', '2018-12-12', '17:37:17', '2018-12-13 00:37:17', '2018-12-13 00:37:17'),
(221, 221, '156.205.249.91', 'http://bsegypt.net/ad/8/topics', 'unknown', 'http://bsegypt.net/ad/8/topics', '0.10562515', '2018-12-12', '17:37:38', '2018-12-13 00:37:38', '2018-12-13 00:37:38'),
(222, 222, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.53188396', '2018-12-12', '17:37:39', '2018-12-13 00:37:39', '2018-12-13 00:37:39'),
(223, 223, '156.205.249.91', 'http://bsegypt.net/ad/8/topics/33/edit', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', '0.17925191', '2018-12-12', '17:37:45', '2018-12-13 00:37:45', '2018-12-13 00:37:45'),
(224, 224, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.20901895', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(225, 225, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.23386407', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(226, 226, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_3.png', '0.42955804', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(227, 227, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.41009402', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(228, 228, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_0.png', '0.49837399', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(229, 229, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_2.png', '0.49073195', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(230, 230, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_1.png', '0.50710416', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(231, 231, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_4.png', '0.309057', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(232, 232, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_5.png', '0.17030597', '2018-12-12', '17:37:47', '2018-12-13 00:37:47', '2018-12-13 00:37:47'),
(233, 233, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/images/marker_6.png', '0.15659404', '2018-12-12', '17:37:47', '2018-12-13 00:37:47', '2018-12-13 00:37:47'),
(234, 234, '156.205.249.91', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.13819313', '2018-12-12', '17:54:19', '2018-12-13 00:54:19', '2018-12-13 00:54:19'),
(235, 235, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.09886599', '2018-12-12', '17:54:19', '2018-12-13 00:54:19', '2018-12-13 00:54:19'),
(236, 236, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09523392', '2018-12-12', '17:54:20', '2018-12-13 00:54:20', '2018-12-13 00:54:20'),
(237, 237, '156.205.249.91', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', '0.11945701', '2018-12-12', '17:56:23', '2018-12-13 00:56:23', '2018-12-13 00:56:23'),
(238, 238, '156.205.249.91', 'http://bsegypt.net/ad/8/topics/create', 'unknown', 'http://bsegypt.net/ad/8/topics/create', '0.1565671', '2018-12-12', '17:56:29', '2018-12-13 00:56:29', '2018-12-13 00:56:29'),
(239, 239, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.10759616', '2018-12-12', '17:56:30', '2018-12-13 00:56:30', '2018-12-13 00:56:30'),
(240, 240, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.11069894', '2018-12-12', '17:56:30', '2018-12-13 00:56:30', '2018-12-13 00:56:30'),
(241, 241, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.16661906', '2018-12-12', '17:56:30', '2018-12-13 00:56:30', '2018-12-13 00:56:30'),
(242, 242, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.1610229', '2018-12-12', '17:56:30', '2018-12-13 00:56:30', '2018-12-13 00:56:30'),
(243, 243, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.49308205', '2018-12-12', '17:57:28', '2018-12-13 00:57:28', '2018-12-13 00:57:28'),
(244, 244, '156.205.249.91', 'http://bsegypt.net/ad/8/topics/create', 'unknown', 'http://bsegypt.net/ad/8/topics/create', '0.12948799', '2018-12-12', '17:57:39', '2018-12-13 00:57:39', '2018-12-13 00:57:39'),
(245, 245, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.09375691', '2018-12-12', '17:57:39', '2018-12-13 00:57:39', '2018-12-13 00:57:39'),
(246, 246, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.08235908', '2018-12-12', '17:57:40', '2018-12-13 00:57:40', '2018-12-13 00:57:40'),
(247, 247, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.15054822', '2018-12-12', '17:57:40', '2018-12-13 00:57:40', '2018-12-13 00:57:40'),
(248, 248, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.15990305', '2018-12-12', '17:57:40', '2018-12-13 00:57:40', '2018-12-13 00:57:40'),
(249, 249, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.27278495', '2018-12-12', '17:58:02', '2018-12-13 00:58:02', '2018-12-13 00:58:02'),
(250, 250, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.25778079', '2018-12-12', '17:58:02', '2018-12-13 00:58:02', '2018-12-13 00:58:02'),
(251, 251, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.26721716', '2018-12-12', '17:58:02', '2018-12-13 00:58:02', '2018-12-13 00:58:02'),
(252, 252, '156.205.249.91', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/8/topics?_pjax=%23view', '0.10791206', '2018-12-12', '17:58:04', '2018-12-13 00:58:04', '2018-12-13 00:58:04'),
(253, 253, '156.205.249.91', 'http://bsegypt.net/ad/settings?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/settings?_pjax=%23view', '0.14523005', '2018-12-12', '17:58:09', '2018-12-13 00:58:09', '2018-12-13 00:58:09'),
(254, 254, '156.205.249.91', 'http://bsegypt.net/ad/settings', 'unknown', 'http://bsegypt.net/ad/settings', '0.65261412', '2018-12-12', '17:58:09', '2018-12-13 00:58:09', '2018-12-13 00:58:09'),
(255, 255, '156.205.249.91', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08013797', '2018-12-12', '17:58:10', '2018-12-13 00:58:10', '2018-12-13 00:58:10'),
(256, 256, '156.205.249.91', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/4/topics?_pjax=%23view', '0.11985397', '2018-12-12', '17:58:27', '2018-12-13 00:58:27', '2018-12-13 00:58:27'),
(257, 257, '156.195.178.199', 'http://bsegypt.net/ad/login', 'unknown', 'http://bsegypt.net/ad/login', '0.47830796', '2018-12-14', '16:17:13', '2018-12-14 23:17:13', '2018-12-14 23:17:13'),
(258, 258, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12097692', '2018-12-14', '16:17:15', '2018-12-14 23:17:15', '2018-12-14 23:17:15'),
(259, 259, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.32745004', '2018-12-14', '16:17:31', '2018-12-14 23:17:31', '2018-12-14 23:17:31'),
(260, 260, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08144188', '2018-12-14', '16:17:32', '2018-12-14 23:17:32', '2018-12-14 23:17:32'),
(261, 261, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07637382', '2018-12-14', '16:17:32', '2018-12-14 23:17:32', '2018-12-14 23:17:32'),
(262, 262, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14577389', '2018-12-14', '16:17:44', '2018-12-14 23:17:44', '2018-12-14 23:17:44'),
(263, 263, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08333492', '2018-12-14', '16:17:45', '2018-12-14 23:17:45', '2018-12-14 23:17:45'),
(264, 264, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07696795', '2018-12-14', '16:17:45', '2018-12-14 23:17:45', '2018-12-14 23:17:45'),
(265, 265, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14075804', '2018-12-14', '18:45:38', '2018-12-15 01:45:38', '2018-12-15 01:45:38'),
(266, 266, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08934808', '2018-12-14', '18:45:39', '2018-12-15 01:45:39', '2018-12-15 01:45:39'),
(267, 267, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.0826869', '2018-12-14', '18:45:40', '2018-12-15 01:45:40', '2018-12-15 01:45:40'),
(268, 268, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.13311195', '2018-12-14', '18:46:11', '2018-12-15 01:46:11', '2018-12-15 01:46:11'),
(269, 269, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08220005', '2018-12-14', '18:46:11', '2018-12-15 01:46:11', '2018-12-15 01:46:11'),
(270, 270, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07947588', '2018-12-14', '18:46:11', '2018-12-15 01:46:11', '2018-12-15 01:46:11'),
(271, 271, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14061499', '2018-12-14', '18:46:17', '2018-12-15 01:46:17', '2018-12-15 01:46:17'),
(272, 272, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.10990787', '2018-12-14', '18:46:17', '2018-12-15 01:46:17', '2018-12-15 01:46:17'),
(273, 273, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08692694', '2018-12-14', '18:46:20', '2018-12-15 01:46:20', '2018-12-15 01:46:20'),
(274, 274, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.225389', '2018-12-14', '18:47:44', '2018-12-15 01:47:44', '2018-12-15 01:47:44'),
(275, 275, '156.195.178.199', 'http://bsegypt.net/ad/admin?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/admin?_pjax=%23view', '0.23257995', '2018-12-14', '18:47:44', '2018-12-15 01:47:44', '2018-12-15 01:47:44'),
(276, 276, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08228207', '2018-12-14', '18:47:44', '2018-12-15 01:47:44', '2018-12-15 01:47:44'),
(277, 277, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07837605', '2018-12-14', '18:47:44', '2018-12-15 01:47:44', '2018-12-15 01:47:44'),
(278, 278, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14337802', '2018-12-14', '18:47:51', '2018-12-15 01:47:51', '2018-12-15 01:47:51'),
(279, 279, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08200884', '2018-12-14', '18:47:52', '2018-12-15 01:47:52', '2018-12-15 01:47:52'),
(280, 280, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.13919306', '2018-12-14', '18:47:52', '2018-12-15 01:47:52', '2018-12-15 01:47:52'),
(281, 281, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.16246891', '2018-12-14', '18:47:59', '2018-12-15 01:47:59', '2018-12-15 01:47:59'),
(282, 282, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08640695', '2018-12-14', '18:48:00', '2018-12-15 01:48:00', '2018-12-15 01:48:00'),
(283, 283, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.082165', '2018-12-14', '18:48:00', '2018-12-15 01:48:00', '2018-12-15 01:48:00'),
(284, 284, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.13722801', '2018-12-14', '18:48:07', '2018-12-15 01:48:07', '2018-12-15 01:48:07'),
(285, 285, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08382702', '2018-12-14', '18:48:08', '2018-12-15 01:48:08', '2018-12-15 01:48:08'),
(286, 286, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07952785', '2018-12-14', '18:48:08', '2018-12-15 01:48:08', '2018-12-15 01:48:08'),
(287, 287, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.12723708', '2018-12-14', '18:48:10', '2018-12-15 01:48:10', '2018-12-15 01:48:10'),
(288, 288, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.11032104', '2018-12-14', '18:48:10', '2018-12-15 01:48:10', '2018-12-15 01:48:10'),
(289, 289, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08069992', '2018-12-14', '18:48:11', '2018-12-15 01:48:11', '2018-12-15 01:48:11'),
(290, 290, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.15663004', '2018-12-14', '18:49:10', '2018-12-15 01:49:10', '2018-12-15 01:49:10'),
(291, 291, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08875704', '2018-12-14', '18:49:10', '2018-12-15 01:49:10', '2018-12-15 01:49:10'),
(292, 292, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08495116', '2018-12-14', '18:49:11', '2018-12-15 01:49:11', '2018-12-15 01:49:11'),
(293, 293, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.09988213', '2018-12-14', '18:50:24', '2018-12-15 01:50:24', '2018-12-15 01:50:24'),
(294, 294, '156.195.178.199', 'http://bsegypt.net/ad/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://bsegypt.net/ad/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.08231211', '2018-12-14', '18:50:25', '2018-12-15 01:50:25', '2018-12-15 01:50:25'),
(295, 295, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.15945292', '2018-12-14', '18:50:55', '2018-12-15 01:50:55', '2018-12-15 01:50:55'),
(296, 296, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08950901', '2018-12-14', '18:50:56', '2018-12-15 01:50:56', '2018-12-15 01:50:56'),
(297, 297, '156.195.178.199', 'http://bsegypt.net/ad/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://bsegypt.net/ad/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.10279608', '2018-12-14', '18:50:56', '2018-12-15 01:50:56', '2018-12-15 01:50:56'),
(298, 298, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08586192', '2018-12-14', '18:50:56', '2018-12-15 01:50:56', '2018-12-15 01:50:56'),
(299, 299, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.13477302', '2018-12-14', '18:51:01', '2018-12-15 01:51:01', '2018-12-15 01:51:01'),
(300, 300, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.07572412', '2018-12-14', '18:51:01', '2018-12-15 01:51:01', '2018-12-15 01:51:01'),
(301, 301, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.0816009', '2018-12-14', '18:51:02', '2018-12-15 01:51:02', '2018-12-15 01:51:02'),
(302, 302, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.11840415', '2018-12-14', '18:51:14', '2018-12-15 01:51:14', '2018-12-15 01:51:14'),
(303, 303, '156.195.178.199', 'http://bsegypt.net/ad/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://bsegypt.net/ad/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.08953595', '2018-12-14', '18:51:14', '2018-12-15 01:51:14', '2018-12-15 01:51:14'),
(304, 304, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.15034103', '2018-12-14', '18:52:11', '2018-12-15 01:52:11', '2018-12-15 01:52:11'),
(305, 305, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08545113', '2018-12-14', '18:52:11', '2018-12-15 01:52:11', '2018-12-15 01:52:11'),
(306, 306, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08685303', '2018-12-14', '18:52:12', '2018-12-15 01:52:12', '2018-12-15 01:52:12'),
(307, 307, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14997911', '2018-12-14', '18:52:24', '2018-12-15 01:52:24', '2018-12-15 01:52:24'),
(308, 308, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08583212', '2018-12-14', '18:52:24', '2018-12-15 01:52:24', '2018-12-15 01:52:24'),
(309, 309, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09356403', '2018-12-14', '18:52:25', '2018-12-15 01:52:25', '2018-12-15 01:52:25'),
(310, 310, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.26779008', '2018-12-14', '18:56:14', '2018-12-15 01:56:14', '2018-12-15 01:56:14'),
(311, 311, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.0858829', '2018-12-14', '18:56:15', '2018-12-15 01:56:15', '2018-12-15 01:56:15'),
(312, 312, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07632208', '2018-12-14', '18:56:15', '2018-12-15 01:56:15', '2018-12-15 01:56:15'),
(313, 313, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.12738419', '2018-12-14', '18:56:17', '2018-12-15 01:56:17', '2018-12-15 01:56:17'),
(314, 314, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14902806', '2018-12-14', '18:56:17', '2018-12-15 01:56:17', '2018-12-15 01:56:17'),
(315, 315, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.146415', '2018-12-14', '18:56:17', '2018-12-15 01:56:17', '2018-12-15 01:56:17'),
(316, 316, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.09317303', '2018-12-14', '18:56:18', '2018-12-15 01:56:18', '2018-12-15 01:56:18'),
(317, 317, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08116698', '2018-12-14', '18:56:19', '2018-12-15 01:56:19', '2018-12-15 01:56:19'),
(318, 318, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14110804', '2018-12-14', '19:02:18', '2018-12-15 02:02:18', '2018-12-15 02:02:18'),
(319, 319, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08404493', '2018-12-14', '19:02:18', '2018-12-15 02:02:18', '2018-12-15 02:02:18'),
(320, 320, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07799816', '2018-12-14', '19:02:19', '2018-12-15 02:02:19', '2018-12-15 02:02:19'),
(321, 321, '156.195.178.199', 'http://bsegypt.net/ad/menus?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/menus?_pjax=%23view', '0.19411802', '2018-12-14', '19:02:35', '2018-12-15 02:02:35', '2018-12-15 02:02:35'),
(322, 322, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.10612607', '2018-12-14', '19:02:44', '2018-12-15 02:02:44', '2018-12-15 02:02:44'),
(323, 323, '156.195.178.199', 'http://bsegypt.net/ad/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://bsegypt.net/ad/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.09335017', '2018-12-14', '19:02:45', '2018-12-15 02:02:45', '2018-12-15 02:02:45'),
(324, 324, '156.195.178.199', 'http://bsegypt.net/ad/menus', 'unknown', 'http://bsegypt.net/ad/menus', '0.12374401', '2018-12-14', '19:03:43', '2018-12-15 02:03:43', '2018-12-15 02:03:43'),
(325, 325, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08506393', '2018-12-14', '19:03:46', '2018-12-15 02:03:46', '2018-12-15 02:03:46'),
(326, 326, '156.195.178.199', 'http://bsegypt.net/ad/menus', 'unknown', 'http://bsegypt.net/ad/menus', '0.11510396', '2018-12-14', '19:03:48', '2018-12-15 02:03:48', '2018-12-15 02:03:48'),
(327, 327, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07725191', '2018-12-14', '19:03:49', '2018-12-15 02:03:49', '2018-12-15 02:03:49'),
(328, 328, '156.195.178.199', 'http://bsegypt.net/ad/menus?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/menus?_pjax=%23view', '0.13648891', '2018-12-14', '19:04:00', '2018-12-15 02:04:00', '2018-12-15 02:04:00'),
(329, 329, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09311509', '2018-12-14', '19:04:19', '2018-12-15 02:04:19', '2018-12-15 02:04:19'),
(330, 330, '156.195.178.199', 'http://bsegypt.net/ad/users/1/edit', 'unknown', 'http://bsegypt.net/ad/users/1/edit', '0.16217303', '2018-12-14', '19:04:42', '2018-12-15 02:04:42', '2018-12-15 02:04:42'),
(331, 331, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08848596', '2018-12-14', '19:04:42', '2018-12-15 02:04:42', '2018-12-15 02:04:42'),
(332, 332, '156.195.178.199', 'http://bsegypt.net/ad/menus?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/menus?_pjax=%23view', '0.12481213', '2018-12-14', '19:04:57', '2018-12-15 02:04:57', '2018-12-15 02:04:57'),
(333, 333, '156.195.178.199', 'http://bsegypt.net/ad/menus/1', 'unknown', 'http://bsegypt.net/ad/menus/1', '0.12627292', '2018-12-14', '19:07:18', '2018-12-15 02:07:18', '2018-12-15 02:07:18'),
(334, 334, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08223891', '2018-12-14', '19:07:19', '2018-12-15 02:07:19', '2018-12-15 02:07:19'),
(335, 335, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/webmaster/sections?_pjax=%23view', '0.13936305', '2018-12-14', '19:08:12', '2018-12-15 02:08:12', '2018-12-15 02:08:12'),
(336, 336, '156.195.178.199', 'http://bsegypt.net/ad/admin', 'unknown', 'http://bsegypt.net/ad/admin', '0.14800906', '2018-12-14', '19:09:54', '2018-12-15 02:09:54', '2018-12-15 02:09:54'),
(337, 337, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', 'unknown', 'http://bsegypt.net/ad/backEnd/assets/styles/flags.css', '0.08402681', '2018-12-14', '19:09:54', '2018-12-15 02:09:54', '2018-12-15 02:09:54'),
(338, 338, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07639694', '2018-12-14', '19:09:56', '2018-12-15 02:09:56', '2018-12-15 02:09:56'),
(339, 339, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/webmaster/sections?_pjax=%23view', '0.10498905', '2018-12-14', '19:10:00', '2018-12-15 02:10:00', '2018-12-15 02:10:00'),
(340, 340, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections/create', 'unknown', 'http://bsegypt.net/ad/webmaster/sections/create', '0.13122416', '2018-12-14', '19:10:03', '2018-12-15 02:10:03', '2018-12-15 02:10:03'),
(341, 341, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08882689', '2018-12-14', '19:10:04', '2018-12-15 02:10:04', '2018-12-15 02:10:04'),
(342, 342, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', '0.10356712', '2018-12-14', '19:14:03', '2018-12-15 02:14:03', '2018-12-15 02:14:03'),
(343, 343, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.07976389', '2018-12-14', '19:14:04', '2018-12-15 02:14:04', '2018-12-15 02:14:04'),
(344, 344, '156.195.178.199', 'http://bsegypt.net/ad/11/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/11/topics?_pjax=%23view', '0.14452291', '2018-12-14', '19:14:12', '2018-12-15 02:14:12', '2018-12-15 02:14:12'),
(345, 345, '156.195.178.199', 'http://bsegypt.net/ad/11/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/11/topics?_pjax=%23view', '0.12125707', '2018-12-14', '19:14:12', '2018-12-15 02:14:12', '2018-12-15 02:14:12'),
(346, 346, '156.195.178.199', 'http://bsegypt.net/ad/11/topics/create', 'unknown', 'http://bsegypt.net/ad/11/topics/create', '0.16184998', '2018-12-14', '19:14:16', '2018-12-15 02:14:16', '2018-12-15 02:14:16'),
(347, 347, '156.195.178.199', 'http://bsegypt.net/ad/11/topics/create', 'unknown', 'http://bsegypt.net/ad/11/topics/create', '0.1203301', '2018-12-14', '19:14:43', '2018-12-15 02:14:43', '2018-12-15 02:14:43'),
(348, 348, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.09416604', '2018-12-14', '19:14:44', '2018-12-15 02:14:44', '2018-12-15 02:14:44'),
(349, 349, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.09298396', '2018-12-14', '19:14:44', '2018-12-15 02:14:44', '2018-12-15 02:14:44'),
(350, 350, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10527992', '2018-12-14', '19:14:44', '2018-12-15 02:14:44', '2018-12-15 02:14:44'),
(351, 351, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/webmaster/sections?_pjax=%23view', '0.11974382', '2018-12-14', '19:16:19', '2018-12-15 02:16:19', '2018-12-15 02:16:19'),
(352, 352, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections/11/edit', 'unknown', 'http://bsegypt.net/ad/webmaster/sections/11/edit', '0.23600411', '2018-12-14', '19:16:24', '2018-12-15 02:16:24', '2018-12-15 02:16:24'),
(353, 353, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.09447408', '2018-12-14', '19:16:25', '2018-12-15 02:16:25', '2018-12-15 02:16:25'),
(354, 354, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections/11/edit', 'unknown', 'http://bsegypt.net/ad/webmaster/sections/11/edit', '0.12332916', '2018-12-14', '19:16:39', '2018-12-15 02:16:39', '2018-12-15 02:16:39'),
(355, 355, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.081604', '2018-12-14', '19:16:39', '2018-12-15 02:16:39', '2018-12-15 02:16:39'),
(356, 356, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/webmaster/sections?_pjax=%23view', '0.12126589', '2018-12-14', '19:16:42', '2018-12-15 02:16:42', '2018-12-15 02:16:42'),
(357, 357, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', '0.11061811', '2018-12-14', '19:20:15', '2018-12-15 02:20:15', '2018-12-15 02:20:15'),
(358, 358, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.08099413', '2018-12-14', '19:20:16', '2018-12-15 02:20:16', '2018-12-15 02:20:16'),
(359, 359, '156.195.178.199', 'http://bsegypt.net/ad/webmaster/sections', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', '0.11575103', '2018-12-14', '19:20:21', '2018-12-15 02:20:21', '2018-12-15 02:20:21'),
(360, 360, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.094347', '2018-12-14', '19:20:22', '2018-12-15 02:20:22', '2018-12-15 02:20:22'),
(361, 361, '156.195.178.199', 'http://bsegypt.net/ad/11/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/11/topics?_pjax=%23view', '0.11669588', '2018-12-14', '19:20:24', '2018-12-15 02:20:24', '2018-12-15 02:20:24'),
(362, 362, '156.195.178.199', 'http://bsegypt.net/ad/11/topics?_pjax=%23view', 'unknown', 'http://bsegypt.net/ad/11/topics?_pjax=%23view', '0.11152816', '2018-12-14', '19:21:23', '2018-12-15 02:21:23', '2018-12-15 02:21:23'),
(363, 363, '156.195.178.199', 'http://bsegypt.net/ad/11/topics/create', 'unknown', 'http://bsegypt.net/ad/11/topics/create', '0.15185213', '2018-12-14', '19:21:26', '2018-12-15 02:21:26', '2018-12-15 02:21:26'),
(364, 364, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.15988898', '2018-12-14', '19:21:27', '2018-12-15 02:21:27', '2018-12-15 02:21:27'),
(365, 365, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.13625002', '2018-12-14', '19:21:27', '2018-12-15 02:21:27', '2018-12-15 02:21:27'),
(366, 366, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.10026503', '2018-12-14', '19:21:32', '2018-12-15 02:21:32', '2018-12-15 02:21:32'),
(367, 367, '156.195.178.199', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://bsegypt.net/ad/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10143995', '2018-12-14', '19:21:33', '2018-12-15 02:21:33', '2018-12-15 02:21:33'),
(368, 368, '::1', 'Charity Challengers', 'unknown', 'http://localhost/ch/', '2.54029799', '2018-12-14', '21:15:43', '2018-12-14 19:15:43', '2018-12-14 19:15:43'),
(369, 369, '::1', 'Charity Challengers', 'unknown', 'http://localhost/ch/', '0.37317204', '2018-12-14', '21:17:38', '2018-12-14 19:17:38', '2018-12-14 19:17:38'),
(370, 370, '::1', 'http://localhost/ch/', 'unknown', 'http://localhost/ch/', '0.41933012', '2018-12-14', '21:21:51', '2018-12-14 19:21:51', '2018-12-14 19:21:51'),
(371, 371, '::1', 'http://localhost/ch/', 'unknown', 'http://localhost/ch/', '0.15937996', '2018-12-14', '21:21:53', '2018-12-14 19:21:53', '2018-12-14 19:21:53'),
(372, 372, '::1', 'http://localhost/ch/', 'unknown', 'http://localhost/ch/', '0.17979884', '2018-12-14', '21:21:55', '2018-12-14 19:21:55', '2018-12-14 19:21:55'),
(373, 373, '::1', 'Charity Challengers', 'unknown', 'http://localhost/ch/', '0.30606794', '2018-12-14', '21:22:03', '2018-12-14 19:22:03', '2018-12-14 19:22:03'),
(374, 374, '::1', 'Charity Challengers', 'unknown', 'http://localhost/ch/', '0.26782084', '2018-12-14', '21:22:11', '2018-12-14 19:22:11', '2018-12-14 19:22:11'),
(375, 368, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '0.69091415', '2018-12-14', '21:30:25', '2018-12-14 19:30:25', '2018-12-14 19:30:25'),
(376, 368, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.17904115', '2018-12-14', '21:30:25', '2018-12-14 19:30:25', '2018-12-14 19:30:25'),
(377, 368, '::1', 'http://localhost/laravel_project-master/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.17840004', '2018-12-14', '21:30:25', '2018-12-14 19:30:25', '2018-12-14 19:30:25'),
(378, 368, '::1', 'http://localhost/laravel_project-master/settings?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/settings?_pjax=%23view', '0.40355706', '2018-12-14', '21:31:06', '2018-12-14 19:31:06', '2018-12-14 19:31:06'),
(379, 368, '::1', 'http://localhost/laravel_project-master/settings', 'unknown', 'http://localhost/laravel_project-master/settings', '0.38285899', '2018-12-14', '21:31:06', '2018-12-14 19:31:06', '2018-12-14 19:31:06'),
(380, 368, '::1', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', '0.25222397', '2018-12-14', '21:41:43', '2018-12-14 19:41:43', '2018-12-14 19:41:43'),
(381, 368, '::1', 'http://localhost/laravel_project-master/11/topics', 'unknown', 'http://localhost/laravel_project-master/11/topics', '0.23484397', '2018-12-14', '21:42:01', '2018-12-14 19:42:01', '2018-12-14 19:42:01'),
(382, 375, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '0.56119418', '2018-12-15', '06:33:06', '2018-12-15 04:33:06', '2018-12-15 04:33:06'),
(383, 375, '::1', 'http://localhost/laravel_project-master/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.96419907', '2018-12-15', '06:33:08', '2018-12-15 04:33:08', '2018-12-15 04:33:08'),
(384, 375, '::1', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.44740295', '2018-12-15', '06:35:25', '2018-12-15 04:35:25', '2018-12-15 04:35:25'),
(385, 375, '::1', 'http://localhost/laravel_project-master/11/topics', 'unknown', 'http://localhost/laravel_project-master/11/topics', '0.48703599', '2018-12-15', '06:35:42', '2018-12-15 04:35:42', '2018-12-15 04:35:42'),
(386, 375, '::1', 'http://localhost/laravel_project-master/11/topics/create', 'unknown', 'http://localhost/laravel_project-master/11/topics/create', '0.56923914', '2018-12-15', '06:58:40', '2018-12-15 04:58:40', '2018-12-15 04:58:40'),
(387, 375, '::1', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.15674114', '2018-12-15', '06:58:41', '2018-12-15 04:58:41', '2018-12-15 04:58:41'),
(388, 375, '::1', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.16517806', '2018-12-15', '06:58:41', '2018-12-15 04:58:41', '2018-12-15 04:58:41'),
(389, 375, '::1', 'http://localhost/laravel_project-master/5/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/5/topics?_pjax=%23view', '0.2438879', '2018-12-15', '07:06:34', '2018-12-15 05:06:34', '2018-12-15 05:06:34'),
(390, 375, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '0.52199221', '2018-12-15', '07:06:34', '2018-12-15 05:06:34', '2018-12-15 05:06:34'),
(391, 375, '::1', 'http://localhost/laravel_project-master/4/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/4/topics?_pjax=%23view', '0.28630996', '2018-12-15', '07:06:36', '2018-12-15 05:06:36', '2018-12-15 05:06:36'),
(392, 375, '::1', 'http://localhost/laravel_project-master/8/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/8/topics?_pjax=%23view', '0.20545888', '2018-12-15', '07:06:38', '2018-12-15 05:06:38', '2018-12-15 05:06:38'),
(393, 375, '::1', 'http://localhost/laravel_project-master/8/topics/33/edit', 'unknown', 'http://localhost/laravel_project-master/8/topics/33/edit', '0.54716182', '2018-12-15', '07:06:53', '2018-12-15 05:06:53', '2018-12-15 05:06:53'),
(394, 375, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_0.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_0.png', '0.39997578', '2018-12-15', '07:06:53', '2018-12-15 05:06:53', '2018-12-15 05:06:53'),
(395, 375, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_1.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_1.png', '0.33747983', '2018-12-15', '07:06:53', '2018-12-15 05:06:53', '2018-12-15 05:06:53'),
(396, 375, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_3.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_3.png', '0.38139582', '2018-12-15', '07:06:53', '2018-12-15 05:06:53', '2018-12-15 05:06:53'),
(397, 375, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_2.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_2.png', '0.41355515', '2018-12-15', '07:06:53', '2018-12-15 05:06:53', '2018-12-15 05:06:53'),
(398, 375, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_4.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_4.png', '0.29190493', '2018-12-15', '07:06:54', '2018-12-15 05:06:54', '2018-12-15 05:06:54'),
(399, 375, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_5.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_5.png', '0.28201103', '2018-12-15', '07:06:54', '2018-12-15 05:06:54', '2018-12-15 05:06:54'),
(400, 375, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_6.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_6.png', '0.29405904', '2018-12-15', '07:06:54', '2018-12-15 05:06:54', '2018-12-15 05:06:54'),
(401, 375, '::1', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', '0.67440391', '2018-12-15', '07:07:25', '2018-12-15 05:07:25', '2018-12-15 05:07:25'),
(402, 375, '::1', 'http://localhost/laravel_project-master/webmaster/sections/8/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/8/edit', '0.35781002', '2018-12-15', '07:07:30', '2018-12-15 05:07:30', '2018-12-15 05:07:30'),
(403, 375, '::1', 'http://localhost/laravel_project-master/8/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/8/sections?_pjax=%23view', '0.27011704', '2018-12-15', '07:07:40', '2018-12-15 05:07:40', '2018-12-15 05:07:40'),
(404, 375, '::1', 'http://localhost/laravel_project-master/8/sections/19/edit', 'unknown', 'http://localhost/laravel_project-master/8/sections/19/edit', '0.35343504', '2018-12-15', '07:07:43', '2018-12-15 05:07:43', '2018-12-15 05:07:43'),
(405, 375, '::1', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', '0.40740085', '2018-12-15', '07:09:23', '2018-12-15 05:09:23', '2018-12-15 05:09:23'),
(406, 375, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/', '15.98889995', '2018-12-15', '14:57:11', '2018-12-15 12:57:11', '2018-12-15 12:57:11'),
(407, 375, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.39604688', '2018-12-15', '14:58:27', '2018-12-15 12:58:27', '2018-12-15 12:58:27'),
(408, 375, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '2.75398493', '2018-12-15', '15:34:09', '2018-12-15 13:34:09', '2018-12-15 13:34:09'),
(409, 375, '::1', 'http://localhost/laravel_project-master/11/topics/52/edit', 'unknown', 'http://localhost/laravel_project-master/11/topics/52/edit', '0.52700305', '2018-12-15', '17:24:31', '2018-12-15 15:24:31', '2018-12-15 15:24:31'),
(410, 375, '::1', 'http://localhost/laravel_project-master/11/topics/53/edit', 'unknown', 'http://localhost/laravel_project-master/11/topics/53/edit', '0.23184085', '2018-12-15', '17:25:57', '2018-12-15 15:25:57', '2018-12-15 15:25:57'),
(411, 375, '::1', 'http://localhost/laravel_project-master/11/topics/54/edit', 'unknown', 'http://localhost/laravel_project-master/11/topics/54/edit', '0.23739481', '2018-12-15', '17:27:47', '2018-12-15 15:27:47', '2018-12-15 15:27:47');
INSERT INTO `analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(412, 375, '::1', 'http://localhost/laravel_project-master/users?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/users?_pjax=%23view', '0.35463405', '2018-12-15', '18:27:50', '2018-12-15 16:27:50', '2018-12-15 16:27:50'),
(413, 375, '::1', 'http://localhost/laravel_project-master/users/permissions/2/edit', 'unknown', 'http://localhost/laravel_project-master/users/permissions/2/edit', '0.2016151', '2018-12-15', '18:29:16', '2018-12-15 16:29:16', '2018-12-15 16:29:16'),
(414, 375, '::1', 'http://localhost/laravel_project-master/users/2/edit', 'unknown', 'http://localhost/laravel_project-master/users/2/edit', '0.29056191', '2018-12-15', '18:29:34', '2018-12-15 16:29:34', '2018-12-15 16:29:34'),
(415, 375, '::1', 'http://localhost/laravel_project-master/menus?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/menus?_pjax=%23view', '0.31090689', '2018-12-15', '18:30:17', '2018-12-15 16:30:17', '2018-12-15 16:30:17'),
(416, 375, '::1', 'http://localhost/laravel_project-master/menus/3/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/3/edit/1', '0.27801394', '2018-12-15', '18:30:51', '2018-12-15 16:30:51', '2018-12-15 16:30:51'),
(417, 375, '::1', 'تحدي الخيرية', 'unknown', 'http://localhost/ch/', '4.57171178', '2018-12-15', '18:44:35', '2018-12-15 16:44:35', '2018-12-15 16:44:35'),
(418, 375, '::1', 'http://localhost/ch/admin', 'unknown', 'http://localhost/ch/admin', '0.86058903', '2018-12-15', '18:44:40', '2018-12-15 16:44:40', '2018-12-15 16:44:40'),
(419, 375, '::1', 'http://localhost/ch/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/ch/backEnd/assets/styles/flags.css', '0.35310602', '2018-12-15', '18:44:41', '2018-12-15 16:44:41', '2018-12-15 16:44:41'),
(420, 375, '::1', 'http://localhost/ch/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://localhost/ch/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.21756601', '2018-12-15', '18:44:41', '2018-12-15 16:44:41', '2018-12-15 16:44:41'),
(421, 376, '156.195.178.199', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.14129806', '2018-12-15', '19:39:00', '2018-12-16 02:39:00', '2018-12-16 02:39:00'),
(422, 377, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.37567616', '2018-12-15', '19:39:03', '2018-12-16 02:39:03', '2018-12-16 02:39:03'),
(423, 378, '156.195.178.199', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.36654019', '2018-12-15', '19:39:20', '2018-12-16 02:39:20', '2018-12-16 02:39:20'),
(424, 379, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.11973286', '2018-12-15', '19:39:20', '2018-12-16 02:39:20', '2018-12-16 02:39:20'),
(425, 380, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11210585', '2018-12-15', '19:39:21', '2018-12-16 02:39:21', '2018-12-16 02:39:21'),
(426, 381, '156.195.178.199', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.36595988', '2018-12-15', '19:39:31', '2018-12-16 02:39:31', '2018-12-16 02:39:31'),
(427, 382, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.16535687', '2018-12-15', '19:39:32', '2018-12-16 02:39:32', '2018-12-16 02:39:32'),
(428, 383, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11295295', '2018-12-15', '19:39:32', '2018-12-16 02:39:32', '2018-12-16 02:39:32'),
(429, 384, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/menus?_pjax=%23view', '0.23375392', '2018-12-15', '19:39:34', '2018-12-16 02:39:34', '2018-12-16 02:39:34'),
(430, 385, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/menus?_pjax=%23view', '0.15184617', '2018-12-15', '19:39:39', '2018-12-16 02:39:39', '2018-12-16 02:39:39'),
(431, 386, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/menus?_pjax=%23view', '0.17393303', '2018-12-15', '19:39:39', '2018-12-16 02:39:39', '2018-12-16 02:39:39'),
(432, 387, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus', 'unknown', 'http://chamspro.com/sitecpanel/menus', '0.17155385', '2018-12-15', '19:39:50', '2018-12-16 02:39:50', '2018-12-16 02:39:50'),
(433, 388, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus', 'unknown', 'http://chamspro.com/sitecpanel/menus', '0.18950796', '2018-12-15', '19:39:54', '2018-12-16 02:39:54', '2018-12-16 02:39:54'),
(434, 389, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12605596', '2018-12-15', '19:40:14', '2018-12-16 02:40:14', '2018-12-16 02:40:14'),
(435, 390, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus', 'unknown', 'http://chamspro.com/sitecpanel/menus', '0.17533588', '2018-12-15', '19:42:54', '2018-12-16 02:42:54', '2018-12-16 02:42:54'),
(436, 391, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12436604', '2018-12-15', '19:42:55', '2018-12-16 02:42:55', '2018-12-16 02:42:55'),
(437, 392, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus', 'unknown', 'http://chamspro.com/sitecpanel/menus', '0.16403484', '2018-12-15', '19:43:02', '2018-12-16 02:43:02', '2018-12-16 02:43:02'),
(438, 393, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11421013', '2018-12-15', '19:43:03', '2018-12-16 02:43:03', '2018-12-16 02:43:03'),
(439, 394, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus/create/1', 'unknown', 'http://chamspro.com/sitecpanel/menus/create/1', '0.23296213', '2018-12-15', '19:43:14', '2018-12-16 02:43:14', '2018-12-16 02:43:14'),
(440, 395, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.13191915', '2018-12-15', '19:43:15', '2018-12-16 02:43:15', '2018-12-16 02:43:15'),
(441, 396, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus/1', 'unknown', 'http://chamspro.com/sitecpanel/menus/1', '0.155478', '2018-12-15', '19:43:26', '2018-12-16 02:43:26', '2018-12-16 02:43:26'),
(442, 397, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.1167419', '2018-12-15', '19:43:29', '2018-12-16 02:43:29', '2018-12-16 02:43:29'),
(443, 398, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus/15/edit/1', 'unknown', 'http://chamspro.com/sitecpanel/menus/15/edit/1', '0.21268797', '2018-12-15', '19:44:46', '2018-12-16 02:44:46', '2018-12-16 02:44:46'),
(444, 399, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10872507', '2018-12-15', '19:44:46', '2018-12-16 02:44:46', '2018-12-16 02:44:46'),
(445, 400, '156.195.178.199', 'http://chamspro.com/sitecpanel/menus/1?id=15', 'unknown', 'http://chamspro.com/sitecpanel/menus/1?id=15', '0.14860106', '2018-12-15', '19:44:53', '2018-12-16 02:44:53', '2018-12-16 02:44:53'),
(446, 401, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10290003', '2018-12-15', '19:44:54', '2018-12-16 02:44:54', '2018-12-16 02:44:54'),
(447, 402, '156.195.178.199', 'http://chamspro.com/sitecpanel/11/topics', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', '0.17141891', '2018-12-15', '19:47:46', '2018-12-16 02:47:46', '2018-12-16 02:47:46'),
(448, 403, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11431789', '2018-12-15', '19:47:47', '2018-12-16 02:47:47', '2018-12-16 02:47:47'),
(449, 404, '156.195.178.199', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', '0.4612031', '2018-12-15', '19:48:25', '2018-12-16 02:48:25', '2018-12-16 02:48:25'),
(450, 405, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.1145041', '2018-12-15', '19:48:25', '2018-12-16 02:48:25', '2018-12-16 02:48:25'),
(451, 406, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.12461114', '2018-12-15', '19:48:26', '2018-12-16 02:48:26', '2018-12-16 02:48:26'),
(452, 407, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11221886', '2018-12-15', '19:48:26', '2018-12-16 02:48:26', '2018-12-16 02:48:26'),
(453, 408, '156.195.178.199', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', '0.36785603', '2018-12-15', '19:49:16', '2018-12-16 02:49:16', '2018-12-16 02:49:16'),
(454, 409, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.11397505', '2018-12-15', '19:49:16', '2018-12-16 02:49:16', '2018-12-16 02:49:16'),
(455, 410, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.12990785', '2018-12-15', '19:49:16', '2018-12-16 02:49:16', '2018-12-16 02:49:16'),
(456, 411, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12311316', '2018-12-15', '19:49:17', '2018-12-16 02:49:17', '2018-12-16 02:49:17'),
(457, 412, '156.195.178.199', 'http://chamspro.com/sitecpanel/8/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/8/topics?_pjax=%23view', '0.18928003', '2018-12-15', '19:49:19', '2018-12-16 02:49:19', '2018-12-16 02:49:19'),
(458, 413, '156.195.178.199', 'http://chamspro.com/sitecpanel/8/topics/create', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', '0.19274521', '2018-12-15', '19:49:22', '2018-12-16 02:49:22', '2018-12-16 02:49:22'),
(459, 414, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.11142778', '2018-12-15', '19:49:23', '2018-12-16 02:49:23', '2018-12-16 02:49:23'),
(460, 415, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.1337719', '2018-12-15', '19:49:23', '2018-12-16 02:49:23', '2018-12-16 02:49:23'),
(461, 416, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11843896', '2018-12-15', '19:49:23', '2018-12-16 02:49:23', '2018-12-16 02:49:23'),
(462, 417, '156.195.178.199', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', '0.24501395', '2018-12-15', '19:49:55', '2018-12-16 02:49:55', '2018-12-16 02:49:55'),
(463, 418, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.10746908', '2018-12-15', '19:49:55', '2018-12-16 02:49:55', '2018-12-16 02:49:55'),
(464, 419, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.44867992', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(465, 420, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_0.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_0.png', '0.46517706', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(466, 421, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_1.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_1.png', '0.44820499', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(467, 422, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_2.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_2.png', '0.36752486', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(468, 423, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_3.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_3.png', '0.26727891', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(469, 424, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_4.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_4.png', '0.2692101', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(470, 425, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_6.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_6.png', '0.39753199', '2018-12-15', '19:49:57', '2018-12-16 02:49:57', '2018-12-16 02:49:57'),
(471, 426, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_5.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_5.png', '0.40443492', '2018-12-15', '19:49:57', '2018-12-16 02:49:57', '2018-12-16 02:49:57'),
(472, 427, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.33609104', '2018-12-15', '19:49:57', '2018-12-16 02:49:57', '2018-12-16 02:49:57'),
(473, 428, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.20960808', '2018-12-15', '19:49:57', '2018-12-16 02:49:57', '2018-12-16 02:49:57'),
(474, 429, '156.195.178.199', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', '0.20945597', '2018-12-15', '19:50:00', '2018-12-16 02:50:00', '2018-12-16 02:50:00'),
(475, 430, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.11052394', '2018-12-15', '19:50:00', '2018-12-16 02:50:00', '2018-12-16 02:50:00'),
(476, 431, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_1.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_1.png', '0.39183307', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(477, 432, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.43055487', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(478, 433, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_0.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_0.png', '0.47921014', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(479, 434, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_2.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_2.png', '0.35865307', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(480, 435, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_3.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_3.png', '0.37480497', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(481, 436, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_4.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_4.png', '0.30809903', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(482, 437, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_5.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_5.png', '0.32410097', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(483, 438, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_6.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_6.png', '0.32639098', '2018-12-15', '19:50:02', '2018-12-16 02:50:02', '2018-12-16 02:50:02'),
(484, 439, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.26907897', '2018-12-15', '19:50:02', '2018-12-16 02:50:02', '2018-12-16 02:50:02'),
(485, 440, '156.195.178.199', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', '0.27794409', '2018-12-15', '19:50:22', '2018-12-16 02:50:22', '2018-12-16 02:50:22'),
(486, 441, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.11094189', '2018-12-15', '19:50:22', '2018-12-16 02:50:22', '2018-12-16 02:50:22'),
(487, 442, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_1.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_1.png', '0.51141596', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(488, 443, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.55207515', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(489, 444, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_0.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_0.png', '0.65610409', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(490, 445, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_2.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_2.png', '0.63979197', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(491, 446, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_3.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_3.png', '0.57262206', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(492, 447, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_4.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_4.png', '0.51977491', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(493, 448, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_5.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_5.png', '0.11622095', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(494, 449, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_6.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_6.png', '0.13698983', '2018-12-15', '19:50:24', '2018-12-16 02:50:24', '2018-12-16 02:50:24'),
(495, 450, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.14151907', '2018-12-15', '19:50:24', '2018-12-16 02:50:24', '2018-12-16 02:50:24'),
(496, 451, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_1.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_1.png', '0.3821919', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(497, 452, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_0.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_0.png', '0.46086907', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(498, 453, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', '0.53858399', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(499, 454, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_2.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_2.png', '0.45314503', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(500, 455, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', '0.60935211', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(501, 456, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_3.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_3.png', '0.46128392', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(502, 457, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', '0.44863605', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(503, 458, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_4.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_4.png', '0.42330503', '2018-12-15', '19:50:30', '2018-12-16 02:50:30', '2018-12-16 02:50:30'),
(504, 460, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_5.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_5.png', '0.348701', '2018-12-15', '19:50:30', '2018-12-16 02:50:30', '2018-12-16 02:50:30'),
(505, 459, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_6.png', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/images/marker_6.png', '0.34835196', '2018-12-15', '19:50:30', '2018-12-16 02:50:30', '2018-12-16 02:50:30'),
(506, 461, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics/create', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/create', '0.1543541', '2018-12-15', '19:50:31', '2018-12-16 02:50:31', '2018-12-16 02:50:31'),
(507, 462, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.10620308', '2018-12-15', '19:50:32', '2018-12-16 02:50:32', '2018-12-16 02:50:32'),
(508, 463, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.11097503', '2018-12-15', '19:50:32', '2018-12-16 02:50:32', '2018-12-16 02:50:32'),
(509, 464, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11059093', '2018-12-15', '19:50:32', '2018-12-16 02:50:32', '2018-12-16 02:50:32'),
(510, 465, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', '0.21779895', '2018-12-15', '19:50:57', '2018-12-16 02:50:57', '2018-12-16 02:50:57'),
(511, 466, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.11023402', '2018-12-15', '19:50:57', '2018-12-16 02:50:57', '2018-12-16 02:50:57'),
(512, 467, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.13873982', '2018-12-15', '19:50:57', '2018-12-16 02:50:57', '2018-12-16 02:50:57'),
(513, 468, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11375713', '2018-12-15', '19:50:58', '2018-12-16 02:50:58', '2018-12-16 02:50:58'),
(514, 469, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', '0.25240517', '2018-12-15', '19:51:04', '2018-12-16 02:51:04', '2018-12-16 02:51:04'),
(515, 470, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.13133788', '2018-12-15', '19:51:04', '2018-12-16 02:51:04', '2018-12-16 02:51:04'),
(516, 471, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.15185499', '2018-12-15', '19:51:05', '2018-12-16 02:51:05', '2018-12-16 02:51:05'),
(517, 472, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11742496', '2018-12-15', '19:51:05', '2018-12-16 02:51:05', '2018-12-16 02:51:05'),
(518, 473, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', '0.36600804', '2018-12-15', '19:51:09', '2018-12-16 02:51:09', '2018-12-16 02:51:09'),
(519, 474, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.11139107', '2018-12-15', '19:51:09', '2018-12-16 02:51:09', '2018-12-16 02:51:09'),
(520, 475, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.10303092', '2018-12-15', '19:51:10', '2018-12-16 02:51:10', '2018-12-16 02:51:10'),
(521, 476, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10157895', '2018-12-15', '19:51:10', '2018-12-16 02:51:10', '2018-12-16 02:51:10'),
(522, 477, '156.195.178.199', 'http://chamspro.com/sitecpanel/4/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/4/topics?_pjax=%23view', '0.14289999', '2018-12-15', '19:51:12', '2018-12-16 02:51:12', '2018-12-16 02:51:12'),
(523, 478, '156.195.178.199', 'http://chamspro.com/sitecpanel/4/topics/create', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/create', '0.188977', '2018-12-15', '19:51:14', '2018-12-16 02:51:14', '2018-12-16 02:51:14'),
(524, 479, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.11651707', '2018-12-15', '19:51:15', '2018-12-16 02:51:15', '2018-12-16 02:51:15'),
(525, 480, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.14664817', '2018-12-15', '19:51:15', '2018-12-16 02:51:15', '2018-12-16 02:51:15'),
(526, 481, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.13440204', '2018-12-15', '19:51:15', '2018-12-16 02:51:15', '2018-12-16 02:51:15'),
(527, 482, '156.195.178.199', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', '0.35761809', '2018-12-15', '19:51:35', '2018-12-16 02:51:35', '2018-12-16 02:51:35'),
(528, 483, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.11358714', '2018-12-15', '19:51:36', '2018-12-16 02:51:36', '2018-12-16 02:51:36'),
(529, 484, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.11880708', '2018-12-15', '19:51:36', '2018-12-16 02:51:36', '2018-12-16 02:51:36'),
(530, 485, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10782099', '2018-12-15', '19:51:36', '2018-12-16 02:51:36', '2018-12-16 02:51:36'),
(531, 486, '156.195.178.199', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', '1.90683913', '2018-12-15', '19:51:45', '2018-12-16 02:51:45', '2018-12-16 02:51:45'),
(532, 487, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.12188911', '2018-12-15', '19:51:45', '2018-12-16 02:51:45', '2018-12-16 02:51:45'),
(533, 488, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.13139796', '2018-12-15', '19:51:45', '2018-12-16 02:51:45', '2018-12-16 02:51:45'),
(534, 489, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12458897', '2018-12-15', '19:51:46', '2018-12-16 02:51:46', '2018-12-16 02:51:46'),
(535, 490, '156.195.178.199', 'http://chamspro.com/sitecpanel/4/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/4/topics?_pjax=%23view', '0.19506598', '2018-12-15', '19:51:48', '2018-12-16 02:51:48', '2018-12-16 02:51:48'),
(536, 491, '156.195.178.199', 'http://chamspro.com/sitecpanel/4/topics', 'unknown', 'http://chamspro.com/sitecpanel/4/topics', '0.19300604', '2018-12-15', '19:52:30', '2018-12-16 02:52:30', '2018-12-16 02:52:30'),
(537, 492, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11241007', '2018-12-15', '19:52:30', '2018-12-16 02:52:30', '2018-12-16 02:52:30'),
(538, 493, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', '0.20900607', '2018-12-15', '19:52:34', '2018-12-16 02:52:34', '2018-12-16 02:52:34'),
(539, 494, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', '0.15015888', '2018-12-15', '19:52:41', '2018-12-16 02:52:41', '2018-12-16 02:52:41'),
(540, 495, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.1752851', '2018-12-15', '19:52:42', '2018-12-16 02:52:42', '2018-12-16 02:52:42'),
(541, 496, '156.195.178.199', 'http://chamspro.com/sitecpanel/5/topics', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', '0.15293598', '2018-12-15', '19:52:45', '2018-12-16 02:52:45', '2018-12-16 02:52:45'),
(542, 497, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.13637686', '2018-12-15', '19:52:46', '2018-12-16 02:52:46', '2018-12-16 02:52:46'),
(543, 498, '156.195.178.199', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.11450481', '2018-12-15', '19:54:16', '2018-12-16 02:54:16', '2018-12-16 02:54:16'),
(544, 499, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11017394', '2018-12-15', '19:54:17', '2018-12-16 02:54:17', '2018-12-16 02:54:17'),
(545, 500, '156.195.178.199', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.19816804', '2018-12-15', '19:54:28', '2018-12-16 02:54:28', '2018-12-16 02:54:28'),
(546, 501, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.1227839', '2018-12-15', '19:54:29', '2018-12-16 02:54:29', '2018-12-16 02:54:29'),
(547, 502, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.14680505', '2018-12-15', '19:54:29', '2018-12-16 02:54:29', '2018-12-16 02:54:29'),
(548, 503, '156.195.178.199', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.21139288', '2018-12-15', '19:54:34', '2018-12-16 02:54:34', '2018-12-16 02:54:34'),
(549, 504, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.13568711', '2018-12-15', '19:54:35', '2018-12-16 02:54:35', '2018-12-16 02:54:35'),
(550, 505, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10678506', '2018-12-15', '19:54:35', '2018-12-16 02:54:35', '2018-12-16 02:54:35'),
(551, 506, '156.195.178.199', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.20228791', '2018-12-15', '19:54:41', '2018-12-16 02:54:41', '2018-12-16 02:54:41'),
(552, 507, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.11406493', '2018-12-15', '19:54:42', '2018-12-16 02:54:42', '2018-12-16 02:54:42'),
(553, 508, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10507584', '2018-12-15', '19:54:42', '2018-12-16 02:54:42', '2018-12-16 02:54:42'),
(554, 509, '156.195.178.199', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.11482191', '2018-12-15', '19:54:53', '2018-12-16 02:54:53', '2018-12-16 02:54:53'),
(555, 510, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11074686', '2018-12-15', '19:54:54', '2018-12-16 02:54:54', '2018-12-16 02:54:54'),
(556, 511, '156.195.178.199', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.20261502', '2018-12-15', '19:55:09', '2018-12-16 02:55:09', '2018-12-16 02:55:09'),
(557, 512, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.1118331', '2018-12-15', '19:55:10', '2018-12-16 02:55:10', '2018-12-16 02:55:10'),
(558, 513, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10466599', '2018-12-15', '19:55:10', '2018-12-16 02:55:10', '2018-12-16 02:55:10'),
(559, 514, '156.195.178.199', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.20000887', '2018-12-15', '19:55:20', '2018-12-16 02:55:20', '2018-12-16 02:55:20'),
(560, 515, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.11941099', '2018-12-15', '19:55:21', '2018-12-16 02:55:21', '2018-12-16 02:55:21'),
(561, 516, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12178397', '2018-12-15', '19:55:21', '2018-12-16 02:55:21', '2018-12-16 02:55:21'),
(562, 517, '156.195.178.199', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.21999788', '2018-12-15', '19:59:30', '2018-12-16 02:59:30', '2018-12-16 02:59:30'),
(563, 518, '156.195.178.199', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.11013103', '2018-12-15', '19:59:37', '2018-12-16 02:59:37', '2018-12-16 02:59:37'),
(564, 519, '156.205.235.193', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.11687207', '2018-12-15', '20:00:19', '2018-12-16 03:00:19', '2018-12-16 03:00:19'),
(565, 520, '156.205.235.193', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11975002', '2018-12-15', '20:00:21', '2018-12-16 03:00:21', '2018-12-16 03:00:21'),
(566, 521, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.16416097', '2018-12-15', '20:00:56', '2018-12-16 03:00:56', '2018-12-16 03:00:56'),
(567, 522, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11245394', '2018-12-15', '20:00:57', '2018-12-16 03:00:57', '2018-12-16 03:00:57'),
(568, 523, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12594509', '2018-12-15', '20:01:20', '2018-12-16 03:01:20', '2018-12-16 03:01:20'),
(569, 524, '156.195.178.199', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.1141398', '2018-12-15', '20:01:27', '2018-12-16 03:01:27', '2018-12-16 03:01:27'),
(570, 525, '156.195.178.199', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10616589', '2018-12-15', '20:01:28', '2018-12-16 03:01:28', '2018-12-16 03:01:28'),
(571, 526, '196.139.12.232', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.11748195', '2018-12-15', '20:01:45', '2018-12-16 03:01:45', '2018-12-16 03:01:45'),
(572, 527, '196.139.12.232', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12564802', '2018-12-15', '20:01:47', '2018-12-16 03:01:47', '2018-12-16 03:01:47'),
(573, 528, '156.205.235.193', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.23622394', '2018-12-15', '20:02:45', '2018-12-16 03:02:45', '2018-12-16 03:02:45'),
(574, 529, '156.205.235.193', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.110847', '2018-12-15', '20:02:46', '2018-12-16 03:02:46', '2018-12-16 03:02:46'),
(575, 530, '156.205.235.193', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11473703', '2018-12-15', '20:02:46', '2018-12-16 03:02:46', '2018-12-16 03:02:46'),
(576, 531, '41.44.119.65', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.11527801', '2018-12-15', '20:02:51', '2018-12-16 03:02:51', '2018-12-16 03:02:51'),
(577, 532, '41.44.119.65', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11033297', '2018-12-15', '20:02:52', '2018-12-16 03:02:52', '2018-12-16 03:02:52'),
(578, 533, '41.44.119.65', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.10987306', '2018-12-15', '20:03:04', '2018-12-16 03:03:04', '2018-12-16 03:03:04'),
(579, 534, '41.44.119.65', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.12174892', '2018-12-15', '20:03:48', '2018-12-16 03:03:48', '2018-12-16 03:03:48'),
(580, 535, '41.44.119.65', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12682509', '2018-12-15', '20:03:49', '2018-12-16 03:03:49', '2018-12-16 03:03:49'),
(581, 536, '156.195.178.199', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.12074184', '2018-12-15', '20:03:51', '2018-12-16 03:03:51', '2018-12-16 03:03:51'),
(582, 537, '41.44.119.65', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.11003304', '2018-12-15', '20:04:26', '2018-12-16 03:04:26', '2018-12-16 03:04:26'),
(583, 538, '41.44.119.65', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12012005', '2018-12-15', '20:04:27', '2018-12-16 03:04:27', '2018-12-16 03:04:27'),
(584, 539, '41.44.119.65', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.22155809', '2018-12-15', '20:04:57', '2018-12-16 03:04:57', '2018-12-16 03:04:57'),
(585, 540, '41.44.119.65', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.11259007', '2018-12-15', '20:04:58', '2018-12-16 03:04:58', '2018-12-16 03:04:58'),
(586, 541, '41.44.119.65', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12377906', '2018-12-15', '20:04:58', '2018-12-16 03:04:58', '2018-12-16 03:04:58'),
(587, 542, '41.44.119.65', 'http://chamspro.com/sitecpanel/4/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/4/topics?_pjax=%23view', '0.16115713', '2018-12-15', '20:05:22', '2018-12-16 03:05:22', '2018-12-16 03:05:22'),
(588, 543, '41.44.119.65', 'http://chamspro.com/sitecpanel/4/topics/26/edit', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/26/edit', '0.21999097', '2018-12-15', '20:05:31', '2018-12-16 03:05:31', '2018-12-16 03:05:31'),
(589, 544, '41.44.119.65', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.12567782', '2018-12-15', '20:05:32', '2018-12-16 03:05:32', '2018-12-16 03:05:32'),
(590, 545, '41.44.119.65', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.57017112', '2018-12-15', '20:05:33', '2018-12-16 03:05:33', '2018-12-16 03:05:33'),
(591, 546, '41.44.119.65', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11034107', '2018-12-15', '20:05:35', '2018-12-16 03:05:35', '2018-12-16 03:05:35'),
(592, 547, '41.44.119.65', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/5/topics?_pjax=%23view', '0.66460395', '2018-12-15', '20:05:48', '2018-12-16 03:05:48', '2018-12-16 03:05:48'),
(593, 548, '41.44.119.65', 'http://chamspro.com/sitecpanel/8/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/8/topics?_pjax=%23view', '0.17561316', '2018-12-15', '20:05:54', '2018-12-16 03:05:54', '2018-12-16 03:05:54'),
(594, 549, '41.44.119.65', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', '0.1387949', '2018-12-15', '20:06:01', '2018-12-16 03:06:01', '2018-12-16 03:06:01'),
(595, 550, '41.44.119.65', 'http://chamspro.com/sitecpanel/8/topics', 'unknown', 'http://chamspro.com/sitecpanel/8/topics', '0.17783999', '2018-12-15', '20:17:41', '2018-12-16 03:17:41', '2018-12-16 03:17:41'),
(596, 551, '41.44.119.65', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.1100359', '2018-12-15', '20:17:43', '2018-12-16 03:17:43', '2018-12-16 03:17:43'),
(597, 552, '156.195.182.158', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.36405516', '2018-12-16', '18:45:08', '2018-12-17 01:45:08', '2018-12-17 01:45:08'),
(598, 553, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.18006301', '2018-12-16', '18:45:16', '2018-12-17 01:45:16', '2018-12-17 01:45:16'),
(599, 554, '156.195.182.158', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.34971404', '2018-12-16', '18:45:23', '2018-12-17 01:45:23', '2018-12-17 01:45:23'),
(600, 555, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.12414098', '2018-12-16', '18:45:24', '2018-12-17 01:45:24', '2018-12-17 01:45:24'),
(601, 556, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.13122392', '2018-12-16', '18:45:25', '2018-12-17 01:45:25', '2018-12-17 01:45:25');
INSERT INTO `analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(602, 557, '156.205.235.193', 'http://sitecpanel.chamspro.com/login', 'unknown', 'http://sitecpanel.chamspro.com/login', '0.11783504', '2018-12-16', '18:45:37', '2018-12-17 01:45:37', '2018-12-17 01:45:37'),
(603, 558, '156.205.235.193', 'http://sitecpanel.chamspro.com/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://sitecpanel.chamspro.com/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.13136101', '2018-12-16', '18:45:39', '2018-12-17 01:45:39', '2018-12-17 01:45:39'),
(604, 559, '156.195.182.158', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.22418594', '2018-12-16', '18:45:44', '2018-12-17 01:45:44', '2018-12-17 01:45:44'),
(605, 560, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.15034103', '2018-12-16', '18:45:45', '2018-12-17 01:45:45', '2018-12-17 01:45:45'),
(606, 561, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11812091', '2018-12-16', '18:45:48', '2018-12-17 01:45:48', '2018-12-17 01:45:48'),
(607, 562, '156.205.235.193', 'http://sitecpanel.chamspro.com/admin', 'unknown', 'http://sitecpanel.chamspro.com/admin', '0.19156194', '2018-12-16', '18:45:57', '2018-12-17 01:45:57', '2018-12-17 01:45:57'),
(608, 563, '156.205.235.193', 'http://sitecpanel.chamspro.com/backEnd/assets/styles/flags.css', 'unknown', 'http://sitecpanel.chamspro.com/backEnd/assets/styles/flags.css', '0.11010003', '2018-12-16', '18:45:57', '2018-12-17 01:45:57', '2018-12-17 01:45:57'),
(609, 564, '156.205.235.193', 'http://sitecpanel.chamspro.com/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://sitecpanel.chamspro.com/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12969208', '2018-12-16', '18:45:58', '2018-12-17 01:45:58', '2018-12-17 01:45:58'),
(610, 565, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.13905191', '2018-12-16', '18:46:00', '2018-12-17 01:46:00', '2018-12-17 01:46:00'),
(611, 566, '156.205.235.193', 'http://sitecpanel.chamspro.com/8/topics?_pjax=%23view', 'unknown', 'http://sitecpanel.chamspro.com/8/topics?_pjax=%23view', '0.21165705', '2018-12-16', '18:46:06', '2018-12-17 01:46:06', '2018-12-17 01:46:06'),
(612, 567, '156.195.182.158', 'http://chamspro.com/sitecpanel/8/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/8/topics?_pjax=%23view', '0.19286299', '2018-12-16', '18:46:08', '2018-12-17 01:46:08', '2018-12-17 01:46:08'),
(613, 568, '156.205.235.193', 'http://sitecpanel.chamspro.com/8/topics', 'unknown', 'http://sitecpanel.chamspro.com/8/topics', '0.16532111', '2018-12-16', '18:46:53', '2018-12-17 01:46:53', '2018-12-17 01:46:53'),
(614, 569, '156.205.235.193', 'http://sitecpanel.chamspro.com/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://sitecpanel.chamspro.com/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.49519897', '2018-12-16', '18:46:54', '2018-12-17 01:46:54', '2018-12-17 01:46:54'),
(615, 570, '156.195.182.158', 'http://chamspro.com/sitecpanel/8/topics/create', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', '0.20181298', '2018-12-16', '18:48:12', '2018-12-17 01:48:12', '2018-12-17 01:48:12'),
(616, 571, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.12572289', '2018-12-16', '18:48:12', '2018-12-17 01:48:12', '2018-12-17 01:48:12'),
(617, 572, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.11757207', '2018-12-16', '18:48:13', '2018-12-17 01:48:13', '2018-12-17 01:48:13'),
(618, 573, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.14147401', '2018-12-16', '18:48:13', '2018-12-17 01:48:13', '2018-12-17 01:48:13'),
(619, 574, '156.195.182.158', 'http://chamspro.com/sitecpanel/8/topics/create', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', '0.1511972', '2018-12-16', '18:48:19', '2018-12-17 01:48:19', '2018-12-17 01:48:19'),
(620, 575, '156.205.235.193', 'http://sitecpanel.chamspro.com/8/topics/create', 'unknown', 'http://sitecpanel.chamspro.com/8/topics/create', '0.17646599', '2018-12-16', '18:48:19', '2018-12-17 01:48:19', '2018-12-17 01:48:19'),
(621, 576, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.37887192', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(622, 577, '156.205.235.193', 'http://sitecpanel.chamspro.com/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://sitecpanel.chamspro.com/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.4240551', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(623, 578, '156.205.235.193', 'http://sitecpanel.chamspro.com/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://sitecpanel.chamspro.com/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.45170498', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(624, 579, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.34062696', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(625, 580, '156.205.235.193', 'http://sitecpanel.chamspro.com/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://sitecpanel.chamspro.com/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.15641999', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(626, 581, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.14260101', '2018-12-16', '18:48:21', '2018-12-17 01:48:21', '2018-12-17 01:48:21'),
(627, 582, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', '0.15036106', '2018-12-16', '18:50:22', '2018-12-17 01:50:22', '2018-12-17 01:50:22'),
(628, 583, '156.205.235.193', 'http://sitecpanel.chamspro.com/11/topics?_pjax=%23view', 'unknown', 'http://sitecpanel.chamspro.com/11/topics?_pjax=%23view', '0.15950179', '2018-12-16', '18:50:44', '2018-12-17 01:50:44', '2018-12-17 01:50:44'),
(629, 584, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', '0.25562906', '2018-12-16', '18:51:03', '2018-12-17 01:51:03', '2018-12-17 01:51:03'),
(630, 585, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.12152982', '2018-12-16', '18:51:03', '2018-12-17 01:51:03', '2018-12-17 01:51:03'),
(631, 586, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.12713504', '2018-12-16', '18:51:04', '2018-12-17 01:51:04', '2018-12-17 01:51:04'),
(632, 587, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11512494', '2018-12-16', '18:51:04', '2018-12-17 01:51:04', '2018-12-17 01:51:04'),
(633, 588, '156.205.235.193', 'http://sitecpanel.chamspro.com/11/topics', 'unknown', 'http://sitecpanel.chamspro.com/11/topics', '0.15472102', '2018-12-16', '18:51:18', '2018-12-17 01:51:18', '2018-12-17 01:51:18'),
(634, 589, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', '0.15432501', '2018-12-16', '18:51:21', '2018-12-17 01:51:21', '2018-12-17 01:51:21'),
(635, 590, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', '0.19493794', '2018-12-16', '18:51:28', '2018-12-17 01:51:28', '2018-12-17 01:51:28'),
(636, 591, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.21475887', '2018-12-16', '18:51:29', '2018-12-17 01:51:29', '2018-12-17 01:51:29'),
(637, 592, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.22239614', '2018-12-16', '18:51:29', '2018-12-17 01:51:29', '2018-12-17 01:51:29'),
(638, 593, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.111938', '2018-12-16', '18:51:29', '2018-12-17 01:51:29', '2018-12-17 01:51:29'),
(639, 594, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', '0.14281487', '2018-12-16', '18:51:33', '2018-12-17 01:51:33', '2018-12-17 01:51:33'),
(640, 595, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', '0.21619987', '2018-12-16', '18:51:49', '2018-12-17 01:51:49', '2018-12-17 01:51:49'),
(641, 596, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', '0.21264791', '2018-12-16', '18:51:49', '2018-12-17 01:51:49', '2018-12-17 01:51:49'),
(642, 597, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', '0.16772795', '2018-12-16', '18:51:49', '2018-12-17 01:51:49', '2018-12-17 01:51:49'),
(643, 598, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', '0.1621139', '2018-12-16', '18:51:49', '2018-12-17 01:51:49', '2018-12-17 01:51:49'),
(644, 599, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', '0.15664506', '2018-12-16', '18:51:50', '2018-12-17 01:51:50', '2018-12-17 01:51:50'),
(645, 600, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', '0.161134', '2018-12-16', '18:51:50', '2018-12-17 01:51:50', '2018-12-17 01:51:50'),
(646, 601, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', '0.15927505', '2018-12-16', '18:51:50', '2018-12-17 01:51:50', '2018-12-17 01:51:50'),
(647, 602, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.13135219', '2018-12-16', '18:52:00', '2018-12-17 01:52:00', '2018-12-17 01:52:00'),
(648, 603, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics/create', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/create', '0.15405607', '2018-12-16', '18:52:23', '2018-12-17 01:52:23', '2018-12-17 01:52:23'),
(649, 604, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.20833707', '2018-12-16', '18:52:24', '2018-12-17 01:52:24', '2018-12-17 01:52:24'),
(650, 605, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.20710206', '2018-12-16', '18:52:24', '2018-12-17 01:52:24', '2018-12-17 01:52:24'),
(651, 606, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10681605', '2018-12-16', '18:52:24', '2018-12-17 01:52:24', '2018-12-17 01:52:24'),
(652, 607, '156.195.182.158', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.27557015', '2018-12-16', '18:54:18', '2018-12-17 01:54:18', '2018-12-17 01:54:18'),
(653, 608, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.12872195', '2018-12-16', '18:54:21', '2018-12-17 01:54:21', '2018-12-17 01:54:21'),
(654, 609, '156.195.182.158', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.10496902', '2018-12-16', '18:54:21', '2018-12-17 01:54:21', '2018-12-17 01:54:21'),
(655, 610, '156.195.182.158', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.12491417', '2018-12-16', '18:56:42', '2018-12-17 01:56:42', '2018-12-17 01:56:42'),
(656, 611, '156.205.235.193', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.11888313', '2018-12-16', '18:57:02', '2018-12-17 01:57:02', '2018-12-17 01:57:02'),
(657, 612, '156.205.235.193', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.18513608', '2018-12-16', '18:57:07', '2018-12-17 01:57:07', '2018-12-17 01:57:07'),
(658, 613, '156.205.235.193', 'http://chamspro.com/sitecpanel/admin', 'unknown', 'http://chamspro.com/sitecpanel/admin', '0.23920894', '2018-12-16', '18:57:52', '2018-12-17 01:57:52', '2018-12-17 01:57:52'),
(659, 614, '156.205.235.193', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/assets/styles/flags.css', '0.1161921', '2018-12-16', '18:57:52', '2018-12-17 01:57:52', '2018-12-17 01:57:52'),
(660, 615, '156.205.235.193', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.11658692', '2018-12-16', '18:57:53', '2018-12-17 01:57:53', '2018-12-17 01:57:53'),
(661, 616, '156.205.235.193', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', '0.48816609', '2018-12-16', '18:58:03', '2018-12-17 01:58:03', '2018-12-17 01:58:03'),
(662, 617, '156.205.235.193', 'http://chamspro.com/sitecpanel/11/topics/create', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/create', '0.18386006', '2018-12-16', '18:58:07', '2018-12-17 01:58:07', '2018-12-17 01:58:07'),
(663, 618, '156.205.235.193', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.153368', '2018-12-16', '18:58:08', '2018-12-17 01:58:08', '2018-12-17 01:58:08'),
(664, 619, '156.205.235.193', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.16079998', '2018-12-16', '18:58:08', '2018-12-17 01:58:08', '2018-12-17 01:58:08'),
(665, 620, '156.205.235.193', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.12607098', '2018-12-16', '18:58:08', '2018-12-17 01:58:08', '2018-12-17 01:58:08'),
(666, 621, '156.195.182.158', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/11/topics?_pjax=%23view', '0.17157817', '2018-12-16', '18:58:26', '2018-12-17 01:58:26', '2018-12-17 01:58:26'),
(667, 622, '156.205.235.193', 'http://chamspro.com/sitecpanel/8/topics?_pjax=%23view', 'unknown', 'http://chamspro.com/sitecpanel/8/topics?_pjax=%23view', '0.18677688', '2018-12-16', '18:59:45', '2018-12-17 01:59:45', '2018-12-17 01:59:45'),
(668, 623, '197.43.141.176', 'http://chamspro.com/sitecpanel/login', 'unknown', 'http://chamspro.com/sitecpanel/login', '0.51099515', '2018-12-16', '22:22:25', '2018-12-17 05:22:25', '2018-12-17 05:22:25'),
(669, 624, '197.43.141.176', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://chamspro.com/sitecpanel/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.1486609', '2018-12-16', '22:22:27', '2018-12-17 05:22:27', '2018-12-17 05:22:27'),
(670, 625, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '1.14749813', '2018-12-18', '20:29:39', '2018-12-18 18:29:39', '2018-12-18 18:29:39'),
(671, 625, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.24010921', '2018-12-18', '20:29:39', '2018-12-18 18:29:39', '2018-12-18 18:29:39'),
(672, 625, '::1', 'http://localhost/laravel_project-master/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.386379', '2018-12-18', '20:29:40', '2018-12-18 18:29:40', '2018-12-18 18:29:40'),
(673, 625, '::1', 'http://localhost/laravel_project-master/settings', 'unknown', 'http://localhost/laravel_project-master/settings', '0.40285397', '2018-12-18', '20:31:48', '2018-12-18 18:31:48', '2018-12-18 18:31:48'),
(674, 625, '::1', 'http://localhost/laravel_project-master/settings?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/settings?_pjax=%23view', '0.42806983', '2018-12-18', '20:31:48', '2018-12-18 18:31:48', '2018-12-18 18:31:48'),
(675, 625, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/', '0.64164591', '2018-12-18', '20:31:54', '2018-12-18 18:31:54', '2018-12-18 18:31:54'),
(676, 625, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '0.5283041', '2018-12-18', '20:31:57', '2018-12-18 18:31:57', '2018-12-18 18:31:57'),
(677, 625, '::1', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', '0.32473683', '2018-12-18', '20:32:29', '2018-12-18 18:32:29', '2018-12-18 18:32:29'),
(678, 625, '::1', 'http://localhost/laravel_project-master/webmaster/sections/create', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/create', '0.19480705', '2018-12-18', '20:32:33', '2018-12-18 18:32:33', '2018-12-18 18:32:33'),
(679, 625, '::1', 'http://localhost/laravel_project-master/webmaster/sections', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections', '0.22582507', '2018-12-18', '20:33:31', '2018-12-18 18:33:31', '2018-12-18 18:33:31'),
(680, 625, '::1', 'http://localhost/laravel_project-master/12/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/12/topics?_pjax=%23view', '0.65835404', '2018-12-18', '20:36:58', '2018-12-18 18:36:58', '2018-12-18 18:36:58'),
(681, 625, '::1', 'http://localhost/laravel_project-master/12/topics/create', 'unknown', 'http://localhost/laravel_project-master/12/topics/create', '0.39609289', '2018-12-18', '20:37:00', '2018-12-18 18:37:00', '2018-12-18 18:37:00'),
(682, 625, '::1', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.15531397', '2018-12-18', '20:37:01', '2018-12-18 18:37:01', '2018-12-18 18:37:01'),
(683, 625, '::1', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '1.04325294', '2018-12-18', '20:37:01', '2018-12-18 18:37:01', '2018-12-18 18:37:01'),
(684, 625, '::1', 'http://localhost/laravel_project-master/12/topics/56/edit', 'unknown', 'http://localhost/laravel_project-master/12/topics/56/edit', '0.70890713', '2018-12-18', '20:38:42', '2018-12-18 18:38:42', '2018-12-18 18:38:42'),
(685, 625, '::1', 'http://localhost/laravel_project-master/12/topics/57/edit', 'unknown', 'http://localhost/laravel_project-master/12/topics/57/edit', '0.21575689', '2018-12-18', '20:39:15', '2018-12-18 18:39:15', '2018-12-18 18:39:15'),
(686, 625, '::1', 'http://localhost/laravel_project-master/12/topics/58/edit', 'unknown', 'http://localhost/laravel_project-master/12/topics/58/edit', '0.20960784', '2018-12-18', '20:39:40', '2018-12-18 18:39:40', '2018-12-18 18:39:40'),
(687, 625, '::1', 'http://localhost/laravel_project-master/13/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/13/topics?_pjax=%23view', '0.22990918', '2018-12-18', '20:41:58', '2018-12-18 18:41:58', '2018-12-18 18:41:58'),
(688, 625, '::1', 'http://localhost/laravel_project-master/13/topics/create', 'unknown', 'http://localhost/laravel_project-master/13/topics/create', '0.20308781', '2018-12-18', '20:41:59', '2018-12-18 18:41:59', '2018-12-18 18:41:59'),
(689, 625, '::1', 'http://localhost/laravel_project-master/webmaster?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster?_pjax=%23view', '0.43402505', '2018-12-18', '20:48:28', '2018-12-18 18:48:28', '2018-12-18 18:48:28'),
(690, 625, '::1', 'http://localhost/laravel_project-master/15/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/15/topics?_pjax=%23view', '0.18976092', '2018-12-18', '20:52:54', '2018-12-18 18:52:54', '2018-12-18 18:52:54'),
(691, 625, '::1', 'http://localhost/laravel_project-master/15/topics/create', 'unknown', 'http://localhost/laravel_project-master/15/topics/create', '0.20495987', '2018-12-18', '20:52:56', '2018-12-18 18:52:56', '2018-12-18 18:52:56'),
(692, 625, '::1', 'http://localhost/laravel_project-master/14/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/14/topics?_pjax=%23view', '0.19049191', '2018-12-18', '20:54:56', '2018-12-18 18:54:56', '2018-12-18 18:54:56'),
(693, 625, '::1', 'http://localhost/laravel_project-master/14/topics', 'unknown', 'http://localhost/laravel_project-master/14/topics', '0.23149395', '2018-12-18', '20:55:27', '2018-12-18 18:55:27', '2018-12-18 18:55:27'),
(694, 625, '::1', 'http://localhost/laravel_project-master/14/topics/create', 'unknown', 'http://localhost/laravel_project-master/14/topics/create', '0.22851205', '2018-12-18', '20:55:37', '2018-12-18 18:55:37', '2018-12-18 18:55:37'),
(695, 625, '::1', 'http://localhost/laravel_project-master/14/topics/59/edit', 'unknown', 'http://localhost/laravel_project-master/14/topics/59/edit', '0.22340894', '2018-12-18', '20:56:06', '2018-12-18 18:56:06', '2018-12-18 18:56:06'),
(696, 625, '::1', 'http://localhost/laravel_project-master/14/topics/60/edit', 'unknown', 'http://localhost/laravel_project-master/14/topics/60/edit', '0.25245285', '2018-12-18', '20:56:43', '2018-12-18 18:56:43', '2018-12-18 18:56:43'),
(697, 625, '::1', 'http://localhost/laravel_project-master/14/topics/61/edit', 'unknown', 'http://localhost/laravel_project-master/14/topics/61/edit', '0.22852612', '2018-12-18', '20:57:19', '2018-12-18 18:57:19', '2018-12-18 18:57:19'),
(698, 625, '::1', 'http://localhost/laravel_project-master/8/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/8/topics?_pjax=%23view', '0.41495895', '2018-12-18', '21:00:20', '2018-12-18 19:00:20', '2018-12-18 19:00:20'),
(699, 625, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '0.57392788', '2018-12-18', '21:00:22', '2018-12-18 19:00:22', '2018-12-18 19:00:22'),
(700, 625, '::1', 'http://localhost/laravel_project-master/8/topics/create', 'unknown', 'http://localhost/laravel_project-master/8/topics/create', '0.19095898', '2018-12-18', '21:00:23', '2018-12-18 19:00:23', '2018-12-18 19:00:23'),
(701, 625, '::1', 'http://localhost/laravel_project-master/4/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/4/topics?_pjax=%23view', '0.18721509', '2018-12-18', '21:00:34', '2018-12-18 19:00:34', '2018-12-18 19:00:34'),
(702, 625, '::1', 'http://localhost/laravel_project-master/webmaster/sections/8/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/8/edit', '0.62397003', '2018-12-18', '21:00:51', '2018-12-18 19:00:51', '2018-12-18 19:00:51'),
(703, 625, '::1', 'http://localhost/laravel_project-master/8/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/8/sections?_pjax=%23view', '0.53204203', '2018-12-18', '21:01:22', '2018-12-18 19:01:22', '2018-12-18 19:01:22'),
(704, 625, '::1', 'http://localhost/laravel_project-master/8/sections/create', 'unknown', 'http://localhost/laravel_project-master/8/sections/create', '0.19401383', '2018-12-18', '21:01:23', '2018-12-18 19:01:23', '2018-12-18 19:01:23'),
(705, 625, '::1', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.14806104', '2018-12-18', '21:01:29', '2018-12-18 19:01:29', '2018-12-18 19:01:29'),
(706, 625, '::1', 'http://localhost/laravel_project-master/15/topics/62/edit', 'unknown', 'http://localhost/laravel_project-master/15/topics/62/edit', '0.33227086', '2018-12-18', '21:26:21', '2018-12-18 19:26:21', '2018-12-18 19:26:21'),
(707, 625, '::1', 'http://localhost/laravel_project-master/15/topics/63/edit', 'unknown', 'http://localhost/laravel_project-master/15/topics/63/edit', '0.23378611', '2018-12-18', '21:27:16', '2018-12-18 19:27:16', '2018-12-18 19:27:16'),
(708, 625, '::1', 'http://localhost/laravel_project-master/15/topics/64/edit', 'unknown', 'http://localhost/laravel_project-master/15/topics/64/edit', '0.22562003', '2018-12-18', '21:27:43', '2018-12-18 19:27:43', '2018-12-18 19:27:43'),
(709, 625, '::1', 'http://localhost/laravel_project-master/13/topics/65/edit', 'unknown', 'http://localhost/laravel_project-master/13/topics/65/edit', '0.29229999', '2018-12-18', '21:31:35', '2018-12-18 19:31:35', '2018-12-18 19:31:35'),
(710, 625, '::1', 'http://localhost/laravel_project-master/16/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/16/topics?_pjax=%23view', '0.22864509', '2018-12-18', '21:45:00', '2018-12-18 19:45:00', '2018-12-18 19:45:00'),
(711, 625, '::1', 'http://localhost/laravel_project-master/16/topics/create', 'unknown', 'http://localhost/laravel_project-master/16/topics/create', '0.27715492', '2018-12-18', '21:45:02', '2018-12-18 19:45:02', '2018-12-18 19:45:02'),
(712, 625, '::1', 'http://localhost/laravel_project-master/16/topics/66/edit', 'unknown', 'http://localhost/laravel_project-master/16/topics/66/edit', '0.22874403', '2018-12-18', '21:45:51', '2018-12-18 19:45:51', '2018-12-18 19:45:51'),
(713, 626, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '4.76786709', '2018-12-20', '19:04:42', '2018-12-20 17:04:42', '2018-12-20 17:04:42'),
(714, 626, '::1', 'http://localhost/laravel_project-master/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/jquery/screenfull/dist/screenfull.min.js', '0.51618195', '2018-12-20', '19:04:44', '2018-12-20 17:04:44', '2018-12-20 17:04:44'),
(715, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections?page=2', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?page=2', '0.97316003', '2018-12-20', '19:05:05', '2018-12-20 17:05:05', '2018-12-20 17:05:05'),
(716, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections/20/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/20/edit', '1.34173107', '2018-12-20', '19:11:33', '2018-12-20 17:11:33', '2018-12-20 17:11:33'),
(717, 626, '::1', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.min.css', '0.14959407', '2018-12-20', '19:11:33', '2018-12-20 17:11:33', '2018-12-20 17:11:33'),
(718, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections/23/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/23/edit', '0.43423915', '2018-12-20', '19:17:01', '2018-12-20 17:17:01', '2018-12-20 17:17:01'),
(719, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections?page=1', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?page=1', '0.2288208', '2018-12-20', '19:22:26', '2018-12-20 17:22:26', '2018-12-20 17:22:26'),
(720, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections/4/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/4/edit', '0.25297999', '2018-12-20', '19:22:30', '2018-12-20 17:22:30', '2018-12-20 17:22:30'),
(721, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections/5/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/5/edit', '0.5476172', '2018-12-20', '19:22:32', '2018-12-20 17:22:32', '2018-12-20 17:22:32'),
(722, 626, '::1', 'http://localhost/laravel_project-master/8/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/8/sections?_pjax=%23view', '0.88434505', '2018-12-20', '19:23:02', '2018-12-20 17:23:02', '2018-12-20 17:23:02'),
(723, 626, '::1', 'http://localhost/laravel_project-master/21/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/21/topics?_pjax=%23view', '0.35534406', '2018-12-20', '19:25:47', '2018-12-20 17:25:47', '2018-12-20 17:25:47'),
(724, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', '0.17037821', '2018-12-20', '19:26:00', '2018-12-20 17:26:00', '2018-12-20 17:26:00'),
(725, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections/8/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/8/edit', '0.28260994', '2018-12-20', '19:26:09', '2018-12-20 17:26:09', '2018-12-20 17:26:09'),
(726, 626, '::1', 'http://localhost/laravel_project-master/8/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/8/topics?_pjax=%23view', '0.401546', '2018-12-20', '19:26:43', '2018-12-20 17:26:43', '2018-12-20 17:26:43'),
(727, 626, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '1.05267382', '2018-12-20', '19:26:45', '2018-12-20 17:26:45', '2018-12-20 17:26:45'),
(728, 626, '::1', 'http://localhost/laravel_project-master/8/topics/create', 'unknown', 'http://localhost/laravel_project-master/8/topics/create', '0.24145198', '2018-12-20', '19:26:56', '2018-12-20 17:26:56', '2018-12-20 17:26:56'),
(729, 626, '::1', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', 'unknown', 'http://localhost/laravel_project-master/backEnd/libs/js/iconpicker/fontawesome-iconpicker.js', '0.17693591', '2018-12-20', '19:26:56', '2018-12-20 17:26:56', '2018-12-20 17:26:56'),
(730, 626, '::1', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.14727402', '2018-12-20', '19:28:31', '2018-12-20 17:28:31', '2018-12-20 17:28:31'),
(731, 626, '::1', 'http://localhost/laravel_project-master/12/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/12/topics?_pjax=%23view', '0.38424683', '2018-12-20', '20:01:35', '2018-12-20 18:01:35', '2018-12-20 18:01:35'),
(732, 626, '::1', 'http://localhost/laravel_project-master/12/topics', 'unknown', 'http://localhost/laravel_project-master/12/topics', '0.34676003', '2018-12-20', '20:01:37', '2018-12-20 18:01:37', '2018-12-20 18:01:37'),
(733, 626, '::1', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', '0.19765902', '2018-12-20', '20:02:21', '2018-12-20 18:02:21', '2018-12-20 18:02:21'),
(734, 626, '::1', 'http://localhost/laravel_project-master/11/topics/create', 'unknown', 'http://localhost/laravel_project-master/11/topics/create', '0.41222', '2018-12-20', '20:02:23', '2018-12-20 18:02:23', '2018-12-20 18:02:23'),
(735, 626, '::1', 'http://localhost/laravel_project-master/12/topics/56/edit', 'unknown', 'http://localhost/laravel_project-master/12/topics/56/edit', '0.57734704', '2018-12-20', '20:35:18', '2018-12-20 18:35:18', '2018-12-20 18:35:18'),
(736, 626, '::1', 'http://localhost/laravel_project-master/13/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/13/topics?_pjax=%23view', '0.19407701', '2018-12-20', '20:52:00', '2018-12-20 18:52:00', '2018-12-20 18:52:00'),
(737, 626, '::1', 'http://localhost/laravel_project-master/22/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/22/topics?_pjax=%23view', '0.2158618', '2018-12-20', '20:54:08', '2018-12-20 18:54:08', '2018-12-20 18:54:08'),
(738, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections', '0.21870685', '2018-12-20', '20:57:40', '2018-12-20 18:57:40', '2018-12-20 18:57:40'),
(739, 626, '::1', 'http://localhost/laravel_project-master/webmaster/sections/11/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/11/edit', '0.21986485', '2018-12-20', '20:57:46', '2018-12-20 18:57:46', '2018-12-20 18:57:46'),
(740, 626, '::1', 'http://localhost/laravel_project-master/11/topics', 'unknown', 'http://localhost/laravel_project-master/11/topics', '0.25412416', '2018-12-20', '20:58:06', '2018-12-20 18:58:06', '2018-12-20 18:58:06'),
(741, 626, '::1', 'http://localhost/laravel_project-master/14/topics', 'unknown', 'http://localhost/laravel_project-master/14/topics', '0.43219399', '2018-12-20', '22:47:51', '2018-12-20 20:47:51', '2018-12-20 20:47:51'),
(742, 626, '::1', 'http://localhost/laravel_project-master/17/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/17/topics?_pjax=%23view', '0.89102817', '2018-12-20', '22:55:54', '2018-12-20 20:55:54', '2018-12-20 20:55:54'),
(743, 626, '::1', 'http://localhost/laravel_project-master/18/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/18/topics?_pjax=%23view', '0.2291069', '2018-12-20', '22:56:30', '2018-12-20 20:56:30', '2018-12-20 20:56:30'),
(744, 626, '::1', 'http://localhost/laravel_project-master/19/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/19/topics?_pjax=%23view', '0.24641681', '2018-12-20', '23:19:01', '2018-12-20 21:19:01', '2018-12-20 21:19:01'),
(745, 626, '::1', 'http://localhost/laravel_project-master/20/topics', 'unknown', 'http://localhost/laravel_project-master/20/topics', '0.28260303', '2018-12-20', '23:21:23', '2018-12-20 21:21:23', '2018-12-20 21:21:23'),
(746, 626, '::1', 'http://localhost/laravel_project-master/21/topics', 'unknown', 'http://localhost/laravel_project-master/21/topics', '0.29412293', '2018-12-20', '23:22:41', '2018-12-20 21:22:41', '2018-12-20 21:22:41'),
(747, 626, '::1', 'http://localhost/laravel_project-master/22/topics', 'unknown', 'http://localhost/laravel_project-master/22/topics', '0.29330111', '2018-12-20', '23:24:16', '2018-12-20 21:24:16', '2018-12-20 21:24:16'),
(748, 626, '::1', 'http://localhost/laravel_project-master/23/topics', 'unknown', 'http://localhost/laravel_project-master/23/topics', '0.28800011', '2018-12-20', '23:25:37', '2018-12-20 21:25:37', '2018-12-20 21:25:37'),
(749, 626, '::1', 'http://localhost/laravel_project-master/24/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/24/topics?_pjax=%23view', '0.19618106', '2018-12-20', '23:26:43', '2018-12-20 21:26:43', '2018-12-20 21:26:43'),
(750, 627, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '1.24839211', '2018-12-21', '11:40:44', '2018-12-21 09:40:44', '2018-12-21 09:40:44'),
(751, 627, '::1', 'http://localhost/laravel_project-master/22/topics', 'unknown', 'http://localhost/laravel_project-master/22/topics', '0.71873808', '2018-12-21', '11:41:59', '2018-12-21 09:41:59', '2018-12-21 09:41:59'),
(752, 627, '::1', 'http://localhost/laravel_project-master/8/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/8/topics?_pjax=%23view', '0.3350091', '2018-12-21', '11:42:12', '2018-12-21 09:42:12', '2018-12-21 09:42:12'),
(753, 627, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '1.32979298', '2018-12-21', '11:42:14', '2018-12-21 09:42:14', '2018-12-21 09:42:14'),
(754, 627, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '0.690593', '2018-12-21', '11:42:21', '2018-12-21 09:42:21', '2018-12-21 09:42:21'),
(755, 627, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.38938498', '2018-12-21', '11:42:21', '2018-12-21 09:42:21', '2018-12-21 09:42:21'),
(756, 627, '::1', 'http://localhost/laravel_project-master/8/topics/create', 'unknown', 'http://localhost/laravel_project-master/8/topics/create', '0.30766201', '2018-12-21', '11:42:27', '2018-12-21 09:42:27', '2018-12-21 09:42:27'),
(757, 627, '::1', 'http://localhost/laravel_project-master/17/topics', 'unknown', 'http://localhost/laravel_project-master/17/topics', '0.25699711', '2018-12-21', '11:54:49', '2018-12-21 09:54:49', '2018-12-21 09:54:49'),
(758, 627, '::1', 'http://localhost/laravel_project-master/22/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/22/topics?_pjax=%23view', '0.19500804', '2018-12-21', '12:00:06', '2018-12-21 10:00:06', '2018-12-21 10:00:06'),
(759, 627, '::1', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', '0.2447679', '2018-12-21', '12:00:11', '2018-12-21 10:00:11', '2018-12-21 10:00:11'),
(760, 627, '::1', 'http://localhost/laravel_project-master/webmaster/sections/25/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/25/edit', '2.75549197', '2018-12-21', '12:02:56', '2018-12-21 10:02:56', '2018-12-21 10:02:56'),
(761, 627, '::1', 'http://localhost/laravel_project-master/8/topics', 'unknown', 'http://localhost/laravel_project-master/8/topics', '0.28047705', '2018-12-21', '12:03:17', '2018-12-21 10:03:17', '2018-12-21 10:03:17'),
(762, 627, '::1', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.23327088', '2018-12-21', '12:03:29', '2018-12-21 10:03:29', '2018-12-21 10:03:29'),
(763, 627, '::1', 'http://localhost/laravel_project-master/17/topics/create', 'unknown', 'http://localhost/laravel_project-master/17/topics/create', '2.2196281', '2018-12-21', '13:12:06', '2018-12-21 11:12:06', '2018-12-21 11:12:06'),
(764, 627, '::1', 'http://localhost/laravel_project-master/15/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/15/topics?_pjax=%23view', '0.37086892', '2018-12-21', '13:12:38', '2018-12-21 11:12:38', '2018-12-21 11:12:38'),
(765, 627, '::1', 'http://localhost/laravel_project-master/15/topics/create', 'unknown', 'http://localhost/laravel_project-master/15/topics/create', '0.24193287', '2018-12-21', '13:12:39', '2018-12-21 11:12:39', '2018-12-21 11:12:39'),
(766, 627, '::1', 'http://localhost/laravel_project-master/13/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/13/topics?_pjax=%23view', '0.35703397', '2018-12-21', '13:15:00', '2018-12-21 11:15:00', '2018-12-21 11:15:00'),
(767, 627, '::1', 'http://localhost/laravel_project-master/12/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/12/topics?_pjax=%23view', '0.19831491', '2018-12-21', '13:15:01', '2018-12-21 11:15:01', '2018-12-21 11:15:01'),
(768, 627, '::1', 'http://localhost/laravel_project-master/12/topics/create', 'unknown', 'http://localhost/laravel_project-master/12/topics/create', '0.2108252', '2018-12-21', '13:15:03', '2018-12-21 11:15:03', '2018-12-21 11:15:03'),
(769, 627, '::1', 'http://localhost/laravel_project-master/13/topics/create', 'unknown', 'http://localhost/laravel_project-master/13/topics/create', '0.20016313', '2018-12-21', '13:15:09', '2018-12-21 11:15:09', '2018-12-21 11:15:09'),
(770, 627, '::1', 'http://localhost/laravel_project-master/14/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/14/topics?_pjax=%23view', '0.19765282', '2018-12-21', '13:15:17', '2018-12-21 11:15:17', '2018-12-21 11:15:17'),
(771, 627, '::1', 'http://localhost/laravel_project-master/17/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/17/topics?_pjax=%23view', '0.19031382', '2018-12-21', '13:15:19', '2018-12-21 11:15:19', '2018-12-21 11:15:19'),
(772, 627, '::1', 'http://localhost/laravel_project-master/16/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/16/topics?_pjax=%23view', '0.19658399', '2018-12-21', '13:15:21', '2018-12-21 11:15:21', '2018-12-21 11:15:21'),
(773, 627, '::1', 'http://localhost/laravel_project-master/17/topics/67/edit', 'unknown', 'http://localhost/laravel_project-master/17/topics/67/edit', '0.38571787', '2018-12-21', '13:16:38', '2018-12-21 11:16:38', '2018-12-21 11:16:38'),
(774, 627, '::1', 'http://localhost/laravel_project-master/17/topics/68/edit', 'unknown', 'http://localhost/laravel_project-master/17/topics/68/edit', '0.25653911', '2018-12-21', '13:17:04', '2018-12-21 11:17:04', '2018-12-21 11:17:04'),
(775, 627, '::1', 'http://localhost/laravel_project-master/18/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/18/topics?_pjax=%23view', '0.20684409', '2018-12-21', '13:17:06', '2018-12-21 11:17:06', '2018-12-21 11:17:06'),
(776, 627, '::1', 'http://localhost/laravel_project-master/18/topics/create', 'unknown', 'http://localhost/laravel_project-master/18/topics/create', '0.19744301', '2018-12-21', '13:17:08', '2018-12-21 11:17:08', '2018-12-21 11:17:08'),
(777, 627, '::1', 'http://localhost/laravel_project-master/18/topics/69/edit', 'unknown', 'http://localhost/laravel_project-master/18/topics/69/edit', '0.22890401', '2018-12-21', '13:18:16', '2018-12-21 11:18:16', '2018-12-21 11:18:16'),
(778, 627, '::1', 'http://localhost/laravel_project-master/19/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/19/topics?_pjax=%23view', '0.1894331', '2018-12-21', '13:18:22', '2018-12-21 11:18:22', '2018-12-21 11:18:22'),
(779, 627, '::1', 'http://localhost/laravel_project-master/19/topics/create', 'unknown', 'http://localhost/laravel_project-master/19/topics/create', '0.18940592', '2018-12-21', '13:18:26', '2018-12-21 11:18:26', '2018-12-21 11:18:26'),
(780, 627, '::1', 'http://localhost/laravel_project-master/19/topics/70/edit', 'unknown', 'http://localhost/laravel_project-master/19/topics/70/edit', '0.24330997', '2018-12-21', '13:19:17', '2018-12-21 11:19:17', '2018-12-21 11:19:17'),
(781, 627, '::1', 'http://localhost/laravel_project-master/20/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/20/topics?_pjax=%23view', '0.19568515', '2018-12-21', '13:19:20', '2018-12-21 11:19:20', '2018-12-21 11:19:20'),
(782, 627, '::1', 'http://localhost/laravel_project-master/20/topics/create', 'unknown', 'http://localhost/laravel_project-master/20/topics/create', '0.18875504', '2018-12-21', '13:19:22', '2018-12-21 11:19:22', '2018-12-21 11:19:22'),
(783, 627, '::1', 'http://localhost/laravel_project-master/20/topics/71/edit', 'unknown', 'http://localhost/laravel_project-master/20/topics/71/edit', '0.22987604', '2018-12-21', '13:20:00', '2018-12-21 11:20:00', '2018-12-21 11:20:00'),
(784, 627, '::1', 'http://localhost/laravel_project-master/21/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/21/topics?_pjax=%23view', '0.19228506', '2018-12-21', '13:20:04', '2018-12-21 11:20:04', '2018-12-21 11:20:04'),
(785, 627, '::1', 'http://localhost/laravel_project-master/21/topics/create', 'unknown', 'http://localhost/laravel_project-master/21/topics/create', '0.19061613', '2018-12-21', '13:20:13', '2018-12-21 11:20:13', '2018-12-21 11:20:13'),
(786, 627, '::1', 'http://localhost/laravel_project-master/21/topics/72/edit', 'unknown', 'http://localhost/laravel_project-master/21/topics/72/edit', '0.22434497', '2018-12-21', '13:20:42', '2018-12-21 11:20:42', '2018-12-21 11:20:42'),
(787, 627, '::1', 'http://localhost/laravel_project-master/22/topics/create', 'unknown', 'http://localhost/laravel_project-master/22/topics/create', '0.20729804', '2018-12-21', '13:20:54', '2018-12-21 11:20:54', '2018-12-21 11:20:54'),
(788, 627, '::1', 'http://localhost/laravel_project-master/22/topics/73/edit', 'unknown', 'http://localhost/laravel_project-master/22/topics/73/edit', '0.22769499', '2018-12-21', '13:21:13', '2018-12-21 11:21:13', '2018-12-21 11:21:13'),
(789, 627, '::1', 'http://localhost/laravel_project-master/23/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/23/topics?_pjax=%23view', '0.19109082', '2018-12-21', '13:21:17', '2018-12-21 11:21:17', '2018-12-21 11:21:17'),
(790, 627, '::1', 'http://localhost/laravel_project-master/23/topics/create', 'unknown', 'http://localhost/laravel_project-master/23/topics/create', '0.22305584', '2018-12-21', '13:21:20', '2018-12-21 11:21:20', '2018-12-21 11:21:20'),
(791, 627, '::1', 'http://localhost/laravel_project-master/16/topics', 'unknown', 'http://localhost/laravel_project-master/16/topics', '0.33764791', '2018-12-21', '13:25:27', '2018-12-21 11:25:27', '2018-12-21 11:25:27'),
(792, 627, '::1', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', '0.4093101', '2018-12-21', '13:30:34', '2018-12-21 11:30:34', '2018-12-21 11:30:34'),
(793, 627, '::1', 'http://localhost/laravel_project-master/23/topics', 'unknown', 'http://localhost/laravel_project-master/23/topics', '0.2248888', '2018-12-21', '13:35:03', '2018-12-21 11:35:03', '2018-12-21 11:35:03'),
(794, 627, '::1', 'http://localhost/laravel_project-master/24/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/24/topics?_pjax=%23view', '0.1961081', '2018-12-21', '13:35:04', '2018-12-21 11:35:04', '2018-12-21 11:35:04'),
(795, 627, '::1', 'http://localhost/laravel_project-master/24/topics/create', 'unknown', 'http://localhost/laravel_project-master/24/topics/create', '0.20387101', '2018-12-21', '13:35:06', '2018-12-21 11:35:06', '2018-12-21 11:35:06'),
(796, 627, '::1', 'http://localhost/laravel_project-master/24/topics/74/edit', 'unknown', 'http://localhost/laravel_project-master/24/topics/74/edit', '0.34013796', '2018-12-21', '13:35:16', '2018-12-21 11:35:16', '2018-12-21 11:35:16'),
(797, 627, '::1', 'http://localhost/laravel_project-master/23/topics/75/edit', 'unknown', 'http://localhost/laravel_project-master/23/topics/75/edit', '0.22776794', '2018-12-21', '13:35:35', '2018-12-21 11:35:35', '2018-12-21 11:35:35'),
(798, 627, '::1', 'http://localhost/laravel_project-master/8/topics/87/edit', 'unknown', 'http://localhost/laravel_project-master/8/topics/87/edit', '4.08129597', '2018-12-21', '14:02:12', '2018-12-21 12:02:12', '2018-12-21 12:02:12'),
(799, 627, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_3.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_3.png', '1.21619415', '2018-12-21', '14:02:19', '2018-12-21 12:02:19', '2018-12-21 12:02:19'),
(800, 627, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_0.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_0.png', '2.12631583', '2018-12-21', '14:02:21', '2018-12-21 12:02:21', '2018-12-21 12:02:21'),
(801, 627, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_1.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_1.png', '2.77724695', '2018-12-21', '14:02:21', '2018-12-21 12:02:21', '2018-12-21 12:02:21');
INSERT INTO `analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(802, 627, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_6.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_6.png', '0.16634607', '2018-12-21', '14:02:24', '2018-12-21 12:02:24', '2018-12-21 12:02:24'),
(803, 627, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_2.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_2.png', '8.83925796', '2018-12-21', '14:02:27', '2018-12-21 12:02:27', '2018-12-21 12:02:27'),
(804, 627, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_4.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_4.png', '9.26020098', '2018-12-21', '14:02:28', '2018-12-21 12:02:28', '2018-12-21 12:02:28'),
(805, 627, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_5.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_5.png', '14.48966408', '2018-12-21', '14:02:33', '2018-12-21 12:02:33', '2018-12-21 12:02:33'),
(806, 627, '::1', 'http://localhost/laravel_project-master/webmaster/sections/8/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/8/edit', '0.41851711', '2018-12-21', '14:10:35', '2018-12-21 12:10:35', '2018-12-21 12:10:35'),
(807, 627, '::1', 'http://localhost/laravel_project-master/webmaster/sections/25/edit?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/25/edit?_pjax=%23view', '0.25001812', '2018-12-21', '14:11:50', '2018-12-21 12:11:50', '2018-12-21 12:11:50'),
(808, 627, '::1', 'http://localhost/laravel_project-master/webmaster/sections', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections', '0.20540214', '2018-12-21', '14:13:18', '2018-12-21 12:13:18', '2018-12-21 12:13:18'),
(809, 627, '::1', 'http://localhost/laravel_project-master/webmaster/sections/create', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/create', '0.18130112', '2018-12-21', '14:13:29', '2018-12-21 12:13:29', '2018-12-21 12:13:29'),
(810, 627, '::1', 'http://localhost/laravel_project-master/26/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/26/topics?_pjax=%23view', '0.22587299', '2018-12-21', '14:14:24', '2018-12-21 12:14:24', '2018-12-21 12:14:24'),
(811, 627, '::1', 'http://localhost/laravel_project-master/26/topics/create', 'unknown', 'http://localhost/laravel_project-master/26/topics/create', '0.2252419', '2018-12-21', '14:14:33', '2018-12-21 12:14:33', '2018-12-21 12:14:33'),
(812, 627, '::1', 'http://localhost/laravel_project-master/webmaster/sections?page=2', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?page=2', '0.18524289', '2018-12-21', '14:16:27', '2018-12-21 12:16:27', '2018-12-21 12:16:27'),
(813, 627, '::1', 'http://localhost/laravel_project-master/webmaster/sections/26/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/26/edit', '0.23186302', '2018-12-21', '14:16:32', '2018-12-21 12:16:32', '2018-12-21 12:16:32'),
(814, 627, '::1', 'http://localhost/laravel_project-master/24/topics', 'unknown', 'http://localhost/laravel_project-master/24/topics', '1.19015503', '2018-12-21', '14:21:01', '2018-12-21 12:21:01', '2018-12-21 12:21:01'),
(815, 627, '::1', 'http://localhost/laravel_project-master/26/topics', 'unknown', 'http://localhost/laravel_project-master/26/topics', '0.21235514', '2018-12-21', '14:21:26', '2018-12-21 12:21:26', '2018-12-21 12:21:26'),
(816, 627, '::1', 'http://localhost/laravel_project-master/26/topics/88/edit', 'unknown', 'http://localhost/laravel_project-master/26/topics/88/edit', '0.29785204', '2018-12-21', '14:23:32', '2018-12-21 12:23:32', '2018-12-21 12:23:32'),
(817, 628, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '0.53955793', '2018-12-22', '05:04:50', '2018-12-22 03:04:50', '2018-12-22 03:04:50'),
(818, 628, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '1.24104619', '2018-12-22', '05:05:13', '2018-12-22 03:05:13', '2018-12-22 03:05:13'),
(819, 628, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '0.81215119', '2018-12-22', '05:05:16', '2018-12-22 03:05:16', '2018-12-22 03:05:16'),
(820, 628, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.21450591', '2018-12-22', '05:05:17', '2018-12-22 03:05:17', '2018-12-22 03:05:17'),
(821, 628, '::1', 'http://localhost/laravel_project-master/26/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/26/topics?_pjax=%23view', '0.33038306', '2018-12-22', '05:05:54', '2018-12-22 03:05:54', '2018-12-22 03:05:54'),
(822, 628, '::1', 'http://localhost/laravel_project-master/26/topics/create', 'unknown', 'http://localhost/laravel_project-master/26/topics/create', '0.26998377', '2018-12-22', '05:05:56', '2018-12-22 03:05:56', '2018-12-22 03:05:56'),
(823, 628, '::1', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.16379094', '2018-12-22', '05:06:03', '2018-12-22 03:06:03', '2018-12-22 03:06:03'),
(824, 628, '::1', 'http://localhost/laravel_project-master/15/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/15/topics?_pjax=%23view', '0.42696118', '2018-12-22', '06:07:54', '2018-12-22 04:07:54', '2018-12-22 04:07:54'),
(825, 628, '::1', 'http://localhost/laravel_project-master/15/topics/create', 'unknown', 'http://localhost/laravel_project-master/15/topics/create', '0.17867184', '2018-12-22', '06:07:59', '2018-12-22 04:07:59', '2018-12-22 04:07:59'),
(826, 628, '::1', 'http://localhost/laravel_project-master/15/topics/89/edit', 'unknown', 'http://localhost/laravel_project-master/15/topics/89/edit', '0.54503107', '2018-12-22', '06:08:36', '2018-12-22 04:08:36', '2018-12-22 04:08:36'),
(827, 628, '::1', 'http://localhost/laravel_project-master/26/topics/88/edit', 'unknown', 'http://localhost/laravel_project-master/26/topics/88/edit', '0.48543191', '2018-12-22', '06:45:21', '2018-12-22 04:45:21', '2018-12-22 04:45:21'),
(828, 628, '::1', 'http://localhost/laravel_project-master/20/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/20/topics?_pjax=%23view', '0.22955704', '2018-12-22', '07:28:06', '2018-12-22 05:28:06', '2018-12-22 05:28:06'),
(829, 628, '::1', 'http://localhost/laravel_project-master/404', 'unknown', 'http://localhost/laravel_project-master/404', '0.14681911', '2018-12-22', '07:35:45', '2018-12-22 05:35:45', '2018-12-22 05:35:45'),
(830, 628, '::1', 'http://localhost/laravel_project-master/26/topics', 'unknown', 'http://localhost/laravel_project-master/26/topics', '0.19233203', '2018-12-22', '07:36:15', '2018-12-22 05:36:15', '2018-12-22 05:36:15'),
(831, 628, '::1', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', '0.23297', '2018-12-22', '09:29:12', '2018-12-22 07:29:12', '2018-12-22 07:29:12'),
(832, 628, '::1', 'http://localhost/laravel_project-master/11/topics/create', 'unknown', 'http://localhost/laravel_project-master/11/topics/create', '0.31548285', '2018-12-22', '09:29:14', '2018-12-22 07:29:14', '2018-12-22 07:29:14'),
(833, 629, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '2.95652103', '2018-12-24', '19:10:23', '2018-12-24 17:10:23', '2018-12-24 17:10:23'),
(834, 629, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '0.99731994', '2018-12-24', '19:10:40', '2018-12-24 17:10:40', '2018-12-24 17:10:40'),
(835, 629, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.23102713', '2018-12-24', '19:10:40', '2018-12-24 17:10:40', '2018-12-24 17:10:40'),
(836, 629, '::1', 'http://localhost/laravel_project-master/26/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/26/topics?_pjax=%23view', '0.37687492', '2018-12-24', '19:10:51', '2018-12-24 17:10:51', '2018-12-24 17:10:51'),
(837, 629, '::1', 'http://localhost/laravel_project-master/26/topics/create', 'unknown', 'http://localhost/laravel_project-master/26/topics/create', '0.41756701', '2018-12-24', '19:10:53', '2018-12-24 17:10:53', '2018-12-24 17:10:53'),
(838, 630, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/', '9.06751895', '2019-01-04', '04:41:11', '2019-01-04 02:41:11', '2019-01-04 02:41:11'),
(839, 630, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '0.51839113', '2019-01-04', '04:42:01', '2019-01-04 02:42:01', '2019-01-04 02:42:01'),
(840, 630, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '0.83299279', '2019-01-04', '04:42:37', '2019-01-04 02:42:37', '2019-01-04 02:42:37'),
(841, 630, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.27269602', '2019-01-04', '04:42:37', '2019-01-04 02:42:37', '2019-01-04 02:42:37'),
(842, 630, '::1', 'http://localhost/laravel_project-master/21/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/21/topics?_pjax=%23view', '0.30232882', '2019-01-04', '04:46:51', '2019-01-04 02:46:51', '2019-01-04 02:46:51'),
(843, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', '0.23607993', '2019-01-04', '04:47:01', '2019-01-04 02:47:01', '2019-01-04 02:47:01'),
(844, 630, '::1', 'http://localhost/laravel_project-master/settings', 'unknown', 'http://localhost/laravel_project-master/settings', '0.30109596', '2019-01-04', '04:47:12', '2019-01-04 02:47:12', '2019-01-04 02:47:12'),
(845, 630, '::1', 'http://localhost/laravel_project-master/settings?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/settings?_pjax=%23view', '0.33407712', '2019-01-04', '04:47:12', '2019-01-04 02:47:12', '2019-01-04 02:47:12'),
(846, 630, '::1', 'http://localhost/laravel_project-master/13/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/13/topics?_pjax=%23view', '0.30864596', '2019-01-04', '04:58:25', '2019-01-04 02:58:25', '2019-01-04 02:58:25'),
(847, 630, '::1', 'http://localhost/laravel_project-master/16/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/16/topics?_pjax=%23view', '0.22609496', '2019-01-04', '04:58:49', '2019-01-04 02:58:49', '2019-01-04 02:58:49'),
(848, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/create', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/create', '0.47611785', '2019-01-04', '04:59:00', '2019-01-04 02:59:00', '2019-01-04 02:59:00'),
(849, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections', '0.23576689', '2019-01-04', '04:59:41', '2019-01-04 02:59:41', '2019-01-04 02:59:41'),
(850, 630, '::1', 'http://localhost/laravel_project-master/19/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/19/topics?_pjax=%23view', '0.21135807', '2019-01-04', '04:59:43', '2019-01-04 02:59:43', '2019-01-04 02:59:43'),
(851, 630, '::1', 'http://localhost/laravel_project-master/27/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/27/topics?_pjax=%23view', '0.19421911', '2019-01-04', '04:59:47', '2019-01-04 02:59:47', '2019-01-04 02:59:47'),
(852, 630, '::1', 'http://localhost/laravel_project-master/27/topics', 'unknown', 'http://localhost/laravel_project-master/27/topics', '0.23814392', '2019-01-04', '05:01:03', '2019-01-04 03:01:03', '2019-01-04 03:01:03'),
(853, 630, '::1', 'http://localhost/laravel_project-master/27/topics/create', 'unknown', 'http://localhost/laravel_project-master/27/topics/create', '0.43073583', '2019-01-04', '05:01:56', '2019-01-04 03:01:56', '2019-01-04 03:01:56'),
(854, 630, '::1', 'http://localhost/laravel_project-master/27/topics/90/edit', 'unknown', 'http://localhost/laravel_project-master/27/topics/90/edit', '0.56200314', '2019-01-04', '05:08:13', '2019-01-04 03:08:13', '2019-01-04 03:08:13'),
(855, 630, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_1.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_1.png', '0.62364697', '2019-01-04', '05:08:13', '2019-01-04 03:08:13', '2019-01-04 03:08:13'),
(856, 630, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_4.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_4.png', '0.59577918', '2019-01-04', '05:08:13', '2019-01-04 03:08:13', '2019-01-04 03:08:13'),
(857, 630, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_2.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_2.png', '0.62808418', '2019-01-04', '05:08:13', '2019-01-04 03:08:13', '2019-01-04 03:08:13'),
(858, 630, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_5.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_5.png', '0.61649084', '2019-01-04', '05:08:13', '2019-01-04 03:08:13', '2019-01-04 03:08:13'),
(859, 630, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_3.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_3.png', '0.60919905', '2019-01-04', '05:08:13', '2019-01-04 03:08:13', '2019-01-04 03:08:13'),
(860, 630, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_0.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_0.png', '0.65446615', '2019-01-04', '05:08:13', '2019-01-04 03:08:13', '2019-01-04 03:08:13'),
(861, 630, '::1', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_6.png', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/images/marker_6.png', '0.18214178', '2019-01-04', '05:08:14', '2019-01-04 03:08:14', '2019-01-04 03:08:14'),
(862, 630, '::1', 'http://localhost/laravel_project-master/24/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/24/topics?_pjax=%23view', '0.62515187', '2019-01-04', '05:08:32', '2019-01-04 03:08:32', '2019-01-04 03:08:32'),
(863, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections?page=2', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?page=2', '0.20147705', '2019-01-04', '05:09:45', '2019-01-04 03:09:45', '2019-01-04 03:09:45'),
(864, 630, '::1', 'http://localhost/laravel_project-master/23/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/23/topics?_pjax=%23view', '0.23648882', '2019-01-04', '05:09:58', '2019-01-04 03:09:58', '2019-01-04 03:09:58'),
(865, 630, '::1', 'http://localhost/laravel_project-master/users?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/users?_pjax=%23view', '0.4633069', '2019-01-04', '05:10:05', '2019-01-04 03:10:05', '2019-01-04 03:10:05'),
(866, 630, '::1', 'http://localhost/laravel_project-master/users/permissions/2/edit', 'unknown', 'http://localhost/laravel_project-master/users/permissions/2/edit', '0.30718493', '2019-01-04', '05:10:13', '2019-01-04 03:10:13', '2019-01-04 03:10:13'),
(867, 630, '::1', 'http://localhost/laravel_project-master/users/create', 'unknown', 'http://localhost/laravel_project-master/users/create', '0.25626421', '2019-01-04', '05:11:42', '2019-01-04 03:11:42', '2019-01-04 03:11:42'),
(868, 630, '::1', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', '0.36186409', '2019-01-04', '19:33:16', '2019-01-04 17:33:16', '2019-01-04 17:33:16'),
(869, 630, '::1', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/11/topics?_pjax=%23view', '0.2142148', '2019-01-04', '19:33:16', '2019-01-04 17:33:16', '2019-01-04 17:33:16'),
(870, 630, '::1', 'http://localhost/laravel_project-master/menus?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/menus?_pjax=%23view', '0.63873816', '2019-01-04', '19:33:36', '2019-01-04 17:33:36', '2019-01-04 17:33:36'),
(871, 630, '::1', 'http://localhost/laravel_project-master/menus/3/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/3/edit/1', '0.31599116', '2019-01-04', '19:33:42', '2019-01-04 17:33:42', '2019-01-04 17:33:42'),
(872, 630, '::1', 'http://localhost/laravel_project-master/menus/1?id=3', 'unknown', 'http://localhost/laravel_project-master/menus/1?id=3', '0.19804406', '2019-01-04', '19:34:32', '2019-01-04 17:34:32', '2019-01-04 17:34:32'),
(873, 630, '::1', 'http://localhost/laravel_project-master/menus/4/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/4/edit/1', '0.20017886', '2019-01-04', '19:34:42', '2019-01-04 17:34:42', '2019-01-04 17:34:42'),
(874, 630, '::1', 'http://localhost/laravel_project-master/menus/1', 'unknown', 'http://localhost/laravel_project-master/menus/1', '0.20631719', '2019-01-04', '19:34:46', '2019-01-04 17:34:46', '2019-01-04 17:34:46'),
(875, 630, '::1', 'http://localhost/laravel_project-master/menus/5/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/5/edit/1', '0.19226408', '2019-01-04', '19:34:49', '2019-01-04 17:34:49', '2019-01-04 17:34:49'),
(876, 630, '::1', 'http://localhost/laravel_project-master/menus/1?id=5', 'unknown', 'http://localhost/laravel_project-master/menus/1?id=5', '0.21710086', '2019-01-04', '19:34:52', '2019-01-04 17:34:52', '2019-01-04 17:34:52'),
(877, 630, '::1', 'http://localhost/laravel_project-master/menus/11/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/11/edit/1', '0.19937301', '2019-01-04', '19:34:59', '2019-01-04 17:34:59', '2019-01-04 17:34:59'),
(878, 630, '::1', 'http://localhost/laravel_project-master/menus/1?id=11', 'unknown', 'http://localhost/laravel_project-master/menus/1?id=11', '0.20794487', '2019-01-04', '19:35:03', '2019-01-04 17:35:03', '2019-01-04 17:35:03'),
(879, 630, '::1', 'http://localhost/laravel_project-master/menus/6/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/6/edit/1', '0.18855286', '2019-01-04', '19:35:05', '2019-01-04 17:35:05', '2019-01-04 17:35:05'),
(880, 630, '::1', 'http://localhost/laravel_project-master/menus/1?id=6', 'unknown', 'http://localhost/laravel_project-master/menus/1?id=6', '0.20688295', '2019-01-04', '19:35:08', '2019-01-04 17:35:08', '2019-01-04 17:35:08'),
(881, 630, '::1', 'http://localhost/laravel_project-master/menus/7/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/7/edit/1', '0.22604299', '2019-01-04', '19:35:11', '2019-01-04 17:35:11', '2019-01-04 17:35:11'),
(882, 630, '::1', 'http://localhost/laravel_project-master/menus/1?id=7', 'unknown', 'http://localhost/laravel_project-master/menus/1?id=7', '0.23707199', '2019-01-04', '19:35:15', '2019-01-04 17:35:15', '2019-01-04 17:35:15'),
(883, 630, '::1', 'http://localhost/laravel_project-master/menus/8/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/8/edit/1', '0.18669319', '2019-01-04', '19:35:19', '2019-01-04 17:35:19', '2019-01-04 17:35:19'),
(884, 630, '::1', 'http://localhost/laravel_project-master/menus/1?id=8', 'unknown', 'http://localhost/laravel_project-master/menus/1?id=8', '0.21251893', '2019-01-04', '19:35:22', '2019-01-04 17:35:22', '2019-01-04 17:35:22'),
(885, 630, '::1', 'http://localhost/laravel_project-master/menus/9/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/9/edit/1', '0.18593788', '2019-01-04', '19:35:28', '2019-01-04 17:35:28', '2019-01-04 17:35:28'),
(886, 630, '::1', 'http://localhost/laravel_project-master/menus/1?id=9', 'unknown', 'http://localhost/laravel_project-master/menus/1?id=9', '0.23258281', '2019-01-04', '19:35:30', '2019-01-04 17:35:30', '2019-01-04 17:35:30'),
(887, 630, '::1', 'http://localhost/laravel_project-master/menus/10/edit/1', 'unknown', 'http://localhost/laravel_project-master/menus/10/edit/1', '0.18671107', '2019-01-04', '19:35:34', '2019-01-04 17:35:34', '2019-01-04 17:35:34'),
(888, 630, '::1', 'http://localhost/laravel_project-master/menus/1?id=10', 'unknown', 'http://localhost/laravel_project-master/menus/1?id=10', '0.189466', '2019-01-04', '19:35:37', '2019-01-04 17:35:37', '2019-01-04 17:35:37'),
(889, 630, '::1', 'http://localhost/laravel_project-master/24/topics', 'unknown', 'http://localhost/laravel_project-master/24/topics', '0.19945812', '2019-01-04', '19:37:44', '2019-01-04 17:37:44', '2019-01-04 17:37:44'),
(890, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/28/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/28/edit', '0.50976205', '2019-01-04', '19:40:21', '2019-01-04 17:40:21', '2019-01-04 17:40:21'),
(891, 630, '::1', 'http://localhost/laravel_project-master/11/topics/create', 'unknown', 'http://localhost/laravel_project-master/11/topics/create', '0.34172511', '2019-01-04', '19:41:12', '2019-01-04 17:41:12', '2019-01-04 17:41:12'),
(892, 630, '::1', 'http://localhost/laravel_project-master/18/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/18/topics?_pjax=%23view', '0.22391009', '2019-01-04', '19:41:16', '2019-01-04 17:41:16', '2019-01-04 17:41:16'),
(893, 630, '::1', 'http://localhost/laravel_project-master/28/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/28/topics?_pjax=%23view', '0.19403815', '2019-01-04', '19:41:19', '2019-01-04 17:41:19', '2019-01-04 17:41:19'),
(894, 630, '::1', 'http://localhost/laravel_project-master/28/topics/create', 'unknown', 'http://localhost/laravel_project-master/28/topics/create', '0.18992591', '2019-01-04', '19:41:21', '2019-01-04 17:41:21', '2019-01-04 17:41:21'),
(895, 630, '::1', 'http://localhost/laravel_project-master/28/topics/91/edit', 'unknown', 'http://localhost/laravel_project-master/28/topics/91/edit', '0.495327', '2019-01-04', '19:41:39', '2019-01-04 17:41:39', '2019-01-04 17:41:39'),
(896, 630, '::1', 'http://localhost/laravel_project-master/17/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/17/topics?_pjax=%23view', '0.22303414', '2019-01-04', '19:41:49', '2019-01-04 17:41:49', '2019-01-04 17:41:49'),
(897, 630, '::1', 'http://localhost/laravel_project-master/17/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/17/topics?_pjax=%23view', '0.20851088', '2019-01-04', '19:41:49', '2019-01-04 17:41:49', '2019-01-04 17:41:49'),
(898, 630, '::1', 'http://localhost/laravel_project-master/28/topics', 'unknown', 'http://localhost/laravel_project-master/28/topics', '0.24433112', '2019-01-04', '19:46:56', '2019-01-04 17:46:56', '2019-01-04 17:46:56'),
(899, 630, '::1', 'http://localhost/laravel_project-master/22/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/22/topics?_pjax=%23view', '0.19326806', '2019-01-04', '19:49:12', '2019-01-04 17:49:12', '2019-01-04 17:49:12'),
(900, 630, '::1', 'http://localhost/laravel_project-master/22/topics', 'unknown', 'http://localhost/laravel_project-master/22/topics', '0.19599891', '2019-01-04', '19:49:26', '2019-01-04 17:49:26', '2019-01-04 17:49:26'),
(901, 630, '::1', 'http://localhost/laravel_project-master/14/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/14/topics?_pjax=%23view', '0.26128697', '2019-01-04', '19:50:47', '2019-01-04 17:50:47', '2019-01-04 17:50:47'),
(902, 630, '::1', 'http://localhost/laravel_project-master/14/topics', 'unknown', 'http://localhost/laravel_project-master/14/topics', '0.19958615', '2019-01-04', '19:50:57', '2019-01-04 17:50:57', '2019-01-04 17:50:57'),
(903, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/11/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/11/edit', '0.21755385', '2019-01-04', '19:51:20', '2019-01-04 17:51:20', '2019-01-04 17:51:20'),
(904, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/12/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/12/edit', '0.21366596', '2019-01-04', '19:51:49', '2019-01-04 17:51:49', '2019-01-04 17:51:49'),
(905, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/13/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/13/edit', '0.21873713', '2019-01-04', '19:52:44', '2019-01-04 17:52:44', '2019-01-04 17:52:44'),
(906, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/14/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/14/edit', '0.21914196', '2019-01-04', '19:52:58', '2019-01-04 17:52:58', '2019-01-04 17:52:58'),
(907, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/15/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/15/edit', '0.21926594', '2019-01-04', '19:53:14', '2019-01-04 17:53:14', '2019-01-04 17:53:14'),
(908, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/16/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/16/edit', '0.25631809', '2019-01-04', '19:53:33', '2019-01-04 17:53:33', '2019-01-04 17:53:33'),
(909, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/17/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/17/edit', '0.22013402', '2019-01-04', '19:53:50', '2019-01-04 17:53:50', '2019-01-04 17:53:50'),
(910, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/18/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/18/edit', '0.21867609', '2019-01-04', '19:54:09', '2019-01-04 17:54:09', '2019-01-04 17:54:09'),
(911, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/19/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/19/edit', '0.29246283', '2019-01-04', '19:54:27', '2019-01-04 17:54:27', '2019-01-04 17:54:27'),
(912, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/20/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/20/edit', '0.24109101', '2019-01-04', '19:54:48', '2019-01-04 17:54:48', '2019-01-04 17:54:48'),
(913, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/21/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/21/edit', '0.2442441', '2019-01-04', '19:55:22', '2019-01-04 17:55:22', '2019-01-04 17:55:22'),
(914, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/22/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/22/edit', '0.24342012', '2019-01-04', '19:55:39', '2019-01-04 17:55:39', '2019-01-04 17:55:39'),
(915, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/27/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/27/edit', '0.22074795', '2019-01-04', '19:56:04', '2019-01-04 17:56:04', '2019-01-04 17:56:04'),
(916, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/23/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/23/edit', '0.21707702', '2019-01-04', '19:56:45', '2019-01-04 17:56:45', '2019-01-04 17:56:45'),
(917, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/24/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/24/edit', '0.21544385', '2019-01-04', '19:57:01', '2019-01-04 17:57:01', '2019-01-04 17:57:01'),
(918, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/26/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/26/edit', '0.21869802', '2019-01-04', '19:57:25', '2019-01-04 17:57:25', '2019-01-04 17:57:25'),
(919, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/30/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/30/edit', '0.24092507', '2019-01-04', '20:07:37', '2019-01-04 18:07:37', '2019-01-04 18:07:37'),
(920, 630, '::1', 'http://localhost/laravel_project-master/29/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/29/topics?_pjax=%23view', '0.18511701', '2019-01-04', '20:09:02', '2019-01-04 18:09:02', '2019-01-04 18:09:02'),
(921, 630, '::1', 'http://localhost/laravel_project-master/30/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/30/topics?_pjax=%23view', '0.18727398', '2019-01-04', '20:09:03', '2019-01-04 18:09:03', '2019-01-04 18:09:03'),
(922, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/4/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/4/edit', '0.24432802', '2019-01-04', '20:09:39', '2019-01-04 18:09:39', '2019-01-04 18:09:39'),
(923, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections?page=1', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?page=1', '0.17885208', '2019-01-04', '20:10:35', '2019-01-04 18:10:35', '2019-01-04 18:10:35'),
(924, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections?page=2&_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?page=2&_pjax=%23view', '0.25003695', '2019-01-04', '20:15:35', '2019-01-04 18:15:35', '2019-01-04 18:15:35'),
(925, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/29/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/29/edit', '0.22397614', '2019-01-04', '20:15:42', '2019-01-04 18:15:42', '2019-01-04 18:15:42'),
(926, 630, '::1', 'http://localhost/laravel_project-master/webmaster/sections/31/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/31/edit', '0.21414495', '2019-01-04', '20:16:30', '2019-01-04 18:16:30', '2019-01-04 18:16:30'),
(927, 630, '::1', 'http://localhost/laravel_project-master/webmaster/banners?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/banners?_pjax=%23view', '0.31573796', '2019-01-04', '20:17:35', '2019-01-04 18:17:35', '2019-01-04 18:17:35'),
(928, 630, '::1', 'http://localhost/laravel_project-master/31/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/31/topics?_pjax=%23view', '0.18527699', '2019-01-04', '20:18:06', '2019-01-04 18:18:06', '2019-01-04 18:18:06'),
(929, 630, '::1', 'http://localhost/laravel_project-master/28/topics/93/edit', 'unknown', 'http://localhost/laravel_project-master/28/topics/93/edit', '0.27675509', '2019-01-04', '20:26:33', '2019-01-04 18:26:33', '2019-01-04 18:26:33'),
(930, 630, '::1', 'http://localhost/laravel_project-master/28/topics/94/edit', 'unknown', 'http://localhost/laravel_project-master/28/topics/94/edit', '0.26542711', '2019-01-04', '20:28:09', '2019-01-04 18:28:09', '2019-01-04 18:28:09'),
(931, 630, '::1', 'http://localhost/laravel_project-master/28/topics/92/edit', 'unknown', 'http://localhost/laravel_project-master/28/topics/92/edit', '0.23428488', '2019-01-04', '20:28:24', '2019-01-04 18:28:24', '2019-01-04 18:28:24'),
(932, 630, '::1', 'http://localhost/laravel_project-master/29/topics/create', 'unknown', 'http://localhost/laravel_project-master/29/topics/create', '0.20769501', '2019-01-04', '20:29:59', '2019-01-04 18:29:59', '2019-01-04 18:29:59'),
(933, 630, '::1', 'http://localhost/laravel_project-master/29/topics/95/edit', 'unknown', 'http://localhost/laravel_project-master/29/topics/95/edit', '0.25955009', '2019-01-04', '20:31:24', '2019-01-04 18:31:24', '2019-01-04 18:31:24'),
(934, 630, '::1', 'http://localhost/laravel_project-master/29/topics/96/edit', 'unknown', 'http://localhost/laravel_project-master/29/topics/96/edit', '0.23320103', '2019-01-04', '20:33:06', '2019-01-04 18:33:06', '2019-01-04 18:33:06'),
(935, 630, '::1', 'http://localhost/laravel_project-master/30/topics/create', 'unknown', 'http://localhost/laravel_project-master/30/topics/create', '0.2526269', '2019-01-04', '20:33:39', '2019-01-04 18:33:39', '2019-01-04 18:33:39'),
(936, 630, '::1', 'http://localhost/laravel_project-master/30/topics/97/edit', 'unknown', 'http://localhost/laravel_project-master/30/topics/97/edit', '0.24996591', '2019-01-04', '20:34:48', '2019-01-04 18:34:48', '2019-01-04 18:34:48'),
(937, 630, '::1', 'http://localhost/laravel_project-master/30/topics/98/edit', 'unknown', 'http://localhost/laravel_project-master/30/topics/98/edit', '0.244241', '2019-01-04', '20:36:29', '2019-01-04 18:36:29', '2019-01-04 18:36:29'),
(938, 630, '::1', 'http://localhost/laravel_project-master/31/topics/create', 'unknown', 'http://localhost/laravel_project-master/31/topics/create', '0.1871109', '2019-01-04', '20:37:19', '2019-01-04 18:37:19', '2019-01-04 18:37:19'),
(939, 630, '::1', 'http://localhost/laravel_project-master/31/topics/99/edit', 'unknown', 'http://localhost/laravel_project-master/31/topics/99/edit', '0.22629213', '2019-01-04', '20:38:27', '2019-01-04 18:38:27', '2019-01-04 18:38:27'),
(940, 630, '::1', 'http://localhost/laravel_project-master/31/topics/100/edit', 'unknown', 'http://localhost/laravel_project-master/31/topics/100/edit', '0.24014616', '2019-01-04', '20:39:59', '2019-01-04 18:39:59', '2019-01-04 18:39:59'),
(941, 630, '::1', 'http://localhost/laravel_project-master/31/topics/101/edit', 'unknown', 'http://localhost/laravel_project-master/31/topics/101/edit', '0.22393394', '2019-01-04', '20:41:49', '2019-01-04 18:41:49', '2019-01-04 18:41:49'),
(942, 630, '::1', 'http://localhost/laravel_project-master/4/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/4/topics?_pjax=%23view', '0.23345208', '2019-01-04', '20:43:16', '2019-01-04 18:43:16', '2019-01-04 18:43:16'),
(943, 630, '::1', 'http://localhost/laravel_project-master/4/topics/23/edit', 'unknown', 'http://localhost/laravel_project-master/4/topics/23/edit', '0.23881316', '2019-01-04', '20:43:20', '2019-01-04 18:43:20', '2019-01-04 18:43:20'),
(944, 630, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '0.76653504', '2019-01-04', '20:43:22', '2019-01-04 18:43:22', '2019-01-04 18:43:22'),
(945, 630, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '0.73609304', '2019-01-04', '20:43:22', '2019-01-04 18:43:22', '2019-01-04 18:43:22'),
(946, 630, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '0.77153301', '2019-01-04', '20:43:22', '2019-01-04 18:43:22', '2019-01-04 18:43:22'),
(947, 630, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '0.74099207', '2019-01-04', '20:43:22', '2019-01-04 18:43:22', '2019-01-04 18:43:22'),
(948, 630, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '0.76761484', '2019-01-04', '20:43:22', '2019-01-04 18:43:22', '2019-01-04 18:43:22'),
(949, 630, '::1', 'Charity Challengers', 'unknown', 'http://localhost/laravel_project-master/home', '0.76760507', '2019-01-04', '20:43:22', '2019-01-04 18:43:22', '2019-01-04 18:43:22'),
(950, 630, '::1', 'http://localhost/laravel_project-master/4/topics/24/edit', 'unknown', 'http://localhost/laravel_project-master/4/topics/24/edit', '0.23391795', '2019-01-04', '20:48:38', '2019-01-04 18:48:38', '2019-01-04 18:48:38'),
(951, 630, '::1', 'http://localhost/laravel_project-master/4/topics/25/edit', 'unknown', 'http://localhost/laravel_project-master/4/topics/25/edit', '0.23617697', '2019-01-04', '20:49:33', '2019-01-04 18:49:33', '2019-01-04 18:49:33'),
(952, 630, '::1', 'http://localhost/laravel_project-master/32/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/32/topics?_pjax=%23view', '0.19514918', '2019-01-04', '20:51:56', '2019-01-04 18:51:56', '2019-01-04 18:51:56'),
(953, 630, '::1', 'http://localhost/laravel_project-master/32/topics', 'unknown', 'http://localhost/laravel_project-master/32/topics', '0.23892403', '2019-01-04', '20:53:39', '2019-01-04 18:53:39', '2019-01-04 18:53:39'),
(954, 630, '::1', 'http://localhost/laravel_project-master/32/topics/create', 'unknown', 'http://localhost/laravel_project-master/32/topics/create', '12.35082984', '2019-01-04', '21:31:27', '2019-01-04 19:31:27', '2019-01-04 19:31:27'),
(955, 631, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '0.24273705', '2019-01-05', '11:25:22', '2019-01-05 09:25:22', '2019-01-05 09:25:22'),
(956, 631, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '1.00300407', '2019-01-05', '11:25:57', '2019-01-05 09:25:57', '2019-01-05 09:25:57'),
(957, 631, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.35968804', '2019-01-05', '11:25:57', '2019-01-05 09:25:57', '2019-01-05 09:25:57'),
(958, 631, '::1', 'http://localhost/laravel_project-master/28/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/28/topics?_pjax=%23view', '0.35835481', '2019-01-05', '11:26:17', '2019-01-05 09:26:17', '2019-01-05 09:26:17'),
(959, 631, '::1', 'http://localhost/laravel_project-master/28/topics/91/edit', 'unknown', 'http://localhost/laravel_project-master/28/topics/91/edit', '0.42102718', '2019-01-05', '11:26:20', '2019-01-05 09:26:20', '2019-01-05 09:26:20'),
(960, 631, '::1', 'http://localhost/laravel_project-master/4/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/4/topics?_pjax=%23view', '2.94500208', '2019-01-05', '11:29:49', '2019-01-05 09:29:49', '2019-01-05 09:29:49'),
(961, 631, '::1', 'http://localhost/laravel_project-master/4/topics/23/edit', 'unknown', 'http://localhost/laravel_project-master/4/topics/23/edit', '0.31955004', '2019-01-05', '11:29:54', '2019-01-05 09:29:54', '2019-01-05 09:29:54'),
(962, 631, '::1', 'http://localhost/laravel_project-master/4/topics', 'unknown', 'http://localhost/laravel_project-master/4/topics', '3.28422785', '2019-01-05', '12:01:29', '2019-01-05 10:01:29', '2019-01-05 10:01:29'),
(963, 631, '::1', 'http://localhost/laravel_project-master/assets/template/css/style.css', 'unknown', 'http://localhost/laravel_project-master/assets/template/css/style.css', '0.50553203', '2019-01-05', '12:37:19', '2019-01-05 10:37:19', '2019-01-05 10:37:19'),
(964, 631, '::1', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.16333604', '2019-01-05', '18:50:46', '2019-01-05 16:50:46', '2019-01-05 16:50:46'),
(965, 631, '::1', 'http://localhost/laravel_project-master/29/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/29/topics?_pjax=%23view', '0.23058796', '2019-01-05', '19:13:11', '2019-01-05 17:13:11', '2019-01-05 17:13:11'),
(966, 631, '::1', 'http://localhost/laravel_project-master/30/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/30/topics?_pjax=%23view', '0.23727107', '2019-01-05', '19:15:19', '2019-01-05 17:15:19', '2019-01-05 17:15:19'),
(967, 631, '::1', 'http://localhost/laravel_project-master/30/topics', 'unknown', 'http://localhost/laravel_project-master/30/topics', '2.45818686', '2019-01-05', '20:03:22', '2019-01-05 18:03:22', '2019-01-05 18:03:22'),
(968, 631, '::1', 'http://localhost/laravel_project-master/fruits/images/catalog/peach.jpg', 'unknown', 'http://localhost/laravel_project-master/fruits/images/catalog/peach.jpg', '0.38857388', '2019-01-05', '20:17:41', '2019-01-05 18:17:41', '2019-01-05 18:17:41'),
(969, 632, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '0.94272304', '2019-01-07', '04:13:15', '2019-01-07 02:13:15', '2019-01-07 02:13:15'),
(970, 632, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '1.17114615', '2019-01-07', '04:16:04', '2019-01-07 02:16:04', '2019-01-07 02:16:04'),
(971, 632, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.2909348', '2019-01-07', '04:16:04', '2019-01-07 02:16:04', '2019-01-07 02:16:04'),
(972, 632, '::1', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', '0.5186491', '2019-01-07', '04:16:21', '2019-01-07 02:16:21', '2019-01-07 02:16:21'),
(973, 632, '::1', 'http://localhost/laravel_project-master/webmaster/sections/create', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/create', '0.24716616', '2019-01-07', '04:16:25', '2019-01-07 02:16:25', '2019-01-07 02:16:25'),
(974, 632, '::1', 'http://localhost/laravel_project-master/webmaster/sections', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections', '0.18204403', '2019-01-07', '04:17:58', '2019-01-07 02:17:58', '2019-01-07 02:17:58'),
(975, 632, '::1', 'http://localhost/laravel_project-master/33/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/33/topics?_pjax=%23view', '0.56726909', '2019-01-07', '04:18:05', '2019-01-07 02:18:05', '2019-01-07 02:18:05'),
(976, 632, '::1', 'http://localhost/laravel_project-master/33/topics/create', 'unknown', 'http://localhost/laravel_project-master/33/topics/create', '0.46203995', '2019-01-07', '04:18:08', '2019-01-07 02:18:08', '2019-01-07 02:18:08'),
(977, 632, '::1', 'http://localhost/laravel_project-master/webmaster/sections?page=2', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?page=2', '0.22878695', '2019-01-07', '04:32:41', '2019-01-07 02:32:41', '2019-01-07 02:32:41'),
(978, 632, '::1', 'http://localhost/laravel_project-master/webmaster/sections/26/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/26/edit', '0.67571092', '2019-01-07', '04:33:05', '2019-01-07 02:33:05', '2019-01-07 02:33:05'),
(979, 632, '::1', 'http://localhost/laravel_project-master/26/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/26/topics?_pjax=%23view', '0.21401', '2019-01-07', '04:33:16', '2019-01-07 02:33:16', '2019-01-07 02:33:16'),
(980, 632, '::1', 'http://localhost/laravel_project-master/26/topics/create', 'unknown', 'http://localhost/laravel_project-master/26/topics/create', '0.27997398', '2019-01-07', '04:33:20', '2019-01-07 02:33:20', '2019-01-07 02:33:20'),
(981, 632, '::1', 'http://localhost/laravel_project-master/33/topics', 'unknown', 'http://localhost/laravel_project-master/33/topics', '0.24576402', '2019-01-07', '13:40:40', '2019-01-07 11:40:40', '2019-01-07 11:40:40'),
(982, 632, '::1', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://localhost/laravel_project-master/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.3994329', '2019-01-07', '13:53:27', '2019-01-07 11:53:27', '2019-01-07 11:53:27'),
(983, 632, '::1', 'http://localhost/laravel_project-master/33/topics/102/edit', 'unknown', 'http://localhost/laravel_project-master/33/topics/102/edit', '1.33208799', '2019-01-07', '14:11:55', '2019-01-07 12:11:55', '2019-01-07 12:11:55'),
(984, 632, '::1', 'http://localhost/laravel_project-master/28/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/28/topics?_pjax=%23view', '0.24695802', '2019-01-07', '17:38:48', '2019-01-07 15:38:48', '2019-01-07 15:38:48'),
(985, 632, '::1', 'http://localhost/laravel_project-master/28/topics/create', 'unknown', 'http://localhost/laravel_project-master/28/topics/create', '0.23391199', '2019-01-07', '17:38:50', '2019-01-07 15:38:50', '2019-01-07 15:38:50'),
(986, 632, '::1', 'http://localhost/laravel_project-master/28/topics', 'unknown', 'http://localhost/laravel_project-master/28/topics', '0.18860722', '2019-01-07', '17:39:19', '2019-01-07 15:39:19', '2019-01-07 15:39:19'),
(987, 632, '::1', 'http://localhost/laravel_project-master/27/topics/create', 'unknown', 'http://localhost/laravel_project-master/27/topics/create', '0.26447487', '2019-01-07', '17:44:37', '2019-01-07 15:44:37', '2019-01-07 15:44:37'),
(988, 632, '::1', 'http://localhost/laravel_project-master/29/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/29/topics?_pjax=%23view', '0.20131516', '2019-01-07', '17:45:11', '2019-01-07 15:45:11', '2019-01-07 15:45:11'),
(989, 632, '::1', 'http://localhost/laravel_project-master/webmaster/sections/27/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/27/edit', '0.43186092', '2019-01-07', '17:46:45', '2019-01-07 15:46:45', '2019-01-07 15:46:45'),
(990, 632, '::1', 'http://localhost/laravel_project-master/28/topics/103/edit', 'unknown', 'http://localhost/laravel_project-master/28/topics/103/edit', '0.52667212', '2019-01-07', '17:49:41', '2019-01-07 15:49:41', '2019-01-07 15:49:41'),
(991, 632, '::1', 'http://localhost/laravel_project-master/settings', 'unknown', 'http://localhost/laravel_project-master/settings', '0.3567431', '2019-01-07', '18:10:04', '2019-01-07 16:10:04', '2019-01-07 16:10:04'),
(992, 632, '::1', 'http://localhost/laravel_project-master/settings?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/settings?_pjax=%23view', '0.39706302', '2019-01-07', '18:10:04', '2019-01-07 16:10:04', '2019-01-07 16:10:04'),
(993, 632, '::1', 'http://localhost/laravel_project-master/28/topics/107/edit', 'unknown', 'http://localhost/laravel_project-master/28/topics/107/edit', '0.29460907', '2019-01-07', '18:18:26', '2019-01-07 16:18:26', '2019-01-07 16:18:26'),
(994, 632, '::1', 'http://localhost/laravel_project-master/29/topics/create', 'unknown', 'http://localhost/laravel_project-master/29/topics/create', '0.21933413', '2019-01-07', '18:18:40', '2019-01-07 16:18:40', '2019-01-07 16:18:40'),
(995, 632, '::1', 'http://localhost/laravel_project-master/30/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/30/topics?_pjax=%23view', '0.21082401', '2019-01-07', '18:18:44', '2019-01-07 16:18:44', '2019-01-07 16:18:44'),
(996, 632, '::1', 'http://localhost/laravel_project-master/30/topics/create', 'unknown', 'http://localhost/laravel_project-master/30/topics/create', '0.214782', '2019-01-07', '18:28:53', '2019-01-07 16:28:53', '2019-01-07 16:28:53'),
(997, 632, '::1', 'http://localhost/laravel_project-master/28/topics/91/edit', 'unknown', 'http://localhost/laravel_project-master/28/topics/91/edit', '0.33062005', '2019-01-07', '18:40:50', '2019-01-07 16:40:50', '2019-01-07 16:40:50'),
(998, 632, '::1', 'http://localhost/laravel_project-master/webmaster/sections?page=3', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?page=3', '0.18164086', '2019-01-07', '18:58:14', '2019-01-07 16:58:14', '2019-01-07 16:58:14'),
(999, 632, '::1', 'http://localhost/laravel_project-master/webmaster/sections/14/edit', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections/14/edit', '0.26845503', '2019-01-07', '18:58:29', '2019-01-07 16:58:29', '2019-01-07 16:58:29'),
(1000, 632, '::1', 'http://localhost/laravel_project-master/14/topics?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/14/topics?_pjax=%23view', '0.20255899', '2019-01-07', '18:58:40', '2019-01-07 16:58:40', '2019-01-07 16:58:40'),
(1001, 632, '::1', 'http://localhost/laravel_project-master/14/topics/59/edit', 'unknown', 'http://localhost/laravel_project-master/14/topics/59/edit', '0.24668503', '2019-01-07', '18:58:45', '2019-01-07 16:58:45', '2019-01-07 16:58:45'),
(1002, 632, '::1', 'http://localhost/laravel_project-master/29/topics/95/edit', 'unknown', 'http://localhost/laravel_project-master/29/topics/95/edit', '0.23886585', '2019-01-07', '19:24:43', '2019-01-07 17:24:43', '2019-01-07 17:24:43'),
(1003, 632, '::1', 'http://localhost/laravel_project-master/30/topics/97/edit', 'unknown', 'http://localhost/laravel_project-master/30/topics/97/edit', '0.24614406', '2019-01-07', '19:30:21', '2019-01-07 17:30:21', '2019-01-07 17:30:21'),
(1004, 633, '::1', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', '0.71518278', '2019-01-11', '12:11:53', '2019-01-11 10:11:53', '2019-01-11 10:11:53'),
(1005, 633, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', '0.20655513', '2019-01-11', '12:13:40', '2019-01-11 10:13:40', '2019-01-11 10:13:40'),
(1006, 633, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', '0.19183111', '2019-01-11', '12:13:40', '2019-01-11 10:13:40', '2019-01-11 10:13:40'),
(1007, 633, '::1', 'http://localhost/books/public/images/robinsonfresh/sliders/slider_pagination.png', 'unknown', 'http://localhost/books/public/images/robinsonfresh/sliders/slider_pagination.png', '0.66168189', '2019-01-11', '19:34:07', '2019-01-11 17:34:07', '2019-01-11 17:34:07');
INSERT INTO `analytics_pages` (`id`, `visitor_id`, `ip`, `title`, `name`, `query`, `load_time`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1008, 633, '::1', 'http://localhost/books/public/images/robinsonfresh/icons/product_slider_arrows.png', 'unknown', 'http://localhost/books/public/images/robinsonfresh/icons/product_slider_arrows.png', '0.18451405', '2019-01-11', '19:34:08', '2019-01-11 17:34:08', '2019-01-11 17:34:08'),
(1009, 633, '::1', 'http://localhost/books/public/frontEnd/img/RobinsonFresh/favicon.ico', 'unknown', 'http://localhost/books/public/frontEnd/img/RobinsonFresh/favicon.ico', '0.40287685', '2019-01-11', '19:34:21', '2019-01-11 17:34:21', '2019-01-11 17:34:21'),
(1010, 633, '::1', 'http://localhost/books/', 'unknown', 'http://localhost/books/', '0.17475891', '2019-01-11', '19:41:21', '2019-01-11 17:41:21', '2019-01-11 17:41:21'),
(1011, 633, '::1', 'http://localhost/books/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/books/backEnd/assets/styles/flags.css', '0.26121497', '2019-01-11', '19:44:54', '2019-01-11 17:44:54', '2019-01-11 17:44:54'),
(1012, 633, '::1', 'http://localhost/books/users?_pjax=%23view', 'unknown', 'http://localhost/books/users?_pjax=%23view', '0.46657515', '2019-01-11', '19:45:35', '2019-01-11 17:45:35', '2019-01-11 17:45:35'),
(1013, 633, '::1', 'http://localhost/books/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/books/webmaster/sections?_pjax=%23view', '0.25960398', '2019-01-11', '19:45:38', '2019-01-11 17:45:38', '2019-01-11 17:45:38'),
(1014, 633, '::1', 'http://localhost/books/webmaster/sections', 'unknown', 'http://localhost/books/webmaster/sections', '0.24882889', '2019-01-11', '19:45:48', '2019-01-11 17:45:48', '2019-01-11 17:45:48'),
(1015, 633, '::1', 'http://localhost/books/settings?_pjax=%23view', 'unknown', 'http://localhost/books/settings?_pjax=%23view', '0.421386', '2019-01-11', '19:48:19', '2019-01-11 17:48:19', '2019-01-11 17:48:19'),
(1016, 633, '::1', 'http://localhost/books/settings', 'unknown', 'http://localhost/books/settings', '0.42136788', '2019-01-11', '19:48:19', '2019-01-11 17:48:19', '2019-01-11 17:48:19'),
(1017, 633, '::1', 'http://localhost/books/public/uploads/settings/15472363251529.jpg', 'unknown', 'http://localhost/books/public/uploads/settings/15472363251529.jpg', '0.27579093', '2019-01-11', '19:52:06', '2019-01-11 17:52:06', '2019-01-11 17:52:06'),
(1018, 633, '::1', 'http://localhost/books/public/uploads/settings/15472363258346.jpg', 'unknown', 'http://localhost/books/public/uploads/settings/15472363258346.jpg', '0.31221199', '2019-01-11', '19:52:06', '2019-01-11 17:52:06', '2019-01-11 17:52:06'),
(1019, 633, '::1', 'http://localhost/books/public/uploads/settings/15472363253820.jpg', 'unknown', 'http://localhost/books/public/uploads/settings/15472363253820.jpg', '0.32898092', '2019-01-11', '19:52:06', '2019-01-11 17:52:06', '2019-01-11 17:52:06'),
(1020, 633, '::1', 'http://localhost/books/public/uploads/settings/15472363255905.jpg', 'unknown', 'http://localhost/books/public/uploads/settings/15472363255905.jpg', '1.25472999', '2019-01-11', '19:52:07', '2019-01-11 17:52:07', '2019-01-11 17:52:07'),
(1021, 633, '::1', 'http://localhost/books/public/uploads/settings/15472363552873.jpg', 'unknown', 'http://localhost/books/public/uploads/settings/15472363552873.jpg', '0.32534289', '2019-01-11', '19:52:35', '2019-01-11 17:52:35', '2019-01-11 17:52:35'),
(1022, 633, '::1', 'http://localhost/books/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', 'unknown', 'http://localhost/books/public/backEnd/assets/bootstrap/dist/css/bootstrap.min.css.map', '0.17673993', '2019-01-11', '19:57:08', '2019-01-11 17:57:08', '2019-01-11 17:57:08'),
(1023, 634, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '5.78967714', '2019-01-12', '11:17:43', '2019-01-12 09:17:43', '2019-01-12 09:17:43'),
(1024, 635, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '0.7908659', '2019-01-12', '11:17:52', '2019-01-12 09:17:52', '2019-01-12 09:17:52'),
(1025, 634, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.39999604', '2019-01-12', '11:17:53', '2019-01-12 09:17:53', '2019-01-12 09:17:53'),
(1026, 634, '::1', 'http://localhost/laravel_project-master/settings', 'unknown', 'http://localhost/laravel_project-master/settings', '0.35187888', '2019-01-12', '11:19:11', '2019-01-12 09:19:11', '2019-01-12 09:19:11'),
(1027, 634, '::1', 'http://localhost/laravel_project-master/settings?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/settings?_pjax=%23view', '0.37428284', '2019-01-12', '11:19:11', '2019-01-12 09:19:11', '2019-01-12 09:19:11'),
(1028, 634, '::1', 'http://localhost/laravel_project-master/public/uploads/settings/15472363255905.jpg', 'unknown', 'http://localhost/laravel_project-master/public/uploads/settings/15472363255905.jpg', '0.293396', '2019-01-12', '11:19:12', '2019-01-12 09:19:12', '2019-01-12 09:19:12'),
(1029, 634, '::1', 'http://localhost/laravel_project-master/public/uploads/settings/15472363253820.jpg', 'unknown', 'http://localhost/laravel_project-master/public/uploads/settings/15472363253820.jpg', '0.31362915', '2019-01-12', '11:19:12', '2019-01-12 09:19:12', '2019-01-12 09:19:12'),
(1030, 634, '::1', 'http://localhost/laravel_project-master/public/uploads/settings/15472363258346.jpg', 'unknown', 'http://localhost/laravel_project-master/public/uploads/settings/15472363258346.jpg', '0.33332205', '2019-01-12', '11:19:12', '2019-01-12 09:19:12', '2019-01-12 09:19:12'),
(1031, 634, '::1', 'http://localhost/laravel_project-master/public/uploads/settings/15472363552873.jpg', 'unknown', 'http://localhost/laravel_project-master/public/uploads/settings/15472363552873.jpg', '0.40626597', '2019-01-12', '11:19:12', '2019-01-12 09:19:12', '2019-01-12 09:19:12'),
(1032, 634, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', '0.17914486', '2019-01-12', '11:19:36', '2019-01-12 09:19:36', '2019-01-12 09:19:36'),
(1033, 634, '::1', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', '0.14257312', '2019-01-12', '11:20:20', '2019-01-12 09:20:20', '2019-01-12 09:20:20'),
(1034, 634, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', '0.13189292', '2019-01-12', '11:20:57', '2019-01-12 09:20:57', '2019-01-12 09:20:57'),
(1035, 634, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '0.41391206', '2019-01-12', '11:21:49', '2019-01-12 09:21:49', '2019-01-12 09:21:49'),
(1036, 634, '::1', 'http://localhost/laravel_project-master/webmaster?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster?_pjax=%23view', '0.42419481', '2019-01-12', '11:23:09', '2019-01-12 09:23:09', '2019-01-12 09:23:09'),
(1037, 634, '::1', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', '0.30554008', '2019-01-12', '11:23:10', '2019-01-12 09:23:10', '2019-01-12 09:23:10'),
(1038, 634, '::1', 'http://localhost/laravel_project-master/webmaster/banners?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/banners?_pjax=%23view', '0.36576796', '2019-01-12', '11:23:11', '2019-01-12 09:23:11', '2019-01-12 09:23:11'),
(1039, 636, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '0.95902514', '2019-01-21', '18:52:52', '2019-01-21 16:52:52', '2019-01-21 16:52:52'),
(1040, 637, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', '0.77091813', '2019-03-09', '13:10:44', '2019-03-09 11:10:44', '2019-03-09 11:10:44'),
(1041, 637, '::1', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', '0.17170095', '2019-03-09', '13:10:45', '2019-03-09 11:10:45', '2019-03-09 11:10:45'),
(1042, 638, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', '0.42795992', '2019-03-11', '20:23:38', '2019-03-11 18:23:38', '2019-03-11 18:23:38'),
(1043, 638, '::1', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', '0.15617299', '2019-03-11', '20:23:43', '2019-03-11 18:23:43', '2019-03-11 18:23:43'),
(1044, 639, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', '1.44891095', '2019-04-12', '12:04:19', '2019-04-12 10:04:19', '2019-04-12 10:04:19'),
(1045, 639, '::1', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', '0.14361215', '2019-04-12', '12:04:21', '2019-04-12 10:04:22', '2019-04-12 10:04:22'),
(1046, 639, '::1', 'http://localhost/forsan/', 'unknown', 'http://localhost/forsan/', '0.82819986', '2019-04-12', '12:09:02', '2019-04-12 10:09:02', '2019-04-12 10:09:02'),
(1047, 639, '::1', 'http://localhost/forsan/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/forsan/backEnd/assets/styles/flags.css', '0.39519095', '2019-04-12', '12:09:03', '2019-04-12 10:09:03', '2019-04-12 10:09:03'),
(1048, 639, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', '0.37849379', '2019-04-12', '12:11:34', '2019-04-12 10:11:34', '2019-04-12 10:11:34'),
(1049, 640, '127.0.0.1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', '0.39852405', '2019-04-13', '02:44:36', '2019-04-13 00:44:36', '2019-04-13 00:44:36'),
(1050, 641, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', '0.39840007', '2019-04-13', '02:44:36', '2019-04-13 00:44:36', '2019-04-13 00:44:36'),
(1051, 641, '::1', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', '0.22282887', '2019-04-13', '02:44:37', '2019-04-13 00:44:37', '2019-04-13 00:44:37'),
(1052, 641, '::1', 'http://localhost/forsan/login', 'unknown', 'http://localhost/forsan/login', '0.42238784', '2019-04-13', '02:44:54', '2019-04-13 00:44:54', '2019-04-13 00:44:54'),
(1053, 641, '::1', 'http://localhost/forsan/', 'unknown', 'http://localhost/forsan/', '0.53171086', '2019-04-13', '02:45:04', '2019-04-13 00:45:04', '2019-04-13 00:45:04'),
(1054, 641, '::1', 'http://localhost/forsan/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/forsan/backEnd/assets/styles/flags.css', '0.22640014', '2019-04-13', '02:45:05', '2019-04-13 00:45:05', '2019-04-13 00:45:05'),
(1055, 641, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', '0.33679199', '2019-04-13', '02:45:18', '2019-04-13 00:45:18', '2019-04-13 00:45:18'),
(1056, 642, '::1', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', '0.27534008', '2019-04-19', '03:10:15', '2019-04-19 01:10:15', '2019-04-19 01:10:15'),
(1057, 642, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', '0.33661509', '2019-04-19', '03:23:35', '2019-04-19 01:23:35', '2019-04-19 01:23:35'),
(1058, 642, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', '0.42388892', '2019-04-19', '03:23:35', '2019-04-19 01:23:35', '2019-04-19 01:23:35'),
(1059, 642, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '0.231951', '2019-04-19', '03:37:50', '2019-04-19 01:37:50', '2019-04-19 01:37:50'),
(1060, 642, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '0.55858707', '2019-04-19', '03:37:53', '2019-04-19 01:37:53', '2019-04-19 01:37:53'),
(1061, 642, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.25731397', '2019-04-19', '03:37:53', '2019-04-19 01:37:53', '2019-04-19 01:37:53'),
(1062, 642, '::1', 'http://localhost/laravel_project-master/settings', 'unknown', 'http://localhost/laravel_project-master/settings', '0.49269485', '2019-04-19', '03:38:07', '2019-04-19 01:38:07', '2019-04-19 01:38:07'),
(1063, 642, '::1', 'http://localhost/laravel_project-master/settings?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/settings?_pjax=%23view', '0.52189612', '2019-04-19', '03:38:07', '2019-04-19 01:38:07', '2019-04-19 01:38:07'),
(1064, 642, '::1', 'http://localhost/laravel_project-master/public/uploads/settings/15472363258346.jpg', 'unknown', 'http://localhost/laravel_project-master/public/uploads/settings/15472363258346.jpg', '0.52596307', '2019-04-19', '03:38:07', '2019-04-19 01:38:07', '2019-04-19 01:38:07'),
(1065, 642, '::1', 'http://localhost/laravel_project-master/public/uploads/settings/15472363253820.jpg', 'unknown', 'http://localhost/laravel_project-master/public/uploads/settings/15472363253820.jpg', '0.54828', '2019-04-19', '03:38:07', '2019-04-19 01:38:07', '2019-04-19 01:38:07'),
(1066, 642, '::1', 'http://localhost/laravel_project-master/public/uploads/settings/15472363552873.jpg', 'unknown', 'http://localhost/laravel_project-master/public/uploads/settings/15472363552873.jpg', '0.56954002', '2019-04-19', '03:38:07', '2019-04-19 01:38:07', '2019-04-19 01:38:07'),
(1067, 642, '::1', 'http://localhost/laravel_project-master/public/uploads/settings/15472363255905.jpg', 'unknown', 'http://localhost/laravel_project-master/public/uploads/settings/15472363255905.jpg', '0.71967006', '2019-04-19', '03:38:08', '2019-04-19 01:38:08', '2019-04-19 01:38:08'),
(1068, 642, '::1', 'http://localhost/laravel_project-master/users?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/users?_pjax=%23view', '0.59845519', '2019-04-19', '03:38:12', '2019-04-19 01:38:12', '2019-04-19 01:38:12'),
(1069, 642, '::1', 'http://localhost/laravel_project-master/menus?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/menus?_pjax=%23view', '0.91203713', '2019-04-19', '03:38:24', '2019-04-19 01:38:24', '2019-04-19 01:38:24'),
(1070, 642, '::1', 'http://localhost/laravel_project-master/webmaster/banners?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/banners?_pjax=%23view', '0.54920101', '2019-04-19', '03:38:29', '2019-04-19 01:38:29', '2019-04-19 01:38:29'),
(1071, 642, '::1', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', '0.3321929', '2019-04-19', '03:38:31', '2019-04-19 01:38:31', '2019-04-19 01:38:31'),
(1072, 642, '::1', 'http://localhost/laravel_project-master/webmaster?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster?_pjax=%23view', '0.36298418', '2019-04-19', '03:38:33', '2019-04-19 01:38:33', '2019-04-19 01:38:33'),
(1073, 642, '::1', 'http://localhost/laravel_project-master/webmaster', 'unknown', 'http://localhost/laravel_project-master/webmaster', '0.25816202', '2019-04-19', '03:38:40', '2019-04-19 01:38:40', '2019-04-19 01:38:40'),
(1074, 642, '::1', 'http://localhost/forsan/login', 'unknown', 'http://localhost/forsan/login', '0.49055886', '2019-04-19', '03:41:54', '2019-04-19 01:41:54', '2019-04-19 01:41:54'),
(1075, 642, '::1', 'http://localhost/forsan/', 'unknown', 'http://localhost/forsan/', '0.578686', '2019-04-19', '03:42:05', '2019-04-19 01:42:05', '2019-04-19 01:42:05'),
(1076, 642, '::1', 'http://localhost/forsan/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/forsan/backEnd/assets/styles/flags.css', '0.26721811', '2019-04-19', '03:42:05', '2019-04-19 01:42:05', '2019-04-19 01:42:05'),
(1077, 643, '::1', 'http://localhost/forsan/login', 'unknown', 'http://localhost/forsan/login', '1.37853599', '2019-08-03', '15:48:48', '2019-08-03 13:48:48', '2019-08-03 13:48:48'),
(1078, 643, '::1', 'http://localhost/forsan/admin', 'unknown', 'http://localhost/forsan/admin', '2.38927507', '2019-08-03', '15:49:20', '2019-08-03 13:49:20', '2019-08-03 13:49:20'),
(1079, 643, '::1', 'http://localhost/forsan/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/forsan/backEnd/assets/styles/flags.css', '0.38692188', '2019-08-03', '15:49:21', '2019-08-03 13:49:21', '2019-08-03 13:49:21'),
(1080, 643, '::1', 'http://localhost/forsan/', 'unknown', 'http://localhost/forsan/', '5.62234998', '2019-08-03', '15:50:23', '2019-08-03 13:50:23', '2019-08-03 13:50:23'),
(1081, 643, '::1', 'http://localhost/laravel_project-master/login', 'unknown', 'http://localhost/laravel_project-master/login', '1.09679914', '2019-08-03', '15:51:10', '2019-08-03 13:51:10', '2019-08-03 13:51:10'),
(1082, 643, '::1', 'http://localhost/laravel_project-master/admin', 'unknown', 'http://localhost/laravel_project-master/admin', '1.34560704', '2019-08-03', '15:51:16', '2019-08-03 13:51:16', '2019-08-03 13:51:16'),
(1083, 643, '::1', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', 'unknown', 'http://localhost/laravel_project-master/backEnd/assets/styles/flags.css', '0.30726981', '2019-08-03', '15:51:16', '2019-08-03 13:51:16', '2019-08-03 13:51:16'),
(1084, 643, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/sliders/slider_pagination.png', '0.51178789', '2019-08-03', '15:52:14', '2019-08-03 13:52:14', '2019-08-03 13:52:14'),
(1085, 643, '::1', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', 'unknown', 'http://localhost/laravel_project-master/public/images/robinsonfresh/icons/product_slider_arrows.png', '0.20743203', '2019-08-03', '15:52:17', '2019-08-03 13:52:17', '2019-08-03 13:52:17'),
(1086, 643, '::1', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/img/RobinsonFresh/favicon.ico', '0.20214105', '2019-08-03', '15:52:20', '2019-08-03 13:52:20', '2019-08-03 13:52:20'),
(1087, 643, '::1', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections?_pjax=%23view', '0.56273198', '2019-08-03', '16:07:59', '2019-08-03 14:07:59', '2019-08-03 14:07:59'),
(1088, 643, '::1', 'http://localhost/laravel_project-master/webmaster/sections', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections', '0.27779984', '2019-08-03', '16:08:05', '2019-08-03 14:08:05', '2019-08-03 14:08:05');

-- --------------------------------------------------------

--
-- Table structure for table `analytics_visitors`
--

CREATE TABLE `analytics_visitors` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_cor1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location_cor2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `os` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `browser` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resolution` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referrer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `analytics_visitors`
--

INSERT INTO `analytics_visitors` (`id`, `ip`, `city`, `country_code`, `country`, `region`, `full_address`, `location_cor1`, `location_cor2`, `os`, `browser`, `resolution`, `referrer`, `hostname`, `org`, `date`, `time`, `created_at`, `updated_at`) VALUES
(1, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/create', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:33:44', '2018-12-11 11:33:44', '2018-12-11 11:33:44'),
(2, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/create', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:34:17', '2018-12-11 11:34:17', '2018-12-11 11:34:17'),
(3, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:34:18', '2018-12-11 11:34:18', '2018-12-11 11:34:18'),
(4, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:34:18', '2018-12-11 11:34:18', '2018-12-11 11:34:18'),
(5, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:34:18', '2018-12-11 11:34:18', '2018-12-11 11:34:18'),
(6, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:34:27', '2018-12-11 11:34:27', '2018-12-11 11:34:27'),
(7, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:34:32', '2018-12-11 11:34:32', '2018-12-11 11:34:32'),
(8, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:34:32', '2018-12-11 11:34:32', '2018-12-11 11:34:32'),
(9, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:34:33', '2018-12-11 11:34:33', '2018-12-11 11:34:33'),
(10, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:34:33', '2018-12-11 11:34:33', '2018-12-11 11:34:33'),
(11, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/50/edit', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:35:46', '2018-12-11 11:35:46', '2018-12-11 11:35:46'),
(12, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:35:55', '2018-12-11 11:35:55', '2018-12-11 11:35:55'),
(13, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:35:56', '2018-12-11 11:35:56', '2018-12-11 11:35:56'),
(14, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:35:56', '2018-12-11 11:35:56', '2018-12-11 11:35:56'),
(15, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:35:56', '2018-12-11 11:35:56', '2018-12-11 11:35:56'),
(16, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:36:03', '2018-12-11 11:36:03', '2018-12-11 11:36:03'),
(17, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:36:03', '2018-12-11 11:36:03', '2018-12-11 11:36:03'),
(18, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:36:04', '2018-12-11 11:36:04', '2018-12-11 11:36:04'),
(19, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:36:04', '2018-12-11 11:36:04', '2018-12-11 11:36:04'),
(20, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:36:04', '2018-12-11 11:36:04', '2018-12-11 11:36:04'),
(21, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:36:05', '2018-12-11 11:36:05', '2018-12-11 11:36:05'),
(22, '156.211.3.79', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.211.79.3-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '04:36:05', '2018-12-11 11:36:05', '2018-12-11 11:36:05'),
(23, '156.217.194.51', 'Tanta', 'EG', 'Egypt', 'Gharbia', 'Gharbia, Tanta, Egypt', '30.7885', '31.0019', 'Windows 8.1', 'Chrome', 'unknown', 'unknown', 'host-156.217.51.194-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '08:18:10', '2018-12-11 15:18:10', '2018-12-11 15:18:10'),
(24, '156.217.194.51', 'Tanta', 'EG', 'Egypt', 'Gharbia', 'Gharbia, Tanta, Egypt', '30.7885', '31.0019', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/login', 'host-156.217.51.194-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '08:18:33', '2018-12-11 15:18:33', '2018-12-11 15:18:33'),
(25, '156.217.194.51', 'Tanta', 'EG', 'Egypt', 'Gharbia', 'Gharbia, Tanta, Egypt', '30.7885', '31.0019', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/login', 'host-156.217.51.194-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '08:18:49', '2018-12-11 15:18:49', '2018-12-11 15:18:49'),
(26, '156.217.194.51', 'Tanta', 'EG', 'Egypt', 'Gharbia', 'Gharbia, Tanta, Egypt', '30.7885', '31.0019', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.217.51.194-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '08:18:59', '2018-12-11 15:18:59', '2018-12-11 15:18:59'),
(27, '156.217.194.51', 'Tanta', 'EG', 'Egypt', 'Gharbia', 'Gharbia, Tanta, Egypt', '30.7885', '31.0019', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.217.51.194-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '08:19:06', '2018-12-11 15:19:06', '2018-12-11 15:19:06'),
(28, '156.217.194.51', 'Tanta', 'EG', 'Egypt', 'Gharbia', 'Gharbia, Tanta, Egypt', '30.7885', '31.0019', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-156.217.51.194-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '08:19:08', '2018-12-11 15:19:08', '2018-12-11 15:19:08'),
(29, '156.217.194.51', 'Tanta', 'EG', 'Egypt', 'Gharbia', 'Gharbia, Tanta, Egypt', '30.7885', '31.0019', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics', 'host-156.217.51.194-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '08:19:10', '2018-12-11 15:19:10', '2018-12-11 15:19:10'),
(30, '156.217.194.51', 'Tanta', 'EG', 'Egypt', 'Gharbia', 'Gharbia, Tanta, Egypt', '30.7885', '31.0019', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.217.51.194-static.tedata.net', 'AS8452 TE-AS', '2018-12-11', '08:19:12', '2018-12-11 15:19:12', '2018-12-11 15:19:12'),
(31, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:39:19', '2018-12-11 15:39:19', '2018-12-11 15:39:19'),
(32, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/login', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:41:22', '2018-12-11 15:41:22', '2018-12-11 15:41:22'),
(33, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/login', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:43:03', '2018-12-11 15:43:03', '2018-12-11 15:43:03'),
(34, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:43:04', '2018-12-11 15:43:04', '2018-12-11 15:43:04'),
(35, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:43:05', '2018-12-11 15:43:05', '2018-12-11 15:43:05'),
(36, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:17', '2018-12-11 15:44:17', '2018-12-11 15:44:17'),
(37, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:17', '2018-12-11 15:44:17', '2018-12-11 15:44:17'),
(38, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:18', '2018-12-11 15:44:18', '2018-12-11 15:44:18'),
(39, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:22', '2018-12-11 15:44:22', '2018-12-11 15:44:22'),
(40, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:22', '2018-12-11 15:44:22', '2018-12-11 15:44:22'),
(41, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:23', '2018-12-11 15:44:23', '2018-12-11 15:44:23'),
(42, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:23', '2018-12-11 15:44:23', '2018-12-11 15:44:23'),
(43, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:27', '2018-12-11 15:44:27', '2018-12-11 15:44:27'),
(44, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:59', '2018-12-11 15:44:59', '2018-12-11 15:44:59'),
(45, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/26/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:44:59', '2018-12-11 15:44:59', '2018-12-11 15:44:59'),
(46, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/26/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:45:00', '2018-12-11 15:45:00', '2018-12-11 15:45:00'),
(47, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/26/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:45:10', '2018-12-11 15:45:10', '2018-12-11 15:45:10'),
(48, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:45:32', '2018-12-11 15:45:32', '2018-12-11 15:45:32'),
(49, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:45:35', '2018-12-11 15:45:35', '2018-12-11 15:45:35'),
(50, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:45:53', '2018-12-11 15:45:53', '2018-12-11 15:45:53'),
(51, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:45:54', '2018-12-11 15:45:54', '2018-12-11 15:45:54'),
(52, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:45:54', '2018-12-11 15:45:54', '2018-12-11 15:45:54'),
(53, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:46:15', '2018-12-11 15:46:15', '2018-12-11 15:46:15'),
(54, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:46:18', '2018-12-11 15:46:18', '2018-12-11 15:46:18'),
(55, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:46:19', '2018-12-11 15:46:19', '2018-12-11 15:46:19'),
(56, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:46:20', '2018-12-11 15:46:20', '2018-12-11 15:46:20'),
(57, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:46:25', '2018-12-11 15:46:25', '2018-12-11 15:46:25'),
(58, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:47:04', '2018-12-11 15:47:04', '2018-12-11 15:47:04'),
(59, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:47:33', '2018-12-11 15:47:33', '2018-12-11 15:47:33'),
(60, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:47:53', '2018-12-11 15:47:53', '2018-12-11 15:47:53'),
(61, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:47:53', '2018-12-11 15:47:53', '2018-12-11 15:47:53'),
(62, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:47:53', '2018-12-11 15:47:53', '2018-12-11 15:47:53'),
(63, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:47:54', '2018-12-11 15:47:54', '2018-12-11 15:47:54'),
(64, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:54', '2018-12-11 15:49:54', '2018-12-11 15:49:54'),
(65, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(66, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(67, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(68, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(69, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(70, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(71, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(72, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(73, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:55', '2018-12-11 15:49:55', '2018-12-11 15:49:55'),
(74, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:49:56', '2018-12-11 15:49:56', '2018-12-11 15:49:56'),
(75, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:38', '2018-12-11 15:50:38', '2018-12-11 15:50:38'),
(76, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(77, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(78, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(79, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(80, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(81, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:39', '2018-12-11 15:50:39', '2018-12-11 15:50:39'),
(82, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:40', '2018-12-11 15:50:40', '2018-12-11 15:50:40'),
(83, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:40', '2018-12-11 15:50:40', '2018-12-11 15:50:40'),
(84, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:40', '2018-12-11 15:50:40', '2018-12-11 15:50:40'),
(85, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:40', '2018-12-11 15:50:40', '2018-12-11 15:50:40'),
(86, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/51/edit', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(87, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(88, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(89, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(90, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(91, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(92, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(93, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(94, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:50:57', '2018-12-11 15:50:57', '2018-12-11 15:50:57'),
(95, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:51:04', '2018-12-11 15:51:04', '2018-12-11 15:51:04'),
(96, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:59:36', '2018-12-11 15:59:36', '2018-12-11 15:59:36'),
(97, '196.138.2.12', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-11', '08:59:37', '2018-12-11 15:59:37', '2018-12-11 15:59:37'),
(98, '31.13.127.18', 'unknown', 'IE', 'Ireland', 'unknown', 'unknown, unknown, Ireland', '53.3472', '-6.2439', 'unknown', NULL, 'unknown', 'unknown', 'No Hostname', 'AS32934 Facebook, Inc.', '2018-12-11', '14:56:49', '2018-12-11 21:56:49', '2018-12-11 21:56:49'),
(99, '31.13.127.18', 'unknown', 'IE', 'Ireland', 'unknown', 'unknown, unknown, Ireland', '53.3472', '-6.2439', 'unknown', NULL, 'unknown', 'unknown', 'No Hostname', 'AS32934 Facebook, Inc.', '2018-12-11', '14:56:52', '2018-12-11 21:56:52', '2018-12-11 21:56:52'),
(100, '31.13.127.18', 'unknown', 'IE', 'Ireland', 'unknown', 'unknown, unknown, Ireland', '53.3472', '-6.2439', 'unknown', NULL, 'unknown', 'unknown', 'No Hostname', 'AS32934 Facebook, Inc.', '2018-12-11', '14:56:52', '2018-12-11 21:56:52', '2018-12-11 21:56:52'),
(101, '31.13.127.16', 'unknown', 'IE', 'Ireland', 'unknown', 'unknown, unknown, Ireland', '53.3472', '-6.2439', 'unknown', NULL, 'unknown', 'unknown', 'No Hostname', 'AS32934 Facebook, Inc.', '2018-12-11', '14:56:52', '2018-12-11 21:56:52', '2018-12-11 21:56:52'),
(102, '31.13.127.10', 'unknown', 'IE', 'Ireland', 'unknown', 'unknown, unknown, Ireland', '53.3472', '-6.2439', 'unknown', NULL, 'unknown', 'unknown', 'No Hostname', 'AS32934 Facebook, Inc.', '2018-12-11', '14:56:52', '2018-12-11 21:56:52', '2018-12-11 21:56:52'),
(103, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'https://l.facebook.com/', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:18:31', '2018-12-12 05:18:31', '2018-12-12 05:18:31'),
(104, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/login', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:18:32', '2018-12-12 05:18:32', '2018-12-12 05:18:32'),
(105, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/login', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:19:20', '2018-12-12 05:19:20', '2018-12-12 05:19:20'),
(106, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin?fbclid=IwAR2ug5kphSRVAZufh7htQdhJEyhhxI2MN5J-nzlNlxtQ5DzNLUBC8t4SDH0', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:19:20', '2018-12-12 05:19:20', '2018-12-12 05:19:20'),
(107, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin?fbclid=IwAR2ug5kphSRVAZufh7htQdhJEyhhxI2MN5J-nzlNlxtQ5DzNLUBC8t4SDH0', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:19:21', '2018-12-12 05:19:21', '2018-12-12 05:19:21'),
(108, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin?fbclid=IwAR2ug5kphSRVAZufh7htQdhJEyhhxI2MN5J-nzlNlxtQ5DzNLUBC8t4SDH0', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:19:35', '2018-12-12 05:19:35', '2018-12-12 05:19:35'),
(109, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:20:22', '2018-12-12 05:20:22', '2018-12-12 05:20:22'),
(110, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/23/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:20:22', '2018-12-12 05:20:22', '2018-12-12 05:20:22'),
(111, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/23/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:20:23', '2018-12-12 05:20:23', '2018-12-12 05:20:23'),
(112, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/23/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:20:24', '2018-12-12 05:20:24', '2018-12-12 05:20:24'),
(113, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/23/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:20:53', '2018-12-12 05:20:53', '2018-12-12 05:20:53'),
(114, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:01', '2018-12-12 05:21:01', '2018-12-12 05:21:01'),
(115, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/27/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:01', '2018-12-12 05:21:01', '2018-12-12 05:21:01'),
(116, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/27/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:01', '2018-12-12 05:21:01', '2018-12-12 05:21:01'),
(117, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/27/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:02', '2018-12-12 05:21:02', '2018-12-12 05:21:02'),
(118, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/27/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:02', '2018-12-12 05:21:02', '2018-12-12 05:21:02'),
(119, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/27/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:02', '2018-12-12 05:21:02', '2018-12-12 05:21:02'),
(120, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/5/topics/27/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:38', '2018-12-12 05:21:38', '2018-12-12 05:21:38'),
(121, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:38', '2018-12-12 05:21:38', '2018-12-12 05:21:38'),
(122, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:44', '2018-12-12 05:21:44', '2018-12-12 05:21:44'),
(123, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:44', '2018-12-12 05:21:44', '2018-12-12 05:21:44'),
(124, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(125, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(126, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(127, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(128, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(129, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(130, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(131, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(132, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:45', '2018-12-12 05:21:45', '2018-12-12 05:21:45'),
(133, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(134, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(135, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(136, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(137, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:48', '2018-12-12 05:21:48', '2018-12-12 05:21:48'),
(138, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:49', '2018-12-12 05:21:49', '2018-12-12 05:21:49'),
(139, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:49', '2018-12-12 05:21:49', '2018-12-12 05:21:49'),
(140, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:49', '2018-12-12 05:21:49', '2018-12-12 05:21:49'),
(141, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:49', '2018-12-12 05:21:49', '2018-12-12 05:21:49'),
(142, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:21:50', '2018-12-12 05:21:50', '2018-12-12 05:21:50'),
(143, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:37', '2018-12-12 05:22:37', '2018-12-12 05:22:37'),
(144, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/settings', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:41', '2018-12-12 05:22:41', '2018-12-12 05:22:41'),
(145, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:41', '2018-12-12 05:22:41', '2018-12-12 05:22:41'),
(146, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:42', '2018-12-12 05:22:42', '2018-12-12 05:22:42'),
(147, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:42', '2018-12-12 05:22:42', '2018-12-12 05:22:42'),
(148, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:46', '2018-12-12 05:22:46', '2018-12-12 05:22:46'),
(149, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:58', '2018-12-12 05:22:58', '2018-12-12 05:22:58'),
(150, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:58', '2018-12-12 05:22:58', '2018-12-12 05:22:58'),
(151, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:58', '2018-12-12 05:22:58', '2018-12-12 05:22:58'),
(152, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:22:59', '2018-12-12 05:22:59', '2018-12-12 05:22:59'),
(153, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:01', '2018-12-12 05:23:01', '2018-12-12 05:23:01'),
(154, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:01', '2018-12-12 05:23:01', '2018-12-12 05:23:01'),
(155, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:02', '2018-12-12 05:23:02', '2018-12-12 05:23:02'),
(156, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:03', '2018-12-12 05:23:03', '2018-12-12 05:23:03'),
(157, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:03', '2018-12-12 05:23:03', '2018-12-12 05:23:03'),
(158, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:03', '2018-12-12 05:23:03', '2018-12-12 05:23:03'),
(159, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:07', '2018-12-12 05:23:07', '2018-12-12 05:23:07'),
(160, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:08', '2018-12-12 05:23:08', '2018-12-12 05:23:08');
INSERT INTO `analytics_visitors` (`id`, `ip`, `city`, `country_code`, `country`, `region`, `full_address`, `location_cor1`, `location_cor2`, `os`, `browser`, `resolution`, `referrer`, `hostname`, `org`, `date`, `time`, `created_at`, `updated_at`) VALUES
(161, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:08', '2018-12-12 05:23:08', '2018-12-12 05:23:08'),
(162, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:08', '2018-12-12 05:23:08', '2018-12-12 05:23:08'),
(163, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:08', '2018-12-12 05:23:08', '2018-12-12 05:23:08'),
(164, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:10', '2018-12-12 05:23:10', '2018-12-12 05:23:10'),
(165, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:11', '2018-12-12 05:23:11', '2018-12-12 05:23:11'),
(166, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:11', '2018-12-12 05:23:11', '2018-12-12 05:23:11'),
(167, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:11', '2018-12-12 05:23:11', '2018-12-12 05:23:11'),
(168, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics/create', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:19', '2018-12-12 05:23:19', '2018-12-12 05:23:19'),
(169, '197.43.27.65', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 8.1', 'Chrome', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-197.43.27.65.tedata.net', 'AS8452 TE-AS', '2018-12-11', '22:23:19', '2018-12-12 05:23:19', '2018-12-12 05:23:19'),
(170, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:11:55', '2018-12-13 00:11:55', '2018-12-13 00:11:55'),
(171, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/login', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:11:58', '2018-12-13 00:11:58', '2018-12-13 00:11:58'),
(172, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/login', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:12:24', '2018-12-13 00:12:24', '2018-12-13 00:12:24'),
(173, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/login', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:12:40', '2018-12-13 00:12:40', '2018-12-13 00:12:40'),
(174, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/login', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:14:10', '2018-12-13 00:14:10', '2018-12-13 00:14:10'),
(175, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:14:11', '2018-12-13 00:14:11', '2018-12-13 00:14:11'),
(176, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:14:19', '2018-12-13 00:14:19', '2018-12-13 00:14:19'),
(177, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:32:48', '2018-12-13 00:32:48', '2018-12-13 00:32:48'),
(178, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:32:48', '2018-12-13 00:32:48', '2018-12-13 00:32:48'),
(179, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:32:48', '2018-12-13 00:32:48', '2018-12-13 00:32:48'),
(180, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:32:49', '2018-12-13 00:32:49', '2018-12-13 00:32:49'),
(181, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:32:52', '2018-12-13 00:32:52', '2018-12-13 00:32:52'),
(182, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:32:53', '2018-12-13 00:32:53', '2018-12-13 00:32:53'),
(183, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:32:53', '2018-12-13 00:32:53', '2018-12-13 00:32:53'),
(184, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:32:59', '2018-12-13 00:32:59', '2018-12-13 00:32:59'),
(185, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:33:02', '2018-12-13 00:33:02', '2018-12-13 00:33:02'),
(186, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/4/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:33:16', '2018-12-13 00:33:16', '2018-12-13 00:33:16'),
(187, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/5/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:33:23', '2018-12-13 00:33:23', '2018-12-13 00:33:23'),
(188, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:33:45', '2018-12-13 00:33:45', '2018-12-13 00:33:45'),
(189, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:33:46', '2018-12-13 00:33:46', '2018-12-13 00:33:46'),
(190, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:33:46', '2018-12-13 00:33:46', '2018-12-13 00:33:46'),
(191, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:34:53', '2018-12-13 00:34:53', '2018-12-13 00:34:53'),
(192, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:34:56', '2018-12-13 00:34:56', '2018-12-13 00:34:56'),
(193, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:34:57', '2018-12-13 00:34:57', '2018-12-13 00:34:57'),
(194, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:34:57', '2018-12-13 00:34:57', '2018-12-13 00:34:57'),
(195, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:34:59', '2018-12-13 00:34:59', '2018-12-13 00:34:59'),
(196, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:02', '2018-12-13 00:35:02', '2018-12-13 00:35:02'),
(197, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:03', '2018-12-13 00:35:03', '2018-12-13 00:35:03'),
(198, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:03', '2018-12-13 00:35:03', '2018-12-13 00:35:03'),
(199, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(200, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(201, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(202, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(203, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(204, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(205, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(206, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(207, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:35:04', '2018-12-13 00:35:04', '2018-12-13 00:35:04'),
(208, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:16', '2018-12-13 00:36:16', '2018-12-13 00:36:16'),
(209, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:16', '2018-12-13 00:36:16', '2018-12-13 00:36:16'),
(210, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(211, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(212, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(213, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(214, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(215, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(216, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/32/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(217, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:17', '2018-12-13 00:36:17', '2018-12-13 00:36:17'),
(218, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:36:19', '2018-12-13 00:36:19', '2018-12-13 00:36:19'),
(219, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:16', '2018-12-13 00:37:16', '2018-12-13 00:37:16'),
(220, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:17', '2018-12-13 00:37:17', '2018-12-13 00:37:17'),
(221, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:38', '2018-12-13 00:37:38', '2018-12-13 00:37:38'),
(222, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:39', '2018-12-13 00:37:39', '2018-12-13 00:37:39'),
(223, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:45', '2018-12-13 00:37:45', '2018-12-13 00:37:45'),
(224, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(225, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(226, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(227, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(228, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(229, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(230, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(231, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:46', '2018-12-13 00:37:46', '2018-12-13 00:37:46'),
(232, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:47', '2018-12-13 00:37:47', '2018-12-13 00:37:47'),
(233, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/33/edit', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:37:47', '2018-12-13 00:37:47', '2018-12-13 00:37:47'),
(234, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:54:19', '2018-12-13 00:54:19', '2018-12-13 00:54:19'),
(235, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:54:19', '2018-12-13 00:54:19', '2018-12-13 00:54:19'),
(236, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:54:20', '2018-12-13 00:54:20', '2018-12-13 00:54:20'),
(237, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:56:23', '2018-12-13 00:56:23', '2018-12-13 00:56:23'),
(238, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:56:29', '2018-12-13 00:56:29', '2018-12-13 00:56:29'),
(239, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:56:30', '2018-12-13 00:56:30', '2018-12-13 00:56:30'),
(240, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:56:30', '2018-12-13 00:56:30', '2018-12-13 00:56:30'),
(241, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:56:30', '2018-12-13 00:56:30', '2018-12-13 00:56:30'),
(242, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:56:30', '2018-12-13 00:56:30', '2018-12-13 00:56:30'),
(243, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:57:28', '2018-12-13 00:57:28', '2018-12-13 00:57:28'),
(244, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:57:39', '2018-12-13 00:57:39', '2018-12-13 00:57:39'),
(245, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:57:39', '2018-12-13 00:57:39', '2018-12-13 00:57:39'),
(246, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:57:40', '2018-12-13 00:57:40', '2018-12-13 00:57:40'),
(247, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:57:40', '2018-12-13 00:57:40', '2018-12-13 00:57:40'),
(248, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:57:40', '2018-12-13 00:57:40', '2018-12-13 00:57:40'),
(249, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:58:02', '2018-12-13 00:58:02', '2018-12-13 00:58:02'),
(250, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:58:02', '2018-12-13 00:58:02', '2018-12-13 00:58:02'),
(251, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:58:02', '2018-12-13 00:58:02', '2018-12-13 00:58:02'),
(252, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics/create', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:58:04', '2018-12-13 00:58:04', '2018-12-13 00:58:04'),
(253, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:58:09', '2018-12-13 00:58:09', '2018-12-13 00:58:09'),
(254, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/8/topics', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:58:09', '2018-12-13 00:58:09', '2018-12-13 00:58:09'),
(255, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:58:10', '2018-12-13 00:58:10', '2018-12-13 00:58:10'),
(256, '156.205.249.91', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://bsegypt.net/ad/settings', 'host-156.205.91.249-static.tedata.net', 'AS8452 TE-AS', '2018-12-12', '17:58:27', '2018-12-13 00:58:27', '2018-12-13 00:58:27'),
(257, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '16:17:13', '2018-12-14 23:17:13', '2018-12-14 23:17:13'),
(258, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/login', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '16:17:15', '2018-12-14 23:17:15', '2018-12-14 23:17:15'),
(259, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/login', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '16:17:31', '2018-12-14 23:17:31', '2018-12-14 23:17:31'),
(260, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '16:17:32', '2018-12-14 23:17:32', '2018-12-14 23:17:32'),
(261, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '16:17:32', '2018-12-14 23:17:32', '2018-12-14 23:17:32'),
(262, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '16:17:44', '2018-12-14 23:17:44', '2018-12-14 23:17:44'),
(263, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '16:17:45', '2018-12-14 23:17:45', '2018-12-14 23:17:45'),
(264, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '16:17:45', '2018-12-14 23:17:45', '2018-12-14 23:17:45'),
(265, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:45:38', '2018-12-15 01:45:38', '2018-12-15 01:45:38'),
(266, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:45:39', '2018-12-15 01:45:39', '2018-12-15 01:45:39'),
(267, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:45:40', '2018-12-15 01:45:40', '2018-12-15 01:45:40'),
(268, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:46:11', '2018-12-15 01:46:11', '2018-12-15 01:46:11'),
(269, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:46:11', '2018-12-15 01:46:11', '2018-12-15 01:46:11'),
(270, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:46:11', '2018-12-15 01:46:11', '2018-12-15 01:46:11'),
(271, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:46:17', '2018-12-15 01:46:17', '2018-12-15 01:46:17'),
(272, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:46:17', '2018-12-15 01:46:17', '2018-12-15 01:46:17'),
(273, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:46:20', '2018-12-15 01:46:20', '2018-12-15 01:46:20'),
(274, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:47:44', '2018-12-15 01:47:44', '2018-12-15 01:47:44'),
(275, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:47:44', '2018-12-15 01:47:44', '2018-12-15 01:47:44'),
(276, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:47:44', '2018-12-15 01:47:44', '2018-12-15 01:47:44'),
(277, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:47:44', '2018-12-15 01:47:44', '2018-12-15 01:47:44'),
(278, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:47:51', '2018-12-15 01:47:51', '2018-12-15 01:47:51'),
(279, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:47:52', '2018-12-15 01:47:52', '2018-12-15 01:47:52'),
(280, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:47:52', '2018-12-15 01:47:52', '2018-12-15 01:47:52'),
(281, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:47:59', '2018-12-15 01:47:59', '2018-12-15 01:47:59'),
(282, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:48:00', '2018-12-15 01:48:00', '2018-12-15 01:48:00'),
(283, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:48:00', '2018-12-15 01:48:00', '2018-12-15 01:48:00'),
(284, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:48:07', '2018-12-15 01:48:07', '2018-12-15 01:48:07'),
(285, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:48:08', '2018-12-15 01:48:08', '2018-12-15 01:48:08'),
(286, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:48:08', '2018-12-15 01:48:08', '2018-12-15 01:48:08'),
(287, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:48:10', '2018-12-15 01:48:10', '2018-12-15 01:48:10'),
(288, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:48:10', '2018-12-15 01:48:10', '2018-12-15 01:48:10'),
(289, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:48:11', '2018-12-15 01:48:11', '2018-12-15 01:48:11'),
(290, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:49:10', '2018-12-15 01:49:10', '2018-12-15 01:49:10'),
(291, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:49:10', '2018-12-15 01:49:10', '2018-12-15 01:49:10'),
(292, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:49:11', '2018-12-15 01:49:11', '2018-12-15 01:49:11'),
(293, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:50:24', '2018-12-15 01:50:24', '2018-12-15 01:50:24'),
(294, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:50:25', '2018-12-15 01:50:25', '2018-12-15 01:50:25'),
(295, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:50:55', '2018-12-15 01:50:55', '2018-12-15 01:50:55'),
(296, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:50:56', '2018-12-15 01:50:56', '2018-12-15 01:50:56'),
(297, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:50:56', '2018-12-15 01:50:56', '2018-12-15 01:50:56'),
(298, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:50:56', '2018-12-15 01:50:56', '2018-12-15 01:50:56'),
(299, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:51:01', '2018-12-15 01:51:01', '2018-12-15 01:51:01'),
(300, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:51:01', '2018-12-15 01:51:01', '2018-12-15 01:51:01'),
(301, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:51:02', '2018-12-15 01:51:02', '2018-12-15 01:51:02'),
(302, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:51:14', '2018-12-15 01:51:14', '2018-12-15 01:51:14'),
(303, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:51:14', '2018-12-15 01:51:14', '2018-12-15 01:51:14'),
(304, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:52:11', '2018-12-15 01:52:11', '2018-12-15 01:52:11'),
(305, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:52:11', '2018-12-15 01:52:11', '2018-12-15 01:52:11'),
(306, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:52:12', '2018-12-15 01:52:12', '2018-12-15 01:52:12'),
(307, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:52:24', '2018-12-15 01:52:24', '2018-12-15 01:52:24'),
(308, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:52:24', '2018-12-15 01:52:24', '2018-12-15 01:52:24'),
(309, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:52:25', '2018-12-15 01:52:25', '2018-12-15 01:52:25'),
(310, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:56:14', '2018-12-15 01:56:14', '2018-12-15 01:56:14'),
(311, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:56:15', '2018-12-15 01:56:15', '2018-12-15 01:56:15'),
(312, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:56:15', '2018-12-15 01:56:15', '2018-12-15 01:56:15');
INSERT INTO `analytics_visitors` (`id`, `ip`, `city`, `country_code`, `country`, `region`, `full_address`, `location_cor1`, `location_cor2`, `os`, `browser`, `resolution`, `referrer`, `hostname`, `org`, `date`, `time`, `created_at`, `updated_at`) VALUES
(313, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:56:17', '2018-12-15 01:56:17', '2018-12-15 01:56:17'),
(314, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:56:17', '2018-12-15 01:56:17', '2018-12-15 01:56:17'),
(315, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:56:17', '2018-12-15 01:56:17', '2018-12-15 01:56:17'),
(316, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:56:18', '2018-12-15 01:56:18', '2018-12-15 01:56:18'),
(317, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '18:56:19', '2018-12-15 01:56:19', '2018-12-15 01:56:19'),
(318, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:02:18', '2018-12-15 02:02:18', '2018-12-15 02:02:18'),
(319, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:02:18', '2018-12-15 02:02:18', '2018-12-15 02:02:18'),
(320, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:02:19', '2018-12-15 02:02:19', '2018-12-15 02:02:19'),
(321, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:02:35', '2018-12-15 02:02:35', '2018-12-15 02:02:35'),
(322, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:02:44', '2018-12-15 02:02:44', '2018-12-15 02:02:44'),
(323, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:02:45', '2018-12-15 02:02:45', '2018-12-15 02:02:45'),
(324, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:03:43', '2018-12-15 02:03:43', '2018-12-15 02:03:43'),
(325, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:03:46', '2018-12-15 02:03:46', '2018-12-15 02:03:46'),
(326, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:03:48', '2018-12-15 02:03:48', '2018-12-15 02:03:48'),
(327, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:03:49', '2018-12-15 02:03:49', '2018-12-15 02:03:49'),
(328, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:04:00', '2018-12-15 02:04:00', '2018-12-15 02:04:00'),
(329, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:04:19', '2018-12-15 02:04:19', '2018-12-15 02:04:19'),
(330, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:04:42', '2018-12-15 02:04:42', '2018-12-15 02:04:42'),
(331, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/users/1/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:04:42', '2018-12-15 02:04:42', '2018-12-15 02:04:42'),
(332, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/users/1/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:04:57', '2018-12-15 02:04:57', '2018-12-15 02:04:57'),
(333, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:07:18', '2018-12-15 02:07:18', '2018-12-15 02:07:18'),
(334, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/menus/1', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:07:19', '2018-12-15 02:07:19', '2018-12-15 02:07:19'),
(335, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/menus/1', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:08:12', '2018-12-15 02:08:12', '2018-12-15 02:08:12'),
(336, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:09:54', '2018-12-15 02:09:54', '2018-12-15 02:09:54'),
(337, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:09:54', '2018-12-15 02:09:54', '2018-12-15 02:09:54'),
(338, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:09:56', '2018-12-15 02:09:56', '2018-12-15 02:09:56'),
(339, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:10:00', '2018-12-15 02:10:00', '2018-12-15 02:10:00'),
(340, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:10:03', '2018-12-15 02:10:03', '2018-12-15 02:10:03'),
(341, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:10:04', '2018-12-15 02:10:04', '2018-12-15 02:10:04'),
(342, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:14:03', '2018-12-15 02:14:03', '2018-12-15 02:14:03'),
(343, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:14:04', '2018-12-15 02:14:04', '2018-12-15 02:14:04'),
(344, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:14:12', '2018-12-15 02:14:12', '2018-12-15 02:14:12'),
(345, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:14:12', '2018-12-15 02:14:12', '2018-12-15 02:14:12'),
(346, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:14:16', '2018-12-15 02:14:16', '2018-12-15 02:14:16'),
(347, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:14:43', '2018-12-15 02:14:43', '2018-12-15 02:14:43'),
(348, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:14:44', '2018-12-15 02:14:44', '2018-12-15 02:14:44'),
(349, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:14:44', '2018-12-15 02:14:44', '2018-12-15 02:14:44'),
(350, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:14:44', '2018-12-15 02:14:44', '2018-12-15 02:14:44'),
(351, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:16:19', '2018-12-15 02:16:19', '2018-12-15 02:16:19'),
(352, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:16:24', '2018-12-15 02:16:24', '2018-12-15 02:16:24'),
(353, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections/11/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:16:25', '2018-12-15 02:16:25', '2018-12-15 02:16:25'),
(354, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections/11/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:16:39', '2018-12-15 02:16:39', '2018-12-15 02:16:39'),
(355, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections/11/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:16:39', '2018-12-15 02:16:39', '2018-12-15 02:16:39'),
(356, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections/11/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:16:42', '2018-12-15 02:16:42', '2018-12-15 02:16:42'),
(357, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:20:15', '2018-12-15 02:20:15', '2018-12-15 02:20:15'),
(358, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:20:16', '2018-12-15 02:20:16', '2018-12-15 02:20:16'),
(359, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:20:21', '2018-12-15 02:20:21', '2018-12-15 02:20:21'),
(360, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:20:22', '2018-12-15 02:20:22', '2018-12-15 02:20:22'),
(361, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/webmaster/sections', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:20:24', '2018-12-15 02:20:24', '2018-12-15 02:20:24'),
(362, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:21:23', '2018-12-15 02:21:23', '2018-12-15 02:21:23'),
(363, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:21:26', '2018-12-15 02:21:26', '2018-12-15 02:21:26'),
(364, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:21:27', '2018-12-15 02:21:27', '2018-12-15 02:21:27'),
(365, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:21:27', '2018-12-15 02:21:27', '2018-12-15 02:21:27'),
(366, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:21:32', '2018-12-15 02:21:32', '2018-12-15 02:21:32'),
(367, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://bsegypt.net/ad/11/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-14', '19:21:33', '2018-12-15 02:21:33', '2018-12-15 02:21:33'),
(368, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2018-12-14', '21:15:43', '2018-12-14 19:15:43', '2018-12-14 19:15:43'),
(369, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2018-12-14', '21:17:38', '2018-12-14 19:17:38', '2018-12-14 19:17:38'),
(370, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2018-12-14', '21:21:51', '2018-12-14 19:21:51', '2018-12-14 19:21:51'),
(371, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2018-12-14', '21:21:53', '2018-12-14 19:21:53', '2018-12-14 19:21:53'),
(372, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2018-12-14', '21:21:55', '2018-12-14 19:21:55', '2018-12-14 19:21:55'),
(373, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2018-12-14', '21:22:03', '2018-12-14 19:22:03', '2018-12-14 19:22:03'),
(374, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'http://localhost/ch/admin', 'No Hostname', 'unknown', '2018-12-14', '21:22:11', '2018-12-14 19:22:11', '2018-12-14 19:22:11'),
(375, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/11/topics', 'No Hostname', 'unknown', '2018-12-15', '06:33:06', '2018-12-15 04:33:06', '2018-12-15 04:33:06'),
(376, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:00', '2018-12-16 02:39:00', '2018-12-16 02:39:00'),
(377, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:03', '2018-12-16 02:39:03', '2018-12-16 02:39:03'),
(378, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:20', '2018-12-16 02:39:20', '2018-12-16 02:39:20'),
(379, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:20', '2018-12-16 02:39:20', '2018-12-16 02:39:20'),
(380, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:21', '2018-12-16 02:39:21', '2018-12-16 02:39:21'),
(381, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:31', '2018-12-16 02:39:31', '2018-12-16 02:39:31'),
(382, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:32', '2018-12-16 02:39:32', '2018-12-16 02:39:32'),
(383, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:32', '2018-12-16 02:39:32', '2018-12-16 02:39:32'),
(384, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:34', '2018-12-16 02:39:34', '2018-12-16 02:39:34'),
(385, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:39', '2018-12-16 02:39:39', '2018-12-16 02:39:39'),
(386, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:39', '2018-12-16 02:39:39', '2018-12-16 02:39:39'),
(387, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:50', '2018-12-16 02:39:50', '2018-12-16 02:39:50'),
(388, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:39:54', '2018-12-16 02:39:54', '2018-12-16 02:39:54'),
(389, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:40:14', '2018-12-16 02:40:14', '2018-12-16 02:40:14'),
(390, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:42:54', '2018-12-16 02:42:54', '2018-12-16 02:42:54'),
(391, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:42:55', '2018-12-16 02:42:55', '2018-12-16 02:42:55'),
(392, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:43:02', '2018-12-16 02:43:02', '2018-12-16 02:43:02'),
(393, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:43:03', '2018-12-16 02:43:03', '2018-12-16 02:43:03'),
(394, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:43:14', '2018-12-16 02:43:14', '2018-12-16 02:43:14'),
(395, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus/create/1', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:43:15', '2018-12-16 02:43:15', '2018-12-16 02:43:15'),
(396, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus/create/1', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:43:26', '2018-12-16 02:43:26', '2018-12-16 02:43:26'),
(397, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus/1', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:43:29', '2018-12-16 02:43:29', '2018-12-16 02:43:29'),
(398, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus/1', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:44:46', '2018-12-16 02:44:46', '2018-12-16 02:44:46'),
(399, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus/15/edit/1', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:44:46', '2018-12-16 02:44:46', '2018-12-16 02:44:46'),
(400, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus/15/edit/1', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:44:53', '2018-12-16 02:44:53', '2018-12-16 02:44:53'),
(401, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus/1?id=15', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:44:54', '2018-12-16 02:44:54', '2018-12-16 02:44:54'),
(402, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/menus/1?id=15', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:47:46', '2018-12-16 02:47:46', '2018-12-16 02:47:46'),
(403, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:47:47', '2018-12-16 02:47:47', '2018-12-16 02:47:47'),
(404, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:48:25', '2018-12-16 02:48:25', '2018-12-16 02:48:25'),
(405, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:48:25', '2018-12-16 02:48:25', '2018-12-16 02:48:25'),
(406, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:48:26', '2018-12-16 02:48:26', '2018-12-16 02:48:26'),
(407, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:48:26', '2018-12-16 02:48:26', '2018-12-16 02:48:26'),
(408, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:16', '2018-12-16 02:49:16', '2018-12-16 02:49:16'),
(409, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:16', '2018-12-16 02:49:16', '2018-12-16 02:49:16'),
(410, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:16', '2018-12-16 02:49:16', '2018-12-16 02:49:16'),
(411, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:17', '2018-12-16 02:49:17', '2018-12-16 02:49:17'),
(412, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:19', '2018-12-16 02:49:19', '2018-12-16 02:49:19'),
(413, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:22', '2018-12-16 02:49:22', '2018-12-16 02:49:22'),
(414, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:23', '2018-12-16 02:49:23', '2018-12-16 02:49:23'),
(415, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:23', '2018-12-16 02:49:23', '2018-12-16 02:49:23'),
(416, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:23', '2018-12-16 02:49:23', '2018-12-16 02:49:23'),
(417, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:55', '2018-12-16 02:49:55', '2018-12-16 02:49:55'),
(418, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:55', '2018-12-16 02:49:55', '2018-12-16 02:49:55'),
(419, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(420, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(421, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(422, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(423, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(424, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:56', '2018-12-16 02:49:56', '2018-12-16 02:49:56'),
(425, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:57', '2018-12-16 02:49:57', '2018-12-16 02:49:57'),
(426, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:57', '2018-12-16 02:49:57', '2018-12-16 02:49:57'),
(427, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:57', '2018-12-16 02:49:57', '2018-12-16 02:49:57'),
(428, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:49:57', '2018-12-16 02:49:57', '2018-12-16 02:49:57'),
(429, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:00', '2018-12-16 02:50:00', '2018-12-16 02:50:00'),
(430, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:00', '2018-12-16 02:50:00', '2018-12-16 02:50:00'),
(431, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(432, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(433, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(434, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(435, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(436, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(437, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:01', '2018-12-16 02:50:01', '2018-12-16 02:50:01'),
(438, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:02', '2018-12-16 02:50:02', '2018-12-16 02:50:02'),
(439, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:02', '2018-12-16 02:50:02', '2018-12-16 02:50:02'),
(440, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:22', '2018-12-16 02:50:22', '2018-12-16 02:50:22'),
(441, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:22', '2018-12-16 02:50:22', '2018-12-16 02:50:22'),
(442, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(443, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(444, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(445, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(446, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(447, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(448, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:23', '2018-12-16 02:50:23', '2018-12-16 02:50:23'),
(449, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:24', '2018-12-16 02:50:24', '2018-12-16 02:50:24'),
(450, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:24', '2018-12-16 02:50:24', '2018-12-16 02:50:24'),
(451, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(452, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(453, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/55/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(454, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(455, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(456, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29'),
(457, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:29', '2018-12-16 02:50:29', '2018-12-16 02:50:29');
INSERT INTO `analytics_visitors` (`id`, `ip`, `city`, `country_code`, `country`, `region`, `full_address`, `location_cor1`, `location_cor2`, `os`, `browser`, `resolution`, `referrer`, `hostname`, `org`, `date`, `time`, `created_at`, `updated_at`) VALUES
(458, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:30', '2018-12-16 02:50:30', '2018-12-16 02:50:30'),
(459, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:30', '2018-12-16 02:50:30', '2018-12-16 02:50:30'),
(460, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:30', '2018-12-16 02:50:30', '2018-12-16 02:50:30'),
(461, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:31', '2018-12-16 02:50:31', '2018-12-16 02:50:31'),
(462, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:32', '2018-12-16 02:50:32', '2018-12-16 02:50:32'),
(463, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:32', '2018-12-16 02:50:32', '2018-12-16 02:50:32'),
(464, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:32', '2018-12-16 02:50:32', '2018-12-16 02:50:32'),
(465, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:57', '2018-12-16 02:50:57', '2018-12-16 02:50:57'),
(466, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:57', '2018-12-16 02:50:57', '2018-12-16 02:50:57'),
(467, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:57', '2018-12-16 02:50:57', '2018-12-16 02:50:57'),
(468, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:50:58', '2018-12-16 02:50:58', '2018-12-16 02:50:58'),
(469, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:04', '2018-12-16 02:51:04', '2018-12-16 02:51:04'),
(470, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:04', '2018-12-16 02:51:04', '2018-12-16 02:51:04'),
(471, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:05', '2018-12-16 02:51:05', '2018-12-16 02:51:05'),
(472, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:05', '2018-12-16 02:51:05', '2018-12-16 02:51:05'),
(473, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:09', '2018-12-16 02:51:09', '2018-12-16 02:51:09'),
(474, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:09', '2018-12-16 02:51:09', '2018-12-16 02:51:09'),
(475, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:10', '2018-12-16 02:51:10', '2018-12-16 02:51:10'),
(476, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:10', '2018-12-16 02:51:10', '2018-12-16 02:51:10'),
(477, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics/56/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:12', '2018-12-16 02:51:12', '2018-12-16 02:51:12'),
(478, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:14', '2018-12-16 02:51:14', '2018-12-16 02:51:14'),
(479, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:15', '2018-12-16 02:51:15', '2018-12-16 02:51:15'),
(480, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:15', '2018-12-16 02:51:15', '2018-12-16 02:51:15'),
(481, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:15', '2018-12-16 02:51:15', '2018-12-16 02:51:15'),
(482, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/create', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:35', '2018-12-16 02:51:35', '2018-12-16 02:51:35'),
(483, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:36', '2018-12-16 02:51:36', '2018-12-16 02:51:36'),
(484, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:36', '2018-12-16 02:51:36', '2018-12-16 02:51:36'),
(485, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:36', '2018-12-16 02:51:36', '2018-12-16 02:51:36'),
(486, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:45', '2018-12-16 02:51:45', '2018-12-16 02:51:45'),
(487, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:45', '2018-12-16 02:51:45', '2018-12-16 02:51:45'),
(488, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:45', '2018-12-16 02:51:45', '2018-12-16 02:51:45'),
(489, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:46', '2018-12-16 02:51:46', '2018-12-16 02:51:46'),
(490, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/57/edit', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:51:48', '2018-12-16 02:51:48', '2018-12-16 02:51:48'),
(491, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:52:30', '2018-12-16 02:52:30', '2018-12-16 02:52:30'),
(492, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:52:30', '2018-12-16 02:52:30', '2018-12-16 02:52:30'),
(493, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:52:34', '2018-12-16 02:52:34', '2018-12-16 02:52:34'),
(494, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:52:41', '2018-12-16 02:52:41', '2018-12-16 02:52:41'),
(495, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:52:42', '2018-12-16 02:52:42', '2018-12-16 02:52:42'),
(496, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:52:45', '2018-12-16 02:52:45', '2018-12-16 02:52:45'),
(497, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:52:46', '2018-12-16 02:52:46', '2018-12-16 02:52:46'),
(498, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:16', '2018-12-16 02:54:16', '2018-12-16 02:54:16'),
(499, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:16', '2018-12-16 02:54:16', '2018-12-16 02:54:16'),
(500, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:28', '2018-12-16 02:54:28', '2018-12-16 02:54:28'),
(501, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:29', '2018-12-16 02:54:29', '2018-12-16 02:54:29'),
(502, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:29', '2018-12-16 02:54:29', '2018-12-16 02:54:29'),
(503, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:34', '2018-12-16 02:54:34', '2018-12-16 02:54:34'),
(504, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:35', '2018-12-16 02:54:35', '2018-12-16 02:54:35'),
(505, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:35', '2018-12-16 02:54:35', '2018-12-16 02:54:35'),
(506, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:41', '2018-12-16 02:54:41', '2018-12-16 02:54:41'),
(507, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:42', '2018-12-16 02:54:42', '2018-12-16 02:54:42'),
(508, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:42', '2018-12-16 02:54:42', '2018-12-16 02:54:42'),
(509, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:53', '2018-12-16 02:54:53', '2018-12-16 02:54:53'),
(510, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:54:54', '2018-12-16 02:54:54', '2018-12-16 02:54:54'),
(511, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:55:09', '2018-12-16 02:55:09', '2018-12-16 02:55:09'),
(512, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:55:10', '2018-12-16 02:55:10', '2018-12-16 02:55:10'),
(513, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:55:10', '2018-12-16 02:55:10', '2018-12-16 02:55:10'),
(514, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:55:20', '2018-12-16 02:55:20', '2018-12-16 02:55:20'),
(515, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:55:21', '2018-12-16 02:55:21', '2018-12-16 02:55:21'),
(516, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:55:21', '2018-12-16 02:55:21', '2018-12-16 02:55:21'),
(517, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:59:30', '2018-12-16 02:59:30', '2018-12-16 02:59:30'),
(518, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'unknown', NULL, 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '19:59:37', '2018-12-16 02:59:37', '2018-12-16 02:59:37'),
(519, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'unknown', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:00:19', '2018-12-16 03:00:19', '2018-12-16 03:00:19'),
(520, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:00:21', '2018-12-16 03:00:21', '2018-12-16 03:00:21'),
(521, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:00:56', '2018-12-16 03:00:56', '2018-12-16 03:00:56'),
(522, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:00:57', '2018-12-16 03:00:57', '2018-12-16 03:00:57'),
(523, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:01:20', '2018-12-16 03:01:20', '2018-12-16 03:01:20'),
(524, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:01:27', '2018-12-16 03:01:27', '2018-12-16 03:01:27'),
(525, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:01:28', '2018-12-16 03:01:28', '2018-12-16 03:01:28'),
(526, '196.139.12.232', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-15', '20:01:45', '2018-12-16 03:01:45', '2018-12-16 03:01:45'),
(527, '196.139.12.232', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'No Hostname', 'AS36935 Vodafone Data', '2018-12-15', '20:01:47', '2018-12-16 03:01:47', '2018-12-16 03:01:47'),
(528, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:02:45', '2018-12-16 03:02:45', '2018-12-16 03:02:45'),
(529, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:02:46', '2018-12-16 03:02:46', '2018-12-16 03:02:46'),
(530, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:02:46', '2018-12-16 03:02:46', '2018-12-16 03:02:46'),
(531, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:02:51', '2018-12-16 03:02:51', '2018-12-16 03:02:51'),
(532, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:02:52', '2018-12-16 03:02:52', '2018-12-16 03:02:52'),
(533, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:03:04', '2018-12-16 03:03:04', '2018-12-16 03:03:04'),
(534, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:03:48', '2018-12-16 03:03:48', '2018-12-16 03:03:48'),
(535, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:03:49', '2018-12-16 03:03:49', '2018-12-16 03:03:49'),
(536, '156.195.178.199', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.199.178-static.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:03:51', '2018-12-16 03:03:51', '2018-12-16 03:03:51'),
(537, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:04:26', '2018-12-16 03:04:26', '2018-12-16 03:04:26'),
(538, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:04:27', '2018-12-16 03:04:27', '2018-12-16 03:04:27'),
(539, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:04:57', '2018-12-16 03:04:57', '2018-12-16 03:04:57'),
(540, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:04:58', '2018-12-16 03:04:58', '2018-12-16 03:04:58'),
(541, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:04:58', '2018-12-16 03:04:58', '2018-12-16 03:04:58'),
(542, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:05:22', '2018-12-16 03:05:22', '2018-12-16 03:05:22'),
(543, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:05:31', '2018-12-16 03:05:31', '2018-12-16 03:05:31'),
(544, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/26/edit', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:05:32', '2018-12-16 03:05:32', '2018-12-16 03:05:32'),
(545, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/26/edit', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:05:33', '2018-12-16 03:05:33', '2018-12-16 03:05:33'),
(546, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/26/edit', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:05:35', '2018-12-16 03:05:35', '2018-12-16 03:05:35'),
(547, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/4/topics/26/edit', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:05:48', '2018-12-16 03:05:48', '2018-12-16 03:05:48'),
(548, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/5/topics', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:05:54', '2018-12-16 03:05:54', '2018-12-16 03:05:54'),
(549, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:06:01', '2018-12-16 03:06:01', '2018-12-16 03:06:01'),
(550, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/settings', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:17:41', '2018-12-16 03:17:41', '2018-12-16 03:17:41'),
(551, '41.44.119.65', 'unknown', 'EG', 'Egypt', 'unknown', 'unknown, unknown, Egypt', '30.0355', '31.2230', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics', 'host-41.44.119.65.tedata.net', 'AS8452 TE-AS', '2018-12-15', '20:17:43', '2018-12-16 03:17:43', '2018-12-16 03:17:43'),
(552, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:08', '2018-12-17 01:45:08', '2018-12-17 01:45:08'),
(553, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:16', '2018-12-17 01:45:16', '2018-12-17 01:45:16'),
(554, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:23', '2018-12-17 01:45:23', '2018-12-17 01:45:23'),
(555, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:24', '2018-12-17 01:45:24', '2018-12-17 01:45:24'),
(556, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:25', '2018-12-17 01:45:25', '2018-12-17 01:45:25'),
(557, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'unknown', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:37', '2018-12-17 01:45:37', '2018-12-17 01:45:37'),
(558, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/login', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:39', '2018-12-17 01:45:39', '2018-12-17 01:45:39'),
(559, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:44', '2018-12-17 01:45:44', '2018-12-17 01:45:44'),
(560, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:45', '2018-12-17 01:45:45', '2018-12-17 01:45:45'),
(561, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:48', '2018-12-17 01:45:48', '2018-12-17 01:45:48'),
(562, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/login', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:57', '2018-12-17 01:45:57', '2018-12-17 01:45:57'),
(563, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/admin', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:57', '2018-12-17 01:45:57', '2018-12-17 01:45:57'),
(564, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/admin', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:45:58', '2018-12-17 01:45:58', '2018-12-17 01:45:58'),
(565, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:46:00', '2018-12-17 01:46:00', '2018-12-17 01:46:00'),
(566, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/admin', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:46:06', '2018-12-17 01:46:06', '2018-12-17 01:46:06'),
(567, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:46:08', '2018-12-17 01:46:08', '2018-12-17 01:46:08'),
(568, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/8/topics', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:46:53', '2018-12-17 01:46:53', '2018-12-17 01:46:53'),
(569, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/8/topics', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:46:54', '2018-12-17 01:46:54', '2018-12-17 01:46:54'),
(570, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:12', '2018-12-17 01:48:12', '2018-12-17 01:48:12'),
(571, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:12', '2018-12-17 01:48:12', '2018-12-17 01:48:12'),
(572, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:13', '2018-12-17 01:48:13', '2018-12-17 01:48:13'),
(573, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:13', '2018-12-17 01:48:13', '2018-12-17 01:48:13'),
(574, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:19', '2018-12-17 01:48:19', '2018-12-17 01:48:19'),
(575, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/8/topics', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:19', '2018-12-17 01:48:19', '2018-12-17 01:48:19'),
(576, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(577, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/8/topics/create', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(578, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/8/topics/create', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(579, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(580, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/8/topics/create', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:20', '2018-12-17 01:48:20', '2018-12-17 01:48:20'),
(581, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:48:21', '2018-12-17 01:48:21', '2018-12-17 01:48:21'),
(582, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/8/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:50:22', '2018-12-17 01:50:22', '2018-12-17 01:50:22'),
(583, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/8/topics/create', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:50:44', '2018-12-17 01:50:44', '2018-12-17 01:50:44'),
(584, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:03', '2018-12-17 01:51:03', '2018-12-17 01:51:03'),
(585, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:03', '2018-12-17 01:51:03', '2018-12-17 01:51:03'),
(586, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:04', '2018-12-17 01:51:04', '2018-12-17 01:51:04'),
(587, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:04', '2018-12-17 01:51:04', '2018-12-17 01:51:04'),
(588, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://sitecpanel.chamspro.com/8/topics', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:18', '2018-12-17 01:51:18', '2018-12-17 01:51:18'),
(589, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:21', '2018-12-17 01:51:21', '2018-12-17 01:51:21'),
(590, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:28', '2018-12-17 01:51:28', '2018-12-17 01:51:28'),
(591, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:29', '2018-12-17 01:51:29', '2018-12-17 01:51:29'),
(592, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:29', '2018-12-17 01:51:29', '2018-12-17 01:51:29'),
(593, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/54/edit', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:29', '2018-12-17 01:51:29', '2018-12-17 01:51:29'),
(594, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:33', '2018-12-17 01:51:33', '2018-12-17 01:51:33'),
(595, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:49', '2018-12-17 01:51:49', '2018-12-17 01:51:49'),
(596, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:49', '2018-12-17 01:51:49', '2018-12-17 01:51:49'),
(597, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:49', '2018-12-17 01:51:49', '2018-12-17 01:51:49'),
(598, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:49', '2018-12-17 01:51:49', '2018-12-17 01:51:49'),
(599, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:50', '2018-12-17 01:51:50', '2018-12-17 01:51:50'),
(600, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:50', '2018-12-17 01:51:50', '2018-12-17 01:51:50'),
(601, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:51:50', '2018-12-17 01:51:50', '2018-12-17 01:51:50'),
(602, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:52:00', '2018-12-17 01:52:00', '2018-12-17 01:52:00'),
(603, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:52:23', '2018-12-17 01:52:23', '2018-12-17 01:52:23'),
(604, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:52:24', '2018-12-17 01:52:24', '2018-12-17 01:52:24');
INSERT INTO `analytics_visitors` (`id`, `ip`, `city`, `country_code`, `country`, `region`, `full_address`, `location_cor1`, `location_cor2`, `os`, `browser`, `resolution`, `referrer`, `hostname`, `org`, `date`, `time`, `created_at`, `updated_at`) VALUES
(605, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:52:24', '2018-12-17 01:52:24', '2018-12-17 01:52:24'),
(606, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/create', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:52:24', '2018-12-17 01:52:24', '2018-12-17 01:52:24'),
(607, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:54:18', '2018-12-17 01:54:18', '2018-12-17 01:54:18'),
(608, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:54:21', '2018-12-17 01:54:21', '2018-12-17 01:54:21'),
(609, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:54:21', '2018-12-17 01:54:21', '2018-12-17 01:54:21'),
(610, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'unknown', NULL, 'unknown', 'unknown', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:56:42', '2018-12-17 01:56:42', '2018-12-17 01:56:42'),
(611, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'unknown', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:57:02', '2018-12-17 01:57:02', '2018-12-17 01:57:02'),
(612, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Firefox', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:57:07', '2018-12-17 01:57:07', '2018-12-17 01:57:07'),
(613, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'unknown', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:57:52', '2018-12-17 01:57:52', '2018-12-17 01:57:52'),
(614, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:57:52', '2018-12-17 01:57:52', '2018-12-17 01:57:52'),
(615, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:57:53', '2018-12-17 01:57:53', '2018-12-17 01:57:53'),
(616, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:58:03', '2018-12-17 01:58:03', '2018-12-17 01:58:03'),
(617, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:58:07', '2018-12-17 01:58:07', '2018-12-17 01:58:07'),
(618, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/create', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:58:08', '2018-12-17 01:58:08', '2018-12-17 01:58:08'),
(619, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/create', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:58:08', '2018-12-17 01:58:08', '2018-12-17 01:58:08'),
(620, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/create', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:58:08', '2018-12-17 01:58:08', '2018-12-17 01:58:08'),
(621, '156.195.182.158', 'Al \'Ashir min Ramadan', 'EG', 'Egypt', 'unknown', 'unknown, Al \'Ashir min Ramadan, Egypt', '30.2964', '31.7463', 'Windows 8', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/admin', 'host-156.195.158.182-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:58:26', '2018-12-17 01:58:26', '2018-12-17 01:58:26'),
(622, '156.205.235.193', 'Cairo', 'EG', 'Egypt', 'Cairo Governorate', 'Cairo Governorate, Cairo, Egypt', '30.0771', '31.2859', 'Windows 7', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/11/topics/create', 'host-156.205.193.235-static.tedata.net', 'AS8452 TE-AS', '2018-12-16', '18:59:45', '2018-12-17 01:59:45', '2018-12-17 01:59:45'),
(623, '197.43.141.176', 'Suez', 'EG', 'Egypt', 'Suez', 'Suez, Suez, Egypt', '29.9737', '32.5263', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/settings', 'host-197.43.141.176.tedata.net', 'AS8452 TE-AS', '2018-12-16', '22:22:25', '2018-12-17 05:22:25', '2018-12-17 05:22:25'),
(624, '197.43.141.176', 'Suez', 'EG', 'Egypt', 'Suez', 'Suez, Suez, Egypt', '29.9737', '32.5263', 'Android', 'Chrome', 'unknown', 'http://chamspro.com/sitecpanel/login', 'host-197.43.141.176.tedata.net', 'AS8452 TE-AS', '2018-12-16', '22:22:27', '2018-12-17 05:22:27', '2018-12-17 05:22:27'),
(625, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/8/topics/create', 'No Hostname', 'unknown', '2018-12-18', '20:29:39', '2018-12-18 18:29:39', '2018-12-18 18:29:39'),
(626, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/webmaster/sections', 'No Hostname', 'unknown', '2018-12-20', '19:04:41', '2018-12-20 17:04:41', '2018-12-20 17:04:41'),
(627, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/8/topics/create', 'No Hostname', 'unknown', '2018-12-21', '11:40:44', '2018-12-21 09:40:44', '2018-12-21 09:40:44'),
(628, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2018-12-22', '05:04:50', '2018-12-22 03:04:50', '2018-12-22 03:04:50'),
(629, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2018-12-24', '19:10:23', '2018-12-24 17:10:23', '2018-12-24 17:10:23'),
(630, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2019-01-04', '04:41:11', '2019-01-04 02:41:11', '2019-01-04 02:41:11'),
(631, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2019-01-05', '11:25:21', '2019-01-05 09:25:21', '2019-01-05 09:25:21'),
(632, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2019-01-07', '04:13:15', '2019-01-07 02:13:15', '2019-01-07 02:13:15'),
(633, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/', 'No Hostname', 'unknown', '2019-01-11', '12:11:53', '2019-01-11 10:11:53', '2019-01-11 10:11:53'),
(634, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2019-01-12', '11:17:42', '2019-01-12 09:17:42', '2019-01-12 09:17:42'),
(635, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2019-01-12', '11:17:52', '2019-01-12 09:17:52', '2019-01-12 09:17:52'),
(636, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2019-01-21', '18:52:52', '2019-01-21 16:52:52', '2019-01-21 16:52:52'),
(637, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/css/screen.css?v=2', 'No Hostname', 'unknown', '2019-03-09', '13:10:44', '2019-03-09 11:10:44', '2019-03-09 11:10:44'),
(638, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/css/screen.css?v=2', 'No Hostname', 'unknown', '2019-03-11', '20:23:38', '2019-03-11 18:23:38', '2019-03-11 18:23:38'),
(639, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/css/screen.css?v=2', 'No Hostname', 'unknown', '2019-04-12', '12:04:19', '2019-04-12 10:04:19', '2019-04-12 10:04:19'),
(640, '127.0.0.1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8.1', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/css/screen.css?v=2', 'No Hostname', 'unknown', '2019-04-13', '02:44:36', '2019-04-13 00:44:36', '2019-04-13 00:44:36'),
(641, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8.1', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/public/frontEnd/css/screen.css?v=2', 'No Hostname', 'unknown', '2019-04-13', '02:44:36', '2019-04-13 00:44:36', '2019-04-13 00:44:36'),
(642, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8.1', 'Chrome', 'unknown', 'http://localhost/laravel_project-master/view/contact', 'No Hostname', 'unknown', '2019-04-19', '03:10:15', '2019-04-19 01:10:15', '2019-04-19 01:10:15'),
(643, '::1', 'unknown', 'unknown', 'unknown', 'unknown', 'unknown, unknown, unknown', 'unknown', 'unknown', 'Windows 8.1', 'Chrome', 'unknown', 'unknown', 'No Hostname', 'unknown', '2019-08-03', '15:48:48', '2019-08-03 13:48:48', '2019-08-03 13:48:48');

-- --------------------------------------------------------

--
-- Table structure for table `attach_files`
--

CREATE TABLE `attach_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `section_id` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `code` text COLLATE utf8mb4_unicode_ci,
  `file_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_type` tinyint(4) DEFAULT NULL,
  `youtube_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `section_id`, `title_ar`, `title_en`, `details_ar`, `details_en`, `code`, `file_ar`, `file_en`, `video_type`, `youtube_link`, `link_url`, `icon`, `status`, `visits`, `row_no`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'بنر رقم 3', 'Banner #3', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '14888109018814.jpg', '14888109012163.jpg', NULL, NULL, '#', NULL, 1, 0, 3, 1, 1, '2017-03-06 11:06:24', '2017-03-07 18:27:19'),
(2, 1, 'بنر رقم 2', 'Banner #2', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '14888112232028.jpg', '14888112237145.jpg', NULL, NULL, '#', NULL, 1, 0, 2, 1, 1, '2017-03-06 11:06:24', '2017-03-07 18:27:19'),
(3, 2, 'تصميم ريسبونسف', 'Responsive Design', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '', '', NULL, NULL, '#', 'fa-object-group', 1, 0, 1, 1, NULL, '2017-03-06 11:06:24', '2017-03-07 18:27:19'),
(4, 2, ' احدث التقنيات', 'HTML5 & CSS3', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '', '', NULL, NULL, '#', 'fa-html5', 1, 0, 2, 1, NULL, '2017-03-06 11:06:24', '2017-03-07 18:27:19'),
(5, 2, 'باستخدام بوتستراب', 'Bootstrap Used', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '', '', NULL, NULL, '#', 'fa-code', 1, 0, 3, 1, NULL, '2017-03-06 11:06:24', '2017-03-07 18:27:19'),
(6, 2, 'تصميم كلاسيكي', 'Classic Design', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '', '', NULL, NULL, '#', 'fa-laptop', 1, 0, 4, 1, NULL, '2017-03-06 11:06:24', '2017-03-07 18:27:19'),
(7, 1, 'بنر رقم 1', 'Banner #1', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', NULL, '14888126419392.jpg', '14888126415336.jpg', NULL, NULL, '#', NULL, 1, 0, 1, 1, 1, '2017-03-06 13:04:01', '2017-03-07 18:27:19'),
(8, 3, 'بنر جانبي تجريبي', 'Side banner sample', NULL, NULL, NULL, '14888184555359.png', '14888184559632.png', NULL, NULL, '#', NULL, 1, 0, 5, 1, 1, '2017-03-06 14:25:52', '2017-03-07 18:27:19');

-- --------------------------------------------------------

--
-- Table structure for table `cau_accounts`
--

CREATE TABLE `cau_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_trans_id` int(10) UNSIGNED DEFAULT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_avd_photos`
--

CREATE TABLE `cau_avd_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_approv` date DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_trans_id` int(11) DEFAULT NULL,
  `width` tinyint(4) DEFAULT NULL,
  `height` tinyint(4) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `format` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_id` int(10) UNSIGNED DEFAULT NULL,
  `photo` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_classes`
--

CREATE TABLE `cau_classes` (
  `class_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_trans_id` int(10) UNSIGNED DEFAULT NULL,
  `description` varchar(800) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_trans_id` int(10) UNSIGNED DEFAULT NULL,
  `doable` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='charity classes (tree nodes)';

-- --------------------------------------------------------

--
-- Table structure for table `cau_classtree`
--

CREATE TABLE `cau_classtree` (
  `id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `grand_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_compaigns`
--

CREATE TABLE `cau_compaigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `responsible` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title_trans_id` int(10) UNSIGNED DEFAULT NULL,
  `slogan` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slogan_trans_id` int(10) UNSIGNED DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_id` int(10) UNSIGNED DEFAULT NULL,
  `photo` int(10) UNSIGNED DEFAULT NULL,
  `photo_gal` int(10) UNSIGNED DEFAULT NULL,
  `video_gal` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_countries`
--

CREATE TABLE `cau_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `abrv` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name_trans_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_country_states`
--

CREATE TABLE `cau_country_states` (
  `id` int(10) UNSIGNED NOT NULL,
  `country` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_trans_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_photogal_info`
--

CREATE TABLE `cau_photogal_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_trans_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_photogal_jo`
--

CREATE TABLE `cau_photogal_jo` (
  `id` int(10) UNSIGNED NOT NULL,
  `photogal_info` int(10) UNSIGNED NOT NULL,
  `photo` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_photos`
--

CREATE TABLE `cau_photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_trans_id` int(10) UNSIGNED DEFAULT NULL,
  `width` int(10) UNSIGNED DEFAULT NULL,
  `height` int(10) UNSIGNED DEFAULT NULL,
  `size` int(10) UNSIGNED DEFAULT NULL,
  `format` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `notes` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes_trans_id` int(10) UNSIGNED DEFAULT NULL,
  `photo` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_projects`
--

CREATE TABLE `cau_projects` (
  `id` int(11) NOT NULL,
  `project_code` varchar(225) NOT NULL,
  `class` int(11) NOT NULL,
  `sponsor` int(11) NOT NULL,
  `date_aprroved` date NOT NULL,
  `approval_statement` varchar(225) NOT NULL,
  `country_id` int(11) NOT NULL,
  `recipient_ch` int(11) NOT NULL,
  `title` varchar(225) DEFAULT NULL,
  `title_trans_id` varchar(225) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `account` int(11) NOT NULL,
  `approval_statement_num` int(11) NOT NULL,
  `citiy_id` int(11) NOT NULL,
  `share_price` int(11) NOT NULL,
  `shares_max` int(11) NOT NULL,
  `shares_count` int(11) NOT NULL,
  `date_open` date NOT NULL,
  `date_close` date NOT NULL,
  `slogan` varchar(255) DEFAULT NULL,
  `slogan_trans_id` int(11) DEFAULT NULL,
  `description_short` varchar(200) DEFAULT NULL,
  `description_short_trans_id` int(11) DEFAULT NULL,
  `description` int(11) DEFAULT NULL,
  `description_trans_id` int(11) DEFAULT NULL,
  `compaign` int(11) NOT NULL,
  `adv_photo` int(11) NOT NULL,
  `thump_photo` int(11) NOT NULL,
  `photp_gal` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `adv_video` int(11) NOT NULL,
  `video_gal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cau_projects`
--

INSERT INTO `cau_projects` (`id`, `project_code`, `class`, `sponsor`, `date_aprroved`, `approval_statement`, `country_id`, `recipient_ch`, `title`, `title_trans_id`, `status`, `account`, `approval_statement_num`, `citiy_id`, `share_price`, `shares_max`, `shares_count`, `date_open`, `date_close`, `slogan`, `slogan_trans_id`, `description_short`, `description_short_trans_id`, `description`, `description_trans_id`, `compaign`, `adv_photo`, `thump_photo`, `photp_gal`, `topic_id`, `adv_video`, `video_gal`, `created_at`, `updated_at`) VALUES
(1, '54kkn', 56, 65, '2018-12-20', 'jj5hjhjghj', 60, 66, NULL, NULL, 67, 69, 3445, 64, 256, 15, 165, '2018-12-20', '2018-12-20', NULL, NULL, NULL, NULL, NULL, NULL, 70, 71, 72, 73, 87, 75, 75, '2018-12-21 12:02:06', '2018-12-21 12:02:06'),
(2, '54kkn', 58, 0, '2018-12-20', 'dasdasdasdasds', 60, 0, NULL, NULL, 67, 69, 6564, 63, 515644, 4654, 5464, '2018-12-20', '2018-12-20', NULL, NULL, NULL, NULL, NULL, NULL, 70, 71, 72, 73, 88, 75, 0, '2018-12-21 12:23:32', '2018-12-22 07:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `cau_proj_status`
--

CREATE TABLE `cau_proj_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'مفتوح',
  `status_trans_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_recipients_ch`
--

CREATE TABLE `cau_recipients_ch` (
  `id` int(10) UNSIGNED NOT NULL,
  `cntry_state` int(10) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_trans_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_videogal_info`
--

CREATE TABLE `cau_videogal_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_trans_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_videogal_jo`
--

CREATE TABLE `cau_videogal_jo` (
  `id` int(10) UNSIGNED NOT NULL,
  `videogal_info` int(10) UNSIGNED NOT NULL,
  `video` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cau_videos`
--

CREATE TABLE `cau_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` date DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_trans_id` int(10) UNSIGNED NOT NULL,
  `width` int(10) UNSIGNED DEFAULT NULL,
  `height` int(10) UNSIGNED DEFAULT NULL,
  `size` int(10) UNSIGNED DEFAULT NULL,
  `length` smallint(5) UNSIGNED DEFAULT NULL,
  `youtube_path` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `format` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` longblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_communities`
--

CREATE TABLE `char_communities` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `date_started` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_comm_emp_jo`
--

CREATE TABLE `char_comm_emp_jo` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee` int(10) UNSIGNED NOT NULL,
  `community` int(10) UNSIGNED NOT NULL,
  `date_join` date DEFAULT NULL,
  `date_leave` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_comm_leaders`
--

CREATE TABLE `char_comm_leaders` (
  `id` int(10) UNSIGNED NOT NULL,
  `community` int(10) UNSIGNED NOT NULL,
  `leader` int(10) UNSIGNED NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_departments`
--

CREATE TABLE `char_departments` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dep_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_started` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_dept_emp_jo`
--

CREATE TABLE `char_dept_emp_jo` (
  `id` int(10) UNSIGNED NOT NULL,
  `employee` int(10) UNSIGNED NOT NULL,
  `department` int(10) UNSIGNED NOT NULL,
  `date_join` date DEFAULT NULL,
  `date_leave` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_dept_managers`
--

CREATE TABLE `char_dept_managers` (
  `id` int(10) UNSIGNED NOT NULL,
  `department` int(10) UNSIGNED NOT NULL,
  `manager` int(10) UNSIGNED NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_employees`
--

CREATE TABLE `char_employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `emp_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `fisrt_bame` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `middel_bame` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `family_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('M','F','','') COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `hire_date` date NOT NULL,
  `photo` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_employee_job_jo`
--

CREATE TABLE `char_employee_job_jo` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` int(11) UNSIGNED NOT NULL,
  `employee` int(10) UNSIGNED NOT NULL,
  `department` int(10) UNSIGNED DEFAULT NULL,
  `community` int(10) UNSIGNED DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `description` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_job_titles`
--

CREATE TABLE `char_job_titles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` date DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_members`
--

CREATE TABLE `char_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `membership` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `char_membership_types`
--

CREATE TABLE `char_membership_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `annual_fee` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `topic_id`, `name`, `email`, `date`, `comment`, `status`, `row_no`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 9, 'Roza Hesham', 'email@site.com', '2017-03-06 15:55:21', 'Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti.', 1, 1, NULL, NULL, '2017-03-06 13:55:21', '2017-03-06 13:55:21'),
(2, 9, 'Adam Ali', 'emm@site.com', '2017-03-06 15:55:59', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.', 1, 2, NULL, NULL, '2017-03-06 13:55:59', '2017-03-06 13:55:59');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `group_id`, `first_name`, `last_name`, `company`, `email`, `password`, `phone`, `country_id`, `city`, `address`, `photo`, `notes`, `last_login`, `last_login_ip`, `facebook_id`, `twitter_id`, `google_id`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 2, 'Sara', 'Smith', 'HiMan Company', 'email@site.com', NULL, '234-245-5674', 68, NULL, NULL, '14889022279857.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2017-03-07 13:57:07', '2017-03-07 13:57:07'),
(2, 2, 'Maro', 'Faheed', 'Havway  Company', 'email@site.com', NULL, '386-755-7788', 30, NULL, NULL, '14889022842486.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2017-03-07 13:58:04', '2017-03-07 13:58:35'),
(3, 2, 'Adam', 'Ali', 'Trident company', 'email@site.com', NULL, '589-234-2342', 35, NULL, NULL, '14889023586327.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2017-03-07 13:59:08', '2017-03-07 13:59:18'),
(4, 2, 'Donal', 'Tashee', 'Hamer school', 'email@site.com', NULL, '674-274-8944', 41, NULL, NULL, '14889024177699.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2017-03-07 14:00:17', '2017-03-07 14:00:17'),
(5, NULL, 'Mona', 'Lamen', 'Troly Company', 'email@site.com', NULL, '324-674-4578', 10, 'Moco', NULL, '14889024974798.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, '2017-03-07 14:01:37', '2017-03-07 14:01:37');

-- --------------------------------------------------------

--
-- Table structure for table `contacts_groups`
--

CREATE TABLE `contacts_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts_groups`
--

INSERT INTO `contacts_groups` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Newsletter Emails', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(2, 'VIP', 1, NULL, '2017-03-07 13:56:11', '2017-03-07 13:56:11'),
(3, 'Customers', 1, NULL, '2017-03-07 13:56:24', '2017-03-07 13:56:24');

-- --------------------------------------------------------

--
-- Table structure for table `contactus`
--

CREATE TABLE `contactus` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `user_message` varchar(250) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contactus`
--

INSERT INTO `contactus` (`id`, `name`, `email`, `user_message`, `updated_at`, `created_at`) VALUES
(1, 'sdfds', 'dsfds', '', '2019-04-13 03:11:36', '2019-04-13 03:11:36'),
(2, 'Hazem', 'h.elshenawy93@gmail.com', '', '2019-04-13 03:12:21', '2019-04-13 03:12:21'),
(3, 'Hazem', 'h.elshenawy93@gmail.com', '', '2019-04-13 03:13:17', '2019-04-13 03:13:17'),
(4, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-13 03:13:30', '2019-04-13 03:13:30'),
(5, 'الاميريه', 'admin@Site.com', '', '2019-04-19 01:11:18', '2019-04-19 01:11:18'),
(6, 'الاميريه', 'admin@Site.com', '', '2019-04-19 01:11:29', '2019-04-19 01:11:29'),
(7, 'الاميريه', 'admin@Site.com', '', '2019-04-19 01:15:33', '2019-04-19 01:15:33'),
(8, 'الاميريه', 'admin@Site.com', '', '2019-04-19 01:15:47', '2019-04-19 01:15:47'),
(9, 'الاميريه', 'admin@Site.com', '', '2019-04-19 01:17:49', '2019-04-19 01:17:49'),
(10, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:18:04', '2019-04-19 01:18:04'),
(11, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:18:31', '2019-04-19 01:18:31'),
(12, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:18:59', '2019-04-19 01:18:59'),
(13, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:19:41', '2019-04-19 01:19:41'),
(14, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:19:55', '2019-04-19 01:19:55'),
(15, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:19:55', '2019-04-19 01:19:55'),
(16, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:21:25', '2019-04-19 01:21:25'),
(17, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:21:51', '2019-04-19 01:21:51'),
(18, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:22:34', '2019-04-19 01:22:34'),
(19, 'الاميريه', 'h.elshenawy93@gmail.com', '', '2019-04-19 01:22:47', '2019-04-19 01:22:47'),
(20, 'esalm elshenawy', 'islam.elshenawy@trioconceptme.com', '', '2019-08-09 11:16:35', '2019-08-09 11:16:35');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `title_ar`, `title_en`, `tel`, `created_at`, `updated_at`) VALUES
(1, 'AL', 'ألبانيا', 'Albania', '355', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(2, 'DZ', 'الجزائر', 'Algeria', '213', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(3, 'AS', 'ساموا الأمريكية', 'American Samoa', '1-684', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(4, 'AD', 'أندورا', 'Andorra', '376', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(5, 'AO', 'أنغولا', 'Angola', '244', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(6, 'AI', 'أنغيلا', 'Anguilla', '1-264', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(7, 'AR', 'الأرجنتين', 'Argentina', '54', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(8, 'AM', 'أرمينيا', 'Armenia', '374', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(9, 'AW', 'أروبا', 'Aruba', '297', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(10, 'AU', 'أستراليا', 'Australia', '61', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(11, 'AT', 'النمسا', 'Austria', '43', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(12, 'AZ', 'أذربيجان', 'Azerbaijan', '994', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(13, 'BS', 'جزر البهاما', 'Bahamas', '1-242', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(14, 'BH', 'البحرين', 'Bahrain', '973', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(15, 'BD', 'بنغلاديش', 'Bangladesh', '880', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(16, 'BB', 'بربادوس', 'Barbados', '1-246', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(17, 'BY', 'روسيا البيضاء', 'Belarus', '375', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(18, 'BE', 'بلجيكا', 'Belgium', '32', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(19, 'BZ', 'بليز', 'Belize', '501', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(20, 'BJ', 'بنين', 'Benin', '229', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(21, 'BM', 'برمودا', 'Bermuda', '1-441', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(22, 'BT', 'بوتان', 'Bhutan', '975', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(23, 'BO', 'بوليفيا', 'Bolivia', '591', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(24, 'BA', 'البوسنة والهرسك', 'Bosnia and Herzegovina', '387', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(25, 'BW', 'بوتسوانا', 'Botswana', '267', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(26, 'BR', 'البرازيل', 'Brazil', '55', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(27, 'VG', 'جزر فيرجن البريطانية', 'British Virgin Islands', '1-284', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(28, 'IO', 'إقليم المحيط الهندي البريطاني', 'British Indian Ocean Territory', '246', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(29, 'BN', 'بروناي دار السلام', 'Brunei Darussalam', '673', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(30, 'BG', 'بلغاريا', 'Bulgaria', '359', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(31, 'BF', 'بوركينا فاسو', 'Burkina Faso', '226', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(32, 'BI', 'بوروندي', 'Burundi', '257', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(33, 'KH', 'كمبوديا', 'Cambodia', '855', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(34, 'CM', 'الكاميرون', 'Cameroon', '237', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(35, 'CA', 'كندا', 'Canada', '1', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(36, 'CV', 'الرأس الأخضر', 'Cape Verde', '238', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(37, 'KY', 'جزر كايمان', 'Cayman Islands', '1-345', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(38, 'CF', 'افريقيا الوسطى', 'Central African Republic', '236', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(39, 'TD', 'تشاد', 'Chad', '235', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(40, 'CL', 'تشيلي', 'Chile', '56', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(41, 'CN', 'الصين', 'China', '86', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(42, 'HK', 'هونغ كونغ', 'Hong Kong', '852', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(43, 'MO', 'ماكاو', 'Macao', '853', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(44, 'CX', 'جزيرة الكريسماس', 'Christmas Island', '61', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(45, 'CC', 'جزر كوكوس (كيلينغ)', 'Cocos (Keeling) Islands', '61', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(46, 'CO', 'كولومبيا', 'Colombia', '57', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(47, 'KM', 'جزر القمر', 'Comoros', '269', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(48, 'CK', 'جزر كوك', 'Cook Islands', '682', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(49, 'CR', 'كوستا ريكا', 'Costa Rica', '506', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(50, 'HR', 'كرواتيا', 'Croatia', '385', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(51, 'CU', 'كوبا', 'Cuba', '53', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(52, 'CY', 'قبرص', 'Cyprus', '357', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(53, 'CZ', 'الجمهورية التشيكية', 'Czech Republic', '420', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(54, 'DK', 'الدنمارك', 'Denmark', '45', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(55, 'DJ', 'جيبوتي', 'Djibouti', '253', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(56, 'DM', 'دومينيكا', 'Dominica', '1-767', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(57, 'DO', 'جمهورية الدومينيكان', 'Dominican Republic', '1-809', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(58, 'EC', 'الاكوادور', 'Ecuador', '593', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(59, 'EG', 'مصر', 'Egypt', '20', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(60, 'SV', 'السلفادور', 'El Salvador', '503', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(61, 'GQ', 'غينيا الاستوائية', 'Equatorial Guinea', '240', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(62, 'ER', 'إريتريا', 'Eritrea', '291', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(63, 'EE', 'استونيا', 'Estonia', '372', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(64, 'ET', 'أثيوبيا', 'Ethiopia', '251', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(65, 'FO', 'جزر فارو', 'Faroe Islands', '298', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(66, 'FJ', 'فيجي', 'Fiji', '679', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(67, 'FI', 'فنلندا', 'Finland', '358', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(68, 'FR', 'فرنسا', 'France', '33', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(69, 'GF', 'جيانا الفرنسية', 'French Guiana', '689', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(70, 'GA', 'الغابون', 'Gabon', '241', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(71, 'GM', 'غامبيا', 'Gambia', '220', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(72, 'GE', 'جورجيا', 'Georgia', '995', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(73, 'DE', 'ألمانيا', 'Germany', '49', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(74, 'GH', 'غانا', 'Ghana', '233', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(75, 'GI', 'جبل طارق', 'Gibraltar', '350', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(76, 'GR', 'يونان', 'Greece', '30', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(77, 'GL', 'غرينلاند', 'Greenland', '299', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(78, 'GD', 'غرينادا', 'Grenada', '1-473', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(79, 'GU', 'غوام', 'Guam', '1-671', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(80, 'GT', 'غواتيمالا', 'Guatemala', '502', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(81, 'GN', 'غينيا', 'Guinea', '224', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(82, 'GW', 'غينيا-بيساو', 'Guinea-Bissau', '245', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(83, 'GY', 'غيانا', 'Guyana', '592', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(84, 'HT', 'هايتي', 'Haiti', '509', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(85, 'HN', 'هندوراس', 'Honduras', '504', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(86, 'HU', 'هنغاريا', 'Hungary', '36', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(87, 'IS', 'أيسلندا', 'Iceland', '354', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(88, 'IN', 'الهند', 'India', '91', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(89, 'ID', 'أندونيسيا', 'Indonesia', '62', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(90, 'IR', 'جمهورية إيران الإسلامية', 'Iran, Islamic Republic of', '98', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(91, 'IQ', 'العراق', 'Iraq', '964', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(92, 'IE', 'أيرلندا', 'Ireland', '353', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(93, 'IM', 'جزيرة مان', 'Isle of Man', '44-1624', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(94, 'IL', 'إسرائيل', 'Israel', '972', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(95, 'IT', 'إيطاليا', 'Italy', '39', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(96, 'JM', 'جامايكا', 'Jamaica', '1-876', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(97, 'JP', 'اليابان', 'Japan', '81', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(98, 'JE', 'جيرسي', 'Jersey', '44-1534', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(99, 'JO', 'الأردن', 'Jordan', '962', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(100, 'KZ', 'كازاخستان', 'Kazakhstan', '7', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(101, 'KE', 'كينيا', 'Kenya', '254', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(102, 'KI', 'كيريباس', 'Kiribati', '686', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(103, 'KW', 'الكويت', 'Kuwait', '965', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(104, 'KG', 'قيرغيزستان', 'Kyrgyzstan', '996', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(105, 'LV', 'لاتفيا', 'Latvia', '371', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(106, 'LB', 'لبنان', 'Lebanon', '961', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(107, 'LS', 'ليسوتو', 'Lesotho', '266', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(108, 'LR', 'ليبيريا', 'Liberia', '231', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(109, 'LY', 'ليبيا', 'Libya', '218', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(110, 'LI', 'ليشتنشتاين', 'Liechtenstein', '423', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(111, 'LT', 'ليتوانيا', 'Lithuania', '370', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(112, 'LU', 'لوكسمبورغ', 'Luxembourg', '352', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(113, 'MK', 'مقدونيا، جمهورية', 'Macedonia', '389', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(114, 'MG', 'مدغشقر', 'Madagascar', '261', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(115, 'MW', 'ملاوي', 'Malawi', '265', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(116, 'MY', 'ماليزيا', 'Malaysia', '60', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(117, 'MV', 'جزر المالديف', 'Maldives', '960', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(118, 'ML', 'مالي', 'Mali', '223', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(119, 'MT', 'مالطا', 'Malta', '356', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(120, 'MH', 'جزر مارشال', 'Marshall Islands', '692', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(121, 'MR', 'موريتانيا', 'Mauritania', '222', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(122, 'MU', 'موريشيوس', 'Mauritius', '230', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(123, 'YT', 'مايوت', 'Mayotte', '262', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(124, 'MX', 'المكسيك', 'Mexico', '52', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(125, 'FM', 'ولايات ميكرونيزيا الموحدة', 'Micronesia', '691', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(126, 'MD', 'مولدوفا', 'Moldova', '373', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(127, 'MC', 'موناكو', 'Monaco', '377', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(128, 'MN', 'منغوليا', 'Mongolia', '976', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(129, 'ME', 'الجبل الأسود', 'Montenegro', '382', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(130, 'MS', 'مونتسيرات', 'Montserrat', '1-664', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(131, 'MA', 'المغرب', 'Morocco', '212', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(132, 'MZ', 'موزمبيق', 'Mozambique', '258', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(133, 'MM', 'ميانمار', 'Myanmar', '95', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(134, 'NA', 'ناميبيا', 'Namibia', '264', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(135, 'NR', 'ناورو', 'Nauru', '674', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(136, 'NP', 'نيبال', 'Nepal', '977', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(137, 'NL', 'هولندا', 'Netherlands', '31', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(138, 'AN', 'جزر الأنتيل الهولندية', 'Netherlands Antilles', '599', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(139, 'NC', 'كاليدونيا الجديدة', 'New Caledonia', '687', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(140, 'NZ', 'نيوزيلندا', 'New Zealand', '64', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(141, 'NI', 'نيكاراغوا', 'Nicaragua', '505', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(142, 'NE', 'النيجر', 'Niger', '227', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(143, 'NG', 'نيجيريا', 'Nigeria', '234', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(144, 'NU', 'نيوي', 'Niue', '683', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(145, 'NO', 'النرويج', 'Norway', '47', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(146, 'OM', 'عمان', 'Oman', '968', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(147, 'PK', 'باكستان', 'Pakistan', '92', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(148, 'PW', 'بالاو', 'Palau', '680', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(149, 'PS', 'فلسطين', 'Palestinian', '972', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(150, 'PA', 'بناما', 'Panama', '507', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(151, 'PY', 'باراغواي', 'Paraguay', '595', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(152, 'PE', 'بيرو', 'Peru', '51', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(153, 'PH', 'الفلبين', 'Philippines', '63', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(154, 'PN', 'بيتكيرن', 'Pitcairn', '870', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(155, 'PL', 'بولندا', 'Poland', '48', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(156, 'PT', 'البرتغال', 'Portugal', '351', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(157, 'PR', 'بويرتو ريكو', 'Puerto Rico', '1-787', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(158, 'QA', 'قطر', 'Qatar', '974', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(159, 'RO', 'رومانيا', 'Romania', '40', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(160, 'RU', 'الفيدرالية الروسية', 'Russian Federation', '7', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(161, 'RW', 'رواندا', 'Rwanda', '250', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(162, 'SH', 'سانت هيلينا', 'Saint Helena', '290', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(163, 'KN', 'سانت كيتس ونيفيس', 'Saint Kitts and Nevis', '1-869', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(164, 'LC', 'سانت لوسيا', 'Saint Lucia', '1-758', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(165, 'PM', 'سان بيار وميكلون', 'Saint Pierre and Miquelon', '508', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(166, 'VC', 'سانت فنسنت وجزر غرينادين', 'Saint Vincent and Grenadines', '1-784', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(167, 'WS', 'ساموا', 'Samoa', '685', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(168, 'SM', 'سان مارينو', 'San Marino', '378', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(169, 'ST', 'ساو تومي وبرينسيبي', 'Sao Tome and Principe', '239', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(170, 'SA', 'المملكة العربية السعودية', 'Saudi Arabia', '966', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(171, 'SN', 'السنغال', 'Senegal', '221', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(172, 'RS', 'صربيا', 'Serbia', '381', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(173, 'SC', 'سيشيل', 'Seychelles', '248', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(174, 'SL', 'سيرا ليون', 'Sierra Leone', '232', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(175, 'SG', 'سنغافورة', 'Singapore', '65', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(176, 'SK', 'سلوفاكيا', 'Slovakia', '421', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(177, 'SI', 'سلوفينيا', 'Slovenia', '386', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(178, 'SB', 'جزر سليمان', 'Solomon Islands', '677', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(179, 'SO', 'الصومال', 'Somalia', '252', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(180, 'ZA', 'جنوب أفريقيا', 'South Africa', '27', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(181, 'ES', 'إسبانيا', 'Spain', '34', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(182, 'LK', 'سيريلانكا', 'Sri Lanka', '94', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(183, 'SD', 'السودان', 'Sudan', '249', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(184, 'SR', 'سورينام', 'Suriname', '597', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(185, 'SJ', 'جزر سفالبارد وجان ماين', 'Svalbard and Jan Mayen Islands', '47', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(186, 'SZ', 'سوازيلاند', 'Swaziland', '268', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(187, 'SE', 'السويد', 'Sweden', '46', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(188, 'CH', 'سويسرا', 'Switzerland', '41', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(189, 'SY', 'سوريا', 'Syrian Arab Republic', '963', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(190, 'TW', 'تايوان، جمهورية الصين', 'Taiwan, Republic of China', '886', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(191, 'TJ', 'طاجيكستان', 'Tajikistan', '992', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(192, 'TZ', 'تنزانيا', 'Tanzania', '255', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(193, 'TH', 'تايلاند', 'Thailand', '66', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(194, 'TG', 'توغو', 'Togo', '228', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(195, 'TK', 'توكيلاو', 'Tokelau', '690', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(196, 'TO', 'تونغا', 'Tonga', '676', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(197, 'TT', 'ترينداد وتوباغو', 'Trinidad and Tobago', '1-868', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(198, 'TN', 'تونس', 'Tunisia', '216', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(199, 'TR', 'تركيا', 'Turkey', '90', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(200, 'TM', 'تركمانستان', 'Turkmenistan', '993', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(201, 'TC', 'جزر تركس وكايكوس', 'Turks and Caicos Islands', '1-649', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(202, 'TV', 'توفالو', 'Tuvalu', '688', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(203, 'UG', 'أوغندا', 'Uganda', '256', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(204, 'UA', 'أوكرانيا', 'Ukraine', '380', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(205, 'AE', 'الإمارات العربية المتحدة', 'United Arab Emirates', '971', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(206, 'GB', 'المملكة المتحدة', 'United Kingdom', '44', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(207, 'US', 'الولايات المتحدة الأمريكية', 'United States of America', '1', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(208, 'UY', 'أوروغواي', 'Uruguay', '598', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(209, 'UZ', 'أوزبكستان', 'Uzbekistan', '998', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(210, 'VU', 'فانواتو', 'Vanuatu', '678', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(211, 'VE', 'فنزويلا', 'Venezuela', '58', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(212, 'VN', 'فيتنام', 'Viet Nam', '84', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(213, 'WF', 'واليس وفوتونا', 'Wallis and Futuna Islands', '681', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(214, 'YE', 'اليمن', 'Yemen', '967', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(215, 'ZM', 'زامبيا', 'Zambia', '260', '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(216, 'ZW', 'زيمبابوي', 'Zimbabwe', '263', '2017-11-08 13:25:54', '2017-11-08 13:25:54');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `user_id`, `type`, `title`, `details`, `start_date`, `end_date`, `color`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'test note to my calendar', 'this is a test note to my calendar', '2017-12-07 00:00:00', '2017-12-07 00:00:00', NULL, 1, NULL, '2017-03-07 14:06:32', '2017-03-07 14:06:32'),
(2, 1, 1, 'meeting test for multi purposes', 'meeting test for multi purposes meeting test for multi purposes', '2017-11-07 16:03:00', '2017-11-07 16:03:00', NULL, 1, NULL, '2017-03-07 14:07:06', '2017-03-07 14:07:06'),
(3, 1, 2, 'Test the events on calendar', 'sample to test', '2017-12-07 16:00:00', '2017-12-07 18:00:00', NULL, 1, NULL, '2017-03-07 14:07:53', '2017-03-07 14:07:53'),
(4, 1, 3, 'Site task test will take half month', 'test task', '2017-11-07 00:00:00', '2017-11-22 00:00:00', NULL, 1, NULL, '2017-03-07 14:08:53', '2017-03-07 14:08:53'),
(5, 1, 0, 'my test note i just test', 'my test note i just test', '2017-09-22 00:00:00', '2017-09-22 00:00:00', NULL, 1, NULL, '2017-03-07 14:09:26', '2017-03-07 14:09:26');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `key`, `updated_at`, `created_at`, `status`) VALUES
(1, 'العربيه ', '0', '2018-12-15 15:23:20', '2018-12-15 15:23:20', 0),
(2, 'الانجليزيه', '0', '2018-12-15 15:23:33', '2018-12-15 15:23:33', 0),
(3, 'الاسبانية', '', '2018-12-15 15:24:30', '2018-12-15 15:24:30', 0),
(4, 'الانجليزيه', '', '2018-12-15 15:25:57', '2018-12-15 15:25:57', 0),
(5, 'الانجليزيه', 'en', '2018-12-15 15:27:47', '2018-12-15 15:27:47', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lang_language_info`
--

CREATE TABLE `lang_language_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `direction` enum('RTL','LTR','','') COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lang_site_langusges`
--

CREATE TABLE `lang_site_langusges` (
  `id` int(10) UNSIGNED NOT NULL,
  `lang` int(10) UNSIGNED NOT NULL,
  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ltm_translations`
--

CREATE TABLE `ltm_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ltm_translations`
--

INSERT INTO `ltm_translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 'backLang', 'APIKeyGenerate', 'Generate a new API key', '2017-11-09 18:52:11', '2018-12-15 01:54:02'),
(2, 0, 'en', 'validation', 'custom.subscribe_name.required', NULL, '2017-11-12 02:57:33', '2017-11-12 02:57:33'),
(3, 0, 'en', 'validation', 'attributes', NULL, '2017-11-12 02:57:33', '2017-11-12 02:57:33'),
(4, 0, 'en', 'validation', 'custom.subscribe_email.required', NULL, '2017-11-12 02:57:33', '2017-11-12 02:57:33'),
(5, 0, 'en', 'validation', 'custom.api_key.required', NULL, '2017-11-12 02:59:17', '2017-11-12 02:59:17'),
(6, 0, 'en', 'validation', 'custom.subscribe_email.email', NULL, '2017-11-12 03:00:18', '2017-11-12 03:00:18'),
(7, 1, 'ar', 'pagination', 'previous', '&laquo; السابق', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(8, 1, 'ar', 'pagination', 'next', 'التالي &raquo;', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(9, 1, 'ar', 'passwords', 'password', 'Passwords must be at least six characters and match the confirmation.', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(10, 1, 'ar', 'passwords', 'reset', 'Your password has been reset!', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(11, 1, 'ar', 'passwords', 'sent', 'We have e-mailed your password reset link!', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(12, 1, 'ar', 'passwords', 'token', 'This password reset token is invalid.', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(13, 1, 'ar', 'passwords', 'user', 'We can\'t find a user with that e-mail address.', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(14, 1, 'ar', 'backLang', 'direction', 'rtl', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(15, 1, 'ar', 'backLang', 'code', 'ar', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(16, 1, 'ar', 'backLang', 'lang', 'AR', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(17, 1, 'ar', 'backLang', 'left', 'right', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(18, 1, 'ar', 'backLang', 'right', 'left', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(19, 1, 'ar', 'backLang', 'arabicBox', '<small>[ العربية ]</small>', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(20, 1, 'ar', 'backLang', 'englishBox', '<small>[ English ]</small>', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(21, 1, 'ar', 'backLang', 'rtl', 'rtl', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(22, 1, 'ar', 'backLang', 'ltr', 'ltr', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(23, 1, 'ar', 'backLang', 'boxCode', 'ar', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(24, 1, 'ar', 'backLang', 'boxCodeOther', 'en', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(25, 1, 'ar', 'backLang', 'translations', 'ترجمة الكلمات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(26, 1, 'ar', 'backLang', 'calendarLanguage', 'ar', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(27, 1, 'ar', 'backLang', 'home', 'الرئيسية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(28, 1, 'ar', 'backLang', 'main', 'الرئيسي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(29, 1, 'ar', 'backLang', 'siteData', 'بيانات الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(30, 1, 'ar', 'backLang', 'settings', 'الإعدادات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(31, 1, 'ar', 'backLang', 'dashboard', 'الشاشة الرئيسية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(32, 1, 'ar', 'backLang', 'visitorsAnalytics', 'تحليلات الزوار', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(33, 1, 'ar', 'backLang', 'visitorsAnalyticsBydate', 'حسب التاريخ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(34, 1, 'ar', 'backLang', 'visitorsAnalyticsByCity', 'المدينة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(35, 1, 'ar', 'backLang', 'visitorsAnalyticsByRegion', 'المنطقة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(36, 1, 'ar', 'backLang', 'visitorsAnalyticsByAddress', 'العنوان', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(37, 1, 'ar', 'backLang', 'visitorsAnalyticsByCountry', 'الدولة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(38, 1, 'ar', 'backLang', 'visitorsAnalyticsByOperatingSystem', 'نظام التشغيل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(39, 1, 'ar', 'backLang', 'visitorsAnalyticsByBrowser', 'المتصفح', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(40, 1, 'ar', 'backLang', 'visitorsAnalyticsByScreenResolution', 'دقة الشاشة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(41, 1, 'ar', 'backLang', 'visitorsAnalyticsByReachWay', 'طريقة الوصول', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(42, 1, 'ar', 'backLang', 'visitorsAnalyticsByHostName', 'اسم المضيف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(43, 1, 'ar', 'backLang', 'visitorsAnalyticsByOrganization', 'شركة الاتصال', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(44, 1, 'ar', 'backLang', 'visitorsAnalyticsVisitorsHistory', 'سجل الزوار', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(45, 1, 'ar', 'backLang', 'visitorsAnalyticsIPInquiry', 'استعلام عن IP', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(46, 1, 'ar', 'backLang', 'visitorsAnalyticsLastVisit', 'أخر زيارة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(47, 1, 'ar', 'backLang', 'members', 'الأعضاء', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(48, 1, 'ar', 'backLang', 'adsBanners', 'البنرات الإعلانية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(49, 1, 'ar', 'backLang', 'siteInbox', 'بريد الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(50, 1, 'ar', 'backLang', 'newsletter', 'جهات الاتصال', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(51, 1, 'ar', 'backLang', 'calendar', 'الأجندة والملاحظات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(52, 1, 'ar', 'backLang', 'generalSiteSettings', 'الإعدادات العامة للموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(53, 1, 'ar', 'backLang', 'webmasterTools', 'أدوات مشرف الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(54, 1, 'ar', 'backLang', 'generalSettings', 'الإعدادات العامة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(55, 1, 'ar', 'backLang', 'siteSectionsSettings', 'إعدادات أقسام الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(56, 1, 'ar', 'backLang', 'adsBannersSettings', 'إعدادات البنرات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(57, 1, 'ar', 'backLang', 'languageSettings', 'إعدادات اللغة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(58, 1, 'ar', 'backLang', 'siteMenus', 'قوائم الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(59, 1, 'ar', 'backLang', 'usersPermissions', 'المستخدمين والصلاحيات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(60, 1, 'ar', 'backLang', 'shoppingCart', 'سلة التسوق', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(61, 1, 'ar', 'backLang', 'orders', 'الطلبات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(62, 1, 'ar', 'backLang', 'logout', 'خروج', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(63, 1, 'ar', 'backLang', 'checkAll', 'تحديد الكل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(64, 1, 'ar', 'backLang', 'profile', 'الملف الشخصي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(65, 1, 'ar', 'backLang', 'search', 'بحث', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(66, 1, 'ar', 'backLang', 'new', 'إضافة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(67, 1, 'ar', 'backLang', 'add', 'إضافة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(68, 1, 'ar', 'backLang', 'update', 'حفظ التعديل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(69, 1, 'ar', 'backLang', 'save', 'حفظ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(70, 1, 'ar', 'backLang', 'saveOrder', 'حفظ الترتيب', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(71, 1, 'ar', 'backLang', 'edit', 'تعديل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(72, 1, 'ar', 'backLang', 'delete', 'حذف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(73, 1, 'ar', 'backLang', 'undoDelete', 'تراجع عن الحذف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(74, 1, 'ar', 'backLang', 'cancel', 'إلغاء', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(75, 1, 'ar', 'backLang', 'apply', 'تنفيذ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(77, 1, 'ar', 'backLang', 'notActive', 'غير مفعل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(78, 1, 'ar', 'backLang', 'status', 'الحالة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(79, 1, 'ar', 'backLang', 'visits', 'الزيارات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(80, 1, 'ar', 'backLang', 'contents', 'المحتويات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(81, 1, 'ar', 'backLang', 'options', 'خيارات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(82, 1, 'ar', 'backLang', 'showing', 'عرض', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(83, 1, 'ar', 'backLang', 'bulkAction', 'الخيارات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(84, 1, 'ar', 'backLang', 'activeSelected', 'تفعيل السجلات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(85, 1, 'ar', 'backLang', 'blockSelected', 'حظر السجلات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(86, 1, 'ar', 'backLang', 'deleteSelected', 'حذف السجلات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(87, 1, 'ar', 'backLang', 'of', 'من أصل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(88, 1, 'ar', 'backLang', 'records', 'سجلات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(89, 1, 'ar', 'backLang', 'confirmation', 'تأكيد', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(90, 1, 'ar', 'backLang', 'yes', 'نعم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(91, 1, 'ar', 'backLang', 'no', 'لا', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(92, 1, 'ar', 'backLang', 'confirmationDeleteMsg', 'هل أنت متأكد أنك تريد الحذف بالفعل؟', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(93, 1, 'ar', 'backLang', 'sectionsOf', 'تصنيفات ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(94, 1, 'ar', 'backLang', 'comments', 'التعليقات ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(95, 1, 'ar', 'backLang', 'videoTypes', 'الأنواع المسموحة: mp4, ogv, webm', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(96, 1, 'ar', 'backLang', 'imagesTypes', 'الأنواع المسموحة: .png, .jpg, .jpeg, .gif', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(97, 1, 'ar', 'backLang', 'audioTypes', 'الأنواع المسموحة: .mp3, .wav', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(98, 1, 'ar', 'backLang', 'attachTypes', 'الأنواع المسموحة: .* الكل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(99, 1, 'ar', 'backLang', 'addDone', 'تمت عملية الإضافة بنجاح', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(100, 1, 'ar', 'backLang', 'saveDone', 'تمت حفظ التعديلات التي أجريتها بنجاح', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(101, 1, 'ar', 'backLang', 'deleteDone', 'تمت حذف البيانات بنجاح', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(102, 1, 'ar', 'backLang', 'erorr', 'هناك خطأ برجاء إعادة المحاولة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(103, 1, 'ar', 'backLang', 'noData', 'لا يوجد أية بيانات هنا حتى الآن', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(104, 1, 'ar', 'backLang', 'appsSettings', 'تفعيل التطبيقات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(105, 1, 'ar', 'backLang', 'fieldsSettings', 'اعدادات الحقول', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(106, 1, 'ar', 'backLang', 'frontSettings', 'إعدادات الواجهة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(107, 1, 'ar', 'backLang', 'arabicLanguageFields', 'تفعيل حقول اللغة العربية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(108, 1, 'ar', 'backLang', 'englishLanguageFields', 'تفعيل حقول اللغة الإنجليزية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(109, 1, 'ar', 'backLang', 'seoTab', 'تفعيل تبويب SEO', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(110, 1, 'ar', 'backLang', 'seoTabTitle', 'خيارات SEO', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(111, 1, 'ar', 'backLang', 'defaultCurrency', 'العملة الإفتراضية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(112, 1, 'ar', 'backLang', 'commentsStatus', 'حالة التعليقات الجديدة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(113, 1, 'ar', 'backLang', 'automaticPublish', 'تفعيل مباشر عند الإضافة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(114, 1, 'ar', 'backLang', 'manualByAdmin', 'يدويا بعد المراجعة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(115, 1, 'ar', 'backLang', 'headerMenu', 'قائمة الهيدر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(116, 1, 'ar', 'backLang', 'footerMenu', 'قائمة الفوتر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(117, 1, 'ar', 'backLang', 'homeSlideBanners', 'البنرات بالصفحة الرئيسية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(118, 1, 'ar', 'backLang', 'homeTextBanners', ' البنرات النصية بالرئيسية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(119, 1, 'ar', 'backLang', 'sideBanners', 'بنرات جانبية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(120, 1, 'ar', 'backLang', 'none', 'بدون', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(121, 1, 'ar', 'backLang', 'newsletterGroup', 'المجموعة الخاصة بإيميلات النشرة البريدية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(122, 1, 'ar', 'backLang', 'homeRow1', 'محتويات الرئيسة الصف الأول', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(123, 1, 'ar', 'backLang', 'homeRow2', 'محتويات الرئيسية الصف الثاني', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(124, 1, 'ar', 'backLang', 'homeRow3', 'أخر الموضوعات بالفوتر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(125, 1, 'ar', 'backLang', 'contactPageId', 'صفحة اتصل بنا (التفاصيل والخرائط)', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(126, 1, 'ar', 'backLang', 'activeLanguages', 'اللغات المفعلة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(127, 1, 'ar', 'backLang', 'activeLanguages1', 'لغة واحدة ( الإفتراضية في .env )', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(128, 1, 'ar', 'backLang', 'activeLanguages2', 'جميع اللغات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(129, 1, 'ar', 'backLang', 'homeBanners', 'بنرات الرئيسية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(130, 1, 'ar', 'backLang', 'textBanners', 'بنرات نصية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(131, 1, 'ar', 'backLang', 'sectionName', 'عنوان القسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(132, 1, 'ar', 'backLang', 'sectionType', 'نوع القسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(133, 1, 'ar', 'backLang', 'sectionNew', 'إضافة قسم جديد', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(134, 1, 'ar', 'backLang', 'sectionEdit', 'تعديل القسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(135, 1, 'ar', 'backLang', 'sectionIcon', 'أيقونة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(136, 1, 'ar', 'backLang', 'sectionFather', 'متفرع من', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(137, 1, 'ar', 'backLang', 'sectionNoFather', 'قسم رئيسي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(138, 1, 'ar', 'backLang', 'size', 'المقاس', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(139, 1, 'ar', 'backLang', 'width', 'العرض', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(140, 1, 'ar', 'backLang', 'height', 'الإرتفاع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(141, 1, 'ar', 'backLang', 'descriptionBox', 'حقل للوصف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(142, 1, 'ar', 'backLang', 'linkBox', 'حقل لرابط عند الضغط', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(143, 1, 'ar', 'backLang', 'sectionTypeCode', 'كود / نص', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(144, 1, 'ar', 'backLang', 'sectionTypePhoto', 'صورة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(145, 1, 'ar', 'backLang', 'sectionTypeVideo', 'فيديو', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(146, 1, 'ar', 'backLang', 'langVar', 'اسم متغير اللغة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(147, 1, 'ar', 'backLang', 'sitePages', 'صفحات الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(148, 1, 'ar', 'backLang', 'photos', 'الصور', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(149, 1, 'ar', 'backLang', 'blog', 'المدونة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(150, 1, 'ar', 'backLang', 'services', 'الخدمات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(151, 1, 'ar', 'backLang', 'news', 'الأخبار', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(152, 1, 'ar', 'backLang', 'videos', 'الفيديو', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(153, 1, 'ar', 'backLang', 'sounds', 'الصوتيات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(154, 1, 'ar', 'backLang', 'products', 'المنتجات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(155, 1, 'ar', 'backLang', 'Projects', 'المشروعات ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(156, 1, 'ar', 'backLang', 'Slider', 'السلايدر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(157, 1, 'ar', 'backLang', 'typeTextPages', 'صفحات عامة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(158, 1, 'ar', 'backLang', 'typePhotos', 'صور', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(159, 1, 'ar', 'backLang', 'typeVideos', 'فيديوهات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(160, 1, 'ar', 'backLang', 'typeSounds', 'صوتيات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(161, 1, 'ar', 'backLang', 'hasCategories', 'يشمل تصنيفات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(162, 1, 'ar', 'backLang', 'reviewsAvailable', 'تفعيل التعليقات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(163, 1, 'ar', 'backLang', 'dateField', 'حقل تاريخ الإضافة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(164, 1, 'ar', 'backLang', 'longTextField', 'حقل نص طويل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(165, 1, 'ar', 'backLang', 'allowEditor', 'إستخدام المحرر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(166, 1, 'ar', 'backLang', 'attachFileField', 'حقل ملف مرفق', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(167, 1, 'ar', 'backLang', 'additionalImages', 'صور إضافية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(168, 1, 'ar', 'backLang', 'attachGoogleMaps', 'إرفاق خرائط جوجل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(169, 1, 'ar', 'backLang', 'attachOrderForm', 'إرفاق نموذج للطلب', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(170, 1, 'ar', 'backLang', 'withoutCategories', 'بدون تصنيفات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(171, 1, 'ar', 'backLang', 'mainCategoriesOnly', 'تصنيفات رئيسية فقط', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(172, 1, 'ar', 'backLang', 'mainAndSubCategories', 'تصنيفات رئيسية وفرعية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(173, 1, 'ar', 'backLang', 'sectionIconPicker', 'إختيار أيقونة للقسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(174, 1, 'ar', 'backLang', 'topicsIconPicker', 'إختيار أيقونة للموضوعات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(175, 1, 'ar', 'backLang', 'iconPicker', 'إختيار أيقونة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(176, 1, 'ar', 'backLang', 'siteInfoSettings', 'بيانات الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(177, 1, 'ar', 'backLang', 'siteStatusSettings', 'حالة الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(178, 1, 'ar', 'backLang', 'siteSocialSettings', 'الشبكات الإجتماعية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(179, 1, 'ar', 'backLang', 'siteContactsSettings', 'بيانات الإتصال', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(180, 1, 'ar', 'backLang', 'websiteTitle', 'عنوان الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(181, 1, 'ar', 'backLang', 'metaDescription', 'وصف الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(182, 1, 'ar', 'backLang', 'metaKeywords', 'كلمات دلالية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(183, 1, 'ar', 'backLang', 'websiteUrl', 'رابط الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(184, 1, 'ar', 'backLang', 'websiteNotificationEmail', 'بريد استلام التلميحات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(185, 1, 'ar', 'backLang', 'websiteNotificationEmail1', 'ارسل لي بريد في حالة رسائل جديدة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(186, 1, 'ar', 'backLang', 'websiteNotificationEmail2', 'ارسل لي بريد في حالة تعليقات جديدة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(187, 1, 'ar', 'backLang', 'websiteNotificationEmail3', 'ارسل لي بريد في حالة طلبات جديدة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(188, 1, 'ar', 'backLang', 'emailNotifications', 'رسائل التنبيه', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(189, 1, 'ar', 'backLang', 'contactAddress', 'العنوان', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(190, 1, 'ar', 'backLang', 'contactPhone', 'الهاتف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(191, 1, 'ar', 'backLang', 'contactFax', 'الفاكس', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(192, 1, 'ar', 'backLang', 'contactMobile', 'الموبايل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(193, 1, 'ar', 'backLang', 'contactEmail', 'البريد الإلكتروني', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(194, 1, 'ar', 'backLang', 'worksTime', 'مواعيد العمل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(195, 1, 'ar', 'backLang', 'siteStatusSettingsPublished', 'فعال للجميع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(196, 1, 'ar', 'backLang', 'siteStatusSettingsClosed', 'مغلق', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(197, 1, 'ar', 'backLang', 'siteStatusSettingsMsg', 'رسالة في حالة الإغلاق', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(198, 1, 'ar', 'backLang', 'facebook', 'Facebook', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(199, 1, 'ar', 'backLang', 'twitter', 'Twitter', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(200, 1, 'ar', 'backLang', 'google', 'Google+', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(201, 1, 'ar', 'backLang', 'linkedin', 'Linkedin', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(202, 1, 'ar', 'backLang', 'youtube', 'Youtube', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(203, 1, 'ar', 'backLang', 'instagram', 'Instagram', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(204, 1, 'ar', 'backLang', 'pinterest', 'Pinterest', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(205, 1, 'ar', 'backLang', 'tumblr', 'Tumblr', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(206, 1, 'ar', 'backLang', 'flickr', 'Flickr', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(207, 1, 'ar', 'backLang', 'whatapp', 'Whatsapp', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(208, 1, 'ar', 'backLang', 'styleSettings', 'خيارات التصميم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(209, 1, 'ar', 'backLang', 'siteLogo', 'شعار الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(210, 1, 'ar', 'backLang', 'favicon', 'ايقونة المفضلة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(211, 1, 'ar', 'backLang', 'appleIcon', 'ايقونة آبل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(212, 1, 'ar', 'backLang', 'styleColor1', 'لون التصميم الأساسي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(213, 1, 'ar', 'backLang', 'styleColor2', 'لون التصميم الثانوى', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(214, 1, 'ar', 'backLang', 'restoreDefault', 'إستعادة الإفتراضي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(215, 1, 'ar', 'backLang', 'layoutMode', 'نظام القالب', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(216, 1, 'ar', 'backLang', 'wide', 'عرض كامل Wide', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(217, 1, 'ar', 'backLang', 'boxed', 'عرض محدد Boxed', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(218, 1, 'ar', 'backLang', 'backgroundStyle', 'نوع الخلفية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(219, 1, 'ar', 'backLang', 'colorBackground', 'لون', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(220, 1, 'ar', 'backLang', 'patternsBackground', 'رسومي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(221, 1, 'ar', 'backLang', 'imageBackground', 'صورة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(222, 1, 'ar', 'backLang', 'newsletterSubscribe', 'الإشتراك في النشرة البريدية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(223, 1, 'ar', 'backLang', 'footerStyle', 'تصميم الفوتر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(224, 1, 'ar', 'backLang', 'footerStyleBg', 'خلفية الفوتر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(225, 1, 'ar', 'backLang', 'preLoad', 'التحميل المسبق (جاري التحميل)', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(226, 1, 'ar', 'backLang', 'bannerAdd', ' إضافة بنر جديد', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(227, 1, 'ar', 'backLang', 'bannerEdit', 'تعديل البنر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(228, 1, 'ar', 'backLang', 'bannerTitle', 'عنوان الإعلان', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(229, 1, 'ar', 'backLang', 'bannerSection', 'القسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(230, 1, 'ar', 'backLang', 'bannerPhoto', 'صورة الإعلان', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(231, 1, 'ar', 'backLang', 'bannerDetails', 'التفاصيل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(232, 1, 'ar', 'backLang', 'bannerLinkUrl', 'رابط عند الضغط', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(233, 1, 'ar', 'backLang', 'bannerVideoType', 'نوع الفيديو', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(234, 1, 'ar', 'backLang', 'bannerVideoType1', 'فيديو مباشر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(235, 1, 'ar', 'backLang', 'bannerVideoType2', 'فيديو من يوتيوب', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(236, 1, 'ar', 'backLang', 'bannerVideoType3', 'فيديو من فيميو', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(237, 1, 'ar', 'backLang', 'bannerVideoFile', 'ملف الفيديو', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(238, 1, 'ar', 'backLang', 'bannerVideoUrl', 'رابط الفيديو من يوتيوب', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(239, 1, 'ar', 'backLang', 'bannerVideoUrl2', 'رابط الفيديو من فيميو', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(240, 1, 'ar', 'backLang', 'bannerCode', 'كود البنر', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(241, 1, 'ar', 'backLang', 'topicNew', 'إضافة ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(242, 1, 'ar', 'backLang', 'topicEdit', 'تعديل ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(243, 1, 'ar', 'backLang', 'topicName', ' العنوان', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(244, 1, 'ar', 'backLang', 'topicPhoto', 'صورة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(245, 1, 'ar', 'backLang', 'topicSection', 'القسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(246, 1, 'ar', 'backLang', 'topicSelectSection', 'حدد القسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(247, 1, 'ar', 'backLang', 'topicDate', 'التاريخ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(248, 1, 'ar', 'backLang', 'topicAudio', 'الملف الصوتي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(249, 1, 'ar', 'backLang', 'topicVideo', 'ملف الفيديو ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(250, 1, 'ar', 'backLang', 'topicAttach', 'ملف مرفق', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(251, 1, 'ar', 'backLang', 'topicDeletedSection', 'غير تابع لأي قسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(252, 1, 'ar', 'backLang', 'topicTabSection', 'تفاصيل القسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(253, 1, 'ar', 'backLang', 'topicTabDetails', 'تفاصيل الموضوع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(254, 1, 'ar', 'backLang', 'topicSEOTitle', 'عنوان الصفحة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(255, 1, 'ar', 'backLang', 'topicSEODesc', 'الوصف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(256, 1, 'ar', 'backLang', 'topicSEOKeywords', 'الكلمات الدلالية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(257, 1, 'ar', 'backLang', 'topicAdditionalPhotos', 'الصور', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(258, 1, 'ar', 'backLang', 'topicGoogleMaps', 'خرائط جوجل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(259, 1, 'ar', 'backLang', 'topicDropFiles', 'ادرج الصور هنا أو انقر للتحميل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(260, 1, 'ar', 'backLang', 'topicDropFiles2', 'يمكنك تحميل العديد من الصور في نفس الوقت', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(261, 1, 'ar', 'backLang', 'topicCommentName', 'الإسم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(262, 1, 'ar', 'backLang', 'topicCommentEmail', 'البريد الإلكتروني', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(263, 1, 'ar', 'backLang', 'topicComment', 'التعليق', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(264, 1, 'ar', 'backLang', 'topicNewComment', 'تعليق جديد', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(265, 1, 'ar', 'backLang', 'topicNewMap', 'إضافة خريطة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(266, 1, 'ar', 'backLang', 'topicMapTitle', 'العنوان', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(267, 1, 'ar', 'backLang', 'topicMapDetails', 'التفاصيل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(268, 1, 'ar', 'backLang', 'topicMapLocation', 'الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(269, 1, 'ar', 'backLang', 'topicMapIcon', 'أيقونة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(270, 1, 'ar', 'backLang', 'topicMapClick', 'إضغط علي المكان الذي تريد الإضافة فية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(271, 1, 'ar', 'backLang', 'topicMapORClick', 'أو يمكنك الإضافة يدويا من هنا', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(272, 1, 'ar', 'backLang', 'allContacts', 'جميع المسجلين', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(273, 1, 'ar', 'backLang', 'waitActivation', 'بإنتظار التفعيل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(274, 1, 'ar', 'backLang', 'blockedContacts', 'المسجلين المحظورين', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(275, 1, 'ar', 'backLang', 'newGroup', 'مجموعة جديدة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(276, 1, 'ar', 'backLang', 'newContacts', 'إضافة حساب جديد', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(277, 1, 'ar', 'backLang', 'editContacts', 'تعديل الحساب', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(278, 1, 'ar', 'backLang', 'deleteContacts', 'حذف الحساب', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(279, 1, 'ar', 'backLang', 'searchAllContacts', 'بحث في جميع المسجلين', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(280, 1, 'ar', 'backLang', 'firstName', 'الإسم الأول', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(281, 1, 'ar', 'backLang', 'lastName', 'الإسم الأخير', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(282, 1, 'ar', 'backLang', 'companyName', 'اسم الشركة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(283, 1, 'ar', 'backLang', 'city', 'المدينة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(284, 1, 'ar', 'backLang', 'country', 'الدولة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(285, 1, 'ar', 'backLang', 'notes', 'ملاحظات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(286, 1, 'ar', 'backLang', 'group', 'التصنيف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(287, 1, 'ar', 'backLang', 'sendEmail', 'ارسال بريد', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(288, 1, 'ar', 'backLang', 'callNow', 'اتصل الآن', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(289, 1, 'ar', 'backLang', 'selectFile', 'حدد الملف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(290, 1, 'ar', 'backLang', 'labels', 'أقسام أخرى', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(291, 1, 'ar', 'backLang', 'inbox', 'البريد الوارد', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(292, 1, 'ar', 'backLang', 'sent', 'المرسل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(293, 1, 'ar', 'backLang', 'draft', 'المسودة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(294, 1, 'ar', 'backLang', 'compose', 'رسالة جديدة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(295, 1, 'ar', 'backLang', 'makeAsRead', 'اجعلها مقروءة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(296, 1, 'ar', 'backLang', 'makeAsUnread', 'اجعلها غير مقروءة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(297, 1, 'ar', 'backLang', 'moveTo', 'نقل إلى ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(298, 1, 'ar', 'backLang', 'replay', 'رد على الرسالة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(299, 1, 'ar', 'backLang', 'forward', 'إعادة توجيه', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(300, 1, 'ar', 'backLang', 'sendTo', 'إلى', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(301, 1, 'ar', 'backLang', 'sendFrom', 'من', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(302, 1, 'ar', 'backLang', 'sendCc', 'Cc', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(303, 1, 'ar', 'backLang', 'sendBcc', 'Bcc', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(304, 1, 'ar', 'backLang', 'sendTitle', 'عنوان الرسالة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(305, 1, 'ar', 'backLang', 'SaveToDraft', 'حفظ بالمسودة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(306, 1, 'ar', 'backLang', 'send', 'ارسال', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(307, 1, 'ar', 'backLang', 'print', 'طباعة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(308, 1, 'ar', 'backLang', 'AttachFiles', 'ملفات مرفقة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(309, 1, 'ar', 'backLang', 'newEvent', 'اضافة حدث / ملاحظة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(310, 1, 'ar', 'backLang', 'eventTotal', 'الاجمالي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(311, 1, 'ar', 'backLang', 'eventTitle', 'الحدث / الملاحظة ', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(312, 1, 'ar', 'backLang', 'eventStart', 'بداية الحدث', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(313, 1, 'ar', 'backLang', 'eventEnd', 'نهاية الحدث', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(314, 1, 'ar', 'backLang', 'eventStart2', 'تاريخ البداية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(315, 1, 'ar', 'backLang', 'eventEnd2', 'تاريخ النهاية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(316, 1, 'ar', 'backLang', 'eventDetails', 'التفاصيل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(317, 1, 'ar', 'backLang', 'eventNote', 'ملاحظة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(318, 1, 'ar', 'backLang', 'eventMeeting', 'موعد', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(319, 1, 'ar', 'backLang', 'eventEvent', 'حدث', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(320, 1, 'ar', 'backLang', 'eventTask', 'مهمة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(321, 1, 'ar', 'backLang', 'eventType', 'النوع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(322, 1, 'ar', 'backLang', 'eventAt', 'في', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(323, 1, 'ar', 'backLang', 'eventToday', 'اليوم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(324, 1, 'ar', 'backLang', 'eventDay', 'عرض يومي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(325, 1, 'ar', 'backLang', 'eventWeek', 'أسبوعي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(326, 1, 'ar', 'backLang', 'eventMonth', 'شهري', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(327, 1, 'ar', 'backLang', 'eventDelete', 'حذف هذا الحدث / الملاحظة؟', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(328, 1, 'ar', 'backLang', 'eventClear', 'حذف جميع الملاحظات والأحداث', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(329, 1, 'ar', 'backLang', 'diagram', 'رسم توضيحي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(330, 1, 'ar', 'backLang', 'barDiagram', 'عرض البيانات في شكل رسم بياني', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(331, 1, 'ar', 'backLang', 'visitors', 'عدد الزوار', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(332, 1, 'ar', 'backLang', 'ip', 'IP', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(333, 1, 'ar', 'backLang', 'pageViews', 'عدد الصفحات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(334, 1, 'ar', 'backLang', 'today', 'اليوم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(335, 1, 'ar', 'backLang', 'yesterday', 'أمس', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(336, 1, 'ar', 'backLang', 'last7Days', 'أخر 7 أيام', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(337, 1, 'ar', 'backLang', 'last30Days', 'أخر 30 يوم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(338, 1, 'ar', 'backLang', 'thisMonth', 'الشهر الحالي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(339, 1, 'ar', 'backLang', 'lastMonth', 'الشهر الماضي', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(340, 1, 'ar', 'backLang', 'applyFrom', 'من', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(341, 1, 'ar', 'backLang', 'applyTo', 'إلى', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(342, 1, 'ar', 'backLang', 'customRange', 'فترة مخصصة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(343, 1, 'ar', 'backLang', 'weekDays', '\"Su\", \"Mo\", \"Tu\", \"We\", \"Th\", \"Fr\", \"Sa\"', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(344, 1, 'ar', 'backLang', 'monthsNames', '\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\", \"August\", \"September\", \"October\", \"November\", \"December\"', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(345, 1, 'ar', 'backLang', 'activity', 'النشاط', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(346, 1, 'ar', 'backLang', 'activitiesHistory', 'سجل الأنشطة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(347, 1, 'ar', 'backLang', 'newUser', 'إضافة مستخدم جديد', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(348, 1, 'ar', 'backLang', 'editUser', 'تعديل مستخدم', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(349, 1, 'ar', 'backLang', 'fullName', 'الإسم بالكامل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(350, 1, 'ar', 'backLang', 'Permission', 'نوع الصلاحيات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(351, 1, 'ar', 'backLang', 'Permission1', 'مسؤول الموقع', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(352, 1, 'ar', 'backLang', 'Permission2', 'مدير الموقع ( جميع الصلاحيات )', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(353, 1, 'ar', 'backLang', 'Permission3', 'مستخدم عادي ( بدون حذف )', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(354, 1, 'ar', 'backLang', 'Permission4', 'مشرف/مراجع ( عرض فقط )', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(355, 1, 'ar', 'backLang', 'personalPhoto', 'صورة شخصية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(356, 1, 'ar', 'backLang', 'loginEmail', 'البريد الإلكتروني', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(357, 1, 'ar', 'backLang', 'loginPassword', 'كلمة المرور', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(358, 1, 'ar', 'backLang', 'users', 'المستخدمين', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(359, 1, 'ar', 'backLang', 'permissions', 'الصلاحيات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(360, 1, 'ar', 'backLang', 'newPermissions', 'إضافة صلاحية جديدة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(361, 1, 'ar', 'backLang', 'title', 'العنوان', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(362, 1, 'ar', 'backLang', 'dataManagements', 'إدارة المحتوى', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(363, 1, 'ar', 'backLang', 'dataManagements1', 'المحتوى الخاص به فقط (للعضويات الشخصية فقط)', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(364, 1, 'ar', 'backLang', 'dataManagements2', 'كافة المحتويات من جميع المستخدمين (لعضويات الإدارة العامة)', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(365, 1, 'ar', 'backLang', 'activeApps', 'التطبيقات المفعلة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(366, 1, 'ar', 'backLang', 'activeSiteSections', 'أقسام الموقع المفعلة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(367, 1, 'ar', 'backLang', 'addPermission', 'صلاحية الإضافة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(368, 1, 'ar', 'backLang', 'editPermission', 'صلاحية التعديل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(369, 1, 'ar', 'backLang', 'deletePermission', 'صلاحية الحذف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(370, 1, 'ar', 'backLang', 'viewOnly', 'مشاهدة فقط', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(371, 1, 'ar', 'backLang', 'editPermissions', 'تعديل الصلاحيات', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(372, 1, 'ar', 'backLang', 'perAdd', 'الإضافة', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(373, 1, 'ar', 'backLang', 'perEdit', 'التعديل', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(374, 1, 'ar', 'backLang', 'perDelete', 'الحذف', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(375, 1, 'ar', 'backLang', 'selectPermissionsType', 'حدد نوع الصلاحية', '2018-12-15 01:54:00', '2018-12-15 01:54:00'),
(376, 1, 'ar', 'backLang', 'newLink', 'اضافة رابط جديد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(377, 1, 'ar', 'backLang', 'newMenu', 'اضافة قائمة جديدة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(378, 1, 'ar', 'backLang', 'menuTitle', 'اسم القائمة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(379, 1, 'ar', 'backLang', 'editSection', 'تعديل رابط', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(380, 1, 'ar', 'backLang', 'linkType', 'نوع الرابط', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(381, 1, 'ar', 'backLang', 'linkType1', 'عنوان رئيسي', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(382, 1, 'ar', 'backLang', 'linkType2', 'رابط مباشر', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(383, 1, 'ar', 'backLang', 'linkType3', 'قسم رئيسي', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(384, 1, 'ar', 'backLang', 'linkType4', 'قائمة منسدلة لقسم', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(385, 1, 'ar', 'backLang', 'linkURL', 'الرابط', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(386, 1, 'ar', 'backLang', 'linkSection', 'قسم الرابط', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(387, 1, 'ar', 'backLang', 'sectionTitle', 'عنوان القسم / الرابط', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(388, 1, 'ar', 'backLang', 'fatherSection', 'القسم الرئيسي', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(389, 1, 'ar', 'backLang', 'latestMessages', 'أحدث الرسائل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(390, 1, 'ar', 'backLang', 'notesEvents', 'الملاحظات والأحداث', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(391, 1, 'ar', 'backLang', 'latestContacts', 'جديد جهات الإتصال', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(392, 1, 'ar', 'backLang', 'more', 'المزيد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(393, 1, 'ar', 'backLang', 'viewMore', 'مشاهدة المزيد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(394, 1, 'ar', 'backLang', 'addNew', 'إضافة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(395, 1, 'ar', 'backLang', 'reports', 'التقارير', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(396, 1, 'ar', 'backLang', 'reportsDetails', 'يمكنك مشاهدة المزيد من التقارير', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(397, 1, 'ar', 'backLang', 'hi', 'أهلاً', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(398, 1, 'ar', 'backLang', 'welcomeBack', 'مرحباً بعودتك', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(399, 1, 'ar', 'backLang', 'lastFor7Days', 'الزوار لأخر ٧ أيام', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(400, 1, 'ar', 'backLang', 'todayByCountry', 'زوار اليوم حسب الدولة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(401, 1, 'ar', 'backLang', 'browsers', 'متصفح الإنترنت', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(402, 1, 'ar', 'backLang', 'browsersCalculated', 'نسبة المتصفحات حسب أخر ٧ أيام', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(403, 1, 'ar', 'backLang', 'visitorsRate', 'معدل الزوار', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(404, 1, 'ar', 'backLang', 'visitorsRateToday', 'معدل الزوار على مدار اليوم الحالي', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(405, 1, 'ar', 'backLang', 'pages', 'الصفحات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(406, 1, 'ar', 'backLang', 'sections', 'الأقسام', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(407, 1, 'ar', 'backLang', 'resultsFoundFor', 'نتائج البحث عن', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(408, 1, 'ar', 'backLang', 'connectEmailToConnect', 'ربط مع البريد الإلكتروني', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(409, 1, 'ar', 'backLang', 'connectEmail', 'البريد الإلكتروني', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(410, 1, 'ar', 'backLang', 'connectPassword', 'كلمة المرور', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(411, 1, 'ar', 'backLang', 'openWebmail', 'افتح البريد الإلكتروني', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(412, 1, 'ar', 'backLang', 'themeSwitcher', 'التصميم واللغة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(413, 1, 'ar', 'backLang', 'foldedAside', 'تصغير القائمة الجانة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(414, 1, 'ar', 'backLang', 'boxedLayout', 'تصغير عرض النافذة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(415, 1, 'ar', 'backLang', 'themes', 'التصاميم', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(416, 1, 'ar', 'backLang', 'themes1', 'فاتح', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(417, 1, 'ar', 'backLang', 'themes2', 'رمادي', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(418, 1, 'ar', 'backLang', 'themes3', 'غامق', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(419, 1, 'ar', 'backLang', 'themes4', 'أسود', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(420, 1, 'ar', 'backLang', 'language', 'اللغة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(421, 1, 'ar', 'backLang', 'change', 'حفظ التغييرات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(422, 1, 'ar', 'backLang', 'sitePreview', ' معاينة الموقع', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(423, 1, 'ar', 'backLang', 'oops', 'عفوا', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(424, 1, 'ar', 'backLang', 'noPermission', 'نأسف ! ليس لديك الصلاحية لزيارة هذه الصفحة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(425, 1, 'ar', 'backLang', 'notFound', 'نأسف! الصفحة التي تبحث عنها غير موجودة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(426, 1, 'ar', 'backLang', 'forgotPassword', 'نسيت كلمة المرور؟', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(427, 1, 'ar', 'backLang', 'signIn', 'تسجيل الدخول', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(428, 1, 'ar', 'backLang', 'keepMeSignedIn', 'حفظ بيانات تسجيل الدخول', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(429, 1, 'ar', 'backLang', 'signedInToControl', 'تسجيل الدخول إلى لوحة التحكم', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(430, 1, 'ar', 'backLang', 'control', 'تحدي الخيرية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(431, 1, 'ar', 'backLang', 'resetPassword', 'إستعادة كلمة المرور', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(432, 1, 'ar', 'backLang', 'confirmPassword', 'تاكيد كلمة المرور', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(433, 1, 'ar', 'backLang', 'newPassword', 'كلمة مرور جديدة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(434, 1, 'ar', 'backLang', 'yourEmail', 'بريدك الإلكتروني', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(435, 1, 'ar', 'backLang', 'sendPasswordResetLink', 'ارسال رابط استعادة كلمة المرور', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(436, 1, 'ar', 'backLang', 'returnTo', 'عودة للخلف', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(437, 1, 'ar', 'backLang', 'enterYourEmail', 'أدخل عنوان البريد الالكتروني أدناه وسوف نرسل لك التعليمات حول كيفية تغيير كلمة المرور الخاصة بك.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(438, 1, 'ar', 'backLang', 'expireDateField', 'حقل تاريخ الإنتهاء', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(439, 1, 'ar', 'backLang', 'expireDate', 'تاريخ الإنتهاء', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(440, 1, 'ar', 'backLang', 'expired', 'منتهي', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(441, 1, 'ar', 'backLang', 'additionalFiles', 'ملفات إضافية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(442, 1, 'ar', 'backLang', 'relatedTopics', 'مواضيع ذات صلة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(443, 1, 'ar', 'backLang', 'relatedTopicsAdd', 'إضافة مواضيع ذات صلة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(444, 1, 'ar', 'backLang', 'relatedTopicsSelect', 'حدد القسم أولا لإختيار المواضيع', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(445, 1, 'ar', 'backLang', 'customFields', 'حقول إضافية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(446, 1, 'ar', 'backLang', 'customFieldsTitle', 'عنوان الحقل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(447, 1, 'ar', 'backLang', 'customFieldsType', 'نوع الحقل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(448, 1, 'ar', 'backLang', 'customFieldsStatus', 'الحالة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(449, 1, 'ar', 'backLang', 'customFieldsNewField', 'إضافة حقل جديد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(450, 1, 'ar', 'backLang', 'customFieldsEditField', 'تعديل حقل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(451, 1, 'ar', 'backLang', 'customFieldsRequired', 'مطلوب', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(452, 1, 'ar', 'backLang', 'customFieldsOptional', 'إختياري', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(453, 1, 'ar', 'backLang', 'customFieldsDefault', 'القيمة الإفتراضية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(454, 1, 'ar', 'backLang', 'customFieldsOptions', 'الإختيارات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(455, 1, 'ar', 'backLang', 'customFieldsOptionsHelp', 'اكتب كل إختيار على سطر منفصل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(456, 1, 'ar', 'backLang', 'customFieldsForAllLang', 'لجميع اللغات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(457, 1, 'ar', 'backLang', 'customFieldsType0', 'حقل نصي', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(458, 1, 'ar', 'backLang', 'customFieldsType1', 'نص طويل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(459, 1, 'ar', 'backLang', 'customFieldsType2', 'رقم', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(460, 1, 'ar', 'backLang', 'customFieldsType3', 'بريد إلكتروني', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(461, 1, 'ar', 'backLang', 'customFieldsType4', 'تاريخ', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(462, 1, 'ar', 'backLang', 'customFieldsType5', 'تاريخ ووقت', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(463, 1, 'ar', 'backLang', 'customFieldsType6', 'إختيار من متعدد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(464, 1, 'ar', 'backLang', 'customFieldsType7', 'إختيارات متعددة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(465, 1, 'ar', 'backLang', 'customFieldsType8', 'صورة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(466, 1, 'ar', 'backLang', 'customFieldsType9', 'ملف مرفق', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(467, 1, 'ar', 'backLang', 'customFieldsType10', 'ملف فيديو', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(468, 1, 'ar', 'backLang', 'customFieldsType11', 'رابط فيديو من يوتيوب', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(469, 1, 'ar', 'backLang', 'customFieldsType12', 'رابط فيديو من فيميو', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(470, 1, 'ar', 'backLang', 'optionalFields', 'الحقول الإختيارية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(471, 1, 'ar', 'backLang', 'additionalTabs', 'تبويبات إضافية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(472, 1, 'ar', 'backLang', 'active_disable', 'تفعيل/إيقاف', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(473, 1, 'ar', 'backLang', 'friendlyURL', 'رابط الصفحة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(474, 1, 'ar', 'backLang', 'seoTabSettings', 'إدارة عنوان الصفحة، وصف ميتا، الكلمات الدلالية ورابط الصفحة. هذا يساعدك على إدارة كيفية ظهور هذا الموضوع على محركات البحث.', '2018-12-15 01:54:01', '2018-12-15 01:54:01');
INSERT INTO `ltm_translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(475, 1, 'ar', 'backLang', 'friendlyURLinks', 'طريقة روابط الواجهة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(476, 1, 'ar', 'backLang', 'friendlyURLinks1', 'استخدام الروابط الإفتراضية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(477, 1, 'ar', 'backLang', 'friendlyURLinks2', 'استخدام الروابط صديقة محركات البحث ', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(478, 1, 'ar', 'backLang', 'registrationSettings', 'إعدادات التسجيل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(479, 1, 'ar', 'backLang', 'allowRegister', 'تفعيل إمكانية التسجيل في لوحة التحكم', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(480, 1, 'ar', 'backLang', 'permissionForNewUsers', 'مجموعة صلاحيات المسجلين الجدد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(481, 1, 'ar', 'backLang', 'createNewAccount', 'إنشاء حساب جديد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(482, 1, 'ar', 'backLang', 'APIStatus', 'تفعيل RESTful API', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(483, 1, 'ar', 'backLang', 'APIKey', 'API مفتاح', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(484, 1, 'ar', 'backLang', 'APIKeyGenerate', 'إنشاء مفتاح جديد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(485, 1, 'ar', 'backLang', 'APIKeyConfirm', 'هل تريد بالفعل إنشاء مفتاح جديد؟', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(486, 1, 'ar', 'backLang', 'homeRow_3', 'محتويات الرئيسة الصف الثالث', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(487, 1, 'ar', 'backLang', 'partners', 'العملاء', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(488, 1, 'ar', 'validation', 'accepted', 'The :attribute must be accepted.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(489, 1, 'ar', 'validation', 'active_url', 'The :attribute is not a valid URL.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(490, 1, 'ar', 'validation', 'after', 'The :attribute must be a date after :date.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(491, 1, 'ar', 'validation', 'after_or_equal', 'The :attribute must be a date after or equal to :date.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(492, 1, 'ar', 'validation', 'alpha', 'The :attribute may only contain letters.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(493, 1, 'ar', 'validation', 'alpha_dash', 'The :attribute may only contain letters, numbers, and dashes.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(494, 1, 'ar', 'validation', 'alpha_num', 'The :attribute may only contain letters and numbers.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(495, 1, 'ar', 'validation', 'array', 'The :attribute must be an array.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(496, 1, 'ar', 'validation', 'before', 'The :attribute must be a date before :date.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(497, 1, 'ar', 'validation', 'before_or_equal', 'The :attribute must be a date before or equal to :date.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(498, 1, 'ar', 'validation', 'between.numeric', 'The :attribute must be between :min and :max.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(499, 1, 'ar', 'validation', 'between.file', 'The :attribute must be between :min and :max kilobytes.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(500, 1, 'ar', 'validation', 'between.string', 'The :attribute must be between :min and :max characters.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(501, 1, 'ar', 'validation', 'between.array', 'The :attribute must have between :min and :max items.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(502, 1, 'ar', 'validation', 'boolean', 'The :attribute field must be true or false.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(503, 1, 'ar', 'validation', 'confirmed', 'The :attribute confirmation does not match.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(504, 1, 'ar', 'validation', 'date', 'The :attribute is not a valid date.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(505, 1, 'ar', 'validation', 'date_format', 'The :attribute does not match the format :format.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(506, 1, 'ar', 'validation', 'different', 'The :attribute and :other must be different.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(507, 1, 'ar', 'validation', 'digits', 'The :attribute must be :digits digits.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(508, 1, 'ar', 'validation', 'digits_between', 'The :attribute must be between :min and :max digits.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(509, 1, 'ar', 'validation', 'dimensions', 'The :attribute has invalid image dimensions.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(510, 1, 'ar', 'validation', 'distinct', 'The :attribute field has a duplicate value.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(511, 1, 'ar', 'validation', 'email', 'The :attribute must be a valid email address.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(512, 1, 'ar', 'validation', 'exists', 'The selected :attribute is invalid.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(513, 1, 'ar', 'validation', 'file', 'The :attribute must be a file.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(514, 1, 'ar', 'validation', 'filled', 'The :attribute field must have a value.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(515, 1, 'ar', 'validation', 'image', 'The :attribute must be an image.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(516, 1, 'ar', 'validation', 'in', 'The selected :attribute is invalid.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(517, 1, 'ar', 'validation', 'in_array', 'The :attribute field does not exist in :other.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(518, 1, 'ar', 'validation', 'integer', 'The :attribute must be an integer.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(519, 1, 'ar', 'validation', 'ip', 'The :attribute must be a valid IP address.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(520, 1, 'ar', 'validation', 'ipv4', 'The :attribute must be a valid IPv4 address.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(521, 1, 'ar', 'validation', 'ipv6', 'The :attribute must be a valid IPv6 address.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(522, 1, 'ar', 'validation', 'json', 'The :attribute must be a valid JSON string.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(523, 1, 'ar', 'validation', 'max.numeric', 'The :attribute may not be greater than :max.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(524, 1, 'ar', 'validation', 'max.file', 'The :attribute may not be greater than :max kilobytes.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(525, 1, 'ar', 'validation', 'max.string', 'The :attribute may not be greater than :max characters.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(526, 1, 'ar', 'validation', 'max.array', 'The :attribute may not have more than :max items.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(527, 1, 'ar', 'validation', 'mimes', 'The :attribute must be a file of type: :values.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(528, 1, 'ar', 'validation', 'mimetypes', 'The :attribute must be a file of type: :values.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(529, 1, 'ar', 'validation', 'min.numeric', 'The :attribute must be at least :min.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(530, 1, 'ar', 'validation', 'min.file', 'The :attribute must be at least :min kilobytes.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(531, 1, 'ar', 'validation', 'min.string', 'The :attribute must be at least :min characters.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(532, 1, 'ar', 'validation', 'min.array', 'The :attribute must have at least :min items.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(533, 1, 'ar', 'validation', 'not_in', 'The selected :attribute is invalid.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(534, 1, 'ar', 'validation', 'numeric', 'The :attribute must be a number.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(535, 1, 'ar', 'validation', 'present', 'The :attribute field must be present.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(536, 1, 'ar', 'validation', 'regex', 'The :attribute format is invalid.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(537, 1, 'ar', 'validation', 'required', 'The :attribute field is required.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(538, 1, 'ar', 'validation', 'required_if', 'The :attribute field is required when :other is :value.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(539, 1, 'ar', 'validation', 'required_unless', 'The :attribute field is required unless :other is in :values.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(540, 1, 'ar', 'validation', 'required_with', 'The :attribute field is required when :values is present.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(541, 1, 'ar', 'validation', 'required_with_all', 'The :attribute field is required when :values is present.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(542, 1, 'ar', 'validation', 'required_without', 'The :attribute field is required when :values is not present.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(543, 1, 'ar', 'validation', 'required_without_all', 'The :attribute field is required when none of :values are present.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(544, 1, 'ar', 'validation', 'same', 'The :attribute and :other must match.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(545, 1, 'ar', 'validation', 'size.numeric', 'The :attribute must be :size.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(546, 1, 'ar', 'validation', 'size.file', 'The :attribute must be :size kilobytes.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(547, 1, 'ar', 'validation', 'size.string', 'The :attribute must be :size characters.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(548, 1, 'ar', 'validation', 'size.array', 'The :attribute must contain :size items.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(549, 1, 'ar', 'validation', 'string', 'The :attribute must be a string.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(550, 1, 'ar', 'validation', 'timezone', 'The :attribute must be a valid zone.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(551, 1, 'ar', 'validation', 'unique', 'The :attribute has already been taken.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(552, 1, 'ar', 'validation', 'uploaded', 'The :attribute failed to upload.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(553, 1, 'ar', 'validation', 'url', 'The :attribute format is invalid.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(554, 1, 'ar', 'validation', 'custom.attribute-name.rule-name', 'custom-message', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(555, 1, 'ar', 'auth', 'failed', 'These credentials do not match our records.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(556, 1, 'ar', 'auth', 'throttle', 'Too many login attempts. Please try again in :seconds seconds.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(557, 1, 'ar', 'frontLang', 'dashboard', 'لوحة التحكم', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(558, 1, 'ar', 'frontLang', 'toTop', 'إلى الأعلى', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(559, 1, 'ar', 'frontLang', 'AllRightsReserved', 'جميع الحقوق محفوظة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(560, 1, 'ar', 'frontLang', 'moreDetails', 'مزيد من التفاصيل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(561, 1, 'ar', 'frontLang', 'viewMore', 'مشاهدة المزيد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(562, 1, 'ar', 'frontLang', 'readMore', ' إقرأ المزيد ', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(563, 1, 'ar', 'frontLang', 'downloadAttach', 'تحمل المرفقات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(564, 1, 'ar', 'frontLang', 'contactDetails', 'بيانات الإتصال', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(565, 1, 'ar', 'frontLang', 'callUs', 'اتصل الآن', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(566, 1, 'ar', 'frontLang', 'callFax', 'الفاكس', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(567, 1, 'ar', 'frontLang', 'callMobile', 'الجوال', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(568, 1, 'ar', 'frontLang', 'callPhone', 'الهاتف', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(569, 1, 'ar', 'frontLang', 'callTimes', 'أوقات العمل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(570, 1, 'ar', 'frontLang', 'address', 'العنوان', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(571, 1, 'ar', 'frontLang', 'name', 'الإسم', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(572, 1, 'ar', 'frontLang', 'email', 'البريد الإلكتروني', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(573, 1, 'ar', 'frontLang', 'phone', 'رقم الهاتف', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(574, 1, 'ar', 'frontLang', 'latestNews', 'جديد الأخبار', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(575, 1, 'ar', 'frontLang', 'ourGallery', 'معرض الصور', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(576, 1, 'ar', 'frontLang', 'subscribeToOurNewsletter', 'اشترك في  النشرة البريدية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(577, 1, 'ar', 'frontLang', 'subscribeToOurNewsletterDone', 'لقد اشتركت بنجاح', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(578, 1, 'ar', 'frontLang', 'subscribeToOurNewsletterError', 'أنت بالفعل مشترك معنا', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(579, 1, 'ar', 'frontLang', 'newsletter', 'النشرة البريدية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(580, 1, 'ar', 'frontLang', 'subscribe', 'اشترك', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(581, 1, 'ar', 'frontLang', 'yourEmail', 'بريدك الإلكتروني', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(582, 1, 'ar', 'frontLang', 'yourName', 'إسمك', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(583, 1, 'ar', 'frontLang', 'enterYourName', 'برجاء كتابة إسمك', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(584, 1, 'ar', 'frontLang', 'enterYourEmail', 'برجاء كتابة بريدك الإلكتروني', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(585, 1, 'ar', 'frontLang', 'enterYourPhone', 'برجاء كتابة رقم هاتفك ', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(586, 1, 'ar', 'frontLang', 'enterYourSubject', 'برجاء كتابة عنوان لرسالتك', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(587, 1, 'ar', 'frontLang', 'enterYourMessage', 'برجاء كتابة تفاصيل الرسالة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(588, 1, 'ar', 'frontLang', 'youMessageSent', 'تم إرسال رسالتكم بنجاح، وسنقوم بالتواصل معكم في أقرب وقت ممكن. نشكركم لمراسلتنا!', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(589, 1, 'ar', 'frontLang', 'youMessageNotSent', 'خطأ: برجاء إعادة المحاولة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(590, 1, 'ar', 'frontLang', 'subject', 'عنوان الرسالة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(591, 1, 'ar', 'frontLang', 'message', 'تفاصيل الرسالة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(592, 1, 'ar', 'frontLang', 'sendMessage', 'ارسال الرسالة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(593, 1, 'ar', 'frontLang', 'workingHours', 'أوقات العمل', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(594, 1, 'ar', 'frontLang', 'categories', 'التصنيفات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(595, 1, 'ar', 'frontLang', 'noData', 'لا يوجد أية بيانات هنا حتى الآن', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(596, 1, 'ar', 'frontLang', 'mostViewed', 'الأكثر مشاهدة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(597, 1, 'ar', 'frontLang', 'share', 'نشر', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(598, 1, 'ar', 'frontLang', 'locationMap', 'خريطة الموقع', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(599, 1, 'ar', 'frontLang', 'comments', 'التعليقات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(600, 1, 'ar', 'frontLang', 'comment', 'التعليق', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(601, 1, 'ar', 'frontLang', 'enterYourComment', 'برجاء كتابة التعليق', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(602, 1, 'ar', 'frontLang', 'sendComment', 'ارسال التعليق', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(603, 1, 'ar', 'frontLang', 'youCommentSent', 'تم إضافة التعليق بنجاح', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(604, 1, 'ar', 'frontLang', 'newComment', 'إضافة تعليق جديد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(605, 1, 'ar', 'frontLang', 'refresh', 'تحديث', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(606, 1, 'ar', 'frontLang', 'getInTouch', 'يمكنك مراسلتنا عن طريق تعبئة النموذج التالي', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(607, 1, 'ar', 'frontLang', 'homeContents1Title', 'أحدث المقالات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(608, 1, 'ar', 'frontLang', 'homeContents1desc', 'احدث المقالات الجديدة والمميزة التي أضيفت في المدونة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(609, 1, 'ar', 'frontLang', 'homeContents2Title', 'أعمالنا الحالية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(610, 1, 'ar', 'frontLang', 'homeContents2desc', 'مجموعة من أحدث أعمالنا، يمكنك تصفح المزيد', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(611, 1, 'ar', 'frontLang', 'search', 'بحث عن...', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(612, 1, 'ar', 'frontLang', 'visits', 'الزيارات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(613, 1, 'ar', 'frontLang', 'orderForm', 'نموذج الطلب', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(614, 1, 'ar', 'frontLang', 'quantity', 'الكمية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(615, 1, 'ar', 'frontLang', 'yourQuantity', 'برجاء إضافة الكمية المطلوبة', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(616, 1, 'ar', 'frontLang', 'notes', 'ملاحظات', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(617, 1, 'ar', 'frontLang', 'sendOrder', 'ارسال الطلب', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(618, 1, 'ar', 'frontLang', 'youOrderSent', 'تم إرسال الطلب بنجاح، وسنقوم بالتواصل معكم في أقرب وقت ممكن.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(619, 1, 'ar', 'frontLang', 'partners', 'عملائنا', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(620, 1, 'ar', 'frontLang', 'partnersMsg', 'عملاء وشركات متميزة طويلة الأمد مع رواد الشركات المحلية والعالمية', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(621, 1, 'en', 'pagination', 'previous', '&laquo; Previous', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(622, 1, 'en', 'pagination', 'next', 'Next &raquo;', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(623, 1, 'en', 'passwords', 'password', 'Passwords must be at least six characters and match the confirmation.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(624, 1, 'en', 'passwords', 'reset', 'Your password has been reset!', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(625, 1, 'en', 'passwords', 'sent', 'We have e-mailed your password reset link!', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(626, 1, 'en', 'passwords', 'token', 'This password reset token is invalid.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(627, 1, 'en', 'passwords', 'user', 'We can\'t find a user with that e-mail address.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(628, 1, 'en', 'backLang', 'direction', 'ltr', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(629, 1, 'en', 'backLang', 'code', 'en', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(630, 1, 'en', 'backLang', 'lang', 'EN', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(631, 1, 'en', 'backLang', 'left', 'left', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(632, 1, 'en', 'backLang', 'right', 'right', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(633, 1, 'en', 'backLang', 'arabicBox', '<small>[ العربية ]</small>', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(634, 1, 'en', 'backLang', 'englishBox', '<small>[ English ]</small>', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(635, 1, 'en', 'backLang', 'rtl', 'rtl', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(636, 1, 'en', 'backLang', 'ltr', 'ltr', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(637, 1, 'en', 'backLang', 'boxCode', 'en', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(638, 1, 'en', 'backLang', 'boxCodeOther', 'ar', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(639, 1, 'en', 'backLang', 'translations', 'Translations', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(640, 1, 'en', 'backLang', 'calendarLanguage', 'en', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(641, 1, 'en', 'backLang', 'home', 'Home', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(642, 1, 'en', 'backLang', 'main', 'Main', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(643, 1, 'en', 'backLang', 'Slider', 'Slider', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(644, 1, 'en', 'backLang', 'siteData', 'Site Data', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(645, 1, 'en', 'backLang', 'settings', 'Settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(646, 1, 'en', 'backLang', 'dashboard', 'Dashboard', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(647, 1, 'en', 'backLang', 'visitorsAnalytics', 'Analytics', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(648, 1, 'en', 'backLang', 'visitorsAnalyticsBydate', 'By date', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(649, 1, 'en', 'backLang', 'visitorsAnalyticsByCity', 'City', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(650, 1, 'en', 'backLang', 'visitorsAnalyticsByRegion', 'Region', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(651, 1, 'en', 'backLang', 'visitorsAnalyticsByAddress', 'Address', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(652, 1, 'en', 'backLang', 'visitorsAnalyticsByCountry', 'Country', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(653, 1, 'en', 'backLang', 'visitorsAnalyticsByOperatingSystem', 'Operating system', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(654, 1, 'en', 'backLang', 'visitorsAnalyticsByBrowser', 'Browser', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(655, 1, 'en', 'backLang', 'visitorsAnalyticsByScreenResolution', 'Screen resolution', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(656, 1, 'en', 'backLang', 'visitorsAnalyticsByReachWay', 'Reach way', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(657, 1, 'en', 'backLang', 'visitorsAnalyticsByHostName', 'Host name', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(658, 1, 'en', 'backLang', 'visitorsAnalyticsByOrganization', 'Organization', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(659, 1, 'en', 'backLang', 'visitorsAnalyticsVisitorsHistory', 'Visitors History', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(660, 1, 'en', 'backLang', 'visitorsAnalyticsIPInquiry', 'IP Inquiry', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(661, 1, 'en', 'backLang', 'visitorsAnalyticsLastVisit', 'Last Visit', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(662, 1, 'en', 'backLang', 'members', 'Members', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(663, 1, 'en', 'backLang', 'adsBanners', 'Ad. Banners', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(664, 1, 'en', 'backLang', 'siteInbox', 'Inbox', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(665, 1, 'en', 'backLang', 'newsletter', 'Contacts', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(666, 1, 'en', 'backLang', 'calendar', 'Calendar', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(667, 1, 'en', 'backLang', 'generalSiteSettings', 'Settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(668, 1, 'en', 'backLang', 'webmasterTools', 'Webmaster', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(669, 1, 'en', 'backLang', 'generalSettings', 'General Settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(670, 1, 'en', 'backLang', 'siteSectionsSettings', 'Site Sections', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(671, 1, 'en', 'backLang', 'adsBannersSettings', 'Banners settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(672, 1, 'en', 'backLang', 'languageSettings', 'Language settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(673, 1, 'en', 'backLang', 'siteMenus', 'Site Menus', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(674, 1, 'en', 'backLang', 'usersPermissions', 'Users & Permissions', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(675, 1, 'en', 'backLang', 'shoppingCart', 'Shopping Cart', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(676, 1, 'en', 'backLang', 'orders', 'Orders', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(677, 1, 'en', 'backLang', 'logout', 'Logout', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(678, 1, 'en', 'backLang', 'checkAll', 'Check all', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(679, 1, 'en', 'backLang', 'profile', 'Profile', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(680, 1, 'en', 'backLang', 'search', 'Search', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(681, 1, 'en', 'backLang', 'new', 'New', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(682, 1, 'en', 'backLang', 'add', '&nbsp;&nbsp;Add&nbsp;&nbsp;', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(683, 1, 'en', 'backLang', 'update', 'Update', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(684, 1, 'en', 'backLang', 'save', 'Save', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(685, 1, 'en', 'backLang', 'saveOrder', 'Save Order', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(686, 1, 'en', 'backLang', 'edit', 'Edit', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(687, 1, 'en', 'backLang', 'delete', 'Delete', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(688, 1, 'en', 'backLang', 'undoDelete', 'Undo Delete', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(689, 1, 'en', 'backLang', 'cancel', 'Cancel', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(690, 1, 'en', 'backLang', 'apply', 'Apply', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(692, 1, 'en', 'backLang', 'notActive', 'Not Active', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(693, 1, 'en', 'backLang', 'status', 'Status', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(694, 1, 'en', 'backLang', 'visits', 'Visits', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(695, 1, 'en', 'backLang', 'contents', 'Contents', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(696, 1, 'en', 'backLang', 'options', 'Options', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(697, 1, 'en', 'backLang', 'showing', 'Showing', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(698, 1, 'en', 'backLang', 'bulkAction', 'Bulk action', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(699, 1, 'en', 'backLang', 'activeSelected', 'Active selected', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(700, 1, 'en', 'backLang', 'blockSelected', 'Block selected', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(701, 1, 'en', 'backLang', 'deleteSelected', 'Delete selected', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(702, 1, 'en', 'backLang', 'of', 'of', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(703, 1, 'en', 'backLang', 'records', 'records', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(704, 1, 'en', 'backLang', 'confirmation', 'Confirmation', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(705, 1, 'en', 'backLang', 'yes', 'Yes', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(706, 1, 'en', 'backLang', 'no', 'No', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(707, 1, 'en', 'backLang', 'confirmationDeleteMsg', 'Are you sure you want to delete?', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(708, 1, 'en', 'backLang', 'sectionsOf', 'Categories of ', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(709, 1, 'en', 'backLang', 'comments', 'Comments', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(710, 1, 'en', 'backLang', 'videoTypes', 'Extensions: .mp4, .ogv, .webm', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(711, 1, 'en', 'backLang', 'imagesTypes', 'Extensions: .png, .jpg, .jpeg, .gif', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(712, 1, 'en', 'backLang', 'audioTypes', 'Extensions: .mp3, .wav', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(713, 1, 'en', 'backLang', 'attachTypes', 'Extensions: (* ALL)', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(714, 1, 'en', 'backLang', 'addDone', 'New record has been Added successfully', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(715, 1, 'en', 'backLang', 'saveDone', 'Modifications you have made saved successfully', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(716, 1, 'en', 'backLang', 'deleteDone', 'Data was deleted successfully', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(717, 1, 'en', 'backLang', 'erorr', 'There is an error, please try again', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(718, 1, 'en', 'backLang', 'noData', 'There is no data here up to now', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(719, 1, 'en', 'backLang', 'appsSettings', 'Active Apps', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(720, 1, 'en', 'backLang', 'fieldsSettings', 'Fields Settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(721, 1, 'en', 'backLang', 'frontSettings', 'Frontend Settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(722, 1, 'en', 'backLang', 'arabicLanguageFields', 'Arabic language fields', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(723, 1, 'en', 'backLang', 'englishLanguageFields', 'English language fields', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(724, 1, 'en', 'backLang', 'seoTab', 'SEO Tab', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(725, 1, 'en', 'backLang', 'seoTabTitle', 'SEO Settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(726, 1, 'en', 'backLang', 'defaultCurrency', 'Default currency', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(727, 1, 'en', 'backLang', 'commentsStatus', 'Comments Status', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(728, 1, 'en', 'backLang', 'automaticPublish', 'Automatic Publish', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(729, 1, 'en', 'backLang', 'manualByAdmin', 'Manual By Admin', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(730, 1, 'en', 'backLang', 'headerMenu', 'Header Menu', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(731, 1, 'en', 'backLang', 'footerMenu', 'Footer Menu', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(732, 1, 'en', 'backLang', 'homeSlideBanners', 'Home Page Slide Show Banners', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(733, 1, 'en', 'backLang', 'homeTextBanners', 'Home Page Text Banners', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(734, 1, 'en', 'backLang', 'sideBanners', 'Side Banners', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(735, 1, 'en', 'backLang', 'none', 'None', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(736, 1, 'en', 'backLang', 'newsletterGroup', 'Newsletter Emails Group (within contacts)', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(737, 1, 'en', 'backLang', 'homeRow1', 'Home Page Contents ROW 1', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(738, 1, 'en', 'backLang', 'homeRow2', 'Home Page Contents ROW 2', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(739, 1, 'en', 'backLang', 'homeRow3', 'Latest Topic (within footer)', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(740, 1, 'en', 'backLang', 'contactPageId', 'Contact Us Page (Details & Maps)', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(741, 1, 'en', 'backLang', 'activeLanguages', 'Active Languages', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(742, 1, 'en', 'backLang', 'activeLanguages1', 'One Language ( Default in .env )', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(743, 1, 'en', 'backLang', 'activeLanguages2', 'All Languages', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(744, 1, 'en', 'backLang', 'homeBanners', 'Home Banners', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(745, 1, 'en', 'backLang', 'textBanners', 'Text Banners', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(746, 1, 'en', 'backLang', 'sectionName', 'Section title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(747, 1, 'en', 'backLang', 'sectionType', 'Section type', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(748, 1, 'en', 'backLang', 'sectionNew', 'New section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(749, 1, 'en', 'backLang', 'sectionEdit', 'Edit section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(750, 1, 'en', 'backLang', 'sectionIcon', 'Icon', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(751, 1, 'en', 'backLang', 'sectionFather', 'Father Section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(752, 1, 'en', 'backLang', 'sectionNoFather', 'Father Section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(753, 1, 'en', 'backLang', 'size', 'Size', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(754, 1, 'en', 'backLang', 'width', 'Width', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(755, 1, 'en', 'backLang', 'height', 'Height', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(756, 1, 'en', 'backLang', 'descriptionBox', 'Box for description', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(757, 1, 'en', 'backLang', 'linkBox', 'Box for URL', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(758, 1, 'en', 'backLang', 'sectionTypeCode', 'Code / Text', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(759, 1, 'en', 'backLang', 'sectionTypePhoto', 'Photo', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(760, 1, 'en', 'backLang', 'sectionTypeVideo', 'Video', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(761, 1, 'en', 'backLang', 'langVar', 'Language Variable', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(762, 1, 'en', 'backLang', 'sitePages', 'Site pages', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(763, 1, 'en', 'backLang', 'photos', 'Photos', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(764, 1, 'en', 'backLang', 'blog', 'Blog', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(765, 1, 'en', 'backLang', 'services', 'Services', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(766, 1, 'en', 'backLang', 'news', 'News', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(767, 1, 'en', 'backLang', 'videos', 'Videos', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(768, 1, 'en', 'backLang', 'sounds', 'Audio', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(769, 1, 'en', 'backLang', 'products', 'Products', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(770, 1, 'en', 'backLang', 'Projects', 'Projects', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(771, 1, 'en', 'backLang', 'typeTextPages', 'General', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(772, 1, 'en', 'backLang', 'typePhotos', 'Photos', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(773, 1, 'en', 'backLang', 'typeVideos', 'Videos', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(774, 1, 'en', 'backLang', 'typeSounds', 'Audio', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(775, 1, 'en', 'backLang', 'hasCategories', 'Categories', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(776, 1, 'en', 'backLang', 'reviewsAvailable', 'Comments Available', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(777, 1, 'en', 'backLang', 'dateField', 'Date Field', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(778, 1, 'en', 'backLang', 'longTextField', 'Long Text Field', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(779, 1, 'en', 'backLang', 'allowEditor', 'Allow Editor', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(780, 1, 'en', 'backLang', 'attachFileField', 'Attach File Field', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(781, 1, 'en', 'backLang', 'additionalImages', 'Additional Images', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(782, 1, 'en', 'backLang', 'attachGoogleMaps', 'Attach Google Maps', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(783, 1, 'en', 'backLang', 'attachOrderForm', 'Attach Order Form', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(784, 1, 'en', 'backLang', 'withoutCategories', 'Without Categories', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(785, 1, 'en', 'backLang', 'mainCategoriesOnly', 'Main categories only', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(786, 1, 'en', 'backLang', 'mainAndSubCategories', 'Main and sub categories', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(787, 1, 'en', 'backLang', 'sectionIconPicker', 'Section Icon (Picker)', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(788, 1, 'en', 'backLang', 'topicsIconPicker', 'Topics Icon (Picker)', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(789, 1, 'en', 'backLang', 'iconPicker', 'Icon (Picker)', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(790, 1, 'en', 'backLang', 'siteInfoSettings', 'Website Info', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(791, 1, 'en', 'backLang', 'siteStatusSettings', 'Website Status', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(792, 1, 'en', 'backLang', 'siteSocialSettings', 'Social Links', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(793, 1, 'en', 'backLang', 'siteContactsSettings', 'Contacts', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(794, 1, 'en', 'backLang', 'websiteTitle', 'Website Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(795, 1, 'en', 'backLang', 'metaDescription', 'Meta Description', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(796, 1, 'en', 'backLang', 'metaKeywords', 'Meta Keywords', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(797, 1, 'en', 'backLang', 'websiteUrl', 'Website URL', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(798, 1, 'en', 'backLang', 'websiteNotificationEmail', 'Website Notification Email', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(799, 1, 'en', 'backLang', 'websiteNotificationEmail1', 'Send me an email on new contact Messages', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(800, 1, 'en', 'backLang', 'websiteNotificationEmail2', 'Send me an email on new Comments', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(801, 1, 'en', 'backLang', 'websiteNotificationEmail3', 'Send me an email on new Orders', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(802, 1, 'en', 'backLang', 'emailNotifications', 'Email Notifications', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(803, 1, 'en', 'backLang', 'contactAddress', 'Address', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(804, 1, 'en', 'backLang', 'contactPhone', 'Phone', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(805, 1, 'en', 'backLang', 'contactFax', 'Fax', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(806, 1, 'en', 'backLang', 'contactMobile', 'Mobile', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(807, 1, 'en', 'backLang', 'contactEmail', 'Email', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(808, 1, 'en', 'backLang', 'worksTime', 'Working Time', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(809, 1, 'en', 'backLang', 'siteStatusSettingsPublished', 'Published', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(810, 1, 'en', 'backLang', 'siteStatusSettingsClosed', 'Closed', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(811, 1, 'en', 'backLang', 'siteStatusSettingsMsg', 'Close Message', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(812, 1, 'en', 'backLang', 'facebook', 'Facebook', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(813, 1, 'en', 'backLang', 'twitter', 'Twitter', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(814, 1, 'en', 'backLang', 'google', 'Google+', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(815, 1, 'en', 'backLang', 'linkedin', 'Linkedin', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(816, 1, 'en', 'backLang', 'youtube', 'Youtube', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(817, 1, 'en', 'backLang', 'instagram', 'Instagram', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(818, 1, 'en', 'backLang', 'pinterest', 'Pinterest', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(819, 1, 'en', 'backLang', 'tumblr', 'Tumblr', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(820, 1, 'en', 'backLang', 'flickr', 'Flickr', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(821, 1, 'en', 'backLang', 'whatapp', 'Whatsapp', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(822, 1, 'en', 'backLang', 'styleSettings', 'Style Settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(823, 1, 'en', 'backLang', 'siteLogo', 'Site Logo', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(824, 1, 'en', 'backLang', 'favicon', 'Favicon', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(825, 1, 'en', 'backLang', 'appleIcon', 'Apple Icon', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(826, 1, 'en', 'backLang', 'styleColor1', 'Style Color 1', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(827, 1, 'en', 'backLang', 'styleColor2', 'Style Color 2', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(828, 1, 'en', 'backLang', 'restoreDefault', 'Restore Default', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(829, 1, 'en', 'backLang', 'layoutMode', 'Layout Mode', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(830, 1, 'en', 'backLang', 'wide', 'Wide', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(831, 1, 'en', 'backLang', 'boxed', 'Boxed', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(832, 1, 'en', 'backLang', 'backgroundStyle', 'Background Style', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(833, 1, 'en', 'backLang', 'colorBackground', 'Color Background', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(834, 1, 'en', 'backLang', 'patternsBackground', 'Pattern Background', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(835, 1, 'en', 'backLang', 'imageBackground', 'Image Background', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(836, 1, 'en', 'backLang', 'newsletterSubscribe', 'Newsletter Subscribe', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(837, 1, 'en', 'backLang', 'footerStyle', 'Footer Style', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(838, 1, 'en', 'backLang', 'footerStyleBg', 'Footer Background', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(839, 1, 'en', 'backLang', 'preLoad', 'Preloader', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(840, 1, 'en', 'backLang', 'bannerAdd', 'Add New Banner', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(841, 1, 'en', 'backLang', 'bannerEdit', 'Edit Banner', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(842, 1, 'en', 'backLang', 'bannerTitle', 'Banner Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(843, 1, 'en', 'backLang', 'bannerSection', 'Section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(844, 1, 'en', 'backLang', 'bannerPhoto', 'Photo', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(845, 1, 'en', 'backLang', 'bannerDetails', 'Details', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(846, 1, 'en', 'backLang', 'bannerLinkUrl', 'Banner URL', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(847, 1, 'en', 'backLang', 'bannerVideoType', 'Video type', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(848, 1, 'en', 'backLang', 'bannerVideoType1', 'Direct video', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(849, 1, 'en', 'backLang', 'bannerVideoType2', 'Youtube', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(850, 1, 'en', 'backLang', 'bannerVideoType3', 'Vimeo', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(851, 1, 'en', 'backLang', 'bannerVideoFile', 'Video File', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(852, 1, 'en', 'backLang', 'bannerVideoUrl', 'Youtube video URL', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(853, 1, 'en', 'backLang', 'bannerVideoUrl2', 'Vimeo video URL', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(854, 1, 'en', 'backLang', 'bannerCode', 'Banner Code', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(855, 1, 'en', 'backLang', 'topicNew', 'New ', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(856, 1, 'en', 'backLang', 'topicEdit', 'Edit ', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(857, 1, 'en', 'backLang', 'topicName', 'Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(858, 1, 'en', 'backLang', 'topicPhoto', 'Photo', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(859, 1, 'en', 'backLang', 'topicSection', 'Section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(860, 1, 'en', 'backLang', 'topicSelectSection', 'Choose Section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(861, 1, 'en', 'backLang', 'topicDate', 'Date', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(862, 1, 'en', 'backLang', 'topicAudio', 'Audio File', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(863, 1, 'en', 'backLang', 'topicVideo', 'Video File', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(864, 1, 'en', 'backLang', 'topicAttach', 'Attach File', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(865, 1, 'en', 'backLang', 'topicDeletedSection', 'No parent section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(866, 1, 'en', 'backLang', 'topicTabSection', 'Section Details', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(867, 1, 'en', 'backLang', 'topicTabDetails', 'Topic Details', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(868, 1, 'en', 'backLang', 'topicSEOTitle', 'Page title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(869, 1, 'en', 'backLang', 'topicSEODesc', 'Meta Description', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(870, 1, 'en', 'backLang', 'topicSEOKeywords', 'Meta Keywords', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(871, 1, 'en', 'backLang', 'topicAdditionalPhotos', 'Photos', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(872, 1, 'en', 'backLang', 'topicGoogleMaps', 'Google Maps', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(873, 1, 'en', 'backLang', 'topicDropFiles', 'Drop photos here or click to upload', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(874, 1, 'en', 'backLang', 'topicDropFiles2', 'You can upload many photos at the same time', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(875, 1, 'en', 'backLang', 'topicCommentName', 'Name', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(876, 1, 'en', 'backLang', 'topicCommentEmail', 'Email', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(877, 1, 'en', 'backLang', 'topicComment', 'Comment', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(878, 1, 'en', 'backLang', 'topicNewComment', 'New Comment', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(879, 1, 'en', 'backLang', 'topicNewMap', 'New Map', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(880, 1, 'en', 'backLang', 'topicMapTitle', 'Map Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(881, 1, 'en', 'backLang', 'topicMapDetails', 'Details', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(882, 1, 'en', 'backLang', 'topicMapLocation', 'Location', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(883, 1, 'en', 'backLang', 'topicMapIcon', 'Icon', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(884, 1, 'en', 'backLang', 'topicMapClick', 'Click on the location to add New Marker', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(885, 1, 'en', 'backLang', 'topicMapORClick', 'OR click here to add Manually', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(886, 1, 'en', 'backLang', 'allContacts', 'All Contacts', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(887, 1, 'en', 'backLang', 'waitActivation', 'Wait Activation', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(888, 1, 'en', 'backLang', 'blockedContacts', 'Blocked Contacts', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(889, 1, 'en', 'backLang', 'newGroup', 'New Group', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(890, 1, 'en', 'backLang', 'newContacts', 'New Contact', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(891, 1, 'en', 'backLang', 'editContacts', 'Edit Contact', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(892, 1, 'en', 'backLang', 'deleteContacts', 'Delete Contact', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(893, 1, 'en', 'backLang', 'searchAllContacts', 'Search all contacts', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(894, 1, 'en', 'backLang', 'firstName', 'First Name', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(895, 1, 'en', 'backLang', 'lastName', 'Last Name', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(896, 1, 'en', 'backLang', 'companyName', 'Company Name', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(897, 1, 'en', 'backLang', 'city', 'City', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(898, 1, 'en', 'backLang', 'country', 'Country', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(899, 1, 'en', 'backLang', 'notes', 'Notes', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(900, 1, 'en', 'backLang', 'group', 'Group', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(901, 1, 'en', 'backLang', 'sendEmail', 'Send Email', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(902, 1, 'en', 'backLang', 'callNow', 'Call Now', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(903, 1, 'en', 'backLang', 'selectFile', 'Select file', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(904, 1, 'en', 'backLang', 'labels', 'Labels', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(905, 1, 'en', 'backLang', 'inbox', 'Inbox', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(906, 1, 'en', 'backLang', 'sent', 'Sent', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(907, 1, 'en', 'backLang', 'draft', 'Draft', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(908, 1, 'en', 'backLang', 'compose', 'Compose', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(909, 1, 'en', 'backLang', 'makeAsRead', 'Make as read', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(910, 1, 'en', 'backLang', 'makeAsUnread', 'Make as unread', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(911, 1, 'en', 'backLang', 'moveTo', 'Move to ', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(912, 1, 'en', 'backLang', 'replay', 'Replay', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(913, 1, 'en', 'backLang', 'forward', 'Forward', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(914, 1, 'en', 'backLang', 'sendTo', 'To', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(915, 1, 'en', 'backLang', 'sendFrom', 'From', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(916, 1, 'en', 'backLang', 'sendCc', 'Cc', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(917, 1, 'en', 'backLang', 'sendBcc', 'Bcc', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(918, 1, 'en', 'backLang', 'sendTitle', 'Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(919, 1, 'en', 'backLang', 'SaveToDraft', 'Save to draft', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(920, 1, 'en', 'backLang', 'send', 'Send', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(921, 1, 'en', 'backLang', 'print', 'Print', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(922, 1, 'en', 'backLang', 'AttachFiles', 'Attach Files', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(923, 1, 'en', 'backLang', 'newEvent', 'New Event / Note', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(924, 1, 'en', 'backLang', 'eventTotal', 'Total', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(925, 1, 'en', 'backLang', 'eventTitle', 'Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(926, 1, 'en', 'backLang', 'eventStart', 'Event Start', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(927, 1, 'en', 'backLang', 'eventEnd', 'Event End', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(928, 1, 'en', 'backLang', 'eventStart2', 'Start', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(929, 1, 'en', 'backLang', 'eventEnd2', 'End', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(930, 1, 'en', 'backLang', 'eventDetails', 'Details', '2018-12-15 01:54:01', '2018-12-15 01:54:01');
INSERT INTO `ltm_translations` (`id`, `status`, `locale`, `group`, `key`, `value`, `created_at`, `updated_at`) VALUES
(931, 1, 'en', 'backLang', 'eventNote', 'Note', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(932, 1, 'en', 'backLang', 'eventMeeting', 'Meeting', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(933, 1, 'en', 'backLang', 'eventEvent', 'Event', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(934, 1, 'en', 'backLang', 'eventTask', 'Task', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(935, 1, 'en', 'backLang', 'eventType', 'Type', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(936, 1, 'en', 'backLang', 'eventAt', 'At', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(937, 1, 'en', 'backLang', 'eventToday', 'Today', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(938, 1, 'en', 'backLang', 'eventDay', 'Day', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(939, 1, 'en', 'backLang', 'eventWeek', 'Week', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(940, 1, 'en', 'backLang', 'eventMonth', 'Month', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(941, 1, 'en', 'backLang', 'eventDelete', 'Delete this note/Event?', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(942, 1, 'en', 'backLang', 'eventClear', 'Delete All notes & events?', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(943, 1, 'en', 'backLang', 'diagram', 'Diagram', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(944, 1, 'en', 'backLang', 'barDiagram', 'Showing data as a bar diagram', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(945, 1, 'en', 'backLang', 'visitors', 'Visitors', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(946, 1, 'en', 'backLang', 'ip', 'IP', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(947, 1, 'en', 'backLang', 'pageViews', 'Page Views', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(948, 1, 'en', 'backLang', 'today', 'Today', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(949, 1, 'en', 'backLang', 'yesterday', 'Yesterday', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(950, 1, 'en', 'backLang', 'last7Days', 'Last 7 Days', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(951, 1, 'en', 'backLang', 'last30Days', 'Last 30 Days', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(952, 1, 'en', 'backLang', 'thisMonth', 'This Month', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(953, 1, 'en', 'backLang', 'lastMonth', 'Last Month', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(954, 1, 'en', 'backLang', 'applyFrom', 'From', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(955, 1, 'en', 'backLang', 'applyTo', 'To', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(956, 1, 'en', 'backLang', 'customRange', 'Custom Range', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(957, 1, 'en', 'backLang', 'weekDays', '\"Su\", \"Mo\", \"Tu\", \"We\", \"Th\", \"Fr\", \"Sa\"', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(958, 1, 'en', 'backLang', 'monthsNames', '\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\", \"August\", \"September\", \"October\", \"November\", \"December\"', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(959, 1, 'en', 'backLang', 'activity', 'Activity', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(960, 1, 'en', 'backLang', 'activitiesHistory', 'Activities History', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(961, 1, 'en', 'backLang', 'newUser', 'New User', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(962, 1, 'en', 'backLang', 'editUser', 'Edit User', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(963, 1, 'en', 'backLang', 'fullName', 'Full Name', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(964, 1, 'en', 'backLang', 'Permission', 'Permission', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(965, 1, 'en', 'backLang', 'Permission1', 'Webmaster', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(966, 1, 'en', 'backLang', 'Permission2', 'Website Manager ( Full Features )', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(967, 1, 'en', 'backLang', 'Permission3', 'Limited User ( no delete )', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(968, 1, 'en', 'backLang', 'Permission4', 'Review User ( View Only )', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(969, 1, 'en', 'backLang', 'personalPhoto', 'Personal Photo', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(970, 1, 'en', 'backLang', 'loginEmail', 'Login Email', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(971, 1, 'en', 'backLang', 'loginPassword', 'Login Password', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(972, 1, 'en', 'backLang', 'users', 'Users', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(973, 1, 'en', 'backLang', 'permissions', 'Permissions', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(974, 1, 'en', 'backLang', 'newPermissions', 'New Permission', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(975, 1, 'en', 'backLang', 'title', 'Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(976, 1, 'en', 'backLang', 'dataManagements', 'Data Managements', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(977, 1, 'en', 'backLang', 'dataManagements1', 'His Own Data Only (for Personal Accounts)', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(978, 1, 'en', 'backLang', 'dataManagements2', 'All Users Data (for General Administration Accounts)', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(979, 1, 'en', 'backLang', 'activeApps', 'Active Apps', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(980, 1, 'en', 'backLang', 'activeSiteSections', 'Active Site Sections', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(981, 1, 'en', 'backLang', 'addPermission', 'ADD Permission', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(982, 1, 'en', 'backLang', 'editPermission', 'EDIT Permission', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(983, 1, 'en', 'backLang', 'deletePermission', 'DELETE Permission', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(984, 1, 'en', 'backLang', 'viewOnly', 'VIEW ONLY', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(985, 1, 'en', 'backLang', 'editPermissions', 'Edit Permissions', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(986, 1, 'en', 'backLang', 'perAdd', 'ADD', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(987, 1, 'en', 'backLang', 'perEdit', 'EDIT', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(988, 1, 'en', 'backLang', 'perDelete', 'DELETE', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(989, 1, 'en', 'backLang', 'selectPermissionsType', 'Select Permissions Type', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(990, 1, 'en', 'backLang', 'newLink', 'New Link', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(991, 1, 'en', 'backLang', 'newMenu', 'New Menu', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(992, 1, 'en', 'backLang', 'menuTitle', 'Menu Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(993, 1, 'en', 'backLang', 'editSection', 'Edit Link', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(994, 1, 'en', 'backLang', 'linkType', 'Link Type', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(995, 1, 'en', 'backLang', 'linkType1', 'Main Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(996, 1, 'en', 'backLang', 'linkType2', 'Direct Link', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(997, 1, 'en', 'backLang', 'linkType3', 'Main Section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(998, 1, 'en', 'backLang', 'linkType4', 'Drop list', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(999, 1, 'en', 'backLang', 'linkURL', 'Link URL', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1000, 1, 'en', 'backLang', 'linkSection', 'Link Section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1001, 1, 'en', 'backLang', 'sectionTitle', 'Link/Section Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1002, 1, 'en', 'backLang', 'fatherSection', 'Father Section', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1003, 1, 'en', 'backLang', 'latestMessages', 'Messages', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1004, 1, 'en', 'backLang', 'notesEvents', 'Notes & Events', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1005, 1, 'en', 'backLang', 'latestContacts', 'New Contacts', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1006, 1, 'en', 'backLang', 'more', 'More', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1007, 1, 'en', 'backLang', 'viewMore', 'View More', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1008, 1, 'en', 'backLang', 'addNew', 'Add New', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1009, 1, 'en', 'backLang', 'reports', 'Reports', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1010, 1, 'en', 'backLang', 'reportsDetails', 'You can view more reports', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1011, 1, 'en', 'backLang', 'hi', 'Hi', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1012, 1, 'en', 'backLang', 'welcomeBack', 'Welcome back', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1013, 1, 'en', 'backLang', 'lastFor7Days', 'Visitors for the last 7 Days', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1014, 1, 'en', 'backLang', 'todayByCountry', 'Today by country', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1015, 1, 'en', 'backLang', 'browsers', 'Browsers', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1016, 1, 'en', 'backLang', 'browsersCalculated', 'Calculated in last 7 days', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1017, 1, 'en', 'backLang', 'visitorsRate', 'Visitors Rate', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1018, 1, 'en', 'backLang', 'visitorsRateToday', 'Visitors rate over the current day', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1019, 1, 'en', 'backLang', 'pages', 'Pages', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1020, 1, 'en', 'backLang', 'sections', 'Sections', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1021, 1, 'en', 'backLang', 'resultsFoundFor', 'Results found for', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1022, 1, 'en', 'backLang', 'connectEmailToConnect', 'Connect to webmail account', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1023, 1, 'en', 'backLang', 'connectEmail', 'Email', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1024, 1, 'en', 'backLang', 'connectPassword', 'Password', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1025, 1, 'en', 'backLang', 'openWebmail', 'Open Webmail', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1026, 1, 'en', 'backLang', 'themeSwitcher', 'Theme Switcher', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1027, 1, 'en', 'backLang', 'foldedAside', 'Folded Aside', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1028, 1, 'en', 'backLang', 'boxedLayout', 'Boxed Layout', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1029, 1, 'en', 'backLang', 'themes', 'Themes', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1030, 1, 'en', 'backLang', 'themes1', 'LIGHT', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1031, 1, 'en', 'backLang', 'themes2', 'GREY', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1032, 1, 'en', 'backLang', 'themes3', 'Dark', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1033, 1, 'en', 'backLang', 'themes4', 'Black', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1034, 1, 'en', 'backLang', 'language', 'Language', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1035, 1, 'en', 'backLang', 'change', 'Change', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1036, 1, 'en', 'backLang', 'sitePreview', 'website', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1037, 1, 'en', 'backLang', 'oops', 'OOPS', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1038, 1, 'en', 'backLang', 'noPermission', 'Sorry! You don\'t have permission to view this page', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1039, 1, 'en', 'backLang', 'notFound', 'The page you are looking for can\'t be found', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1040, 1, 'en', 'backLang', 'forgotPassword', 'Forgot password?', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1041, 1, 'en', 'backLang', 'signIn', 'Sign in', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1042, 1, 'en', 'backLang', 'keepMeSignedIn', 'Keep me signed in', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1043, 1, 'en', 'backLang', 'signedInToControl', 'Sign in to CONTROL', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1044, 1, 'en', 'backLang', 'control', 'Charity Challengers', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1045, 1, 'en', 'backLang', 'resetPassword', 'Reset Password', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1046, 1, 'en', 'backLang', 'confirmPassword', 'Confirm Password', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1047, 1, 'en', 'backLang', 'newPassword', 'New Password', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1048, 1, 'en', 'backLang', 'yourEmail', 'Your Email', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1049, 1, 'en', 'backLang', 'sendPasswordResetLink', 'Send Password Reset Link', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1050, 1, 'en', 'backLang', 'returnTo', 'Return Back', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1051, 1, 'en', 'backLang', 'enterYourEmail', 'Enter your email address below and we will send you instructions on how to change your password.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1052, 1, 'en', 'backLang', 'expireDateField', 'Expire Date Field', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1053, 1, 'en', 'backLang', 'expireDate', 'Expire Date', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1054, 1, 'en', 'backLang', 'expired', 'Expired', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1055, 1, 'en', 'backLang', 'additionalFiles', 'Additional Files', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1056, 1, 'en', 'backLang', 'relatedTopics', 'Related Topics', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1057, 1, 'en', 'backLang', 'relatedTopicsAdd', 'Add Related Topics', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1058, 1, 'en', 'backLang', 'relatedTopicsSelect', 'Select Site Section To load Topics', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1059, 1, 'en', 'backLang', 'customFields', 'Additional Fields', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1060, 1, 'en', 'backLang', 'customFieldsTitle', 'Field Title', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1061, 1, 'en', 'backLang', 'customFieldsType', 'Field Type', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1062, 1, 'en', 'backLang', 'customFieldsStatus', 'Field Status', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1063, 1, 'en', 'backLang', 'customFieldsNewField', 'Add New Field', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1064, 1, 'en', 'backLang', 'customFieldsEditField', 'Field Edit', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1065, 1, 'en', 'backLang', 'customFieldsRequired', 'Required', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1066, 1, 'en', 'backLang', 'customFieldsOptional', 'Optional', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1067, 1, 'en', 'backLang', 'customFieldsDefault', 'Default Value', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1068, 1, 'en', 'backLang', 'customFieldsOptions', 'Options', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1069, 1, 'en', 'backLang', 'customFieldsOptionsHelp', 'Type every option on separate line', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1070, 1, 'en', 'backLang', 'customFieldsForAllLang', 'For All Languages', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1071, 1, 'en', 'backLang', 'customFieldsType0', 'Text Box', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1072, 1, 'en', 'backLang', 'customFieldsType1', 'Text Area', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1073, 1, 'en', 'backLang', 'customFieldsType2', 'Number', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1074, 1, 'en', 'backLang', 'customFieldsType3', 'Email Address', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1075, 1, 'en', 'backLang', 'customFieldsType4', 'Date', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1076, 1, 'en', 'backLang', 'customFieldsType5', 'Date & Time', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1077, 1, 'en', 'backLang', 'customFieldsType6', 'Select', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1078, 1, 'en', 'backLang', 'customFieldsType7', 'Multi Check', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1079, 1, 'en', 'backLang', 'customFieldsType8', 'Photo File', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1080, 1, 'en', 'backLang', 'customFieldsType9', 'Attach File', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1081, 1, 'en', 'backLang', 'customFieldsType10', 'Video File', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1082, 1, 'en', 'backLang', 'customFieldsType11', 'Youtube Video Link', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1083, 1, 'en', 'backLang', 'customFieldsType12', 'Vimeo Video Link', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1084, 1, 'en', 'backLang', 'optionalFields', 'Optional Fields', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1085, 1, 'en', 'backLang', 'additionalTabs', 'Additional Tabs', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1086, 1, 'en', 'backLang', 'active_disable', 'Active/Disable', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1087, 1, 'en', 'backLang', 'friendlyURL', 'Friendly URL', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1088, 1, 'en', 'backLang', 'seoTabSettings', 'Mange your page title, meta description, keywords and unique friendly URL. This help you to manage how this topic shows up on search engines.', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1089, 1, 'en', 'backLang', 'friendlyURLinks', 'Frontend URLs type', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1090, 1, 'en', 'backLang', 'friendlyURLinks1', 'Default URLs', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1091, 1, 'en', 'backLang', 'friendlyURLinks2', 'Search engine friendly URLs', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1092, 1, 'en', 'backLang', 'registrationSettings', 'Registration Settings', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1093, 1, 'en', 'backLang', 'allowRegister', 'Allow registration for Backend', '2018-12-15 01:54:01', '2018-12-15 01:54:01'),
(1094, 1, 'en', 'backLang', 'permissionForNewUsers', 'Permission group for new registration', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1095, 1, 'en', 'backLang', 'createNewAccount', 'Create a new account', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1096, 1, 'en', 'backLang', 'APIStatus', 'RESTful API status', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1097, 1, 'en', 'backLang', 'APIKey', 'API key', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1098, 1, 'en', 'backLang', 'APIKeyConfirm', 'Are you sure you want to generate a new API key?', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1099, 1, 'en', 'backLang', 'homeRow_3', 'Home Page Contents ROW 3', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1100, 1, 'en', 'backLang', 'partners', 'Partners', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1101, 1, 'en', 'validation', 'accepted', 'The :attribute must be accepted.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1102, 1, 'en', 'validation', 'active_url', 'The :attribute is not a valid URL.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1103, 1, 'en', 'validation', 'after', 'The :attribute must be a date after :date.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1104, 1, 'en', 'validation', 'after_or_equal', 'The :attribute must be a date after or equal to :date.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1105, 1, 'en', 'validation', 'alpha', 'The :attribute may only contain letters.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1106, 1, 'en', 'validation', 'alpha_dash', 'The :attribute may only contain letters, numbers, and dashes.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1107, 1, 'en', 'validation', 'alpha_num', 'The :attribute may only contain letters and numbers.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1108, 1, 'en', 'validation', 'array', 'The :attribute must be an array.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1109, 1, 'en', 'validation', 'before', 'The :attribute must be a date before :date.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1110, 1, 'en', 'validation', 'before_or_equal', 'The :attribute must be a date before or equal to :date.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1111, 1, 'en', 'validation', 'between.numeric', 'The :attribute must be between :min and :max.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1112, 1, 'en', 'validation', 'between.file', 'The :attribute must be between :min and :max kilobytes.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1113, 1, 'en', 'validation', 'between.string', 'The :attribute must be between :min and :max characters.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1114, 1, 'en', 'validation', 'between.array', 'The :attribute must have between :min and :max items.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1115, 1, 'en', 'validation', 'boolean', 'The :attribute field must be true or false.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1116, 1, 'en', 'validation', 'confirmed', 'The :attribute confirmation does not match.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1117, 1, 'en', 'validation', 'date', 'The :attribute is not a valid date.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1118, 1, 'en', 'validation', 'date_format', 'The :attribute does not match the format :format.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1119, 1, 'en', 'validation', 'different', 'The :attribute and :other must be different.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1120, 1, 'en', 'validation', 'digits', 'The :attribute must be :digits digits.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1121, 1, 'en', 'validation', 'digits_between', 'The :attribute must be between :min and :max digits.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1122, 1, 'en', 'validation', 'dimensions', 'The :attribute has invalid image dimensions.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1123, 1, 'en', 'validation', 'distinct', 'The :attribute field has a duplicate value.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1124, 1, 'en', 'validation', 'email', 'The :attribute must be a valid email address.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1125, 1, 'en', 'validation', 'exists', 'The selected :attribute is invalid.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1126, 1, 'en', 'validation', 'file', 'The :attribute must be a file.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1127, 1, 'en', 'validation', 'filled', 'The :attribute field must have a value.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1128, 1, 'en', 'validation', 'image', 'The :attribute must be an image.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1129, 1, 'en', 'validation', 'in', 'The selected :attribute is invalid.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1130, 1, 'en', 'validation', 'in_array', 'The :attribute field does not exist in :other.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1131, 1, 'en', 'validation', 'integer', 'The :attribute must be an integer.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1132, 1, 'en', 'validation', 'ip', 'The :attribute must be a valid IP address.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1133, 1, 'en', 'validation', 'ipv4', 'The :attribute must be a valid IPv4 address.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1134, 1, 'en', 'validation', 'ipv6', 'The :attribute must be a valid IPv6 address.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1135, 1, 'en', 'validation', 'json', 'The :attribute must be a valid JSON string.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1136, 1, 'en', 'validation', 'max.numeric', 'The :attribute may not be greater than :max.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1137, 1, 'en', 'validation', 'max.file', 'The :attribute may not be greater than :max kilobytes.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1138, 1, 'en', 'validation', 'max.string', 'The :attribute may not be greater than :max characters.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1139, 1, 'en', 'validation', 'max.array', 'The :attribute may not have more than :max items.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1140, 1, 'en', 'validation', 'mimes', 'The :attribute must be a file of type: :values.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1141, 1, 'en', 'validation', 'mimetypes', 'The :attribute must be a file of type: :values.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1142, 1, 'en', 'validation', 'min.numeric', 'The :attribute must be at least :min.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1143, 1, 'en', 'validation', 'min.file', 'The :attribute must be at least :min kilobytes.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1144, 1, 'en', 'validation', 'min.string', 'The :attribute must be at least :min characters.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1145, 1, 'en', 'validation', 'min.array', 'The :attribute must have at least :min items.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1146, 1, 'en', 'validation', 'not_in', 'The selected :attribute is invalid.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1147, 1, 'en', 'validation', 'numeric', 'The :attribute must be a number.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1148, 1, 'en', 'validation', 'present', 'The :attribute field must be present.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1149, 1, 'en', 'validation', 'regex', 'The :attribute format is invalid.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1150, 1, 'en', 'validation', 'required', 'The :attribute field is required.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1151, 1, 'en', 'validation', 'required_if', 'The :attribute field is required when :other is :value.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1152, 1, 'en', 'validation', 'required_unless', 'The :attribute field is required unless :other is in :values.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1153, 1, 'en', 'validation', 'required_with', 'The :attribute field is required when :values is present.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1154, 1, 'en', 'validation', 'required_with_all', 'The :attribute field is required when :values is present.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1155, 1, 'en', 'validation', 'required_without', 'The :attribute field is required when :values is not present.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1156, 1, 'en', 'validation', 'required_without_all', 'The :attribute field is required when none of :values are present.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1157, 1, 'en', 'validation', 'same', 'The :attribute and :other must match.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1158, 1, 'en', 'validation', 'size.numeric', 'The :attribute must be :size.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1159, 1, 'en', 'validation', 'size.file', 'The :attribute must be :size kilobytes.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1160, 1, 'en', 'validation', 'size.string', 'The :attribute must be :size characters.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1161, 1, 'en', 'validation', 'size.array', 'The :attribute must contain :size items.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1162, 1, 'en', 'validation', 'string', 'The :attribute must be a string.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1163, 1, 'en', 'validation', 'timezone', 'The :attribute must be a valid zone.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1164, 1, 'en', 'validation', 'unique', 'The :attribute has already been taken.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1165, 1, 'en', 'validation', 'uploaded', 'The :attribute failed to upload.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1166, 1, 'en', 'validation', 'url', 'The :attribute format is invalid.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1167, 1, 'en', 'validation', 'custom.attribute-name.rule-name', 'custom-message', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1168, 1, 'en', 'auth', 'failed', 'These credentials do not match our records.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1169, 1, 'en', 'auth', 'throttle', 'Too many login attempts. Please try again in :seconds seconds.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1170, 1, 'en', 'frontLang', 'dashboard', 'Dashboard', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1171, 1, 'en', 'frontLang', 'boxCode', '', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1172, 1, 'en', 'frontLang', 'toTop', 'to Top', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1173, 1, 'en', 'frontLang', 'AllRightsReserved', 'All Rights Reserved', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1174, 1, 'en', 'frontLang', 'moreDetails', 'More Details', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1175, 1, 'en', 'frontLang', 'viewMore', 'View More', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1176, 1, 'en', 'frontLang', 'readMore', 'Read More', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1177, 1, 'en', 'frontLang', 'downloadAttach', 'Download Attachment', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1178, 1, 'en', 'frontLang', 'contactDetails', 'Contact Details', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1179, 1, 'en', 'frontLang', 'callUs', 'Call Us', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1180, 1, 'en', 'frontLang', 'callFax', 'Fax', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1181, 1, 'en', 'frontLang', 'callMobile', 'Mobile', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1182, 1, 'en', 'frontLang', 'callPhone', 'Phone', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1183, 1, 'en', 'frontLang', 'callTimes', 'Working Times', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1184, 1, 'en', 'frontLang', 'address', 'Address', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1185, 1, 'en', 'frontLang', 'name', 'Name', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1186, 1, 'en', 'frontLang', 'email', 'Email', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1187, 1, 'en', 'frontLang', 'phone', 'Phone', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1188, 1, 'en', 'frontLang', 'latestNews', 'Latest News', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1189, 1, 'en', 'frontLang', 'ourGallery', 'Our Gallery', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1190, 1, 'en', 'frontLang', 'subscribeToOurNewsletter', 'Subscribe To Our Newsletter', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1191, 1, 'en', 'frontLang', 'subscribeToOurNewsletterDone', 'You have Subscribed', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1192, 1, 'en', 'frontLang', 'subscribeToOurNewsletterError', 'You Already Subscribed', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1193, 1, 'en', 'frontLang', 'newsletter', 'Our Newsletter', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1194, 1, 'en', 'frontLang', 'subscribe', 'Subscribe', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1195, 1, 'en', 'frontLang', 'yourEmail', 'Your Email', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1196, 1, 'en', 'frontLang', 'yourName', 'Your Name', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1197, 1, 'en', 'frontLang', 'enterYourName', 'Please enter your name', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1198, 1, 'en', 'frontLang', 'enterYourEmail', 'Please enter your email address', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1199, 1, 'en', 'frontLang', 'enterYourPhone', 'Please enter your phone number', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1200, 1, 'en', 'frontLang', 'enterYourSubject', 'Please enter a subject for your message', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1201, 1, 'en', 'frontLang', 'enterYourMessage', 'Please type your message', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1202, 1, 'en', 'frontLang', 'youMessageSent', 'Your message has been sent successfully, We will contact you as soon as possible. Thank you!', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1203, 1, 'en', 'frontLang', 'youMessageNotSent', 'Error: Please try again', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1204, 1, 'en', 'frontLang', 'subject', 'Subject', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1205, 1, 'en', 'frontLang', 'message', 'Message', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1206, 1, 'en', 'frontLang', 'sendMessage', 'Send Message', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1207, 1, 'en', 'frontLang', 'workingHours', 'Working Hours', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1208, 1, 'en', 'frontLang', 'categories', 'Categories', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1209, 1, 'en', 'frontLang', 'noData', 'There is no data here up to now', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1210, 1, 'en', 'frontLang', 'mostViewed', 'Most Viewed', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1211, 1, 'en', 'frontLang', 'share', 'Share', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1212, 1, 'en', 'frontLang', 'locationMap', 'Location Map', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1213, 1, 'en', 'frontLang', 'comments', 'Comments', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1214, 1, 'en', 'frontLang', 'comment', 'Comment', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1215, 1, 'en', 'frontLang', 'enterYourComment', 'Please enter your comment', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1216, 1, 'en', 'frontLang', 'sendComment', 'Send Comment', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1217, 1, 'en', 'frontLang', 'youCommentSent', 'Your Comment has been sent successfully. Thank you!', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1218, 1, 'en', 'frontLang', 'newComment', 'Add New Comment', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1219, 1, 'en', 'frontLang', 'refresh', 'Refresh', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1220, 1, 'en', 'frontLang', 'getInTouch', 'Get in touch with us by filling contact form below', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1221, 1, 'en', 'frontLang', 'homeContents1Title', 'Latest Articles', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1222, 1, 'en', 'frontLang', 'homeContents1desc', 'The latest articles from our blog, you can browse more', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1223, 1, 'en', 'frontLang', 'homeContents2Title', 'Recent Works', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1224, 1, 'en', 'frontLang', 'homeContents2desc', 'Some of our latest works, you can browse more', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1225, 1, 'en', 'frontLang', 'search', 'Search for...', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1226, 1, 'en', 'frontLang', 'visits', 'Visits', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1227, 1, 'en', 'frontLang', 'orderForm', 'Order form', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1228, 1, 'en', 'frontLang', 'quantity', 'Quantity', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1229, 1, 'en', 'frontLang', 'yourQuantity', 'Please enter quantity', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1230, 1, 'en', 'frontLang', 'notes', 'Notes', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1231, 1, 'en', 'frontLang', 'sendOrder', 'Send Order', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1232, 1, 'en', 'frontLang', 'youOrderSent', 'Your Order has been sent successfully. We will contact you as soon as possible.', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1233, 1, 'en', 'frontLang', 'partners', 'Our Partners', '2018-12-15 01:54:02', '2018-12-15 01:54:02'),
(1234, 1, 'en', 'frontLang', 'partnersMsg', 'Long term partnerships with leading local and international companies', '2018-12-15 01:54:02', '2018-12-15 01:54:02');

-- --------------------------------------------------------

--
-- Table structure for table `maps`
--

CREATE TABLE `maps` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `icon` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `maps`
--

INSERT INTO `maps` (`id`, `topic_id`, `longitude`, `latitude`, `title_ar`, `title_en`, `details_ar`, `details_en`, `icon`, `status`, `row_no`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 2, '39.639537564366684', '-101.953125', 'عنوان رئيسي هنا', 'Main Title here', 'Co Rd 6, Kanorado, KS 67741, USA', 'Co Rd 6, Kanorado, KS 67741, USA', 3, 1, 1, 1, 1, '2017-03-06 12:41:56', '2017-03-06 12:45:09'),
(4, 2, '40.136890695345905', '-100.689697265625', 'عنوان رئيسي هنا', 'Main title here', 'Rd 381, McCook, NE 69001, USA', 'Rd 381, McCook, NE 69001, USA', 2, 1, 2, 1, 1, '2017-03-06 12:44:21', '2017-03-06 12:45:30'),
(5, 2, '40.463666324587685', '-103.447265625', 'عنوان رئيسي هنا', 'Main title here', 'Co Rd 6, Merino, CO 80741, USA', 'Co Rd 6, Merino, CO 80741, USA', 5, 1, 3, 1, 1, '2017-03-06 12:44:29', '2017-03-06 12:45:44');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `row_no` int(11) NOT NULL,
  `father_id` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `row_no`, `father_id`, `title_ar`, `title_en`, `status`, `type`, `cat_id`, `link`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'القائمة الرئيسية', 'Main Menu', 1, 0, 0, '', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(3, 1, 1, 'الرئيسية', 'Home', 0, 1, 1, 'home', 1, 1, '2017-03-06 11:06:24', '2019-01-04 17:34:32'),
(4, 2, 1, 'من نحن', 'About', 1, 1, 0, 'topic/about', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(5, 3, 1, 'خدماتنا', 'Services', 0, 3, 1, NULL, 1, 1, '2017-03-06 11:06:24', '2019-01-04 17:34:52'),
(6, 4, 1, 'أخبارنا', 'News', 0, 2, 1, NULL, 1, 1, '2017-03-06 11:06:24', '2019-01-04 17:35:08'),
(7, 5, 1, 'الصور', 'Photos', 0, 2, 1, NULL, 1, 1, '2017-03-06 11:06:24', '2019-01-04 17:35:14'),
(8, 6, 1, 'الفيديو', 'Videos', 0, 3, 1, NULL, 1, 1, '2017-03-06 11:06:24', '2019-01-04 17:35:22'),
(9, 7, 1, 'الصوتيات', 'Audio', 0, 3, 1, NULL, 1, 1, '2017-03-06 11:06:24', '2019-01-04 17:35:30'),
(10, 8, 1, 'المنتجات', 'Products', 0, 3, 1, NULL, 1, 1, '2017-03-06 11:06:24', '2019-01-04 17:35:37'),
(11, 9, 1, 'المدونة', 'Blog', 0, 2, 1, NULL, 1, 1, '2017-03-06 11:06:24', '2019-01-04 17:35:02'),
(12, 10, 1, 'اتصل بنا', 'Contact', 1, 1, 0, 'contact', 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(15, 11, 1, 'test', 'test', 0, 0, 1, NULL, 1, 1, '2018-12-16 02:43:26', '2018-12-16 02:44:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(282, '2014_04_02_193005_create_translations_table', 1),
(283, '2014_10_12_000000_create_users_table', 1),
(284, '2014_10_12_100000_create_password_resets_table', 1),
(285, '2017_09_14_194216_create_webmaster_settings_table', 1),
(286, '2017_09_14_194251_create_webmaster_sections_table', 1),
(287, '2017_09_14_194259_create_webmaster_banners_table', 1),
(288, '2017_09_14_194307_create_webmails_groups_table', 1),
(289, '2017_09_14_194314_create_webmails_files_table', 1),
(290, '2017_09_14_194321_create_webmails_table', 1),
(291, '2017_09_14_194328_create_topics_table', 1),
(292, '2017_09_14_194334_create_settings_table', 1),
(293, '2017_09_14_194342_create_sections_table', 1),
(294, '2017_09_14_194349_create_photos_table', 1),
(295, '2017_09_14_194356_create_permissions_table', 1),
(296, '2017_09_14_194403_create_menus_table', 1),
(297, '2017_09_14_194409_create_maps_table', 1),
(298, '2017_09_14_194417_create_events_table', 1),
(299, '2017_09_14_194424_create_countries_table', 1),
(300, '2017_09_14_194431_create_contacts_groups_table', 1),
(301, '2017_09_14_194438_create_contacts_table', 1),
(302, '2017_09_14_194444_create_comments_table', 1),
(303, '2017_09_14_194452_create_banners_table', 1),
(304, '2017_09_14_194506_create_attach_files_table', 1),
(305, '2017_09_14_194514_create_analytics_visitors_table', 1),
(306, '2017_09_14_194521_create_analytics_pages_table', 1),
(307, '2017_10_06_113629_create_related_topics_table', 1),
(308, '2017_10_07_184011_create_topic_categories_table', 1),
(309, '2017_10_24_194251_create_webmaster_section_fields_table', 1),
(310, '2017_10_24_194304_create_topic_fields_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_status` tinyint(4) NOT NULL DEFAULT '0',
  `add_status` tinyint(4) NOT NULL DEFAULT '0',
  `edit_status` tinyint(4) NOT NULL DEFAULT '0',
  `delete_status` tinyint(4) NOT NULL DEFAULT '0',
  `analytics_status` tinyint(4) NOT NULL DEFAULT '0',
  `inbox_status` tinyint(4) NOT NULL DEFAULT '0',
  `newsletter_status` tinyint(4) NOT NULL DEFAULT '0',
  `calendar_status` tinyint(4) NOT NULL DEFAULT '0',
  `banners_status` tinyint(4) NOT NULL DEFAULT '0',
  `settings_status` tinyint(4) NOT NULL DEFAULT '0',
  `webmaster_status` tinyint(4) NOT NULL DEFAULT '0',
  `data_sections` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `view_status`, `add_status`, `edit_status`, `delete_status`, `analytics_status`, `inbox_status`, `newsletter_status`, `calendar_status`, `banners_status`, `settings_status`, `webmaster_status`, `data_sections`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Webmaster', 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, '1,2,3,4,5,6,7,8,9,11,12,13,14,15,16,17,18,19,20,21,22,23,24,26,27,28,29,30,31,32,33,20,21,22,23,24,25', 1, 1, NULL, '2017-11-08 13:25:54', '2019-08-31 07:07:42'),
(2, 'Website Manager', 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, '27', 1, 1, NULL, '2017-11-08 13:25:54', '2019-01-04 03:10:52'),
(3, 'Limited User', 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, '1,2,3,4,5,6,7,8,9', 1, 1, NULL, '2017-11-08 13:25:54', '2017-11-08 13:25:54');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `topic_id`, `file`, `title`, `row_no`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 9, '14888159357846.jpg', '14888146802295', 1, 1, NULL, '2017-03-06 13:58:55', '2017-03-06 13:58:55'),
(2, 9, '14888159356958.jpg', '14888146712437', 1, 1, NULL, '2017-03-06 13:58:55', '2017-03-06 13:58:55'),
(3, 9, '14888159357505.jpg', '14888155324481', 2, 1, NULL, '2017-03-06 13:58:55', '2017-03-06 13:58:55'),
(4, 12, '14888160421353.jpg', '14888159357505', 1, 1, NULL, '2017-03-06 14:00:42', '2017-03-06 14:00:42'),
(6, 12, '14888162827801.jpg', '14888159356958', 2, 1, NULL, '2017-03-06 14:04:42', '2017-03-06 14:04:42'),
(7, 23, '14888185569533.jpg', 'picjumbo.com_HNCK0183', 1, 1, NULL, '2017-03-06 14:42:36', '2017-03-06 14:42:36'),
(8, 23, '14888185564870.jpg', 'picjumbo.com_HNCK0210', 1, 1, NULL, '2017-03-06 14:42:36', '2017-03-06 14:42:36'),
(9, 23, '14888185567711.jpg', 'picjumbo.com_HNCK1748', 2, 1, NULL, '2017-03-06 14:42:36', '2017-03-06 14:42:36'),
(10, 23, '14888185565392.jpg', 'picjumbo.com_HNCK5322', 2, 1, NULL, '2017-03-06 14:42:36', '2017-03-06 14:42:36'),
(11, 23, '14888185563329.jpg', 'picjumbo.com_IMG_7167', 3, 1, NULL, '2017-03-06 14:42:36', '2017-03-06 14:42:36'),
(12, 23, '14888185566343.jpg', 'picjumbo.com_IMG_7172', 3, 1, NULL, '2017-03-06 14:42:36', '2017-03-06 14:42:36'),
(13, 23, '14888185561337.jpg', 'picjumbo.com_IMG_8868', 4, 1, NULL, '2017-03-06 14:42:36', '2017-03-06 14:42:36'),
(14, 23, '14888185564002.jpg', 'picjumbo.com_IMG_7961', 4, 1, NULL, '2017-03-06 14:42:36', '2017-03-06 14:42:36'),
(15, 24, '14888186143991.jpg', 'picjumbo.com_HNCK7801', 1, 1, NULL, '2017-03-06 14:43:34', '2017-03-06 14:43:34'),
(16, 24, '14888186147889.jpg', 'picjumbo.com_HNCK7784', 2, 1, NULL, '2017-03-06 14:43:34', '2017-03-06 14:43:34'),
(17, 24, '14888186147423.jpg', 'picjumbo.com_HNCK8360', 3, 1, NULL, '2017-03-06 14:43:34', '2017-03-06 14:43:34'),
(18, 24, '14888186141400.jpg', 'picjumbo.com_HNCK8458', 4, 1, NULL, '2017-03-06 14:43:34', '2017-03-06 14:43:34'),
(19, 24, '14888186147346.jpg', 'picjumbo.com_HNCK9016', 5, 1, NULL, '2017-03-06 14:43:34', '2017-03-06 14:43:34'),
(20, 24, '14888186141502.jpg', 'picjumbo.com_IMG_3212', 5, 1, NULL, '2017-03-06 14:43:34', '2017-03-06 14:43:34'),
(21, 24, '14888186143432.jpg', 'picjumbo.com_IMG_5992', 6, 1, NULL, '2017-03-06 14:43:34', '2017-03-06 14:43:34'),
(22, 24, '14888186147500.jpg', 'picjumbo.com_IMG_3640', 6, 1, NULL, '2017-03-06 14:43:34', '2017-03-06 14:43:34'),
(23, 25, '14888186704977.jpg', 'picjumbo.com_HNCK4011', 1, 1, NULL, '2017-03-06 14:44:30', '2017-03-06 14:44:30'),
(24, 25, '14888186701922.jpg', 'picjumbo.com_HNCK3988', 1, 1, NULL, '2017-03-06 14:44:30', '2017-03-06 14:44:30'),
(25, 25, '14888186716815.jpg', 'picjumbo.com_HNCK7802', 2, 1, NULL, '2017-03-06 14:44:31', '2017-03-06 14:44:31'),
(26, 25, '14888186711726.jpg', 'picjumbo.com_HNCK7775', 2, 1, NULL, '2017-03-06 14:44:31', '2017-03-06 14:44:31'),
(27, 25, '14888186715386.jpg', 'picjumbo.com_HNCK8404', 3, 1, NULL, '2017-03-06 14:44:31', '2017-03-06 14:44:31'),
(28, 25, '14888186717969.jpg', 'picjumbo.com_HNCK8478', 3, 1, NULL, '2017-03-06 14:44:31', '2017-03-06 14:44:31'),
(29, 25, '14888186717433.jpg', 'picjumbo.com_HNCK8495', 4, 1, NULL, '2017-03-06 14:44:31', '2017-03-06 14:44:31'),
(30, 25, '14888186717917.jpg', 'picjumbo.com_HNCK8991', 4, 1, NULL, '2017-03-06 14:44:31', '2017-03-06 14:44:31'),
(31, 26, '14888187058652.jpg', 'picjumbo.com_HNCK0210', 1, 1, NULL, '2017-03-06 14:45:05', '2017-03-06 14:45:05'),
(32, 26, '14888187054122.jpg', 'picjumbo.com_HNCK0183', 1, 1, NULL, '2017-03-06 14:45:05', '2017-03-06 14:45:05'),
(33, 26, '14888187065068.jpg', 'picjumbo.com_HNCK1748', 2, 1, NULL, '2017-03-06 14:45:06', '2017-03-06 14:45:06'),
(34, 26, '14888187067771.jpg', 'picjumbo.com_HNCK5322', 2, 1, NULL, '2017-03-06 14:45:06', '2017-03-06 14:45:06'),
(35, 26, '14888187065221.jpg', 'picjumbo.com_IMG_7167', 3, 1, NULL, '2017-03-06 14:45:06', '2017-03-06 14:45:06'),
(36, 26, '14888187065292.jpg', 'picjumbo.com_IMG_7172', 3, 1, NULL, '2017-03-06 14:45:06', '2017-03-06 14:45:06'),
(37, 26, '14888187061421.jpg', 'picjumbo.com_IMG_8868', 4, 1, NULL, '2017-03-06 14:45:06', '2017-03-06 14:45:06'),
(38, 26, '14888187063601.jpg', 'picjumbo.com_IMG_7961', 4, 1, NULL, '2017-03-06 14:45:06', '2017-03-06 14:45:06');

-- --------------------------------------------------------

--
-- Table structure for table `related_topics`
--

CREATE TABLE `related_topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `topic2_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `father_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `title_ar`, `title_en`, `photo`, `icon`, `status`, `visits`, `webmaster_id`, `father_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'تصميم المواقع', 'Web Design', NULL, 'fa-desktop', 1, 0, 7, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:11:25', '2017-03-06 14:11:25'),
(2, 'تطبيقات الهواتف', 'Mobile Applications', NULL, 'fa-apple', 1, 0, 7, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:11:50', '2017-03-06 14:11:50'),
(3, 'رسوم متحركة', 'Motion Draws', NULL, 'fa-motorcycle', 1, 0, 7, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:12:24', '2017-03-06 14:12:24'),
(4, 'تطوير الويب', 'Web Development', NULL, 'fa-html5', 1, 0, 7, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:12:51', '2017-03-06 14:12:51'),
(5, 'تصميم المطبوعات', 'Publications Design', NULL, 'fa-connectdevelop', 1, 0, 7, 0, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:13:41', '2017-03-06 14:13:41'),
(6, 'أرشفة المواقع', 'Search Engines Optmization', NULL, 'fa-line-chart', 1, 0, 7, 0, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:21:52', '2017-03-06 14:21:52'),
(7, 'تصميم ثلاثي اأبعاد', '3d Design', NULL, 'fa-modx', 1, 0, 7, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:22:50', '2017-03-06 14:22:50'),
(8, 'الطبيعة', 'Nature', NULL, 'fa-leaf', 1, 0, 5, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:48:06', '2017-03-06 14:48:06'),
(9, 'مدن وعواصم', 'Cities', NULL, 'fa-map-o', 1, 0, 5, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:48:43', '2017-03-06 14:48:43'),
(10, 'مغامرات', 'Adventures', NULL, 'fa-flag-checkered', 1, 0, 5, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 14:49:27', '2017-03-06 14:49:27'),
(12, 'فيديوهات يوتيوب', 'Youtube Videos', NULL, 'fa-youtube', 1, 0, 5, 0, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 15:10:10', '2017-03-06 15:10:10'),
(13, 'فيديوهات فيميو', 'Vimeo videos', NULL, 'fa-vimeo', 1, 0, 5, 0, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 15:10:37', '2017-03-06 15:10:37'),
(14, 'فيديوهات محملة', 'Hosted videos', NULL, 'fa-database', 1, 0, 5, 0, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 15:11:22', '2017-03-06 15:11:22'),
(15, 'سولو', 'Solo', NULL, NULL, 1, 1, 6, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 16:44:08', '2017-11-08 19:51:42'),
(16, 'بوب ميوزك', 'POP', NULL, NULL, 1, 0, 6, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 16:44:24', '2017-03-06 16:44:24'),
(17, 'صوتيات متنوعة', 'Other Sounds', NULL, NULL, 1, 0, 6, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 16:44:49', '2017-03-06 16:45:30'),
(18, 'اصوات موسيقية', 'Music Sounds', NULL, NULL, 1, 0, 6, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-03-06 16:45:19', '2017-03-06 16:45:30'),
(19, 'قسم منتجات ١', 'Product Category 1', NULL, NULL, 1, 2, 8, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 16:49:22', '2017-11-08 19:51:45'),
(20, 'قسم منتجات ٢', 'Product Category 2', NULL, NULL, 1, 0, 8, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 16:49:41', '2017-03-06 16:52:12'),
(21, 'قسم منتجات ٣', 'Product Category 3', NULL, NULL, 1, 0, 8, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 16:50:00', '2017-03-06 16:52:17'),
(22, 'قسم منتجات ٤', 'Product Category 4', NULL, NULL, 1, 0, 8, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2017-03-06 16:50:25', '2017-03-06 16:52:23');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_title_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_desc_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_desc_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_keywords_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_keywords_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_webmails` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notify_messages_status` tinyint(4) DEFAULT NULL,
  `notify_comments_status` tinyint(4) DEFAULT NULL,
  `notify_orders_status` tinyint(4) DEFAULT NULL,
  `site_url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site_status` tinyint(4) NOT NULL,
  `close_msg` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link2` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link5` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link6` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link7` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link8` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link9` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_link10` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t1_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t1_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t3` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t4` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t5` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t6` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t7_ar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_t7_en` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `style_logo_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_logo_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_fav` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_apple` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_color1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_color2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_type` tinyint(4) DEFAULT NULL,
  `style_bg_type` tinyint(4) DEFAULT NULL,
  `style_bg_pattern` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_bg_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_bg_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_subscribe` tinyint(4) DEFAULT NULL,
  `style_footer` tinyint(4) DEFAULT NULL,
  `style_footer_bg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `style_preload` tinyint(4) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_title_ar`, `site_title_en`, `site_desc_ar`, `site_desc_en`, `site_keywords_ar`, `site_keywords_en`, `site_webmails`, `notify_messages_status`, `notify_comments_status`, `notify_orders_status`, `site_url`, `site_status`, `close_msg`, `social_link1`, `social_link2`, `social_link3`, `social_link4`, `social_link5`, `social_link6`, `social_link7`, `social_link8`, `social_link9`, `social_link10`, `contact_t1_ar`, `contact_t1_en`, `contact_t3`, `contact_t4`, `contact_t5`, `contact_t6`, `contact_t7_ar`, `contact_t7_en`, `style_logo_ar`, `style_logo_en`, `style_fav`, `style_apple`, `style_color1`, `style_color2`, `style_type`, `style_bg_type`, `style_bg_pattern`, `style_bg_color`, `style_bg_image`, `style_subscribe`, `style_footer`, `style_footer_bg`, `style_preload`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'كتب', 'Books', 'وصف الموقع الإلكتروني ونبذة قصيره عنه', '', 'كلمات، دلالية، موقع، موقع إلكتروني', 'key, words, website, web', '', 1, 1, 1, '', 1, 'Website under maintenance \n<h1>Comming SOON</h1>', 'http://localhost/laravel_project-master/', '#', '#', '#', '#', '#', '#', '#', '#', '#', 'المبني - اسم الشارع - المدينة - الدولة', 'Building, Street name, City, Country', '+(00) 0123456789', '+(00) 0123456789', '+(00) 0123456789', 'info@sitename.com', 'من الأحد إلى الخميس 08:00 ص - 05:00 م', 'Sunday to Thursday 08:00 AM to 05:00 PM', '15472363552873.jpg', '15472363253820.jpg', '15472363258346.jpg', '15472363255905.jpg', '#ba350c', '#2e3e4e', 0, 0, NULL, '#2e3e4e', NULL, 1, 1, NULL, 0, 1, 1, '2017-03-06 11:06:23', '2019-01-11 17:55:06');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(10) UNSIGNED NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` longtext COLLATE utf8mb4_unicode_ci,
  `details_en` longtext COLLATE utf8mb4_unicode_ci,
  `date` date DEFAULT NULL,
  `expire_date` date DEFAULT NULL,
  `video_type` tinyint(4) DEFAULT NULL,
  `photo_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video_file` text COLLATE utf8mb4_unicode_ci,
  `audio_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `visits` int(11) NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `row_no` int(11) NOT NULL,
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `title_ar`, `title_en`, `details_ar`, `details_en`, `date`, `expire_date`, `video_type`, `photo_file`, `attach_file`, `video_file`, `audio_file`, `icon`, `status`, `visits`, `webmaster_id`, `section_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `lang_id`, `city_id`, `country_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'من نحن', 'About Us', '<h4 style=\"text-align: justify;\">رؤيتنا</h4>\r\n<p style=\"text-align: justify;\">أن نصبح الشركة الرائدة في هذا المجال على مستوى الشرق الأوسط والمستوي العالمي من خلال الاستفادة من الأفكار المتميزة، ونحن نعمل على تقديم حلول فريدة لعملائنا الكرام لتكون مطابقة لتوقعاتهم من خلال تقديم الخدمات الفعالة.أن نصبح الشركة الرائدة في هذا المجال على مستوى الشرق الأوسط والمستوي العالمي من خلال الاستفادة من الأفكار المتميزة، ونحن نعمل على تقديم حلول فريدة لعملائنا الكرام لتكون مطابقة لتوقعاتهم من خلال تقديم الخدمات الفعالة.</p><p style=\"text-align: justify;\"><br></p>\r\n<h4 style=\"text-align: justify;\">رسالتنا</h4>\r\n<p style=\"text-align: justify;\">رسالتنا هي تمكين عملائنا من تطوير أعمالهم من خلال الأفكار المتميزة، وتقديم الاستشارات الموثوقة والخدمة عالية الجودة، بالإضافة إلى تأسيس مكان رائع نعمل من أجله والذي يجذب الأشخاص المميزين ويعمل على تطويرهم والاحتفاظ بهم.رسالتنا هي تمكين عملائنا من تطوير أعمالهم من خلال الأفكار المتميزة، وتقديم الاستشارات الموثوقة والخدمة عالية الجودة، بالإضافة إلى تأسيس مكان رائع نعمل من أجله والذي يجذب الأشخاص المميزين ويعمل على تطويرهم والاحتفاظ بهم.</p><p style=\"text-align: justify;\"><br></p>\r\n<h4 style=\"text-align: justify;\">فريق العمل</h4>\r\n<p style=\"text-align: justify;\">إن فريق عملنا متنوع ونتفاعل مع بعضنا البعض باحترام متبادل بغض النظر عن الجنس أو الجنسية أو الدين أو العرق، كما نثق في بعضنا البعض ونؤمن بالعدالة والشفافية، نحن نخلق بيئة تعزز التعاون و الإنجازات المتميزة.إن فريق عملنا متنوع ونتفاعل مع بعضنا البعض باحترام متبادل بغض النظر عن الجنس أو الجنسية أو الدين أو العرق، كما نثق في بعضنا البعض ونؤمن بالعدالة والشفافية، نحن نخلق بيئة تعزز التعاون و الإنجازات المتميزة.</p>', '<h4 style=\"text-align: justify; \">Our Vision</h4>\r\n<p style=\"text-align: justify;\">Our vision is to become the leading Company in the region. Using innovative ideas, we provide best of breed solutions . Combining creative problem solving, solid service delivery model.Our vision is to become the leading Company in the region. Using innovative ideas, we provide best of breed solutions . Combining creative problem solving, solid service delivery model.</p><p style=\"text-align: justify;\"><br></p>\r\n<h4 style=\"text-align: justify; \">Our Mission</h4>\r\n<p style=\"text-align: justify;\">Our mission is to enable our clients to develop their business through innovative ideas, advice and quality of service. And to build a great place to work for, that develops and retains great people.Our mission is to enable our clients to develop their business through innovative ideas, advice and quality of service. And to build a great place to work for, that develops and retains great people.</p><p style=\"text-align: justify;\"><br></p>\r\n<h4 style=\"text-align: justify;\">Work Team</h4>\r\n<p style=\"text-align: justify;\">Our team is diversified and we interact with each other with mutual respect regardless of gender, nationality and background. We trust each other and believe in fairness and transparency.Our vision is to become the leading Company in the region. Using innovative ideas, we provide best of breed solutions . Combining creative problem solving, solid service delivery model.</p>', '2017-03-06', NULL, NULL, '14888121759700.jpg', NULL, '', NULL, NULL, 1, 33, 1, 0, 1, 'عن الموقع', 'About Smartend', 'وصف الصفحة الخاصة بمن نحن ليساعد على الأرشفة', 'Page description for good SEO', 'من نحن، نبذة عنا، وصف الموقع، كلمات ، دلالية', 'About, who us, kewords, smartend', NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 11:06:24', '2017-11-08 18:30:29'),
(2, 'اتصل بنا', 'Contact Us', NULL, NULL, '2017-03-06', NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 55, 1, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 11:06:24', '2019-04-13 00:50:41'),
(3, 'الخصوصية', 'Privacy', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', '2017-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(4, 'الشروط والأحكام', 'Terms & Conditions', 'هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص.', 'It is a long established fact that a reader will be distracted by the readable content of a page.', '2017-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2017-03-06 11:06:24', '2017-03-06 11:06:24'),
(5, 'نص تجريبي لاختبار خدمة', 'Nullam mollis dolor', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"text-align: justify; font-size: 13.92px;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify; font-size: 13.92px;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify; font-size: 13.92px;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"text-align: justify; font-size: 13.92px;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify; font-size: 13.92px;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"text-align: justify; \">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"text-align: justify; \">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"text-align: justify;\">&nbsp;</div><div style=\"text-align: justify;\"><br></div></div>', '2017-03-06', NULL, NULL, '14888139271255.jpg', NULL, '', NULL, 'fa-ambulance', 1, 20, 2, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 13:25:27', '2017-11-13 12:31:25'),
(6, 'عنوان تجريبي للخدمات', 'Sample Lorem Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"text-align: justify; \">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify; \">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"text-align: justify; \">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"text-align: justify;\">&nbsp;</div><div style=\"text-align: justify;\"><br></div></div>', '2017-03-06', NULL, NULL, '14888139889647.jpg', NULL, '', NULL, 'fa-cart-plus', 1, 3, 2, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 13:26:28', '2017-11-13 12:31:21'),
(7, 'عنوان تجريبي من الخدمات', 'Gravida tellus suscipit', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"text-align: justify; \">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify; \">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"text-align: justify; \">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"text-align: justify; \">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"text-align: justify;\">&nbsp;</div><div style=\"text-align: justify;\"><br></div></div>', '2017-03-06', NULL, NULL, '14888140236712.jpg', NULL, '', NULL, 'fa-pie-chart', 1, 4, 2, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 13:27:03', '2017-03-07 13:20:33'),
(8, 'نص تجريبي من النصوص', 'Curabitur sit amet era', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"text-align: justify; \">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"text-align: justify; \">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"text-align: justify; \">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"text-align: justify;\">&nbsp;</div><div style=\"text-align: justify;\"><br></div></div>', '2017-03-06', NULL, NULL, '14888140657735.jpg', NULL, '', NULL, 'fa-coffee', 1, 1, 2, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 13:27:45', '2017-03-06 16:42:54'),
(9, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text, sed imperdiet nulla tellus ut diam.', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-06', NULL, NULL, '14888146415538.jpg', NULL, '', NULL, NULL, 1, 12, 3, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 13:37:21', '2017-03-07 13:24:05'),
(10, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Aliquam suscipit, lacus a iaculis adipiscing, Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-06', NULL, NULL, '14888146712437.jpg', NULL, '', NULL, NULL, 1, 3, 3, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 13:37:51', '2017-03-07 13:23:50'),
(11, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text.Suspendisse potenti. Vestibulum lacus', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-06', NULL, NULL, '14888146802295.jpg', NULL, '', NULL, NULL, 1, 0, 3, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 13:38:00', '2017-03-06 14:09:33');
INSERT INTO `topics` (`id`, `title_ar`, `title_en`, `details_ar`, `details_en`, `date`, `expire_date`, `video_type`, `photo_file`, `attach_file`, `video_file`, `audio_file`, `icon`, `status`, `visits`, `webmaster_id`, `section_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `lang_id`, `city_id`, `country_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(12, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Suspendisse potenti. Vestibulum lacus Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-06', NULL, NULL, '14888146896446.jpg', NULL, '', NULL, NULL, 1, 3, 3, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 13:38:09', '2017-03-06 14:09:46'),
(13, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-06', NULL, NULL, '14888155135678.jpg', NULL, NULL, NULL, NULL, 1, 0, 3, 0, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2017-03-06 13:51:53', '2017-03-06 13:51:53'),
(14, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-06', NULL, NULL, '14888155324481.jpg', NULL, NULL, NULL, NULL, 1, 0, 3, 0, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2017-03-06 13:52:12', '2017-03-06 13:52:12'),
(15, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div><br></div></div>', '2017-03-06', NULL, NULL, '14888170311535.jpg', NULL, '', NULL, NULL, 1, 2, 7, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:17:11', '2017-03-07 13:24:35'),
(16, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div><br></div></div>', '2017-03-06', NULL, NULL, '14888170546118.jpg', NULL, '', NULL, NULL, 1, 2, 7, 5, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:17:34', '2017-03-06 16:14:40'),
(17, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div><br></div></div>', '2017-03-06', NULL, NULL, '14888170654620.jpg', NULL, '', NULL, NULL, 1, 0, 7, 2, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:17:45', '2017-03-06 14:29:19'),
(18, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div><br></div></div>', '2017-03-06', NULL, NULL, '14888170745161.jpg', NULL, '', NULL, NULL, 1, 0, 7, 2, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:17:54', '2017-03-06 14:29:33'),
(19, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text, sed imperdiet nulla tellus ut diam.', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div><br></div></div>', '2017-03-06', NULL, NULL, '14888170858180.jpg', NULL, NULL, NULL, NULL, 1, 0, 7, 4, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:18:05', '2017-11-08 19:43:02');
INSERT INTO `topics` (`id`, `title_ar`, `title_en`, `details_ar`, `details_en`, `date`, `expire_date`, `video_type`, `photo_file`, `attach_file`, `video_file`, `audio_file`, `icon`, `status`, `visits`, `webmaster_id`, `section_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `lang_id`, `city_id`, `country_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(20, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text, sed imperdiet nulla tellus ut diam.', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div><br></div></div>', '2017-03-06', NULL, NULL, '14888170994430.jpg', NULL, NULL, NULL, NULL, 1, 2, 7, 1, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:18:19', '2017-11-08 19:43:10'),
(21, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text, sed imperdiet nulla tellus ut diam.', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div><br></div></div>', '2017-03-06', NULL, NULL, '14888171106415.jpg', NULL, NULL, NULL, NULL, 1, 4, 7, 1, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:18:30', '2017-11-08 19:43:24'),
(22, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text, sed imperdiet nulla tellus ut diam.', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div><br></div></div>', '2017-03-06', NULL, NULL, '14888171164162.jpg', NULL, NULL, NULL, NULL, 1, 3, 7, 1, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:18:36', '2017-11-08 19:43:35'),
(23, 'جالري صور ١', 'Cars Gallery', NULL, NULL, '2017-03-06', NULL, NULL, '15466346225208.PNG', NULL, NULL, NULL, NULL, 1, 0, 4, 0, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:42:03', '2019-01-04 18:43:42'),
(24, 'جالري صور ٢', 'Phones Gallery', NULL, NULL, '2017-03-06', NULL, NULL, '15466349269104.PNG', NULL, '', NULL, NULL, 1, 0, 4, 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:43:18', '2019-01-04 18:48:46'),
(25, 'جالري صور 3', 'Laptops Gallery', NULL, NULL, '2017-03-06', NULL, NULL, '15466349784224.PNG', NULL, NULL, NULL, NULL, 1, 0, 4, 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:44:17', '2019-01-04 18:49:38'),
(26, 'جالري صور 4', 'Other Gallery', NULL, NULL, '2017-03-06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 4, 0, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, NULL, '2017-03-06 14:44:54', '2017-03-06 14:45:22'),
(27, 'طبيعة فيديو ١', 'Nature Video 1', NULL, NULL, '2017-03-06', NULL, 0, NULL, NULL, 'https://www.youtube.com/watch?v=PCwL3-hkKrg', NULL, NULL, 1, 0, 5, 8, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:53:42', '2018-12-08 19:12:43'),
(28, 'فيدو معامرات ١', 'Video title here', NULL, NULL, '2017-03-06', NULL, 0, '14888196096249.jpg', NULL, '14888199269864.mp4', NULL, NULL, 1, 0, 5, 14, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 14:58:07', '2017-03-06 15:13:27'),
(29, 'مثال لفيديو من يوتيوب', 'Sample for youtube videos', NULL, NULL, '2017-03-06', NULL, 1, NULL, NULL, 'https://www.youtube.com/watch?v=fHfb5-7xLtc', NULL, NULL, 1, 0, 5, 12, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-06 15:12:20', '2017-11-08 19:40:15'),
(32, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-07', NULL, NULL, '14889008041514.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 19, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-07 13:33:24', '2018-12-13 00:37:16'),
(33, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-07', NULL, NULL, '14889008137532.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 19, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-07 13:33:33', '2018-12-13 00:37:16'),
(34, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-07', NULL, NULL, '14889008358884.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 20, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-07 13:33:55', '2017-11-08 19:44:30'),
(35, 'نص تجريبي لاختبار شكل و حجم النصوص', 'Sample Lorem Ipsum Text', '<div dir=\"rtl\"><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع&nbsp;</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">.يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div><div dir=\"rtl\" style=\"font-size: 13.92px; text-align: justify;\">هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع. هذا نص تجريبي لاختبار شكل و حجم النصوص و طريقة عرضها في هذا المكان و حجم و لون الخط حيث يتم التحكم في هذا النص وامكانية تغييرة في اي وقت عن طريق ادارة الموقع . يتم اضافة هذا النص كنص تجريبي للمعاينة فقط وهو لا يعبر عن أي موضوع محدد انما لتحديد الشكل العام للقسم او الصفحة أو الموقع.</div></div>', '<div dir=\"ltr\"><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed at ante. Mauris eleifend, quam a vulputate dictum, massa quam dapibus leo, eget vulputate orci purus ut lorem. In fringilla mi in ligula. Pellentesque aliquam quam vel dolor. Nunc adipiscing. Sed quam odio, tempus ac, aliquam molestie, varius ac, tellus. Vestibulum ut nulla aliquam risus rutrum interdum. Pellentesque lorem. Curabitur sit amet erat quis risus feugiat viverra. Pellentesque augue justo, sagittis et, lacinia at, venenatis non, arcu. Nunc nec libero. In cursus dictum risus. Etiam tristique nisl a nulla. Ut a orci. Curabitur dolor nunc, egestas at, accumsan at, malesuada nec, magna.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Nulla facilisi. Nunc volutpat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut sit amet orci vel mauris blandit vehicula. Nullam quis enim. Integer dignissim viverra velit. Curabitur in odio. In hac habitasse platea dictumst. Ut consequat, tellus eu volutpat varius, justo orci elementum dolor, sed imperdiet nulla tellus ut diam. Vestibulum ipsum ante, malesuada quis, tempus ac, placerat sit amet, elit.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Sed eget turpis a pede tempor malesuada. Vivamus quis mi at leo pulvinar hendrerit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque aliquet lacus vitae pede. Nullam mollis dolor ac nisi. Phasellus sit amet urna. Praesent pellentesque sapien sed lacus. Donec lacinia odio in odio. In sit amet elit. Maecenas gravida interdum urna. Integer pretium, arcu vitae imperdiet facilisis, elit tellus tempor nisi, vel feugiat ante velit sit amet mauris. Vivamus arcu. Integer pharetra magna ac lacus. Aliquam vitae sapien in nibh vehicula auctor. Suspendisse leo mauris, pulvinar sed, tempor et, consequat ac, lacus. Proin velit. Nulla semper lobortis mauris. Duis urna erat, ornare et, imperdiet eu, suscipit sit amet, massa. Nulla nulla nisi, pellentesque at, egestas quis, fringilla eu, diam.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec semper, sem nec tristique tempus, justo neque commodo nisl, ut gravida sem tellus suscipit nunc. Aliquam erat volutpat. Ut tincidunt pretium elit. Aliquam pulvinar. Nulla cursus. Suspendisse potenti. Etiam condimentum hendrerit felis. Duis iaculis aliquam enim. Donec dignissim augue vitae orci. Curabitur luctus felis a metus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In varius neque at enim. Suspendisse massa nulla, viverra in, bibendum vitae, tempor quis, lorem.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">Donec dapibus orci sit amet elit. Maecenas rutrum ultrices lectus. Aliquam suscipit, lacus a iaculis adipiscing, eros orci pellentesque nisl, non pharetra dolor urna nec dolor. Integer cursus dolor vel magna. Integer ultrices feugiat sem. Proin nec nibh. Duis eu dui quis nunc sagittis lobortis. Fusce pharetra, enim ut sodales luctus, lectus arcu rhoncus purus, in fringilla augue elit vel lacus. In hac habitasse platea dictumst. Aliquam erat volutpat. Fusce iaculis elit id tellus. Ut accumsan malesuada turpis. Suspendisse potenti. Vestibulum lacus augue, lobortis mattis, laoreet in, varius at, nisi. Nunc gravida. Phasellus faucibus. In hac habitasse platea dictumst. Integer tempor lacus eget lectus. Praesent fringilla augue fringilla dui.</div><div dir=\"ltr\" style=\"font-size: 13.92px; text-align: justify;\">&nbsp;</div><div style=\"text-align: justify; \"><br></div></div>', '2017-03-07', NULL, NULL, '14889008434898.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 20, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-07 13:34:03', '2018-12-13 00:37:16'),
(36, 'مثال لملف صوتي تجريبي', 'Audio files sample for test', NULL, NULL, '2017-03-07', NULL, NULL, '14889193305434.jpg', NULL, NULL, '14889192633715.mp3', NULL, 1, 2, 6, 15, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-08 01:41:04', '2017-11-08 19:41:32'),
(37, 'ملف موسيقى تجريبي', 'music audio file demo', NULL, NULL, '2017-03-07', NULL, NULL, NULL, NULL, NULL, '14889195178063.mp3', NULL, 1, 1, 6, 16, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, 1, 1, '2017-03-08 01:45:17', '2017-11-08 19:40:58'),
(38, 'عملائنا 1', 'Partener 1', NULL, NULL, '2017-11-08', NULL, NULL, '15101586286108.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 1, 'عملائنا 1', 'Partener 1', '', '', NULL, NULL, 'aamlaena-1', 'partener-1', 0, 0, 0, 1, NULL, '2017-11-08 14:30:28', '2017-11-08 14:30:28'),
(39, 'عملائنا 2', 'Partener 2', NULL, NULL, '2017-11-08', NULL, NULL, '15101586454457.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 2, 'عملائنا 2', 'Partener 2', '', '', NULL, NULL, 'aamlaena-2', 'partener-2', 0, 0, 0, 1, NULL, '2017-11-08 14:30:45', '2017-11-08 14:30:45'),
(40, 'عملائنا 3', 'Partener 3', NULL, NULL, '2017-11-08', NULL, NULL, '15101586557094.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 3, 'عملائنا 3', 'Partener 3', '', '', NULL, NULL, 'aamlaena-3', 'partener-3', 0, 0, 0, 1, NULL, '2017-11-08 14:30:55', '2017-11-08 14:30:55'),
(41, 'عملائنا 4', 'Partener 4', NULL, NULL, '2017-11-08', NULL, NULL, '15101586647612.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 4, 'عملائنا 4', 'Partener 4', '', '', NULL, NULL, 'aamlaena-4', 'partener-4', 0, 0, 0, 1, NULL, '2017-11-08 14:31:04', '2017-11-08 14:31:04'),
(42, 'عملائنا 5', 'Partener 5', NULL, NULL, '2017-11-08', NULL, NULL, '15101586746144.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 5, 'عملائنا 5', 'Partener 5', '', '', NULL, NULL, 'aamlaena-5', 'partener-5', 0, 0, 0, 1, NULL, '2017-11-08 14:31:14', '2017-11-08 14:31:14'),
(43, 'عملائنا 6', 'Partener 6', NULL, NULL, '2017-11-08', NULL, NULL, '15101586835369.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 6, 'عملائنا 6', 'Partener 6', '', '', NULL, NULL, 'aamlaena-6', 'partener-6', 0, 0, 0, 1, NULL, '2017-11-08 14:31:23', '2017-11-08 14:31:23'),
(44, 'عملائنا 7', 'Partener 7', NULL, NULL, '2017-11-08', NULL, NULL, '15101586994098.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 7, 'عملائنا 7', 'Partener 7', '', '', NULL, NULL, 'aamlaena-7', 'partener-7', 0, 0, 0, 1, NULL, '2017-11-08 14:31:39', '2017-11-08 14:31:39'),
(45, 'عملائنا 8', 'Partener 8', NULL, NULL, '2017-11-08', NULL, NULL, '15101587089368.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 8, 'عملائنا 8', 'Partener 8', '', '', NULL, NULL, 'aamlaena-8', 'partener-8', 0, 0, 0, 1, NULL, '2017-11-08 14:31:48', '2017-11-08 14:31:48'),
(46, 'عملائنا 9', 'Partener 9', NULL, NULL, '2017-11-08', NULL, NULL, '15101587164254.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 9, 'عملائنا 9', 'Partener 9', '', '', NULL, NULL, 'aamlaena-9', 'partener-9', 0, 0, 0, 1, NULL, '2017-11-08 14:31:56', '2017-11-08 14:31:56'),
(47, 'عملائنا 10', 'Partener 10', NULL, NULL, '2017-11-08', NULL, NULL, '15101587316532.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 10, 'عملائنا 10', 'Partener 10', '', '', NULL, NULL, 'aamlaena-10', 'partener-10', 0, 0, 0, 1, NULL, '2017-11-08 14:32:11', '2017-11-08 14:32:11'),
(48, 'عملائنا 11', 'Partener 11', NULL, NULL, '2017-11-08', NULL, NULL, '15101587452912.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 11, 'عملائنا 11', 'Partener 11', '', '', NULL, NULL, 'aamlaena-11', 'partener-11', 0, 0, 0, 1, NULL, '2017-11-08 14:32:25', '2017-11-08 14:32:25'),
(49, 'عملائنا 12', 'Partener 12', NULL, NULL, '2017-11-08', NULL, NULL, '15101587542268.png', NULL, NULL, NULL, NULL, 1, 0, 9, 0, 12, 'عملائنا 12', 'Partener 12', '', '', NULL, NULL, 'aamlaena-12', 'partener-12', 0, 0, 0, 1, NULL, '2017-11-08 14:32:34', '2017-11-08 14:32:34'),
(50, 'فثىتنى', 'test', NULL, NULL, '2018-12-11', NULL, 1, '15445025251117.PNG', NULL, 'https://www.youtube.com/watch?v=Od5SB6NCa2Q', NULL, NULL, 1, 0, 5, 0, 4, 'فثىتنى', 'test', '', '', NULL, NULL, 'fthtn', 'test', 0, 0, 0, 2, NULL, '2018-12-11 11:28:45', '2018-12-11 11:28:45'),
(51, 'مشروع#6', 'Pro#6', '<div dir=rtl>ابجد هوز <br></div>', '<div dir=ltr>lorem ibsum<br></div>', '2018-12-11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 8, 0, 4, 'مشروع#5', 'Pro#5', 'ابجد هوز ', 'lorem ibsum', NULL, NULL, 'mshroaa5', 'pro5', 0, 0, 0, 2, 2, '2018-12-11 15:49:53', '2018-12-13 00:37:38'),
(54, NULL, NULL, NULL, NULL, '2018-12-15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 11, 0, 2, NULL, NULL, '', '', NULL, NULL, '', '', 5, 0, 0, 1, 1, '2018-12-15 15:27:47', '2018-12-16 02:49:15'),
(55, 'ايبنتبيبي', 'cairo', '<div dir=rtl><br></div>sdsadadas', '<div dir=ltr><br></divsdasdas', '2018-12-15', NULL, NULL, '15449033945523.PNG', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 6, 'dd', 'cairo', 'sdsadadas', '', NULL, NULL, 'dd', 'cairo', 0, 0, 0, 1, 1, '2018-12-16 02:49:54', '2018-12-16 02:50:21');
INSERT INTO `topics` (`id`, `title_ar`, `title_en`, `details_ar`, `details_en`, `date`, `expire_date`, `video_type`, `photo_file`, `attach_file`, `video_file`, `audio_file`, `icon`, `status`, `visits`, `webmaster_id`, `section_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `lang_id`, `city_id`, `country_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(56, 'إطعام', 'feeding', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 12, 0, 1, 'إطعام', 'feeding', '', '', NULL, NULL, 'etaaam', 'feeding', 0, 0, 0, 1, NULL, '2018-12-18 18:38:41', '2018-12-18 18:38:41'),
(57, 'كفالة يتيم 0', 'Sponsorship of orphans 0', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 12, 0, 2, 'كفالة يتيم 0', 'Sponsorship of orphans 0', '', '', NULL, NULL, 'kfal-ytym-0', 'sponsorship-of-orphans-0', 0, 0, 0, 1, NULL, '2018-12-18 18:39:15', '2018-12-18 18:39:15'),
(58, 'عمارة مساجد', 'Mosques Building', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 12, 0, 3, 'عمارة مساجد', 'Mosques Building', '', '', NULL, NULL, 'aamar-msajd', 'mosques-building', 0, 0, 0, 1, NULL, '2018-12-18 18:39:40', '2018-12-18 18:39:40'),
(59, 'مصر', 'Egypt', 'منيبن يبنمنبي يبننيتتنتب يسبيسمنيسب يسبيسبنيس يسبيسبس&nbsp; يسبيس يسبيس سبسي&nbsp;', '<p><span style=\"box-sizing: border-box; font-weight: 700; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(0, 0, 0);\">Египет</span><span style=\"color: rgb(117, 117, 117); font-family: Roboto, sans-serif; font-size: 15px;\">&nbsp;– крупнейший производитель и поставщик цитрусовых. Апельсины составляют 65% производства цитрусовых и 30% общего производства&nbsp;фруктов&nbsp;в этой стране. Самые распространенные египетские: сорта Navel и Valencia, Baladi, Shamouti. Россия является крупнейшим импортером египетских апельсинов: в нашу страну поставляется порядка 18% экспортируемых из Египта апельсинов (по данным Fruit News). После введения экономических санкций летом 2014 года доля Египетского экспорта в РФ только выросла и продолжает расти.&nbsp;</span><br></p>', '2018-12-18', NULL, NULL, '15468875295464.PNG', NULL, NULL, NULL, NULL, 1, 0, 14, 0, 1, 'مصر', 'Egypt', '', '', NULL, NULL, 'msr', 'egypt', 0, 0, 0, 1, 1, '2018-12-18 18:56:06', '2019-01-07 17:05:58'),
(60, 'السعوديه', 'Saudi', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 14, 0, 2, 'السعوديه', 'Saudi', '', '', NULL, NULL, 'alsaaodyh', 'saudi', 0, 0, 0, 1, NULL, '2018-12-18 18:56:43', '2018-12-18 18:56:43'),
(61, 'الامارات', 'United Arab Emirates', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 14, 0, 3, 'الامارات', 'United Arab Emirates', '', '', NULL, NULL, 'alamarat', 'united-arab-emirates', 0, 0, 0, 1, NULL, '2018-12-18 18:57:19', '2018-12-18 18:57:19'),
(62, 'القاهره', 'cairo', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 15, 0, 1, 'القاهره', 'cairo', '', '', NULL, NULL, 'alkahrh', '', 0, 0, 59, 1, NULL, '2018-12-18 19:25:24', '2018-12-18 19:25:24'),
(63, 'المدينه المنوره', 'Medina', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 15, 0, 2, 'المدينه المنوره', 'Medina', '', '', NULL, NULL, 'almdynh-almnorh', 'medina', 0, 0, 60, 1, NULL, '2018-12-18 19:27:16', '2018-12-18 19:27:16'),
(64, 'دبى', 'Dubai', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 15, 0, 3, 'دبى', 'Dubai', '', '', NULL, NULL, 'db', 'dubai', 0, 0, 61, 1, NULL, '2018-12-18 19:27:43', '2018-12-18 19:27:43'),
(65, 'الجمعية الشرعيه', 'Legitimacy Assembly', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 13, 0, 1, 'الجمعية الشرعيه', 'Legitimacy Assembly', '', '', NULL, NULL, 'aljmaay-alshraayh', 'legitimacy-assembly', 0, 0, 0, 1, NULL, '2018-12-18 19:31:35', '2018-12-18 19:31:35'),
(66, 'جمعيه البيت المعمور', 'The House of the World', NULL, NULL, '2018-12-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 16, 0, 1, 'جمعيه البيت المعمور', 'The House of the World', '', '', NULL, NULL, 'jmaayh-albyt-almaamor', 'the-house-of-the-world', 0, 0, 0, 1, NULL, '2018-12-18 19:45:51', '2018-12-18 19:45:51'),
(67, 'مغلق', 'close', NULL, NULL, '2018-12-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 17, 0, 1, 'مغلق', 'close', '', '', NULL, NULL, 'mghlk', 'close', 0, 0, 0, 1, NULL, '2018-12-21 11:16:37', '2018-12-21 11:16:37'),
(68, 'مفتوح', 'open', NULL, NULL, '2018-12-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 17, 0, 2, 'مفتوح', 'open', '', '', NULL, NULL, 'mftoh', 'open', 0, 0, 0, 1, NULL, '2018-12-21 11:17:04', '2018-12-21 11:17:04'),
(69, '1213', '131', NULL, NULL, '2018-12-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 18, 0, 1, '1213', '131', '', '', NULL, NULL, '1213', '131', 0, 0, 0, 1, NULL, '2018-12-21 11:18:15', '2018-12-21 11:18:15'),
(70, 'الاطعان', 'Flames', '<p><div class=\"cleaner\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: 400; font-style: normal; font-size: 16px; font-family: \"Times New Roman\"; vertical-align: baseline; clear: both; height: 0px; line-height: 0; color: rgb(0, 0, 0); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"></div></p><div class=\"content\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: 400; font-style: normal; font-size: 16px; font-family: \"Times New Roman\"; vertical-align: baseline; color: rgb(0, 0, 0); font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); text-decoration-style: initial; text-decoration-color: initial;\"><p style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-size: 18px; font-family: \"Century Gothic\", CenturyGothic, Futura, \"Avant Garde\", Avantgarde, Arial, sans-serif; vertical-align: baseline; line-height: 36px; color: rgb(133, 120, 112);\">                                                                                                                                               : VarietiesОбыкновенные: плоды разных размеров, с желтой мякотью, кисло-сладкие на вкус.Пупочные: крупные плоды,  массой 200-250 г. Особенность плодов -  наличие выроста на верхушке плода. Пупочные сорта: Вашингтон Навел, Робертсон Нэйвл, Томсон Нэйвл.Яффские - сочные, сладкие и крупные плоды с толстой, бугристой кожурой темно-оранжевого цвета. Яффские сорта: Валенсия,  Беллади, , Шамути, Халили Иоппа,.Красные (корольки) с небольшими плодами с ярко-красной, вкусной и сочной мякотью.</p></div>', '<div class=\"col-sm-21\" style=\"box-sizing: border-box; margin: 0px; padding: 0px 5px; border: 0px; outline: 0px; font-size: 16px; font-family: \"Times New Roman\"; vertical-align: baseline; position: relative; min-height: 1px; float: left; width: 607.396px; color: rgb(0, 0, 0);\">Египет – крупнейший производитель и поставщик цитрусовых. Апельсины составляют 65% производства цитрусовых и 30% общего производства фруктов в этой стране. Самые распространенные египетские: сорта Navel и Valencia, Baladi, Shamouti. Россия является крупнейшим импортером египетских апельсинов: в нашу страну поставляется порядка 18% экспортируемых из Египта апельсинов (по данным Fruit News). После введения экономических санкций летом 2014 года доля Египетского экспорта в РФ только выросла и продолжает расти. </div><div><br></div>', '2018-12-21', NULL, NULL, '15653561044501.PNG', NULL, NULL, NULL, NULL, 1, 0, 19, 0, 1, 'الاطعان', 'Flames', '', '', NULL, NULL, 'alataaan', 'flames', 0, 0, 0, 1, 1, '2018-12-21 11:19:17', '2019-08-09 11:08:24'),
(71, 'مشروع بناء مسجد', 'Project of building a mosque', NULL, NULL, '2018-12-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 20, 0, 1, 'مشروع بناء مسجد', 'Project of building a mosque', '', '', NULL, NULL, 'mshroaa-bna-msjd', 'project-of-building-a-mosque', 0, 0, 0, 1, NULL, '2018-12-21 11:20:00', '2018-12-21 11:20:00'),
(74, 'test', 'tets', NULL, NULL, '2018-12-21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 24, 0, 1, 'test', 'tets', '', '', NULL, NULL, '', 'tets', 0, 0, 0, 1, NULL, '2018-12-21 11:35:15', '2018-12-21 11:35:15'),
(75, 'COMPANY INFORMATION', 'COMPANY INFORMATION', '<blockquote><blockquote><blockquote><p></p><blockquote><blockquote><blockquote><blockquote><blockquote><p></p><h1><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><b style=\"\"><font color=\"#000000\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</font></b></span></h1><h1><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><b><font color=\"#000000\">Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,</font></b></span></h1><h1><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><b><font color=\"#000000\">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</font></b></span></h1><h1><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><b><font color=\"#000000\">It has survived not only five centuries, but also the leap into electronic typesetting,</font></b></span></h1><h1><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><b><font color=\"#000000\">remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</font></b></span></h1><h1><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><b><font color=\"#000000\">Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</font></b></span></h1><p></p></blockquote></blockquote></blockquote></blockquote></blockquote><p></p></blockquote></blockquote></blockquote>', '<div><div><div><blockquote><blockquote><blockquote><blockquote><blockquote><blockquote><h1 style=\"font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.87);\"><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><span style=\"font-weight: 700;\"><font color=\"#000000\">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</font></span></span></h1><h1 style=\"font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.87);\"><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><span style=\"font-weight: 700;\"><font color=\"#000000\">Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,</font></span></span></h1><h1 style=\"font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.87);\"><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><span style=\"font-weight: 700;\"><font color=\"#000000\">when an unknown printer took a galley of type and scrambled it to make a type specimen book.</font></span></span></h1><h1 style=\"font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.87);\"><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><span style=\"font-weight: 700;\"><font color=\"#000000\">It has survived not only five centuries, but also the leap into electronic typesetting,</font></span></span></h1><h1 style=\"font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.87);\"><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><span style=\"font-weight: 700;\"><font color=\"#000000\">remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing</font></span></span></h1><h1 style=\"font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.87);\"><span style=\"font-size: 13.92px; background-color: rgb(239, 239, 239);\"><span style=\"font-weight: 700;\"><font color=\"#000000\">Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</font></span></span></h1></blockquote></blockquote></blockquote></blockquote></blockquote></blockquote></div></div></div>', '2018-12-21', NULL, NULL, '15653628247991.PNG', NULL, NULL, NULL, NULL, 1, 0, 23, 0, 1, 'tkj', 'dsdas', '', '', NULL, NULL, 'tkj', 'dsdas', 0, 0, 0, 1, 1, '2018-12-21 11:35:35', '2019-08-09 13:29:48'),
(76, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453994815231.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 7, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, 'yshsyanmsy', 'dsadasd', 0, 0, 0, 1, NULL, '2018-12-21 11:38:01', '2018-12-21 11:38:01'),
(77, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453995189919.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 8, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:38:38', '2018-12-21 11:38:38'),
(78, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453995427871.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 9, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:39:02', '2018-12-21 11:39:02'),
(79, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453995525965.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 10, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:39:12', '2018-12-21 11:39:12'),
(80, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453995691332.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 11, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:39:29', '2018-12-21 11:39:29'),
(81, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453995967185.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 12, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:39:56', '2018-12-21 11:39:56'),
(82, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453996372908.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 13, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:40:37', '2018-12-21 11:40:37'),
(83, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453996845972.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 14, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:41:24', '2018-12-21 11:41:24'),
(84, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453997195150.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 15, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:41:59', '2018-12-21 11:41:59'),
(85, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453998142458.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 16, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:43:34', '2018-12-21 11:43:34'),
(86, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15453999249552.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 17, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 11:45:24', '2018-12-21 11:45:24'),
(87, 'يىشسيانمسي', 'dsadasd', '<div dir=\"rtl\">nkjbkj</div>', '<div dir=\"ltr\">klklhklkl</div>', '2018-12-21', NULL, NULL, '15454009263380.jpg', NULL, NULL, NULL, NULL, 1, 0, 8, 0, 18, 'يىشسيانمسي', 'dsadasd', 'nkjbkj', 'klklhklkl', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2018-12-21 12:02:06', '2018-12-21 12:02:06'),
(88, 'dasdas', 'sadasd', '<div dir=\"rtl\">5456nmbnm</div>', '<div dir=\"ltr\">kjkhjkhjk665</div>', '2018-12-21', NULL, NULL, '15454022115437.jpg', NULL, NULL, NULL, NULL, 1, 0, 26, 0, 1, 'dasdas', 'sadasd', '5456nmbnm', 'kjkhjkhjk665', NULL, NULL, 'dasdas', 'sadasd', 0, 63, 0, 1, 1, '2018-12-21 12:23:31', '2018-12-22 07:29:38'),
(89, 'الجيزه', 'giza', NULL, NULL, '2018-12-22', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 15, 0, 4, 'الجيزه', 'giza', '', '', NULL, NULL, 'aljyzh', 'giza', 0, 0, 59, 1, NULL, '2018-12-22 04:08:36', '2018-12-22 04:08:36'),
(90, 'برتقال', 'rereewrew', '<div dir=\"rtl\">fdsfsdfdsfdsf</div>', '<div dir=\"ltr\">fdfdsfdsfdsfds</div>', '2019-01-04', '2019-01-04', NULL, '15465784924799.PNG', NULL, NULL, NULL, NULL, 1, 0, 27, 0, 1, 'برتقال', 'rereewrew', 'fdsfsdfdsfdsf', 'fdfdsfdsfdsfds', NULL, NULL, 'brtkal', 'rereewrew', 0, 0, 0, 1, NULL, '2019-01-04 03:08:12', '2019-01-04 03:08:12'),
(91, 'برتقال', 'ORANGE', '<h3 style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: 500; font-size: 18px; font-family: Roboto, sans-serif; vertical-align: baseline; line-height: 24px; color: rgb(0, 0, 0);\"><br></h3>', '<h3 style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: 500; font-size: 18px; font-family: Roboto, sans-serif; vertical-align: baseline; line-height: 24px; color: rgb(0, 0, 0);\">                                                                                                                                               : Varieties</h3><h3 style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: 500; font-size: 18px; font-family: Roboto, sans-serif; vertical-align: baseline; line-height: 24px; color: rgb(0, 0, 0);\"><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: 400; font-size: 15px; vertical-align: baseline; list-style: none; color: rgb(117, 117, 117);\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\"><em style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-family: inherit; vertical-align: baseline;\">Обыкновенные:</em></strong> плоды разных размеров, с желтой мякотью, кисло-сладкие на вкус.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\"><em style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-family: inherit; vertical-align: baseline;\">Пупочные:</em></strong> крупные плоды,  массой 200-250 г. Особенность плодов -  наличие выроста на верхушке плода. Пупочные сорта: Вашингтон Навел, Робертсон Нэйвл, Томсон Нэйвл.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\"><em style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-family: inherit; vertical-align: baseline;\">Яффские </em></strong>- сочные, сладкие и крупные плоды с толстой, бугристой кожурой темно-оранжевого цвета. Яффские сорта: Валенсия,  Беллади, , Шамути, Халили Иоппа,.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\"><em style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-family: inherit; vertical-align: baseline;\">Красные (корольки)</em></strong> с небольшими плодами с ярко-красной, вкусной и сочной мякотью.</li></ul></h3>', '2019-01-04', NULL, NULL, '15466334525722.PNG', NULL, NULL, NULL, NULL, 1, 0, 19, 0, 1, 'تست', 'test', '', '', NULL, NULL, 'tst', '', 0, 0, 59, 1, 1, '2019-01-04 17:41:39', '2019-01-07 16:42:59'),
(92, 'اليوسفي', 'MANDARIN', '<div dir=rtl><br></div>', '<div dir=\"ltr\"><div class=\"content\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; line-height: 22px; color: rgb(117, 117, 117);\"><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline;\">Сорта мандаринов сегодня делятся на три группы:</p><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: none;\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Первая группа</strong>: это теплолюбивые мандарины благородные.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Вторая группа:</strong> теплолюбивые, мелколистные танжерины или, как их еще называют, итальянские мандарины.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Третья группа:</strong> сатсумы или уншиу. Их родиной является Япония, и они отличаются стойкостью к холоду. Также этот сорт растет на Черноморском побережье. В этом сорте практически не попадаются семена, из-за чего им дали название мандарин бессемянный.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Существуют и гибриды</strong>, полученные в результате скрещивания мандаринов и других фруктов. Наиболее известные – клементины, это смесь мандарина с померанцем. Плоды имеют оранжево-красный окрас и приплюснутую форму, а также обладают сильным ароматом.</li><li></li></ul></div><div class=\"cleaner\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; clear: both; height: 0px; line-height: 0; color: rgb(117, 117, 117);\"></div></div>', '2019-01-04', NULL, NULL, '15466335937137.PNG', NULL, NULL, NULL, NULL, 1, 0, 19, 0, 2, 'MANDARIN', 'MANDARIN', '', 'Сорта мандаринов сегодня делятся на три группы:Первая группа: это теплолюбивые мандарины благородные.Вторая группа:&nbsp;теплолюбивые, мелколистные танжерины или, ка', NULL, NULL, 'mandarin', 'mandarin', 0, 0, 0, 1, 1, '2019-01-04 18:26:33', '2019-01-04 18:29:01'),
(94, 'ليمون', 'LEMON', '<div dir=rtl><br></div>', '<div dir=\"ltr\"><div class=\"content\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; line-height: 22px; color: rgb(117, 117, 117);\"><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: none;\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Древовидные:</strong>&nbsp;высокорослые деревья (до 6 м высотой), плоды образуются в глубине кроны. Древовидные сорта: Лисбон, Ламас (Турция).</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Кустовидные:</strong>&nbsp;небольшие кусты (до 3 - 4 м выс.) с менее густой кроной, плоды образуются на концах ветвей; кустовидные сорта менее урожайны, чем древовидные. Кустовидные: Мейер.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">«ЛАМАС»</strong>&nbsp;- самый качественный сорт Турции, выращивается в Эрдемли-Силифке. Плод средних размеров, кожура желтого цвета, приятного запаха и вкуса. Срок хранения - 9 мес.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">«ЭНТЕРДОНАТ»</strong>&nbsp;- один из ранних сортов. Плоды большого размера, цвет кожуры - светло-зеленый и блестящий. Имеет короткий срок хранения.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">«МЕЙЕР»</strong>&nbsp;- особый вид, гибрид лимона и апельсина, родина которого Китай. По урожайности сорт Мейера стоит на первом месте.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">«ЭВРИКА»</strong>&nbsp;- этот сорт является одним из самых распространенным в ЮАР, Аргентине, Австралии, Калифорнии и Израиле. Плоды среднего размера, более адаптирован к климатическим условиям побережья.</li><li></li></ul></div><div class=\"cleaner\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; clear: both; height: 0px; line-height: 0; color: rgb(117, 117, 117);\"></div></div>', '2019-01-04', NULL, NULL, '15466336899144.PNG', NULL, NULL, NULL, NULL, 1, 0, 19, 0, 3, 'ليمون', 'LEMON', '', 'Древовидные:&nbsp;высокорослые деревья (до 6 м высотой), плоды образуются в глубине кроны. Древовидные сорта: Лисбон, Ламас (Турция).Кустовидные:&nbsp;небольшие куст', NULL, NULL, 'lymon', 'lemon', 0, 0, 0, 1, NULL, '2019-01-04 18:28:09', '2019-01-04 18:28:09'),
(95, 'الفراولة', 'STRAWBERRY', '<div dir=rtl><br></div>', '<div dir=\"ltr\"><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(117, 117, 117);\">Клубнику разделяют по длительности плодоношения и по срокам созревания.</p><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; list-style: none; color: rgb(117, 117, 117);\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">По длительности плодоношения на –<strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\"> созревающую один раз в сезон (июнь-сентябрь месяц)</strong> и <strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">ремонтантную</strong> – плодоносящую с конца весны и до сентября-октября месяца. Немаловажную роль для созревания ягод клубники играет климатическая зона роста растения.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">По срокам созревания сортов</strong>, клубника, плодоносящих один раз в сезон, подразделяется на четыре вида – раннюю, средне раннюю, средне позднюю и позднюю. К ранним сортам относятся – Ольвия, Клери, Чешская Красавица, Заря, Антеа. К средне ранним – Эльсанта, Фестивальная и Столичная. К средне поздним – Презент, Гигантелла, Полка и Ароза. К поздним сортам – Пегас и Чамора Таруса.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Ремонтантная клубника отличается от «сезонной»</strong> сильным развитием вегетативных органов. Соцветия у нее закладываются в 15-20 раз больше, чем у обычных сортов. К ремонтантным сортам относятся Королева Елизавета ІІ, Брайтон.</li></ul></div>', '2019-01-04', NULL, NULL, '15466338839862.PNG', NULL, NULL, NULL, NULL, 1, 0, 21, 0, 1, 'الفراولة', 'STRAWBERRY', '', 'Клубнику разделяют по длительности плодоношения и по срокам созревания.По длительности плодоношения на –&nbsp;созревающую один раз в сезон (июнь-сентябрь месяц)&nbsp', NULL, NULL, 'alfraol', 'strawberry', 0, 0, 59, 1, 1, '2019-01-04 18:31:23', '2019-01-07 17:24:59'),
(96, 'كرز', 'cherries', '<div dir=rtl><br></div>', '<div dir=\"ltr\"><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(117, 117, 117);\">Все сорта черешни принадлежат к двум группам:&nbsp;<strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">гини и бигарро.</strong></p><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; list-style: none; color: rgb(117, 117, 117);\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Сорта черешни группы гини</strong>&nbsp;имеют ранние сроки созревания, у них нежная, сочная и сладкая мякоть, употребляются только в свежем виде, хранятся недолго и малотранспортабельны.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Сорта черешни группы бигарро</strong>&nbsp;имеют плотную, упругую мякоть и средние и поздние сроки созревания. Их можно консервировать и замораживать, хранятся они не очень долго, зато хорошо транспортируются.</li></ul><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(117, 117, 117);\">По времени созревания выделяют три группы черешни:&nbsp;<strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">ранние, средние и поздние сорта.</strong></p><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; list-style: none; color: rgb(117, 117, 117);\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Ранние сорта</strong>&nbsp;черешни характеризуются созреванием плодов примерно в середине июня.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Плоды сортов среднего срока созревают,</strong>&nbsp;начиная с конца июня и заканчивая началом июля.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Поздние сорта</strong>&nbsp;черешни созревают примерно в середине июля.</li></ul></div>', '2019-01-04', NULL, NULL, '15466339867324.PNG', NULL, NULL, NULL, NULL, 1, 0, 21, 0, 2, 'كرز', 'cherries', '', 'Все сорта черешни принадлежат к двум группам:&nbsp;гини и бигарро.Сорта черешни группы гини&nbsp;имеют ранние сроки созревания, у них нежная, сочная и сладкая мякоть', NULL, NULL, 'krz', 'cherries', 0, 0, 0, 1, NULL, '2019-01-04 18:33:06', '2019-01-04 18:33:06'),
(97, 'بطاطا', 'POTATOES', '<div dir=rtl><br></div>', '<div dir=\"ltr\"><div class=\"content\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; line-height: 22px; color: rgb(117, 117, 117);\"><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline;\">В настоящий момент в мире существует большое количество сортов картофеля разного хозяйственного назначения – столового, технического, кормового и универсального. Столовые сорта отличаются высокими вкусовыми качествами и используются в кулинарии.</p><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline;\">По сроку созревания все сорта делятся на:</p><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: none;\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">ранние</strong> (готовы к уборке через 50-65 дней с момента посадки);</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">средне ранние</strong> (уборка через 65-85 дней);</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">средне спелые</strong> (уборка через 85-95 дней);</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">средне поздние</strong> (уборка через 95-110 дней);</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">поздние</strong> (уборка через 110 дней и выше).</li><li></li></ul></div><div class=\"cleaner\" style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; clear: both; height: 0px; line-height: 0; color: rgb(117, 117, 117);\"></div></div>', '2019-01-04', NULL, NULL, '15466340887969.PNG', NULL, NULL, NULL, NULL, 1, 0, 22, 0, 1, 'بطاطا', 'POTATOES', '', 'В настоящий момент в мире существует большое количество сортов картофеля разного хозяйственного назначения – столового, технического, кормового и универсального. Сто', NULL, NULL, 'btata', 'potatoes', 0, 0, 59, 1, 1, '2019-01-04 18:34:48', '2019-01-07 17:30:29'),
(98, 'فلفل', 'PEPPER', '<div dir=rtl><br></div>', '<div dir=\"ltr\"><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(117, 117, 117);\">Сейчас известны более 250 видов и гибридов перца сладкого, хотя видов этого перца всего пять:</p><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; list-style: none; color: rgb(117, 117, 117);\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum annuum – самый распространенный вид перца. К нему относятся: кайенский перец, кубинский перец, все виды болгарского перца. «Annuum» - означает «однолетний», что не соответствует действительности, так как некоторые сорта, являются многолетниками.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum Chinense – «китайский» перец. К этому виду относятся самые жгучие перцы – Хабанеро, скотч Бонет, Ред Савина. Отличается ярко выраженным фруктовым ароматом, напоминающим абрикос.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum Baccatum или «ягодный» перец. В Америке известен как «повислый» перец или «чили». Этот вид отличается наличием желтых либо коричневых пятен на соцветиях.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum Frutescens – «кустистый» перец. Самые известные сорта: Табаско и Малагуэтта.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum Pubescens – «пушистый» перец. Это наименее известный из культивируемых перцев. К этому виду относятся: перуанский Рокотто и мексиканский Манзано. Это самый «капризныЙ» для выращивания вид перца.</li></ul></div>', '2019-01-04', NULL, NULL, '15466341895614.PNG', NULL, NULL, NULL, NULL, 1, 0, 22, 0, 2, 'فلفل', 'PEPPER', '', 'Сейчас известны более 250 видов и гибридов перца сладкого, хотя видов этого перца всего пять:Capsicum annuum – самый распространенный вид перца. К нему относятся: ка', NULL, NULL, 'flfl', 'pepper', 0, 0, 0, 1, NULL, '2019-01-04 18:36:29', '2019-01-04 18:36:29'),
(99, 'تفاح', 'apples & pears', NULL, NULL, '2019-01-04', NULL, NULL, '15466343065742.PNG', NULL, NULL, NULL, NULL, 1, 0, 31, 0, 1, 'تفاح', 'apples & pears', '', '', NULL, NULL, 'tfah', 'apples-pears', 0, 0, 0, 1, NULL, '2019-01-04 18:38:26', '2019-01-04 18:38:26'),
(100, 'نبات الهليون', 'asparagus', NULL, NULL, '2019-01-04', NULL, NULL, '15466343999826.PNG', NULL, NULL, NULL, NULL, 1, 0, 31, 0, 2, 'نبات الهليون', 'asparagus', '', '', NULL, NULL, 'nbat-alhlyon', 'asparagus', 0, 0, 0, 1, NULL, '2019-01-04 18:39:59', '2019-01-04 18:39:59'),
(101, 'توت', 'Berries', NULL, NULL, '2019-01-04', NULL, NULL, '15466345097063.PNG', NULL, NULL, NULL, NULL, 1, 0, 31, 0, 3, 'توت', 'Berries', '', '', NULL, NULL, 'tot', '', 0, 0, 0, 1, NULL, '2019-01-04 18:41:49', '2019-01-04 18:41:49'),
(102, 'مصر', 'Egypt', '<div dir=rtl><br></div>', '<div dir=ltr><br></div>', '2019-01-07', NULL, NULL, '15468703142051.PNG', NULL, NULL, NULL, NULL, 1, 0, 33, 0, 1, 'مصر', 'Egypt', '', '', NULL, NULL, '', '', 0, 0, 0, 1, NULL, '2019-01-07 12:11:54', '2019-01-07 12:11:54'),
(103, 'الرمان', 'Roman', '<div dir=rtl><br></div>', '<div dir=ltr><br></div>', '2019-01-07', NULL, NULL, '15468833808768.PNG', NULL, NULL, NULL, NULL, 1, 0, 19, 0, 4, 'الرمان', 'Roman', '', '', NULL, NULL, 'alrman', 'roman', 0, 0, 0, 1, NULL, '2019-01-07 15:49:40', '2019-01-07 15:49:40'),
(104, 'فراوله', 'farawa', '<div dir=rtl><br></div>', '<div dir=ltr><br></div>', '2019-01-07', NULL, NULL, '15468839988244.PNG', NULL, NULL, NULL, NULL, 1, 0, 19, 0, 5, 'فراوله', 'farawa', '', '', NULL, NULL, 'fraolh', 'farawa', 0, 0, 0, 1, NULL, '2019-01-07 15:59:58', '2019-01-07 15:59:58'),
(105, 'تست', 'ree', '<div dir=\"rtl\">erewrw</div>', '<div dir=\"ltr\">erewrew</div>', '2019-01-07', NULL, NULL, '15653561188675.PNG', NULL, NULL, NULL, NULL, 1, 0, 19, 0, 6, 'تست', 'ree', 'erewrw', 'erewrew', NULL, NULL, '', 'ree', 0, 0, 0, 1, 1, '2019-01-07 16:15:53', '2019-08-09 11:08:38'),
(106, 'تست', 'ree', '<div dir=\"rtl\">erewrw</div>', '<div dir=\"ltr\">erewrew</div>', '2019-01-07', NULL, NULL, '15653561292755.PNG', NULL, NULL, NULL, NULL, 1, 0, 19, 0, 7, 'تست', 'ree', 'erewrw', 'erewrew', NULL, NULL, '', '', 0, 0, 59, 1, 1, '2019-01-07 16:17:55', '2019-08-09 11:08:49'),
(107, 'تست', 'ree', '<div dir=\"rtl\">erewrw</div>', '<div dir=\"ltr\">erewrew</div>', '2019-01-07', NULL, NULL, '15653561432925.PNG', NULL, NULL, NULL, NULL, 1, 0, 19, 0, 8, 'تست', 'ree', 'erewrw', 'erewrew', NULL, NULL, '', '', 0, 0, 59, 1, 1, '2019-01-07 16:18:04', '2019-08-09 11:09:03');
INSERT INTO `topics` (`id`, `title_ar`, `title_en`, `details_ar`, `details_en`, `date`, `expire_date`, `video_type`, `photo_file`, `attach_file`, `video_file`, `audio_file`, `icon`, `status`, `visits`, `webmaster_id`, `section_id`, `row_no`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `lang_id`, `city_id`, `country_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(108, 'كرز', 'cherries', '<div dir=rtl><br></div>', '<div dir=\"ltr\"><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(117, 117, 117);\">Все сорта черешни принадлежат к двум группам:&nbsp;<strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">гини и бигарро.</strong></p><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; list-style: none; color: rgb(117, 117, 117);\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Сорта черешни группы гини</strong>&nbsp;имеют ранние сроки созревания, у них нежная, сочная и сладкая мякоть, употребляются только в свежем виде, хранятся недолго и малотранспортабельны.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Сорта черешни группы бигарро</strong>&nbsp;имеют плотную, упругую мякоть и средние и поздние сроки созревания. Их можно консервировать и замораживать, хранятся они не очень долго, зато хорошо транспортируются.</li></ul><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(117, 117, 117);\">По времени созревания выделяют три группы черешни:&nbsp;<strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">ранние, средние и поздние сорта.</strong></p><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; list-style: none; color: rgb(117, 117, 117);\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Ранние сорта</strong>&nbsp;черешни характеризуются созреванием плодов примерно в середине июня.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Плоды сортов среднего срока созревают,</strong>&nbsp;начиная с конца июня и заканчивая началом июля.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\"><strong style=\"box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-style: inherit; font-family: inherit; vertical-align: baseline; color: rgb(0, 0, 0);\">Поздние сорта</strong>&nbsp;черешни созревают примерно в середине июля.</li></ul></div>', '2019-01-04', NULL, NULL, '15466339867324.PNG', NULL, NULL, NULL, NULL, 1, 0, 21, 0, 2, 'كرز', 'cherries', '', 'Все сорта черешни принадлежат к двум группам:&nbsp;гини и бигарро.Сорта черешни группы гини&nbsp;имеют ранние сроки созревания, у них нежная, сочная и сладкая мякоть', NULL, NULL, 'krz', 'cherries', 0, 0, 0, 1, NULL, '2019-01-04 18:33:06', '2019-01-04 18:33:06'),
(109, 'فلفل', 'PEPPER', '<div dir=rtl><br></div>', '<div dir=\"ltr\"><p style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; color: rgb(117, 117, 117);\">Сейчас известны более 250 видов и гибридов перца сладкого, хотя видов этого перца всего пять:</p><ul style=\"box-sizing: border-box; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 15px; font-family: Roboto, sans-serif; vertical-align: baseline; list-style: none; color: rgb(117, 117, 117);\"><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum annuum – самый распространенный вид перца. К нему относятся: кайенский перец, кубинский перец, все виды болгарского перца. «Annuum» - означает «однолетний», что не соответствует действительности, так как некоторые сорта, являются многолетниками.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum Chinense – «китайский» перец. К этому виду относятся самые жгучие перцы – Хабанеро, скотч Бонет, Ред Савина. Отличается ярко выраженным фруктовым ароматом, напоминающим абрикос.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum Baccatum или «ягодный» перец. В Америке известен как «повислый» перец или «чили». Этот вид отличается наличием желтых либо коричневых пятен на соцветиях.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum Frutescens – «кустистый» перец. Самые известные сорта: Табаско и Малагуэтта.</li><li style=\"box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; outline: 0px; font-weight: inherit; font-style: inherit; font-family: inherit; vertical-align: baseline; list-style: disc inside;\">Capsicum Pubescens – «пушистый» перец. Это наименее известный из культивируемых перцев. К этому виду относятся: перуанский Рокотто и мексиканский Манзано. Это самый «капризныЙ» для выращивания вид перца.</li></ul></div>', '2019-01-04', NULL, NULL, '15466341895614.PNG', NULL, NULL, NULL, NULL, 1, 0, 22, 0, 2, 'فلفل', 'PEPPER', '', 'Сейчас известны более 250 видов и гибридов перца сладкого, хотя видов этого перца всего пять:Capsicum annuum – самый распространенный вид перца. К нему относятся: ка', NULL, NULL, 'flfl', 'pepper', 0, 0, 0, 1, NULL, '2019-01-04 18:36:29', '2019-01-04 18:36:29');

-- --------------------------------------------------------

--
-- Table structure for table `topic_categories`
--

CREATE TABLE `topic_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `topic_categories`
--

INSERT INTO `topic_categories` (`id`, `topic_id`, `section_id`, `created_at`, `updated_at`) VALUES
(4, 28, 8, '2017-11-08 19:39:58', '2017-11-08 19:39:58'),
(5, 28, 9, '2017-11-08 19:39:58', '2017-11-08 19:39:58'),
(6, 28, 10, '2017-11-08 19:39:58', '2017-11-08 19:39:58'),
(7, 28, 14, '2017-11-08 19:39:58', '2017-11-08 19:39:58'),
(8, 29, 8, '2017-11-08 19:40:15', '2017-11-08 19:40:15'),
(9, 29, 9, '2017-11-08 19:40:15', '2017-11-08 19:40:15'),
(10, 29, 10, '2017-11-08 19:40:15', '2017-11-08 19:40:15'),
(11, 29, 12, '2017-11-08 19:40:15', '2017-11-08 19:40:15'),
(12, 36, 15, '2017-11-08 19:40:48', '2017-11-08 19:40:48'),
(13, 36, 16, '2017-11-08 19:40:48', '2017-11-08 19:40:48'),
(14, 36, 18, '2017-11-08 19:40:48', '2017-11-08 19:40:48'),
(15, 37, 16, '2017-11-08 19:40:58', '2017-11-08 19:40:58'),
(16, 37, 17, '2017-11-08 19:40:58', '2017-11-08 19:40:58'),
(17, 15, 1, '2017-11-08 19:42:12', '2017-11-08 19:42:12'),
(18, 15, 2, '2017-11-08 19:42:12', '2017-11-08 19:42:12'),
(19, 16, 5, '2017-11-08 19:42:26', '2017-11-08 19:42:26'),
(20, 16, 6, '2017-11-08 19:42:26', '2017-11-08 19:42:26'),
(21, 17, 3, '2017-11-08 19:42:35', '2017-11-08 19:42:35'),
(22, 18, 1, '2017-11-08 19:42:52', '2017-11-08 19:42:52'),
(23, 18, 6, '2017-11-08 19:42:52', '2017-11-08 19:42:52'),
(24, 19, 1, '2017-11-08 19:43:02', '2017-11-08 19:43:02'),
(25, 20, 3, '2017-11-08 19:43:10', '2017-11-08 19:43:10'),
(26, 21, 1, '2017-11-08 19:43:24', '2017-11-08 19:43:24'),
(27, 21, 2, '2017-11-08 19:43:24', '2017-11-08 19:43:24'),
(28, 21, 3, '2017-11-08 19:43:24', '2017-11-08 19:43:24'),
(29, 22, 2, '2017-11-08 19:43:35', '2017-11-08 19:43:35'),
(30, 22, 4, '2017-11-08 19:43:35', '2017-11-08 19:43:35'),
(42, 32, 19, '2017-11-08 19:47:30', '2017-11-08 19:47:30'),
(43, 32, 20, '2017-11-08 19:47:30', '2017-11-08 19:47:30'),
(44, 33, 22, '2017-11-08 19:48:03', '2017-11-08 19:48:03'),
(45, 34, 21, '2017-11-08 19:48:16', '2017-11-08 19:48:16'),
(46, 35, 19, '2017-11-08 19:48:32', '2017-11-08 19:48:32'),
(47, 35, 20, '2017-11-08 19:48:32', '2017-11-08 19:48:32'),
(48, 35, 21, '2017-11-08 19:48:32', '2017-11-08 19:48:32');

-- --------------------------------------------------------

--
-- Table structure for table `topic_fields`
--

CREATE TABLE `topic_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `field_id` int(11) NOT NULL,
  `field_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `topic_fields`
--

INSERT INTO `topic_fields` (`id`, `topic_id`, `field_id`, `field_value`, `created_at`, `updated_at`) VALUES
(3, 32, 1, '50000 USD', '2017-11-08 19:47:30', '2017-11-08 19:47:30'),
(4, 32, 2, '1', '2017-11-08 19:47:30', '2017-11-08 19:47:30'),
(5, 33, 1, '20000 USD', '2017-11-08 19:48:03', '2017-11-08 19:48:03'),
(6, 33, 2, '2', '2017-11-08 19:48:03', '2017-11-08 19:48:03'),
(7, 34, 1, '30000 USD', '2017-11-08 19:48:17', '2017-11-08 19:48:17'),
(8, 34, 2, '1', '2017-11-08 19:48:17', '2017-11-08 19:48:17'),
(9, 35, 1, '4550 USD', '2017-11-08 19:48:32', '2017-11-08 19:48:32'),
(10, 35, 2, '1', '2017-11-08 19:48:32', '2017-11-08 19:48:32'),
(12, 55, 2, '1', '2018-12-16 02:50:21', '2018-12-16 02:50:21'),
(13, 87, 1, '12313', '2018-12-21 12:02:06', '2018-12-21 12:02:06'),
(14, 87, 2, '1', '2018-12-21 12:02:07', '2018-12-21 12:02:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `connect_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `connect_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `photo`, `permissions_id`, `status`, `connect_email`, `connect_password`, `remember_token`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@site.com', '$2y$10$HtxE53dPU9V4bswYLz/JquEJSF4UDTDqNMNmauoszrGvJLxCLmIPC', NULL, 1, 1, NULL, NULL, 'RtOFPB51UfYZOv5Vfo7J65GEn81HdxzFTbpD6MrIl402ftW0J9BFZQhPm4sf', 1, 1, '2017-11-08 13:25:54', '2018-12-08 21:04:15'),
(2, 'manager', 'manager@site.com', '$2y$10$Vb9uqouQEPz613i1CAMMJunEgTwq7oaf9i18k.eMjD/rymGJcsT5S', NULL, 2, 1, NULL, NULL, 'rJsOCrk8mpmZS23sYZUp3OHw1G1K5mPz6m4bmPQtZjADyTURnBlVyHaC2RJX', 1, 1, '2017-11-08 13:25:54', '2018-12-15 16:29:39'),
(3, 'user', 'user@site.com', '$2y$10$JFfZ4nfOHNJlzEefZk9Oq.QcHzqaIOCM7kU0/0fltjptMrU4hj7UO', NULL, 3, 1, NULL, NULL, NULL, 1, NULL, '2017-11-08 13:25:54', '2017-11-08 13:25:54');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `ig` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group_name_trans_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `description_trans_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_log`
--

CREATE TABLE `users_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `user_group` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `webmails`
--

CREATE TABLE `webmails` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `father_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` longtext COLLATE utf8mb4_unicode_ci,
  `date` datetime NOT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_cc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_bcc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `webmails`
--

INSERT INTO `webmails` (`id`, `cat_id`, `group_id`, `contact_id`, `father_id`, `title`, `details`, `date`, `from_email`, `from_name`, `from_phone`, `to_email`, `to_name`, `to_cc`, `to_bcc`, `status`, `flag`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 0, 2, NULL, NULL, 'ORDER , Qty=12, Nullam mollis dolor', 'dfdfd', '2017-03-07 15:21:20', 'eng_m_mondy@hotmail.com', 'mohamed mondi', '01221485486', 'info@sitename.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-07 13:21:20', '2017-03-07 17:37:48'),
(2, 0, NULL, NULL, NULL, 'Need your help', 'Dear sir,\r\nI need your help to subscribe to your team. Please contact me as soon as possible.\r\n\r\nBest Regards', '2017-03-07 16:04:16', 'ayamen@site.com', 'Amar Yamen', '8378-475-466', 'info@sitename.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-07 14:04:16', '2017-03-07 14:04:16'),
(3, 0, 3, NULL, NULL, 'My test message to this site', 'I just test sending messages\r\nThanks', '2017-03-07 16:05:32', 'email@site.com', 'Donyo Hawa', '343423-543', 'info@sitename.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-07 14:05:32', '2017-03-07 14:11:59'),
(4, 0, 1, NULL, NULL, 'Contact me for support any time', 'This is a test message', '2017-03-07 16:10:29', 'email@site.com', 'MMondi', '7363758', 'info@sitename.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-07 14:10:29', '2017-03-07 14:11:54'),
(5, 0, NULL, NULL, NULL, 'Test mail delivery message', 'Dear team,\r\nThis is a Test mail delivery message\r\nThank you', '2017-03-07 21:06:41', 'email@site.com', 'Ramy Adams', '87557home', 'support@smartfordesign.com', 'Smartend Laravel Site Preview', NULL, NULL, 0, 0, NULL, NULL, '2017-03-08 02:06:41', '2017-03-08 02:06:41'),
(6, 0, NULL, NULL, NULL, 'Test mail delivery message', 'Dear team,\r\nThis is a Test mail delivery message\r\nThank you', '2017-03-07 21:08:54', 'email@site.com', 'Adam Ali', '3432423', 'support@smartfordesign.com', 'Smartend Laravel Site Preview', NULL, NULL, 1, 0, NULL, NULL, '2017-03-08 02:08:54', '2019-08-09 11:22:36'),
(7, 0, NULL, NULL, NULL, NULL, 'test  tesss', '2019-08-27 19:22:46', 'islam.elshenawy@trioconceptme.com', 'esalm elshenawy', '01009739491', '', 'كتب', NULL, NULL, 0, 0, NULL, NULL, '2019-08-27 17:22:46', '2019-08-27 17:22:46');

-- --------------------------------------------------------

--
-- Table structure for table `webmails_files`
--

CREATE TABLE `webmails_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `webmail_id` int(11) NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `webmails_groups`
--

CREATE TABLE `webmails_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `webmails_groups`
--

INSERT INTO `webmails_groups` (`id`, `name`, `color`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Support', '# 00bcd4', 1, NULL, '2017-03-07 14:10:58', '2017-03-07 14:10:58'),
(2, 'Orders', '#f44336', 1, NULL, '2017-03-07 14:11:04', '2017-03-07 14:11:04'),
(3, 'Queries', '#8bc34a', 1, NULL, '2017-03-07 14:11:37', '2017-03-07 14:11:37');

-- --------------------------------------------------------

--
-- Table structure for table `webmaster_banners`
--

CREATE TABLE `webmaster_banners` (
  `id` int(10) UNSIGNED NOT NULL,
  `row_no` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `desc_status` tinyint(4) NOT NULL,
  `link_status` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `icon_status` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `webmaster_banners`
--

INSERT INTO `webmaster_banners` (`id`, `row_no`, `name`, `width`, `height`, `desc_status`, `link_status`, `type`, `icon_status`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'homeBanners', 1600, 500, 1, 1, 1, 0, 1, 1, NULL, '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(2, 2, 'textBanners', 330, 330, 1, 1, 0, 1, 1, 1, NULL, '2017-11-08 13:25:54', '2017-11-08 13:25:54'),
(3, 3, 'sideBanners', 330, 330, 0, 1, 1, 0, 1, 1, NULL, '2017-11-08 13:25:54', '2017-11-08 13:25:54');

-- --------------------------------------------------------

--
-- Table structure for table `webmaster_sections`
--

CREATE TABLE `webmaster_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `row_no` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(4) NOT NULL,
  `sections_status` tinyint(4) NOT NULL,
  `comments_status` tinyint(4) NOT NULL,
  `date_status` tinyint(4) NOT NULL,
  `expire_date_status` tinyint(4) NOT NULL,
  `longtext_status` tinyint(4) NOT NULL,
  `editor_status` tinyint(4) NOT NULL,
  `attach_file_status` tinyint(4) NOT NULL,
  `extra_attach_file_status` tinyint(4) NOT NULL,
  `multi_images_status` tinyint(4) NOT NULL,
  `section_icon_status` tinyint(4) NOT NULL,
  `icon_status` tinyint(4) NOT NULL,
  `maps_status` tinyint(4) NOT NULL,
  `order_status` tinyint(4) NOT NULL,
  `related_status` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `status_color` int(11) NOT NULL DEFAULT '0',
  `size` int(11) NOT NULL,
  `status_delete` int(11) NOT NULL DEFAULT '0',
  `status_type` int(11) NOT NULL DEFAULT '0',
  `style_gold` int(11) NOT NULL DEFAULT '0',
  `seo_title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_description_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_keywords_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `seo_url_slug_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `webmaster_sections`
--

INSERT INTO `webmaster_sections` (`id`, `row_no`, `name`, `type`, `sections_status`, `comments_status`, `date_status`, `expire_date_status`, `longtext_status`, `editor_status`, `attach_file_status`, `extra_attach_file_status`, `multi_images_status`, `section_icon_status`, `icon_status`, `maps_status`, `order_status`, `related_status`, `status`, `status_color`, `size`, `status_delete`, `status_type`, `style_gold`, `seo_title_ar`, `seo_title_en`, `seo_description_ar`, `seo_description_en`, `seo_keywords_ar`, `seo_keywords_en`, `seo_url_slug_ar`, `seo_url_slug_en`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(19, 1, 'fruits', 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'fruits', 'fruits', 1, 1, '2019-08-09 10:59:14', '2019-08-09 11:06:44'),
(21, 2, 'Berries', 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'berries', 'berries', 1, NULL, '2019-08-09 11:11:28', '2019-08-09 11:11:28'),
(22, 3, 'Vegetables', 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'vegetables', 'vegetables', 1, NULL, '2019-08-09 11:14:21', '2019-08-09 11:14:21'),
(23, 4, 'about_us', 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 1, 1, '2019-08-09 12:35:55', '2019-08-09 12:36:26'),
(24, 5, 'blog', 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 'blog', 'blog', 1, 1, '2019-08-09 13:34:46', '2019-08-09 13:35:28');

-- --------------------------------------------------------

--
-- Table structure for table `webmaster_section_fields`
--

CREATE TABLE `webmaster_section_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `webmaster_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `title_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details_ar` text COLLATE utf8mb4_unicode_ci,
  `details_en` text COLLATE utf8mb4_unicode_ci,
  `row_no` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `required` tinyint(4) NOT NULL,
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `webmaster_settings`
--

CREATE TABLE `webmaster_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `ar_box_status` tinyint(4) NOT NULL,
  `en_box_status` tinyint(4) NOT NULL,
  `seo_status` tinyint(4) NOT NULL,
  `analytics_status` tinyint(4) NOT NULL,
  `banners_status` tinyint(4) NOT NULL,
  `inbox_status` tinyint(4) NOT NULL,
  `calendar_status` tinyint(4) NOT NULL,
  `settings_status` tinyint(4) NOT NULL,
  `newsletter_status` tinyint(4) NOT NULL,
  `members_status` tinyint(4) NOT NULL,
  `orders_status` tinyint(4) NOT NULL,
  `shop_status` tinyint(4) NOT NULL,
  `shop_settings_status` tinyint(4) NOT NULL,
  `default_currency_id` int(11) NOT NULL,
  `languages_count` int(11) NOT NULL,
  `latest_news_section_id` int(11) NOT NULL,
  `header_menu_id` int(11) NOT NULL,
  `footer_menu_id` int(11) NOT NULL,
  `home_banners_section_id` int(11) NOT NULL,
  `home_text_banners_section_id` int(11) NOT NULL,
  `side_banners_section_id` int(11) NOT NULL,
  `contact_page_id` int(11) NOT NULL,
  `newsletter_contacts_group` int(11) NOT NULL,
  `new_comments_status` tinyint(4) NOT NULL,
  `links_status` tinyint(4) NOT NULL,
  `register_status` tinyint(4) NOT NULL,
  `permission_group` int(11) NOT NULL,
  `api_status` tinyint(4) NOT NULL,
  `api_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_content1_section_id` int(11) NOT NULL,
  `home_content2_section_id` int(11) NOT NULL,
  `home_content3_section_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `webmaster_settings`
--

INSERT INTO `webmaster_settings` (`id`, `ar_box_status`, `en_box_status`, `seo_status`, `analytics_status`, `banners_status`, `inbox_status`, `calendar_status`, `settings_status`, `newsletter_status`, `members_status`, `orders_status`, `shop_status`, `shop_settings_status`, `default_currency_id`, `languages_count`, `latest_news_section_id`, `header_menu_id`, `footer_menu_id`, `home_banners_section_id`, `home_text_banners_section_id`, `side_banners_section_id`, `contact_page_id`, `newsletter_contacts_group`, `new_comments_status`, `links_status`, `register_status`, `permission_group`, `api_status`, `api_key`, `home_content1_section_id`, `home_content2_section_id`, `home_content3_section_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 1, 2, 3, 2, 1, 0, 0, 0, 3, 1, '571775002564288', 0, 0, 0, 1, 1, '2017-11-08 13:25:54', '2019-08-09 11:21:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `analytics_pages`
--
ALTER TABLE `analytics_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `analytics_visitors`
--
ALTER TABLE `analytics_visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attach_files`
--
ALTER TABLE `attach_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_accounts`
--
ALTER TABLE `cau_accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_avd_photos`
--
ALTER TABLE `cau_avd_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_classes`
--
ALTER TABLE `cau_classes`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `cau_classtree`
--
ALTER TABLE `cau_classtree`
  ADD PRIMARY KEY (`id`),
  ADD KEY `node_id` (`node_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `grand_id` (`grand_id`);

--
-- Indexes for table `cau_compaigns`
--
ALTER TABLE `cau_compaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_countries`
--
ALTER TABLE `cau_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_country_states`
--
ALTER TABLE `cau_country_states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country` (`country`);

--
-- Indexes for table `cau_photogal_info`
--
ALTER TABLE `cau_photogal_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_photogal_jo`
--
ALTER TABLE `cau_photogal_jo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photogal_info` (`photogal_info`);

--
-- Indexes for table `cau_photos`
--
ALTER TABLE `cau_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_projects`
--
ALTER TABLE `cau_projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_proj_status`
--
ALTER TABLE `cau_proj_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_recipients_ch`
--
ALTER TABLE `cau_recipients_ch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_videogal_info`
--
ALTER TABLE `cau_videogal_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cau_videogal_jo`
--
ALTER TABLE `cau_videogal_jo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videogal_info` (`videogal_info`);

--
-- Indexes for table `cau_videos`
--
ALTER TABLE `cau_videos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_communities`
--
ALTER TABLE `char_communities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_comm_emp_jo`
--
ALTER TABLE `char_comm_emp_jo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `community` (`community`),
  ADD KEY `employee` (`employee`);

--
-- Indexes for table `char_comm_leaders`
--
ALTER TABLE `char_comm_leaders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leader` (`leader`);

--
-- Indexes for table `char_departments`
--
ALTER TABLE `char_departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_dept_emp_jo`
--
ALTER TABLE `char_dept_emp_jo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department` (`department`),
  ADD KEY `employee` (`employee`);

--
-- Indexes for table `char_dept_managers`
--
ALTER TABLE `char_dept_managers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manager` (`manager`);

--
-- Indexes for table `char_employees`
--
ALTER TABLE `char_employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_employee_job_jo`
--
ALTER TABLE `char_employee_job_jo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee` (`employee`),
  ADD KEY `department` (`department`),
  ADD KEY `community` (`community`),
  ADD KEY `title` (`title`);

--
-- Indexes for table `char_job_titles`
--
ALTER TABLE `char_job_titles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_members`
--
ALTER TABLE `char_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `membership` (`membership`);

--
-- Indexes for table `char_membership_types`
--
ALTER TABLE `char_membership_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts_groups`
--
ALTER TABLE `contacts_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contactus`
--
ALTER TABLE `contactus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lang_language_info`
--
ALTER TABLE `lang_language_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lang_site_langusges`
--
ALTER TABLE `lang_site_langusges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang` (`lang`);

--
-- Indexes for table `ltm_translations`
--
ALTER TABLE `ltm_translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maps`
--
ALTER TABLE `maps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `related_topics`
--
ALTER TABLE `related_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topic_categories`
--
ALTER TABLE `topic_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topic_fields`
--
ALTER TABLE `topic_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`ig`);

--
-- Indexes for table `users_log`
--
ALTER TABLE `users_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_group` (`user_group`);

--
-- Indexes for table `webmails`
--
ALTER TABLE `webmails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webmails_files`
--
ALTER TABLE `webmails_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webmails_groups`
--
ALTER TABLE `webmails_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webmaster_banners`
--
ALTER TABLE `webmaster_banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webmaster_sections`
--
ALTER TABLE `webmaster_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webmaster_section_fields`
--
ALTER TABLE `webmaster_section_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `webmaster_settings`
--
ALTER TABLE `webmaster_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `analytics_pages`
--
ALTER TABLE `analytics_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1089;

--
-- AUTO_INCREMENT for table `analytics_visitors`
--
ALTER TABLE `analytics_visitors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=644;

--
-- AUTO_INCREMENT for table `attach_files`
--
ALTER TABLE `attach_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `cau_classes`
--
ALTER TABLE `cau_classes`
  MODIFY `class_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cau_projects`
--
ALTER TABLE `cau_projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contacts_groups`
--
ALTER TABLE `contacts_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contactus`
--
ALTER TABLE `contactus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=217;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ltm_translations`
--
ALTER TABLE `ltm_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1235;

--
-- AUTO_INCREMENT for table `maps`
--
ALTER TABLE `maps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=311;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `related_topics`
--
ALTER TABLE `related_topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT for table `topic_categories`
--
ALTER TABLE `topic_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `topic_fields`
--
ALTER TABLE `topic_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `webmails`
--
ALTER TABLE `webmails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `webmails_files`
--
ALTER TABLE `webmails_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `webmails_groups`
--
ALTER TABLE `webmails_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `webmaster_banners`
--
ALTER TABLE `webmaster_banners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `webmaster_sections`
--
ALTER TABLE `webmaster_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `webmaster_section_fields`
--
ALTER TABLE `webmaster_section_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `webmaster_settings`
--
ALTER TABLE `webmaster_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cau_classtree`
--
ALTER TABLE `cau_classtree`
  ADD CONSTRAINT `cau_classtree_ibfk_1` FOREIGN KEY (`node_id`) REFERENCES `cau_classes` (`class_id`),
  ADD CONSTRAINT `cau_classtree_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `cau_classes` (`class_id`),
  ADD CONSTRAINT `cau_classtree_ibfk_3` FOREIGN KEY (`grand_id`) REFERENCES `cau_classes` (`class_id`);

--
-- Constraints for table `cau_country_states`
--
ALTER TABLE `cau_country_states`
  ADD CONSTRAINT `cau_country_states_ibfk_1` FOREIGN KEY (`country`) REFERENCES `cau_countries` (`id`);

--
-- Constraints for table `cau_photogal_jo`
--
ALTER TABLE `cau_photogal_jo`
  ADD CONSTRAINT `cau_photogal_jo_ibfk_1` FOREIGN KEY (`photogal_info`) REFERENCES `cau_photogal_info` (`id`);

--
-- Constraints for table `cau_videogal_jo`
--
ALTER TABLE `cau_videogal_jo`
  ADD CONSTRAINT `cau_videogal_jo_ibfk_1` FOREIGN KEY (`videogal_info`) REFERENCES `cau_videogal_info` (`id`);

--
-- Constraints for table `char_comm_emp_jo`
--
ALTER TABLE `char_comm_emp_jo`
  ADD CONSTRAINT `char_comm_emp_jo_ibfk_1` FOREIGN KEY (`community`) REFERENCES `char_communities` (`id`),
  ADD CONSTRAINT `char_comm_emp_jo_ibfk_2` FOREIGN KEY (`employee`) REFERENCES `char_employees` (`id`);

--
-- Constraints for table `char_comm_leaders`
--
ALTER TABLE `char_comm_leaders`
  ADD CONSTRAINT `char_comm_leaders_ibfk_1` FOREIGN KEY (`leader`) REFERENCES `char_employees` (`id`);

--
-- Constraints for table `char_dept_emp_jo`
--
ALTER TABLE `char_dept_emp_jo`
  ADD CONSTRAINT `char_dept_emp_jo_ibfk_1` FOREIGN KEY (`department`) REFERENCES `char_departments` (`id`),
  ADD CONSTRAINT `char_dept_emp_jo_ibfk_2` FOREIGN KEY (`employee`) REFERENCES `char_employees` (`id`);

--
-- Constraints for table `char_dept_managers`
--
ALTER TABLE `char_dept_managers`
  ADD CONSTRAINT `char_dept_managers_ibfk_1` FOREIGN KEY (`manager`) REFERENCES `char_employees` (`id`);

--
-- Constraints for table `char_employee_job_jo`
--
ALTER TABLE `char_employee_job_jo`
  ADD CONSTRAINT `char_employee_job_jo_ibfk_1` FOREIGN KEY (`employee`) REFERENCES `char_employees` (`id`),
  ADD CONSTRAINT `char_employee_job_jo_ibfk_2` FOREIGN KEY (`department`) REFERENCES `char_departments` (`id`),
  ADD CONSTRAINT `char_employee_job_jo_ibfk_3` FOREIGN KEY (`community`) REFERENCES `char_communities` (`id`),
  ADD CONSTRAINT `char_employee_job_jo_ibfk_4` FOREIGN KEY (`title`) REFERENCES `char_job_titles` (`id`);

--
-- Constraints for table `char_members`
--
ALTER TABLE `char_members`
  ADD CONSTRAINT `char_members_ibfk_1` FOREIGN KEY (`membership`) REFERENCES `char_membership_types` (`id`);

--
-- Constraints for table `lang_site_langusges`
--
ALTER TABLE `lang_site_langusges`
  ADD CONSTRAINT `lang_site_langusges_ibfk_1` FOREIGN KEY (`lang`) REFERENCES `lang_language_info` (`id`);

--
-- Constraints for table `users_log`
--
ALTER TABLE `users_log`
  ADD CONSTRAINT `users_log_ibfk_1` FOREIGN KEY (`user_group`) REFERENCES `users_groups` (`ig`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
