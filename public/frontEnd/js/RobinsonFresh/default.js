// Namespace Object
var NERD = NERD || {};

// Pass reference to jQuery and Namespace
(function ($, APP) {

    // DOM Ready Function
    $(function () {
        APP.PrintAndEmailLink.init();

        $('.region-selector > a').click(function () {
            if ($('.language-dropdown').is(':visible')) {
                $('.language-dropdown').hide();
            } else {
                $('.language-dropdown').show();
            }
        });


    });

    /* ---------------------------------------------------------------------
     PrintAndEmailLink
     Author: David Ly

     Sets up click handler for print and email links
     ------------------------------------------------------------------------ */
    APP.PrintAndEmailLink = {
        init: function () {
            $('.js-print-page').click(function () {
                window.print();
            });
            $('.js-email-page').click(function () {
                var href = $(this).attr('href');
                href = href.replace('{pageTitle}', encodeURIComponent(document.title));
                $(this).attr('href', href);
            });
        }
    };


}(jQuery, NERD));