/* ---------------------------------------------------------------------
Global JavaScript

Target Browsers: All
Authors: Jon Winton
------------------------------------------------------------------------ */

// Namespace Object
var NERD = NERD || {};

// Pass reference to jQuery and Namespace
(function($, APP) {

    // DOM Ready Function
    $(function() {
        APP.HasJS.init();
        APP.AutoReplace.init();
        APP.MainSlider.init();
        APP.ProduceSlider.init();
        APP.DropMenu.init();
    });

    /* ---------------------------------------------------------------------
     HasJS
     Author: Boilerplate

     Replaces "no-js" class with "js" on the html element if JavaScript
     is present. This allows you to style both the JavaScript enhanced
     and non JavaScript experiences.
     ------------------------------------------------------------------------ */
    APP.HasJS = {
        init: function() {
            $('html').removeClass('no-js').addClass('js');
        }
    };

    /* ---------------------------------------------------------------------
     AutoReplace
     Author: Boilerplate

     Mimics HTML5 placeholder behavior in browsers that do not support it.

     Additionally, adds and removes 'placeholder-text' class, used as a styling
     hook for when placeholder text is visible or not visible

     Additionally, sets the field value to the empty string upon form submission
     if the current value is the default text
     ------------------------------------------------------------------------ */
    APP.AutoReplace = {
        $fields: undefined,
        init: function() {
            // Only run the script if 'placeholder' is not natively supported
            if ('placeholder' in document.createElement('input')) {
                return;
            }

            this.$fields = $('[placeholder]');
            this.bind();
        },
        bind: function() {
            this.$fields.each(
                function() {
                    var $this = $(this);
                    var defaultText = $this.attr('placeholder');

                    if ($this.val() === '' || $this.val() === defaultText) {
                        $this.addClass('placeholder-text').val(defaultText);
                    }

                    $this.off('.autoreplace');

                    $this.on(
                        'focus.autoreplace',
                        function() {
                            if ($this.val() === defaultText) {
                                $this.val('').removeClass('placeholder-text');
                            }
                        }
                    );

                    $this.on(
                        'blur.autoreplace',
                        function() {
                            if ($this.val() === '' || $this.val() === defaultText) {
                                $this.val(defaultText).addClass('placeholder-text');
                            }
                        }
                    );

                    $this.parents('form').off('submit.autoreplace').on(
                        'submit.autoreplace',
                        function() {
                            if ($this.val() == defaultText) {
                                $this.val('');
                            }
                        }
                    );
                }
            );
        }
    };

    /* ---------------------------------------------------------------------
     MainSlider
     Author: Jon Winton

     Initiates the slider on the homepage underneath the header

     Additionally generates markup necessary for navigation of slider.
     ------------------------------------------------------------------------ */
    APP.MainSlider = {
        init: function() {
            var $carousel = $('.mainSlider'),
                $pagination = $('.mainSlider-pagination'),
                $slides = $('.mainSlider-slide');

            if ($carousel.length > 0 && $slides.length > 1) {
            // Carousel initialization
            $($carousel)
                .on('jcarousel:create jcarousel:reload', function () {
                    var element = $(this),
                        width = element.innerWidth();

                        // This shows 1 item at a time.
                        // Divide `width` to the number of items you want to display,
                        // eg. `width = width / 3` to display 3 items at a time.
                        element.jcarousel('items').css('width', width + 'px');
                })
                .jcarousel({
                    wrap: 'circular'
                })
                .jcarouselAutoscroll({
                    interval: 30000,
                    scroll: '+=1'
                })
                .hover(function () {
                    $(this).jcarouselAutoscroll('stop');
                }, function () {
                    $(this).jcarouselAutoscroll('start');
                });
            // Pagination initialization
            $($pagination)
                .on('jcarouselpagination:active', 'a', function () {
                    $(this).addClass('active');
                })
                .on('jcarouselpagination:inactive', 'a', function () {
                    $(this).removeClass('active');
                })
                .on('click', function (e) {
                    e.preventDefault();
                })
                .jcarouselPagination({
                    'item': function (page, carouselItems) {
                        return '<a class="mainSlider-nav" href="#' + page + '"></a>';
                    }
                });
           }
        else if($carousel.length > 0)
        {
            var width = $carousel.innerWidth();
            $slides.css('width', width + 'px');
        }
        }           

    };

    /* ---------------------------------------------------------------------
     ProduceSlider
     Author: Jon Winton

     Initiates the slider on the homepage underneath the header

     Additionally generates markup necessary for navigation of slider.
     ------------------------------------------------------------------------ */
    APP.ProduceSlider = {
        init: function() {
            var $carousel = $('.productSlider'),
                $prevArrow = '<a href="#" class="productSlider-prev"></a>',
                $nextArrow = '<a href="#" class="productSlider-next"></a>';

            // Get the inner width of the slider
            function innerWidth($elem){
               var $innerWidth = $elem.innerWidth();
               return $innerWidth;
            };
            // Assign value to target option to determine
            // how many items the slides move forward
            function targetNumber (){
                var width = innerWidth($carousel),
                    targetNumber = null;

                if (width > 1100) {
                    targetNumber = 6;
                } else if (width > 999) {
                    targetNumber = 5;
                } else if (width > 899) {
                    targetNumber = 4;
                } else if (width > 479) {
                    targetNumber = 3;
                } else if (width > 399) {
                    targetNumber = 2;
                } else if (width > 320) {
                    targetNumber = 1;
                }
                return targetNumber;
            };

            // Write the controls to the DOM
            $('.productSlider-controls').html($prevArrow + $nextArrow);


            // Carousel initialization
            $($carousel)
                .on('jcarousel:create jcarousel:reload', function() {
                    var element = $(this),
                        width = innerWidth(element);

                    if (width > 1100) {
                        width = width / 6;
                    } else if (width > 999) {
                        width = width / 5;
                    } else if (width > 899) {
                        width = width / 4;
                    } else if (width > 479) {
                        width = width / 3;
                    } else if (width > 399) {
                        width = width / 2;
                    } else if (width > 320) {
                        width = width / 1;
                    }
                    element.jcarousel('items').css('width', width + 'px');
                })
                .jcarousel({
                    wrap: 'circular'
                });


            // Prev control initialization
            $('.productSlider-prev')
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .jcarouselControl({
                    // Options go here
                    target: '-=' + targetNumber()
                });

            // Next control initialization
            $('.productSlider-next')
                .on('jcarouselcontrol:active', function() {
                    $(this).removeClass('inactive');
                })
                .on('jcarouselcontrol:inactive', function() {
                    $(this).addClass('inactive');
                })
                .jcarouselControl({
                    // Options go here
                    target: '+=' + targetNumber()
                });

            $(window).on('resize', function(){

                $('.productSlider-next').jcarouselControl('reload', {
                    target: '+=' + targetNumber()
                });

                $('.productSlider-prev').jcarouselControl('reload', {
                    target: '-=' + targetNumber()
                });
            });
        }
    };

    /* ---------------------------------------------------------------------
     DropMenu
     Author: Jon Winton

     Mobile and tablet dropdown menu
     ------------------------------------------------------------------------ */
    APP.DropMenu = {
        init: function() {
            var menuBtn = $('.menu-icon');

            $(menuBtn).on('click', this.toggleMenu);
            $(window).on('resize', this.winWidth);
        },
        toggleMenu: function() {
            var menu = $('.mainMenu');
            $(menu).slideToggle(150);
            return false;
        },
        winWidth: function() {
            var winWidth = $(window).width(),
                menu = $('.mainMenu');


            if(winWidth >= 960 && menu.css('display') == 'none' ) {
                menu.css('display', 'block');
            } else if (winWidth <= 959 && menu.css('display') == 'block') {
                menu.css('display', 'none');
            }
        }

    };

}(jQuery, NERD));